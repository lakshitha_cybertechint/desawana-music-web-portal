/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : desawana

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-07-26 22:45:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activations
-- ----------------------------
DROP TABLE IF EXISTS `activations`;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of activations
-- ----------------------------
INSERT INTO `activations` VALUES ('1', '1', 'RCsr7Qt1bBMqFwlW4bBVvtu02UBBQ0hr', '1', '2015-07-12 00:09:31', '2015-07-12 00:09:31', '2015-07-12 00:09:31');
INSERT INTO `activations` VALUES ('5', '7', 'KB71pV1DYudR9TzPPQnGxRXQBoo7jV0E', '1', '2016-01-07 11:35:57', '2016-01-07 11:35:57', '2016-01-07 11:35:57');
INSERT INTO `activations` VALUES ('8', '10', 'Bs5wlBYLO997Z8lcLpO19pj40fx2qVuV', '1', '2016-01-08 12:20:38', '2016-01-08 12:20:38', '2016-01-08 12:20:38');
INSERT INTO `activations` VALUES ('9', '11', '0dsYg0Ft0IZFKMP7qIs1nbpK0f8gif6y', '1', '2016-01-08 12:48:38', '2016-01-08 12:48:38', '2016-01-08 12:48:38');
INSERT INTO `activations` VALUES ('10', '12', 'yfD7K4mMa3eend4vvBBPNVbyFeX3UGZF', '1', '2016-01-08 12:57:38', '2016-01-08 12:57:38', '2016-01-08 12:57:38');
INSERT INTO `activations` VALUES ('14', '9', 'i7J1jS1CbeCsMb40JdWwQLSFfoOWdngc', '1', '2017-01-24 01:05:39', '2017-01-24 01:05:39', '2017-01-24 01:05:39');
INSERT INTO `activations` VALUES ('15', '10', '8eiPuz7KrCZXYjiMEY1wmkLuPbmMrSwI', '1', '2017-12-28 08:10:24', '2017-12-28 08:10:24', '2017-12-28 08:10:24');
INSERT INTO `activations` VALUES ('16', '11', 'htzbXZnJuH098fhWA5e8V5QLberPA1IA', '1', '2018-01-08 08:18:27', '2018-01-08 08:18:27', '2018-01-08 08:18:27');
INSERT INTO `activations` VALUES ('18', '12', 'PnJ03QoBnJMegHXmGCXh6bcoXArycqr5', '1', '2018-01-30 10:15:58', '2018-01-30 10:15:58', '2018-01-30 10:15:58');
INSERT INTO `activations` VALUES ('20', '15', 'ivsgJeobsGYHI1oHtQo5oTzkBOb6A9WL', '1', '2018-02-01 22:44:24', '2018-02-01 22:44:24', '2018-02-01 22:44:24');
INSERT INTO `activations` VALUES ('21', '17', 'jDkUMCYv0VpNfdrlsxKEFa2wCI78R7DP', '1', '2018-02-01 22:45:53', '2018-02-01 22:45:53', '2018-02-01 22:45:53');
INSERT INTO `activations` VALUES ('22', '18', 'WCbfDN6mTvtTfu2DHELZfkgDgd7B6phA', '1', '2018-02-01 22:58:20', '2018-02-01 22:58:20', '2018-02-01 22:58:20');
INSERT INTO `activations` VALUES ('23', '19', '70ytUN9RjCsVU0qnwno2RseFZRulBXZk', '1', '2018-02-08 10:09:36', '2018-02-08 10:09:36', '2018-02-08 10:09:36');
INSERT INTO `activations` VALUES ('24', '11', 'OTRmEEVvix8rTHBvWtEhFhcyp1DYba6G', '1', '2018-04-04 08:19:47', '2018-04-04 08:19:46', '2018-04-04 08:19:47');

-- ----------------------------
-- Table structure for advertisements
-- ----------------------------
DROP TABLE IF EXISTS `advertisements`;
CREATE TABLE `advertisements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position_id` int(11) NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_at` date DEFAULT NULL,
  `expire_at` date DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of advertisements
-- ----------------------------

-- ----------------------------
-- Table structure for advertisement_locations
-- ----------------------------
DROP TABLE IF EXISTS `advertisement_locations`;
CREATE TABLE `advertisement_locations` (
  `id` int(10) unsigned NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad_count` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `google_ad_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of advertisement_locations
-- ----------------------------
INSERT INTO `advertisement_locations` VALUES ('1', 'Top image (Audios, Videos, Lyrics, Artists)', '928px', '70px', '2', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('2', 'Side panel(Audio,Video,Lyric)', '300px', '90px', '3', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('3', 'Pagination Top (Audios, Videos, Lyrics, Artists)', '728px', '90px', '1', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('4', 'Pagination Bottom(Audios, Videos, Lyrics, Artists)', '728px', '90px', '1', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('5', 'Side Panel (Home, Audios, Audio, Videos, Video, Lyrics, Lyric)', '300px', '250px', '3', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('6', 'Related Content Top (Audio, Video, Lyric)', '728px', '90px', '1', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('7', 'Related Content Middle (Audio, Video, Lyric)', '728px', '90px', '1', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('8', 'Related Content Bottom (Audio, Video, Lyric)', '728px', '90px', '1', '1', '0', '2018-07-15 23:31:58', '0000-00-00 00:00:00');
INSERT INTO `advertisement_locations` VALUES ('9', 'Home Floating Ad', '728px', '90px', '1', '1', '0', '2018-07-15 23:31:58', '2018-07-18 16:28:34');

-- ----------------------------
-- Table structure for artists
-- ----------------------------
DROP TABLE IF EXISTS `artists`;
CREATE TABLE `artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `fb` text COLLATE utf8_unicode_ci,
  `twitter` text COLLATE utf8_unicode_ci,
  `utube` text COLLATE utf8_unicode_ci,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of artists
-- ----------------------------
INSERT INTO `artists` VALUES ('1', 'Ashan Fernando', 'Ashan Fernando is a Sri Lankan music composer & singer. Ashan Fernando is a kind of an artist that is truly believes in youth social sense. And as a result he provides very sensational music to the social that will melt your heart.  ', 'https://www.facebook.com/ashanf123/', 'https://twitter.com/AshanFernandoo', 'https://www.youtube.com/channel/UCXYVkl6OYjWm0uJCyIacNJQ', 'core/storage/uploads/images/artists/artists-20180719115749.png', '9', '2018-05-12 08:11:54', '2018-07-20 00:27:50', 'Ashan Fernando is a Sri Lankan music composer & singer. Ashan Fernando is a kind of an artist that is truly believes in youth social sense. And as a result he provides very sensational music to the social that will melt your heart.  ', 'oba miriguwak kiya song download, Ashan Fernando song mp3 download, me nihanda bhawaye dj, nolabena senehe ashan fernando, me nihanda bhawaye lyrics, ashan fernando song list, ashan fernando ahas maliga, ahas maliga mp3, Ashan Fernando, Ashan Fernando songs, Ashan Fernando songs mp3, Ashan Fernando songs video, Ashan Fernando live show songs, Ashan Fernando song download, Ashan Fernando song chords, Ashan Fernando song lyrics, Ashan Fernando song Live');
INSERT INTO `artists` VALUES ('2', 'Dilan Gamage', 'Dilan Gamage is a trending lyricist who is gathering more and more visionary, sensational facts and ideas into a one place to produce a kind of a song that will be a sense to human.', 'https://www.facebook.com/gamagedylan', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511194707.png', '9', '2018-05-12 08:17:08', '2018-07-06 13:19:31', 'Dilan Gamage is a trending lyricist who is gathering more and more visionary, sensational facts and ideas into a one place to produce a kind of a song that will be a sense to human.', 'dilan gamage, dilan gamage songs, dilan gamage contact number, new, sinhala, man pathanawa, dilan gamage lyrics, dilan gamage facebook, dilan gamage new song mp3, dilan gamage new songs, dilan gamage songs, dilan gama, sinhala songs, new sinhala songs, romantic songs, new songs, latest sinhala songa, gamage, top music video in sri lanka, new hit, ashan fernando new songs, song videos, videos, me nihada bawaye, thamath tharahin neda oya, saththai oya, sri lanka music, love song, ');
INSERT INTO `artists` VALUES ('3', 'Damith Asanka', 'Damith Asanka is a highly recommended singer by youth fans and also a trending artist in nowadays. His songs can bring tears to your eyes. ', 'https://www.facebook.com/DamithAsankaOfficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203226.png', '9', '2018-05-12 09:02:26', '2018-07-06 13:29:29', 'Damith Asanka is a highly recommended singer by youth fans and also a trending artist in nowadays. His songs can bring tears to your eyes. ', 'damith asanka, damith, damith asanka new songs, sinhala, damith asanka live show, damith asanka songs, damith asanka all songs, damith asanka album, damith asanka old songs, damith asanka best songs collection, best of damith, srilankan, damith asanka songs lyrics, damith asanka songs non stop, damith asanka songs mp3, mp3 free download, damith asanka mp3, me anantha raathriye, damith asanka video song, damith asanka live, pamawela, kachayak idiriye, me ananth raathriye video, ');
INSERT INTO `artists` VALUES ('4', 'Dilki Uresha', 'Dilki Uresha is a female singer that will be a kind of an female voice to the music social. ', 'https://www.facebook.com/Dilki-Uresha-204234943711424/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203246.png', '9', '2018-05-12 09:02:46', '2018-07-06 13:44:21', 'Dilki Uresha is a female singer that will be a kind of an female voice to the music social. ', '');
INSERT INTO `artists` VALUES ('5', 'Dimanka Wellalage', 'Dimanka Wellalage is a well experienced singer, lyricist and a song producer who has made his own empire of music in the field. And also he is a very popular singer in sri lankan live music events. ', 'https://www.facebook.com/dimankaonline/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCIq8JDQZDa2ZjAGIu4LafxA', 'core/storage/uploads/images/artists/artists-20180511203307.png', '9', '2018-05-12 09:03:07', '2018-07-06 14:00:40', 'Dimanka Wellalage is a well experienced singer, lyricist and a song producer who has made his own empire of music in the field. And also he is a very popular singer in sri lankan live music events. ', 'dimanka wellalage, sajith v chathuranga, dimanka, radeesh vandabona, dimanka wellalage songs, music video, dimanka wellalage songs youtube, dimanka wellalage songs lyrics, dimanka wellalage songs chords, dimanka wellalage songs ananmanan.lk, dimanka wellalage video songs, dimanka wellalage new video songs, dimanka wellalage dj songs, dimanka wellalage new video 2017, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show,');
INSERT INTO `artists` VALUES ('6', 'Jude Rogans', 'Jude Rogans is a well popular and panoplied singer for live singing and his singing can touch your heart by a word. ', 'https://www.facebook.com/%E0%B6%A2%E0%B7%96%E0%B6%A9%E0%B7%8A-%E0%B6%BB%E0%B7%9C%E0%B6%9C%E0%B6%B1%E0%B7%8A%E0%B7%83%E0%B7%8A-1948139895404796/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203338.png', '9', '2018-05-12 09:03:38', '2018-07-06 14:15:25', 'Jude Rogans is a well popular and panoplied singer for live singing and his singing can touch your heart by a word. ', 'jude rogans, sinhala, music, jude, jude rogans new song, live, horen bala, srilanka music, sri lanka, jude rogans new songs, jude rogans amma, jude rogans new song 2018 mp3, jude rogans new song mp3 download, jude rogans new song video, jude rogans nonstop, jude rogans, jude rogans mp3, jude rogans music, jude rogans all songs, jude rogans live, jude rogans live show, ජූඩ් රොගන්ස්, athmedi, new hit music videos, sammatheta pitu pe music video, sammatheta pitu pa adare, ');
INSERT INTO `artists` VALUES ('7', 'Milinda Sandaruwan', 'Milinda Sandaruwan is a opposition singer who has a very romantic and sensible voice. he is a rising star in the youth social nowadays.  ', 'https://www.facebook.com/milindasofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203356.png', '9', '2018-05-12 09:03:57', '2018-07-06 14:22:59', 'Milinda Sandaruwan is a opposition singer who has a very romantic and sensible voice. he is a rising star in the youth social nowadays.  ', 'milinda sandaruwan, new music video, sinhala music video, ashirwadaya, milinda, milinda sandaruwan new song, milinda sandaruwan sudu, mathu dineka, sinhala mp3, sinhala songs, sudu milinda sandaruwan, new sinhala songs, milinda sandaruwan live, milinda sandaruwan new songs, milinda sandaruwan new song 2018, milinda sandaruwan songs, milinda new video, sithin lanwela milinda, iraj, milinda new song bambarindu, rawatuwe obamane milinda sandaruwan, ashirwadaya dennam mp3, ');
INSERT INTO `artists` VALUES ('8', 'Nadeera Nonis', 'Nadeera Nonis is a musician in live music industry and also a part of a live streaming band. Nadeera Nonis is a well experienced singer and his singing techniques are very powerful to entertain your mind in a sensible manner.', 'https://www.facebook.com/pg/Nadeera-Nonis-611056125581229/about/?ref=page_internal', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203417.png', '9', '2018-05-12 09:04:17', '2018-07-06 14:31:22', 'Nadeera Nonis is a musician in live music industry and also a part of a live streaming band. Nadeera Nonis is a well experienced singer and his singing techniques are very powerful to entertain your mind in a sensible manner.', 'nadeera nonis, nadeera, nonis, oba noena karane, dawena duka, hithata hitha, nadeera nonis new song 2017, sinhala songs, nadeera nonis peradaka, nadeera nonis mp3, nadeera nonis new song download, nadeera nonis new song mp3 download, nadeera nonis band, nadeera nonis mp3 song, nadeera nonis new song, nadeera nonis new song mp3, nadeera nonis songs lyrics, nadeera nonis video songs, nadeera nonis new songs video, nadeera nonis latest songs, sinhala, mp3 free download, new song, ');
INSERT INTO `artists` VALUES ('9', 'Rahal Alwis', 'Rahal Alwis is a well known sri lankan singer and also a musician, producer and a lyricist in a top level sensible music. \r\nhis voice is a one of a kind and you all will love it. ', 'https://www.facebook.com/RahalAlwisOfficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203435.png', '9', '2018-05-12 09:04:35', '2018-07-06 14:41:27', 'Rahal Alwis is a well known sri lankan singer and also a musician, producer and a lyricist in a top level sensible music.  his voice is a one of a kind and you all will love it. ', 'rahal alwis, sinhala, video, rahal, alwis, sinhala mp3, rahal alwis new song 2018, rahal alwis new song mp3 download, rahal alwis songs, rahal alwis, new song, sithata hora man, rahal alwis mage so susum, rahal alwis shungari, rahal alwis sithata hora, rahal alwis songs, rahal alwis new song, rahal alwis song, rahal alwis new song 2018, rahal alwis new mp3, shrungari - rahal alwis official music video, hitha riduna hamadama, new sinhala songs, heenayak premayak official music video, ');
INSERT INTO `artists` VALUES ('10', 'Ruwan Hettiarachchi', 'Ruwan Hettiarachchi is an award winning singing artist who has huge collection of songs sung by him. There is a big gathering of fans for him.', 'https://www.facebook.com/Official.Ruwan.Hettiarachchi/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203454.png', '9', '2018-05-12 09:04:54', '2018-07-12 22:43:23', 'Ruwan Hettiarachchi is an award winning singing artist who has huge collection of songs sung by him. There is a big gathering of fans for him.', 'ruwan hettiarachchi, ruwan, sinhala, hettiarachchi, sinhala songs, music, new, nodaka, official, nodaka inna ba, alen weli, rawatuna tharam, ruwan hettiarachchi songs, ruwan hettiarachchi new songs, ruwan hettiarachchi new songs 2016, ruwan hettiarachchi song, ruwan hettiarachchi new song mp3, ruwan hettiarachchi and umariya new song, ruwan hettiarachchi amathumak, ruwan hettiarachchi wife, ruwan hettiarachchi new songs 2017, ruwan hettiarachchi live, ruwan hettiarachchi mp3, sheril dekker, ');
INSERT INTO `artists` VALUES ('11', 'Sadun Perera', 'Sandun perera is a top ranking singing artist who is also doing as a song lyricist. Nowadays sandun perera is a very popular singer amongst the youth.  ', 'https://www.facebook.com/sandunpereralive?lst=100005806563957%3A100002769006391%3A1531370660', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203517.png', '9', '2018-05-12 09:05:17', '2018-07-12 23:10:16', 'Sandun perera is a top ranking singing artist who is also doing as a song lyricist. Nowadays sandun perera is a very popular singer amongst the youth.  ', 'sandun perera, ayeth warak, pooja karannam, sinhala music, sinhala mp3, sinhala songs, sandun, warada piligannawa, sandun new song, sandun perera, sandun perera live show 2018, sandun perera new song lyrics, sandun perera video download, sandun perera new song, sandun perera songs, sandun perera live show, sandun perera mashup, sandun perera warada piligannawa, sandun perera with flashback, sandun perera new song, sandun perera mp3, sandun perera music, sandun perera, new sinhalasongs, ');
INSERT INTO `artists` VALUES ('12', 'Samith Sirimanna', 'samith sirimanna is an upcoming young singer who is willing to be a heart warming singer and of course he is very popular singer among the romantic music lovers.', 'https://www.facebook.com/officialsamith/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203534.png', '9', '2018-05-12 09:05:34', '2018-07-12 23:17:01', 'samith sirimanna is an upcoming young singer who is willing to be a heart warming singer and of course he is very popular singer among the romantic music lovers.', 'samith sirimanna, pin madida, new songs, rathnaththare pihitai, samith new songs, pin madida danne na, samith newsongs, samith sirimanna, samith sirimanna songs, samith sirimanna new song mp3, samith sirimanna new song 2017, samith sirimanna new video song download, samith sirimanna video, samith sirimanna new songs, penenathek mane, 2018 new songs, romantic, true story, new sinhala, sinhala videos, sinhalalanka, sandun, samith sirimanna saba adare live, new music video, rathnaththare, ');
INSERT INTO `artists` VALUES ('13', 'Shahil Himansa', 'Shahil Himansa is a Sri Lankan singer, musician and a music producer and During his musical career, Shahil has released five Music Videos. And also he is a upcoming popular singer in nowadays.', 'https://www.facebook.com/shahilhimansaofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203554.png', '9', '2018-05-12 09:05:54', '2018-07-12 23:23:58', 'Shahil Himansa is a Sri Lankan singer, musician and a music producer and During his musical career, Shahil has released five Music Videos. he is upcoming popular singer in nowadays.', 'shahil himansa, shahil new song, therum giya, new, sinhala, shahil himansa new song 2018, shahil himansa new song download, shahil himansa mp3, shahil himansa video song download, shahil himansa me hitha thawamath, shahil himansa aye noena, sinhala songs, music, shahil himansa, shahil himansa songs, shahil himansa mp3 free download, shahil himansa mp3 song download, shahil himansa new song 2018, shahil himansa mashup, shahil himansa live show, shahil himansa, shahil himansa new song video, ');
INSERT INTO `artists` VALUES ('14', 'Thushara Joshap', 'Thushara Joshap is a well popular upcoming young singer who is also work as a live performer of sri lankan band named \'sahara flash\'. He has a kind of a voice and a new way of performing and because of that everyone is willing to hear from him.', 'https://www.facebook.com/thushasf/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203614.png', '9', '2018-05-12 09:06:14', '2018-07-12 23:59:50', 'Thushara Joshap is a well popular upcoming young singer who is also work as a live performer of sri lankan band named \'sahara flash\'. He has a kind of a voice and a new way of performing and because of that everyone is willing to hear from him.', 'thushara joshap, new, sinhala, rawatuna nowe, thushara, thushara joshap new song, sahara flash, lyrics, 2018, sinhala songs, thushara sandakelum, sinhala new songs 2018, new song thushara sandakelum, thushara josap new song, rawatuna nowe thushara, rawatuna nowe thushara joshap, thushara joshap new song rawatuna nowe, rawatuna nowe song mp3, thushara sandakelum new song, mp3 free download, trending sinhala songs, monawathma wena aye, thushara joshap new, thushara joshap mp3, ');
INSERT INTO `artists` VALUES ('15', 'Viraj Perera', 'Viraj perera is a kind of a singer in nowadays and evryone love his voice. He is a upcoming musician that everyone is willing to hear his voice.', 'https://www.facebook.com/official.VIRAJ.Perera/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203649.png', '9', '2018-05-12 09:06:49', '2018-07-13 00:08:10', 'Viraj perera is a kind of a singer in nowadays and evryone love his voice. He is a upcoming musician that everyone is willing to hear his voice.', 'viraj perera, viraj perera new song, viraj perera songs, sinhala, thahanam dan, sinhala songs, thahanam, viraj perera new song 2017, viraj perera new video song download, viraj perera new song video, viraj perera new song, viraj perera hemin sare, viraj perera saritha, viraj perera mp3 download, viraj perera saritha mp3 song, thahanam dam mata oyage rupe, thahanam viraj perera, viraj perera latest songs, best song of 2017, තහනම් දැන්, srilankan, official full hd video, ');
INSERT INTO `artists` VALUES ('16', 'Chamara Weerasinghe', 'Chamara Weerasinghe is a popular artist who is a romantic and a heart warming singer and also work as a performer in live music band events. In a way he is a new era of music among youth fans.', 'https://www.facebook.com/l.m.chamara.weerasinghe/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180512201320.png', '9', '2018-05-13 08:43:20', '2018-07-13 01:11:20', 'Chamara Weerasinghe is a popular artist who is a romantic and a heart warming singer and also work as a performer in live music band events. In a way he is a new era of music among youth fans.', 'chamara weerasinghe, chamara, best of chamara weerasinghe, chamara weerasingha, sinhala, chamara weerasinghe best songs collection - 01, chamara weerasinghe songs, mage jeewithe pura, ma nowana mama, chamara weerasinghe best songs collection, songs, sinhala songs, weerasinghe, chamara weerasinghe thaththa, chamara weerasinghe wife, chamara weerasinghe live, chamara weerasinghe song, chamara weerasinghe 2018, chamara weerasinghe album free download, chamara weerasinghe live show 2018, ');
INSERT INTO `artists` VALUES ('17', 'Sanuka Wickramasinghe', 'Sanuka wickramasinghe is very popular sri lankan Singer and also works as a live performer, Lyricist, Producer. He is a expert in sound engineering too. Fans are going mad when he sings. And he has his own way performing and he is a bright star in the music field. ', 'https://www.facebook.com/sanukawick/', 'https://twitter.com/sanukawick', 'https://www.youtube.com/user/IamStriKING/', 'core/storage/uploads/images/artists/artists-20180513195806.png', '9', '2018-05-14 08:28:07', '2018-07-13 01:25:48', 'Sanuka wickramasinghe is very popular sri lankan Singer and also works as a live performer, Lyricist, Producer. He is a expert in sound engineering too. Fans are going mad when he sings. And he has his own way performing and he is a bright star in the music field. ', 'sanuka, sanuka wickramasinghe, saragaye, sansara sihine, sanuka wicky, mal wiyan, ciao malli, sanukawicky, sanuka wickramasinghe live, sanuka wickramasinghe, sanuka wickramasinghe youtube, sanuka wickramasinghe saragaye mp3, sanuka wickramasinghe, sanuka wickramasinghe new songs, sanuka wickramasinghe ma nowana mama, sanuka wickramasinghe songs, sanuka wickramasinghe saragaye, sanuka wickramasinghe sansara sihine, sanuka wickramasinghe songs download, sanuka wickramasinghe mp3, sanuka new song, ');
INSERT INTO `artists` VALUES ('18', 'Sydney Chandrasekara', 'Sudney Chandrasekara is a well known person as a experienced lyricist and also working as journalist, tele drama writer, director. ', 'https://www.facebook.com/iamsydneychandrasekara/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180513211135.png', '9', '2018-05-14 09:41:35', '2018-07-19 21:55:11', 'Sudney Chandrasekara is a well known person as a experienced lyricist and also working as journalist, tele drama writer, director. ', 'srilanka, reality, show, sydney chandrasekara, sydney, sydney chandrasekara, sydney chandrasekara songs, sydney chandrasekara new song, sydney chandrasekara lyrics, sydney chandrasekara teledrama, sydney chandrasekara songs, sundara birinda with sydney chandrasekara, ');
INSERT INTO `artists` VALUES ('19', 'Rachithaa Wakista', 'Rachithaa Wakista is a young female lyricist who blessed with the remarkable opportunity to make creations on behalf of Sri Lankan music industry. \r\n', 'https://www.facebook.com/Rachithaa-Wakista-1660354970960653/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514125219.png', '9', '2018-05-15 01:22:19', '2018-07-19 22:04:16', 'Rachithaa Wakista is a young female lyricist who blessed with the remarkable opportunity to make creations on behalf of Sri Lankan music industry. ', 'rachithaa wakista, new music videos, music video, bplus songs, bplus music video, sinhala music video, sinhala songs, new release, sinhala sindu, bplus music, rachithaa wakista, rachita wakista, new sinhala song, bplus video, rachithaa wakista jukebox, rachitha songs, sinhala top hits, jukebox songs, man numbe, ayemath adaren, sangeeth satharasinghe new song, sangeeth satharasinghe, ');
INSERT INTO `artists` VALUES ('20', 'Thilina Ruhunage', 'Thilina ruhunage is a upcoming young musician who is trying to build his own empire in in sri lankan music industry.   ', 'https://www.facebook.com/Thilina-Ruhunage-128818567265075/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514125534.png', '9', '2018-05-15 01:25:34', '2018-07-19 22:12:14', 'Thilina ruhunage is a upcoming young musician who is trying to build his empire in in sri lankan music industry.   ', 'thilina ruhunage, sinhala songs, hiru gossip, thilina, ruhunage, thilina r, hiru fm, es deka pura, sinhala new song, music, sinhala teledrama, sinhala music video, sinhala video songs, me hitha thaniyen, athma liyanage, thilina ruhunage, thilina ruhunage songs, thawa ridawanna, thilina ruhunage mp3 song, thilina ruhunage mp3, thilina ruhunage music, thilina ruhunage songs, thilina ruhunage new songs, thilina ruhunage es deka pura, thilina ruhunage es deka pura, love songs, music video, ');
INSERT INTO `artists` VALUES ('21', 'Lasitha Jayaneththi Arachchige', 'Lasitha Jayaneththi Arachchige is a well known upcoming lyricist who has a special ability to touch anyones heart by his lyrics. ', 'https://www.facebook.com/lasitha.jayaneththiarachchige?lst=100005806563957%3A100000871151867%3A1531973582', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514130614.png', '9', '2018-05-15 01:36:14', '2018-07-19 22:19:19', 'Lasitha Jayaneththi Arachchige is a well known upcoming lyricist who has a special ability to touch anyones heart by his lyrics. ', 'lasitha jayaneththi arachchige, viraj perera, udesh nilanga, sinhala, dilshan l silva, saaritha, youtube, jude rogans, new sinhala songs 2018, new sinhalasongs, new music videos 2018, new hit music videos, video mansala, lasitha jayaneththi arachchige, adare sanwedana, ashirwada soya, lasitha, new song, new, song, video, lyrics, official, mathudinaka, sudu, new hit music video, chamara weerasinghe new music video, best songs on 2017, yanada oya sajeewa erandith, sri lanka music video, ');
INSERT INTO `artists` VALUES ('22', 'DilShan L Silva', 'DilShan L Silva is a music composer, producer and a music engineer who is doing a great job on music industry to make an new era of sri lankan music field.  ', 'https://www.facebook.com/dilshan.lakshitha1?lst=100005806563957%3A100000454174581%3A1531974066', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514130947.png', '9', '2018-05-15 01:39:48', '2018-07-19 22:30:04', 'DilShan L Silva is a music composer, producer and a music engineer who is doing a great job on music industry to make an new era of sri lankan music field.  ', 'dilshan l silva, adare sanwedana, aware aware song, new song, pathanne na, viraj perera new song, udesh nilanga, new songs, sinhala, dilshan l silva songs, dilshan l silva, dilshan l silva new song, kaun tujhe, dilshan l silva new song, dilshan l silva, dilshan l silva facebook, bala innawane, adarei wasthu, hirumusic, jude rogans, coming soon, sameera, nodeka oya, 2018, srilankan, sinhala new songs 2018, music videos, video song, new sinhala song, music video, official music video, ');
INSERT INTO `artists` VALUES ('23', 'Sajith V Chathuranga', '', null, null, null, 'core/storage/uploads/images/artists/artists-20180514134231.png', '9', '2018-05-15 02:12:34', '2018-05-15 02:12:34', null, null);
INSERT INTO `artists` VALUES ('24', 'Prageeth Perera', 'music composer .. audio engineer', 'https://www.facebook.com/Prageeth-Perera-1822270981420746/', '', '', 'core/storage/uploads/images/artists/artists-20180521165251.png', '9', '2018-05-22 05:22:52', '2018-05-22 05:23:52', null, null);
INSERT INTO `artists` VALUES ('25', 'Uma Aseni', '', 'https://www.facebook.com/Uma-Aseni-Kavikari-902415469914173/', '', '', 'core/storage/uploads/images/artists/artists-20180521172620.png', '9', '2018-05-22 05:56:20', '2018-05-22 05:56:20', null, null);
INSERT INTO `artists` VALUES ('26', 'Sangeeth Wickramasinghe', '', 'https://www.facebook.com/sangeeth.wickramasinghe', '', '', 'core/storage/uploads/images/artists/artists-20180521174029.png', '9', '2018-05-22 06:10:29', '2018-05-22 06:10:29', null, null);
INSERT INTO `artists` VALUES ('27', 'Udari Amarathunga', '', 'https://www.facebook.com/Udaamarathunga', '', '', 'core/storage/uploads/images/artists/artists-20180521180357.png', '9', '2018-05-22 06:33:58', '2018-05-22 06:33:58', null, null);
INSERT INTO `artists` VALUES ('28', 'Manuranga Wijesekara', '', 'https://www.facebook.com/manuranga.wijesekara/', '', '', 'core/storage/uploads/images/artists/artists-20180521185815.png', '9', '2018-05-22 07:28:15', '2018-05-22 07:28:15', null, null);
INSERT INTO `artists` VALUES ('29', 'Radeesh Vandabona', '', 'https://www.facebook.com/RadeeshVandebona/', '', 'https://www.youtube.com/channel/UCJHzK5thXCTuL1JK-MBXzfA', 'core/storage/uploads/images/artists/artists-20180524125723.png', '9', '2018-05-25 01:27:25', '2018-05-25 01:27:25', '', 'ananthen piyaba awidin mp3 free download, radeesh vandebona photos, radeesh vandebona duka hithuna, radeesh vandebona mashup, Radeesh Vandabona, Radeesh Vandabona songs, Radeesh Vandabona songs mp3, Radeesh Vandabona songs video, Radeesh Vandabona live show songs, Radeesh Vandabona song download, Radeesh Vandabona song mp3 download, Radeesh Vandabona song chords, Radeesh Vandabona song lyrics, Radeesh Vandabona song Live, Radeesh Vandabona song guitar code, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show, album free download');
INSERT INTO `artists` VALUES ('30', 'Chandana Walpola', '', 'https://www.facebook.com/chandana.walpolaflashback', '', '', 'core/storage/uploads/images/artists/artists-20180526144011.png', '9', '2018-05-27 03:10:11', '2018-05-27 03:10:11', '', '');
INSERT INTO `artists` VALUES ('31', 'Shehan Galahitiyawa', '', 'https://www.facebook.com/shehan.galahitiyawa', '', '', 'core/storage/uploads/images/artists/artists-20180529114625.png', '9', '2018-05-30 00:16:26', '2018-05-30 00:16:26', '', '');
INSERT INTO `artists` VALUES ('32', 'Priyantha Nawalage', '', 'https://www.facebook.com/priyantha.nawalage', '', '', 'core/storage/uploads/images/artists/artists-20180529114930.png', '9', '2018-05-30 00:19:30', '2018-05-30 00:19:30', '', '');
INSERT INTO `artists` VALUES ('33', 'Sandaruwan Jayasinghe', '', 'https://www.facebook.com/sandaruwan.jayasinghe.35', '', '', 'core/storage/uploads/images/artists/artists-20180529130100.png', '9', '2018-05-30 01:31:01', '2018-05-30 01:31:01', '', '');
INSERT INTO `artists` VALUES ('34', 'Kelum Srimal', '', 'https://www.facebook.com/The1nonlyKelumSrimal/', '', '', 'core/storage/uploads/images/artists/artists-20180529130950.png', '9', '2018-05-30 01:39:50', '2018-05-30 01:39:50', '', '');
INSERT INTO `artists` VALUES ('35', 'Sadeeptha Gunawardana', '', 'https://www.facebook.com/sadeeptha.sadeeptha', '', '', 'core/storage/uploads/images/artists/artists-20180529131314.png', '9', '2018-05-30 01:43:15', '2018-05-30 01:43:15', '', '');
INSERT INTO `artists` VALUES ('36', 'Sujith Priyan', '', 'https://www.facebook.com/sujee.sujith', '', '', 'core/storage/uploads/images/artists/artists-20180529132912.png', '9', '2018-05-30 01:59:13', '2018-05-30 01:59:13', '', '');
INSERT INTO `artists` VALUES ('37', 'Nimesh Kulasinghe', '', 'https://www.facebook.com/nimeshKulasingheofficial/', '', '', 'core/storage/uploads/images/artists/artists-20180529134131.png', '9', '2018-05-30 02:11:31', '2018-05-30 02:11:31', '', '');
INSERT INTO `artists` VALUES ('38', 'Athula Jayasinghe', '', 'https://www.facebook.com/athujaya', '', '', 'core/storage/uploads/images/artists/artists-20180529134657.png', '9', '2018-05-30 02:16:57', '2018-05-30 02:16:57', '', '');
INSERT INTO `artists` VALUES ('40', 'Yasas Medagedara', '', 'https://www.facebook.com/yasasm', '', '', 'core/storage/uploads/images/artists/artists-20180529135556.png', '9', '2018-05-30 02:25:56', '2018-05-30 02:25:56', '', '');
INSERT INTO `artists` VALUES ('41', 'Udara Samaraweera', 'Udara Samaraweera can be introduced as a talented young music director who has contributed many of unique and predominant creations to the Music Industry.', 'https://www.facebook.com/CanoeEntertainment/', '', '', 'core/storage/uploads/images/artists/artists-20180529140247.png', '9', '2018-05-30 02:32:47', '2018-05-30 02:32:47', '', '');
INSERT INTO `artists` VALUES ('42', 'Prasanna Thakshila', '', 'https://www.facebook.com/prasannathakhila/', '', '', 'core/storage/uploads/images/artists/artists-20180601152312.png', '9', '2018-06-02 03:53:12', '2018-06-02 03:53:12', '', '');
INSERT INTO `artists` VALUES ('43', 'Rusiru Rupasinghe', '', 'https://www.facebook.com/rusirur321624', '', '', 'core/storage/uploads/images/artists/artists-20180601152623.png', '9', '2018-06-02 03:56:23', '2018-06-02 03:56:23', '', '');
INSERT INTO `artists` VALUES ('44', 'Yasith Dulshan', '', 'https://www.facebook.com/YasithDulRanasinghe/', '', '', 'core/storage/uploads/images/artists/artists-20180601152803.png', '9', '2018-06-02 03:58:03', '2018-06-02 03:58:03', '', '');
INSERT INTO `artists` VALUES ('45', 'Ishan Priyasanka', '', 'https://www.facebook.com/ishan.priyashanka.3', '', '', 'core/storage/uploads/images/artists/artists-20180602133733.png', '9', '2018-06-03 02:07:34', '2018-06-03 02:07:34', '', '');
INSERT INTO `artists` VALUES ('46', 'Tharindu Costa', '', 'https://www.facebook.com/tharindu.costha.1', '', '', 'core/storage/uploads/images/artists/artists-20180602173030.png', '9', '2018-06-03 06:00:30', '2018-06-03 06:00:30', '', '');
INSERT INTO `artists` VALUES ('47', 'Uvindu Dayaratne', '', 'https://www.facebook.com/uvindu.dayaratne', '', '', 'core/storage/uploads/images/artists/artists-20180602173059.png', '9', '2018-06-03 06:01:00', '2018-06-03 06:01:00', '', '');
INSERT INTO `artists` VALUES ('48', 'Harshana Prasad', '', 'https://www.facebook.com/Harshana-prasad-saharaflash-317634255331182/', '', '', 'core/storage/uploads/images/artists/artists-20180602174607.png', '9', '2018-06-03 06:16:08', '2018-06-03 06:16:08', '', '');
INSERT INTO `artists` VALUES ('49', 'Sameera Chathuranga', '', 'https://www.facebook.com/Sameera-chathuranga-165980247195563/', '', 'https://www.youtube.com/channel/UCgaczbpQl7-n7LIMbWlMbDA', 'core/storage/uploads/images/artists/artists-20180606100322.png', '9', '2018-06-06 22:33:23', '2018-06-06 22:33:23', '', '');
INSERT INTO `artists` VALUES ('50', 'Amila Muthugala', '', 'https://www.facebook.com/Amila-muthugala-studio-feel-364889150525359/', '', '', 'core/storage/uploads/images/artists/artists-20180606100857.png', '9', '2018-06-06 22:38:57', '2018-06-06 22:38:57', '', '');
INSERT INTO `artists` VALUES ('51', 'Shenu Kalpa', '', 'https://www.facebook.com/kalpa.danu', '', '', 'core/storage/uploads/images/artists/artists-20180606111252.png', '9', '2018-06-06 23:42:52', '2018-06-06 23:42:52', '', '');
INSERT INTO `artists` VALUES ('52', 'Hashan Thilina', '', 'https://www.facebook.com/hashanmusic', '', '', 'core/storage/uploads/images/artists/artists-20180606112834.png', '9', '2018-06-06 23:58:34', '2018-06-06 23:58:34', '', '');
INSERT INTO `artists` VALUES ('53', 'Prasanna Kumara Dammalage', '', 'https://www.facebook.com/prasanna.kumaradammalage', '', '', 'core/storage/uploads/images/artists/artists-20180606114420.png', '9', '2018-06-07 00:14:20', '2018-06-07 00:14:20', '', '');
INSERT INTO `artists` VALUES ('54', 'Udesh Nilanga', '', 'https://www.facebook.com/udesh.nilanga.7', '', '', 'core/storage/uploads/images/artists/artists-20180606121129.png', '9', '2018-06-07 00:41:29', '2018-06-07 00:41:29', '', '');
INSERT INTO `artists` VALUES ('55', 'Buddika Krishan', '', 'https://www.facebook.com/buddika.krishan', '', '', 'core/storage/uploads/images/artists/artists-20180607123437.png', '9', '2018-06-08 01:04:38', '2018-06-08 01:04:38', '', '');
INSERT INTO `artists` VALUES ('56', 'Nilan Hettiarachchi', '', 'https://www.facebook.com/NILANHETTIARACHCHIOFFICALFANPAGE/', '', '', 'core/storage/uploads/images/artists/artists-20180607131656.png', '9', '2018-06-08 01:46:57', '2018-06-08 01:46:57', '', 'Nilan Hettiarachchi songs, Nilan Hettiarachchi mp3 songs, Nilan Hettiarachchi video songs, Nilan Hettiarachchi video songs, Nilan Hettiarachchi song, Nilan Hettiarachchi songs download, Nilan Hettiarachchi mp3 songs download, ');
INSERT INTO `artists` VALUES ('57', 'Unknown Artist', '-Unknown artist default account', '', '', '', 'core/storage/uploads/images/artists/artists-20180612095806.png', '9', '2018-06-12 22:28:07', '2018-06-12 22:28:07', '', '');
INSERT INTO `artists` VALUES ('58', 'Shahani Perera', '', '', '', '', 'core/storage/uploads/images/artists/artists-20180614163007.png', '9', '2018-06-15 05:00:08', '2018-06-15 05:00:08', '', 'Shahani Perera, Shahani Perera Song, Shahani Perera new song, Shahani Perera Songs, Shahani Perera new Songs, Shahani Perera song download');
INSERT INTO `artists` VALUES ('59', 'Asanjaya Imashath', '', 'https://www.facebook.com/asanjayaimashath/', '', '', 'core/storage/uploads/images/artists/artists-20180614225602.png', '9', '2018-06-15 11:26:02', '2018-06-15 11:26:02', '', '');
INSERT INTO `artists` VALUES ('61', 'Nalin Wijayasinghe', '', 'https://www.facebook.com/napwijayasinghe', '', '', 'core/storage/uploads/images/artists/artists-20180614232044.png', '9', '2018-06-15 11:50:44', '2018-06-15 11:50:44', '', '');
INSERT INTO `artists` VALUES ('62', 'Sajeeka Jayasinghe', '', 'https://www.facebook.com/sajeekajayasinghe', '', '', 'core/storage/uploads/images/artists/artists-20180614232529.png', '9', '2018-06-15 11:55:29', '2018-06-15 11:55:29', '', '');
INSERT INTO `artists` VALUES ('63', 'Ajith Sanjeewa Perera', '', 'https://www.facebook.com/cdsstudio7', '', '', 'core/storage/uploads/images/artists/artists-20180622104432.png', '9', '2018-06-22 23:14:32', '2018-06-22 23:14:32', '', '');
INSERT INTO `artists` VALUES ('64', 'Roshan Perera', '', 'https://www.facebook.com/Roshan-Perera-780750018636211/', '', '', 'core/storage/uploads/images/artists/artists-20180622104907.png', '9', '2018-06-22 23:19:07', '2018-06-22 23:19:07', '', '');
INSERT INTO `artists` VALUES ('65', 'Indika Ruwan (Roony)', '', 'https://www.facebook.com/roo.roony', '', '', 'core/storage/uploads/images/artists/artists-20180629144632.png', '9', '2018-06-30 03:16:33', '2018-06-30 03:16:33', '', '');
INSERT INTO `artists` VALUES ('66', 'Roshan Samarawickrama', '', 'https://www.facebook.com/randika.roshan.5', '', '', 'core/storage/uploads/images/artists/artists-20180629145312.png', '9', '2018-06-30 03:23:12', '2018-06-30 03:23:12', '', '');
INSERT INTO `artists` VALUES ('67', 'Marlon Jerom Alwis', '', 'https://www.facebook.com/marly.brando.75', '', '', 'core/storage/uploads/images/artists/artists-20180629153615.png', '9', '2018-06-30 04:06:15', '2018-06-30 04:06:15', '', '');
INSERT INTO `artists` VALUES ('68', 'Hasitha Hemal', '', 'https://www.facebook.com/hasitha.hemal.731', '', '', 'core/storage/uploads/images/artists/artists-20180629153815.png', '9', '2018-06-30 04:08:15', '2018-06-30 04:08:15', '', '');
INSERT INTO `artists` VALUES ('69', 'Chathura Warnasekara', '', 'https://www.facebook.com/chathura.warnasekara', '', '', 'core/storage/uploads/images/artists/artists-20180629161034.png', '9', '2018-06-30 04:40:34', '2018-06-30 04:40:34', '', '');
INSERT INTO `artists` VALUES ('70', 'Ranga Indunil', '', 'https://www.facebook.com/profile.php?id=100008486551717', '', '', 'core/storage/uploads/images/artists/artists-20180629161722.png', '9', '2018-06-30 04:47:22', '2018-06-30 04:47:22', '', '');
INSERT INTO `artists` VALUES ('71', 'Supun Ravishan', '', 'https://www.facebook.com/supun.ravishan', '', '', 'core/storage/uploads/images/artists/artists-20180629172444.png', '9', '2018-06-30 05:54:45', '2018-06-30 05:54:45', '', '');
INSERT INTO `artists` VALUES ('72', 'Thilini Piyumali', '', 'https://www.facebook.com/thilini.piyumali.14', '', '', 'core/storage/uploads/images/artists/artists-20180629173716.png', '9', '2018-06-30 06:07:16', '2018-06-30 06:07:16', '', '');
INSERT INTO `artists` VALUES ('73', 'Reru', '', 'https://www.facebook.com/profile.php?id=100012034291171', '', '', 'core/storage/uploads/images/artists/artists-20180629210612.png', '9', '2018-06-30 09:36:12', '2018-06-30 09:36:12', '', '');
INSERT INTO `artists` VALUES ('74', 'Sajith Akmeemana', 'Singer , Lyrics Writer & Video Director\r\n\r\nI have done few Audio Tracks and a few Video and another one is yet to come.\r\nMy 1st song \"Tharu Kumari\" was created in 2007, and then after forming a band call \"SilentZ\" we produced another song named \"Adaraye...\"\r\nIn collaboration with Vishal Shenira we sang \"Adaraye Sihinayai\", which was produced by Yasas Medagedara & mixed by Nisal G @ Audio Lab Studios.\r\nI was a student of Avenue Of Audio (AOA) under Mr.Nisal Gangodage....', 'https://www.facebook.com/SajithDanushkaAkmeemana/', '', '', 'core/storage/uploads/images/artists/artists-20180629221502.png', '9', '2018-06-30 10:45:02', '2018-06-30 10:45:02', '', '');
INSERT INTO `artists` VALUES ('75', 'Nimash Fernando', '', 'https://www.facebook.com/nimashlk/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180630134827.png', '9', '2018-07-01 02:18:27', '2018-07-01 02:18:27', '', 'nimash fernando, nimash fernando song, nimash fernando songs, nimash fernando mp3, nimash fernando mp3 songs, nimash fernando mp3 songs download, nimash fernando video songs download, nimash fernando video songs, ');
INSERT INTO `artists` VALUES ('76', 'Nisal Fernando', '', 'https://www.facebook.com/nisalfernandolive/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180630135458.png', '9', '2018-07-01 02:24:58', '2018-07-01 02:24:58', '', 'Nisal Fernando, Nisal Fernando songs,');
INSERT INTO `artists` VALUES ('77', 'Manoj Dewarajage', '', 'https://www.facebook.com/manoj.prera', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180630151458.png', '9', '2018-07-01 03:44:58', '2018-07-01 03:44:58', '', 'Manoj Dewarajage, Manoj Dewarajage songs, Manoj Dewarajage songs download, ');
INSERT INTO `artists` VALUES ('78', 'Damitha Ayodya', '', 'https://www.facebook.com/dayodya', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704141732.png', '9', '2018-07-05 02:47:32', '2018-07-05 02:47:32', '', 'damitha ayodya, damitha ayodya songs, damitha ayodya songs mp3, damitha ayodya mp3 songs download, damitha ayodya songs download, ');
INSERT INTO `artists` VALUES ('79', 'Mahesh Uyanwatta', '', 'https://www.facebook.com/Mahesh-Uyanwatta-1411907875782169/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704142449.png', '9', '2018-07-05 02:54:49', '2018-07-05 02:54:49', '', '');
INSERT INTO `artists` VALUES ('80', 'Shalinda Fernando', '', 'https://www.facebook.com/shali.allright', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704170910.png', '9', '2018-07-05 05:39:10', '2018-07-05 05:39:10', '', 'shalinda fernando, shalinda fernando song, shalinda fernando songs, shalinda fernando mp3 songs, shalinda fernando mp3 songs download, shalinda fernando all right, ');
INSERT INTO `artists` VALUES ('81', 'Thushara Subasinghe', '', 'https://www.facebook.com/Thushara-Subasinghe-Oxygen-1684691545163051/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704181551.png', '9', '2018-07-05 06:45:51', '2018-07-05 06:45:51', '', 'thushara subasinghe, thushara subasinghe new song, thushara subasinghe with oxygen, thushara subasinghe live, thushara subasinghe songs, thushara subasinghe song, thushara subasinghe new song mp3, thushara subasinghe new song 2018 mp3, thushara subasinghe amma, thushara subasinghe with oxygen 2018, thushara subasinghe new song mp3, thushara subasinghe new song live, thushara subasinghe mp3, thushara subasinghe nonstop, ');
INSERT INTO `artists` VALUES ('82', 'Udesh Indula', '', 'https://www.facebook.com/udesh.indula.3', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704185930.png', '9', '2018-07-05 07:29:31', '2018-07-05 07:29:31', '', 'udesh indula, udesh indula new song, derana dream star, udesh indula band, udesh indula film song, udesh indula live show mp3, udesh indula song mp3, udesh indula mp3 download, udesh indula songs list, udesh indula new songs, udesh indula songs, ');
INSERT INTO `artists` VALUES ('83', 'Rahathangama Nagitha Thero', '', 'https://www.facebook.com/rahathangamanagithathero/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704190538.png', '9', '2018-07-05 07:35:38', '2018-07-05 07:35:38', '', '');
INSERT INTO `artists` VALUES ('84', 'Gihen Nishan', '', 'https://www.facebook.com/Gihen-Nishan-142494862537680/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704194521.png', '9', '2018-07-05 08:15:21', '2018-07-05 08:15:21', '', '');
INSERT INTO `artists` VALUES ('85', 'Nalinda Ranasinghe', '', 'https://www.facebook.com/nalindaranasingheevents/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705141639.png', '9', '2018-07-06 02:46:40', '2018-07-06 02:46:40', '', 'nalinda ranasinghe, nalinda ranasinghe songs, nalinda ranasinghe best songs, nalinda ranasinghe music videos, nalinda ranasinghe new songs, nalinda ranasinghe new song, nalinda ranasinghe video songs, nalinda ranasinghe song list, nalinda ranasinghe songs mp3, nalinda ranasinghe live, sinhala songs, sinhala songs old, sinhala video songs, sinhala music video, nalinda, ');
INSERT INTO `artists` VALUES ('86', 'Anushka Madushanka', '', 'https://www.facebook.com/desawanaofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705160421.png', '9', '2018-07-06 04:34:21', '2018-07-06 04:34:21', '', 'anushka madushanka, anushka madushanka songs, anushka madushanka mp3 songs, anushka madushanka songs download, ');
INSERT INTO `artists` VALUES ('87', 'Manoj V', '', 'https://www.facebook.com/profile.php?id=100012253838320', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705171017.png', '9', '2018-07-06 05:40:17', '2018-07-06 05:40:17', '', '');
INSERT INTO `artists` VALUES ('88', 'Amila M Wickramasingha', '', 'https://www.facebook.com/amilamblackdiamondz', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705171619.png', '9', '2018-07-06 05:46:20', '2018-07-06 05:46:20', '', '');
INSERT INTO `artists` VALUES ('89', 'Dileepa Thamel', '', 'https://www.facebook.com/Dileepaofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180707130253.png', '9', '2018-07-08 01:32:53', '2018-07-08 01:33:31', '', '');
INSERT INTO `artists` VALUES ('90', 'Mithila Randika', '', 'https://www.facebook.com/mithila.randika.1', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180708100443.png', '9', '2018-07-08 22:34:43', '2018-07-08 22:34:43', '', '');
INSERT INTO `artists` VALUES ('91', 'Saman Pushpakumara', '', 'https://www.facebook.com/profile.php?id=100014407653497', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180710181110.png', '9', '2018-07-11 06:41:10', '2018-07-11 06:41:10', '', '');
INSERT INTO `artists` VALUES ('92', 'Errol Jayawardena', '', 'https://www.facebook.com/anthonyerrol', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180712153218.png', '9', '2018-07-13 04:02:18', '2018-07-13 04:02:18', '', 'errol jayawardena, errol jayawardena song, errol jayawardena song download, errol jayawardena video, errol jayawardena mp3, errol jayawardena video song, errol jayawardena new song, ');
INSERT INTO `artists` VALUES ('93', 'Sasindu Gunawardhana', '', 'https://www.facebook.com/profile.php?id=100004275435134', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180712154314.png', '9', '2018-07-13 04:13:14', '2018-07-13 04:13:14', '', 'sasindu gunawardhana, sasindu gunawardhana song, sasindu gunawardhana mp3, sasindu gunawardhana songs, sasindu gunawardhana new songs, ');
INSERT INTO `artists` VALUES ('94', 'Janaka Katipearachchige', '', 'https://www.facebook.com/profile.php?id=100008394213952', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713133830.png', '9', '2018-07-14 02:08:30', '2018-07-14 02:08:30', '', 'janaka katipearachchige, janaka katipearachchige song, janaka katipearachchige song, janaka katipearachchige mp3 download, janaka katipearachchige video, janaka katipearachchige mp3 free download, ');
INSERT INTO `artists` VALUES ('95', 'Prageeth Vidana Pathirana', '', 'https://www.facebook.com/prageethvp.official', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713141632.png', '9', '2018-07-14 02:46:32', '2018-07-14 02:46:32', '', 'prageeth vidana pathirana, prageeth vidana pathirana song, prageeth vidana pathirana mp3 song, prageeth vidana pathirana songs download');
INSERT INTO `artists` VALUES ('96', 'Sathsara Kasun Pathirana', '', 'https://www.facebook.com/sathsara.kasunpathirana.1', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713144844.png', '9', '2018-07-14 03:18:44', '2018-07-14 03:18:44', '', 'Sathsara Kasun Pathirana, Sathsara Kasun Pathirana song, Sathsara Kasun Pathirana Video,');
INSERT INTO `artists` VALUES ('97', 'Roshen Walisundara', '', 'https://www.facebook.com/profile.php?id=100009502429594', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713145421.png', '9', '2018-07-14 03:24:21', '2018-07-14 03:24:21', '', '');
INSERT INTO `artists` VALUES ('98', 'Ajith Anthony', '', '', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180716153033.png', '9', '2018-07-17 04:00:36', '2018-07-17 04:00:36', '', 'ajith anthony, ajith anthony song, ajith anthony songs, ajith anthony song mp3, ajith anthony mp3 song download, ');
INSERT INTO `artists` VALUES ('99', 'Tehan Perera', '', 'https://www.facebook.com/tehanperera.fanpage/', 'https://twitter.com/realtehanperera', 'https://www.youtube.com/channel/UCbEcbZElFiOsXSY-Gj0_PDQ', 'core/storage/uploads/images/artists/artists-20180725091727.png', '9', '2018-07-25 21:47:27', '2018-07-25 21:47:27', '', 'tehan perera, tehan perera songs, tehan perera new song, tehan perera new songs, ');
INSERT INTO `artists` VALUES ('100', 'Sampath Fernandopulle', '', 'https://www.facebook.com/sampath.fernandopulle.1', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180725092231.png', '9', '2018-07-25 21:52:32', '2018-07-25 21:52:32', '', '');

-- ----------------------------
-- Table structure for artist_song
-- ----------------------------
DROP TABLE IF EXISTS `artist_song`;
CREATE TABLE `artist_song` (
  `artist_id` int(10) unsigned DEFAULT NULL,
  `song_id` int(10) unsigned DEFAULT NULL,
  KEY `artist_song_artist_id_foreign` (`artist_id`),
  KEY `artist_song_song_id_foreign` (`song_id`),
  CONSTRAINT `artist_song_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `artist_song_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of artist_song
-- ----------------------------
INSERT INTO `artist_song` VALUES ('1', '1');
INSERT INTO `artist_song` VALUES ('1', '2');
INSERT INTO `artist_song` VALUES ('4', '2');
INSERT INTO `artist_song` VALUES ('1', '3');
INSERT INTO `artist_song` VALUES ('1', '6');
INSERT INTO `artist_song` VALUES ('1', '7');
INSERT INTO `artist_song` VALUES ('1', '8');
INSERT INTO `artist_song` VALUES ('1', '9');
INSERT INTO `artist_song` VALUES ('5', '10');
INSERT INTO `artist_song` VALUES ('5', '11');
INSERT INTO `artist_song` VALUES ('7', '12');
INSERT INTO `artist_song` VALUES ('17', '13');
INSERT INTO `artist_song` VALUES ('15', '14');
INSERT INTO `artist_song` VALUES ('17', '15');
INSERT INTO `artist_song` VALUES ('5', '17');
INSERT INTO `artist_song` VALUES ('5', '18');
INSERT INTO `artist_song` VALUES ('5', '19');
INSERT INTO `artist_song` VALUES ('5', '20');
INSERT INTO `artist_song` VALUES ('5', '21');
INSERT INTO `artist_song` VALUES ('5', '22');
INSERT INTO `artist_song` VALUES ('5', '23');
INSERT INTO `artist_song` VALUES ('5', '24');
INSERT INTO `artist_song` VALUES ('5', '25');
INSERT INTO `artist_song` VALUES ('5', '26');
INSERT INTO `artist_song` VALUES ('5', '27');
INSERT INTO `artist_song` VALUES ('5', '28');
INSERT INTO `artist_song` VALUES ('5', '29');
INSERT INTO `artist_song` VALUES ('5', '30');
INSERT INTO `artist_song` VALUES ('5', '31');
INSERT INTO `artist_song` VALUES ('5', '33');
INSERT INTO `artist_song` VALUES ('5', '34');
INSERT INTO `artist_song` VALUES ('14', '35');
INSERT INTO `artist_song` VALUES ('42', '36');
INSERT INTO `artist_song` VALUES ('45', '37');
INSERT INTO `artist_song` VALUES ('14', '38');
INSERT INTO `artist_song` VALUES ('14', '39');
INSERT INTO `artist_song` VALUES ('49', '40');
INSERT INTO `artist_song` VALUES ('51', '41');
INSERT INTO `artist_song` VALUES ('54', '42');
INSERT INTO `artist_song` VALUES ('56', '43');
INSERT INTO `artist_song` VALUES ('55', '44');
INSERT INTO `artist_song` VALUES ('59', '45');
INSERT INTO `artist_song` VALUES ('61', '46');
INSERT INTO `artist_song` VALUES ('58', '47');
INSERT INTO `artist_song` VALUES ('6', '48');
INSERT INTO `artist_song` VALUES ('63', '49');
INSERT INTO `artist_song` VALUES ('65', '50');
INSERT INTO `artist_song` VALUES ('67', '51');
INSERT INTO `artist_song` VALUES ('70', '52');
INSERT INTO `artist_song` VALUES ('5', '32');
INSERT INTO `artist_song` VALUES ('75', '53');
INSERT INTO `artist_song` VALUES ('77', '54');
INSERT INTO `artist_song` VALUES ('11', '55');
INSERT INTO `artist_song` VALUES ('80', '56');
INSERT INTO `artist_song` VALUES ('13', '57');
INSERT INTO `artist_song` VALUES ('81', '58');
INSERT INTO `artist_song` VALUES ('82', '59');
INSERT INTO `artist_song` VALUES ('6', '60');
INSERT INTO `artist_song` VALUES ('86', '61');
INSERT INTO `artist_song` VALUES ('87', '62');
INSERT INTO `artist_song` VALUES ('85', '63');
INSERT INTO `artist_song` VALUES ('14', '64');
INSERT INTO `artist_song` VALUES ('90', '65');
INSERT INTO `artist_song` VALUES ('91', '66');
INSERT INTO `artist_song` VALUES ('92', '67');
INSERT INTO `artist_song` VALUES ('94', '68');
INSERT INTO `artist_song` VALUES ('95', '69');
INSERT INTO `artist_song` VALUES ('98', '70');
INSERT INTO `artist_song` VALUES ('1', '71');
INSERT INTO `artist_song` VALUES ('99', '72');

-- ----------------------------
-- Table structure for audio
-- ----------------------------
DROP TABLE IF EXISTS `audio`;
CREATE TABLE `audio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `src` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) unsigned NOT NULL,
  `dialog` text COLLATE utf8_unicode_ci,
  `etisalat` text COLLATE utf8_unicode_ci,
  `airtel` text COLLATE utf8_unicode_ci,
  `hutch` text COLLATE utf8_unicode_ci,
  `mobitel` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `audio_user_id_foreign` (`user_id`),
  CONSTRAINT `audio_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of audio
-- ----------------------------
INSERT INTO `audio` VALUES ('3', 'Man Pathanawa (Samu Aran Ya Yuthui)', '/core/storage/uploads/audio/audio-20180511195007.mp3', '2018-05-12 08:20:13', '2018-05-12 08:20:13', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('4', 'Heenayakda Me', '/core/storage/uploads/audio/audio-20180511203847.mp3', '2018-05-12 09:08:51', '2018-06-14 02:49:10', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('5', 'Saththai Oya (Man Witharada Adare Kale)', '/core/storage/uploads/audio/audio-20180511204212.mp3', '2018-05-12 09:12:14', '2018-05-12 09:12:14', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('7', 'Kawadahari Wetahei', '/core/storage/uploads/audio/audio-20180513202907.mp3', '2018-05-14 08:59:11', '2018-06-14 02:48:55', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('8', 'Me Nihanda Bhawaye', '/core/storage/uploads/audio/audio-20180513203453.mp3', '2018-05-14 09:04:53', '2018-06-14 02:48:43', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('9', 'Nolabena Senehe New Version', '/core/storage/uploads/audio/audio-20180513203617.mp3', '2018-05-14 09:06:17', '2018-06-14 02:48:26', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('10', 'Oya Nathuwa Dannam Mata (Me Adarayai Theme Song)', '/core/storage/uploads/audio/audio-20180513203709.mp3', '2018-05-14 09:07:15', '2018-06-14 02:48:09', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('11', 'Adarei Katawath Nathi Tharam Kiya', '/core/storage/uploads/audio/audio-20180514124204.mp3', '2018-05-15 01:12:08', '2018-06-14 02:47:48', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('12', 'Adarei Wasthu', '/core/storage/uploads/audio/audio-20180514124246.mp3', '2018-05-15 01:12:50', '2018-06-14 02:47:33', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('13', 'Akeekaruma Man Udura', '/core/storage/uploads/audio/audio-20180514124331.mp3', '2018-05-15 01:13:34', '2018-06-14 02:47:15', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('14', 'Waradak Kiyanne Na', '/core/storage/uploads/audio/audio-20180516173932.mp3', '2018-05-17 06:09:39', '2018-06-14 02:46:57', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('15', 'Sudu (Mathu Dineka)', '/core/storage/uploads/audio/audio-20180521150324.mp3', '2018-05-22 03:33:24', '2018-06-14 02:46:39', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('16', 'Sansara Sihine', '/core/storage/uploads/audio/audio-20180521175259.mp3', '2018-05-22 06:23:05', '2018-06-14 02:46:21', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('17', 'Saaritha', '/core/storage/uploads/audio/audio-20180521183125.mp3', '2018-05-22 07:01:32', '2018-06-14 02:46:03', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('18', 'Saragaye Raye', '/core/storage/uploads/audio/audio-20180521190303.mp3', '2018-05-22 07:33:09', '2018-06-14 02:45:41', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('19', 'Ma Hara Giya Dine', '/core/storage/uploads/audio/audio-20180527181454.mp3', '2018-05-28 06:44:56', '2018-06-14 02:45:24', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('20', 'Amathaka Karanna Ba', '/core/storage/uploads/audio/audio-20180529112307.mp3', '2018-05-29 23:53:15', '2018-06-14 02:45:08', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('21', 'Arayum Karanne', '/core/storage/uploads/audio/audio-20180529112606.mp3', '2018-05-29 23:56:06', '2018-06-14 02:44:52', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('22', 'Awasan Pema Mage', '/core/storage/uploads/audio/audio-20180529121805.mp3', '2018-05-30 00:48:11', '2018-06-14 02:43:44', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('23', 'Durin Hinda Ma', '/core/storage/uploads/audio/audio-20180529122003.mp3', '2018-05-30 00:50:06', '2018-06-14 02:43:15', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('24', 'Kiyabu Lathawe', '/core/storage/uploads/audio/audio-20180529122038.mp3', '2018-05-30 00:50:43', '2018-06-14 02:43:00', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('26', 'Ma Me Tharam Handawala', '/core/storage/uploads/audio/audio-20180529122149.mp3', '2018-05-30 00:51:52', '2018-06-14 02:42:40', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('27', 'Me Hitha Langa', '/core/storage/uploads/audio/audio-20180529122306.mp3', '2018-05-30 00:53:11', '2018-06-14 02:42:25', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('28', 'Nidukin Inu Mana', '/core/storage/uploads/audio/audio-20180529122350.mp3', '2018-05-30 00:53:54', '2018-06-14 02:42:10', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('29', 'Oba Ekka Mama', '/core/storage/uploads/audio/audio-20180529122448.mp3', '2018-05-30 00:54:54', '2018-06-14 02:41:57', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('30', 'Obamada Me Hithata Mage', '/core/storage/uploads/audio/audio-20180529122528.mp3', '2018-05-30 00:55:28', '2018-06-14 02:41:43', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('31', 'Para Kiyana Tharukawi', '/core/storage/uploads/audio/audio-20180529122647.mp3', '2018-05-30 00:56:52', '2018-06-14 02:41:28', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('32', 'Senehasa Bidunath', '/core/storage/uploads/audio/audio-20180529122744.mp3', '2018-05-30 00:57:48', '2018-06-14 02:41:07', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('33', 'Awasana Premayai Mage', '/core/storage/uploads/audio/audio-20180529123835.mp3', '2018-05-30 01:08:37', '2018-06-14 02:40:40', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('34', 'Maa Nisa Pa Sina', '/core/storage/uploads/audio/audio-20180529123912.mp3', '2018-05-30 01:09:12', '2018-06-14 02:40:22', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('35', 'Wenna Thiyena', '/core/storage/uploads/audio/audio-20180529123951.mp3', '2018-05-30 01:09:58', '2018-06-14 02:39:51', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('36', 'Diwranna Behe Neda', '/core/storage/uploads/audio/audio-20180530141438.mp3', '2018-05-31 02:44:38', '2018-06-13 23:25:38', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('37', 'Dahata Nopeni Inna', '/core/storage/uploads/audio/audio-20180601153201.mp3', '2018-06-02 04:02:03', '2018-06-13 23:25:26', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('38', 'Oyata Vitharak', '/core/storage/uploads/audio/audio-20180602145553.mp3', '2018-06-03 03:25:55', '2018-06-13 23:25:11', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('39', 'Hitha Gawa Heena', '/core/storage/uploads/audio/audio-20180602171740.mp3', '2018-06-03 05:47:45', '2018-06-13 23:26:07', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('40', 'Miya Yanna Sudanam (Monawathma Wena Aye)', '/core/storage/uploads/audio/audio-20180602171830.mp3', '2018-06-03 05:48:30', '2018-06-13 23:26:19', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('41', 'Obe Adare', '/core/storage/uploads/audio/audio-20180606101730.mp3', '2018-06-06 22:47:33', '2018-06-13 23:24:41', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('42', 'Mathaka Mawee', '/core/storage/uploads/audio/audio-20180606114901.mp3', '2018-06-07 00:19:04', '2018-06-13 23:24:21', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('43', 'Pem Karala', '/core/storage/uploads/audio/audio-20180606122526.mp3', '2018-06-07 00:55:28', '2018-06-13 23:24:07', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('44', 'Wirasakawee', '/core/storage/uploads/audio/audio-20180607125653.mp3', '2018-06-08 01:26:56', '2018-06-13 23:23:45', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('45', 'Antima Mohothedi', '/core/storage/uploads/audio/audio-20180607132230.mp3', '2018-06-08 01:52:34', '2018-06-12 00:11:27', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('46', 'Pemwathiye', '/core/storage/uploads/audio/audio-20180614230344.mp3', '2018-06-15 11:33:48', '2018-06-15 11:33:48', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('47', 'Aulak Nane', '/core/storage/uploads/audio/audio-20180614230438.mp3', '2018-06-15 11:34:38', '2018-06-15 11:34:38', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('48', 'Sulan Podak Wee', '/core/storage/uploads/audio/audio-20180614235635.mp3', '2018-06-15 12:26:38', '2018-06-15 12:26:38', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('49', 'Sammatheta Pitupe', '/core/storage/uploads/audio/audio-20180619182759.mp3', '2018-06-20 06:57:59', '2018-06-20 06:57:59', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('50', 'Nindath Hora Gaththa Oya', '/core/storage/uploads/audio/audio-20180622105317.mp3', '2018-06-22 23:23:20', '2018-06-22 23:23:20', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('51', 'Seya (Nabara Wu)', '/core/storage/uploads/audio/audio-20180629145836.mp3', '2018-06-30 03:28:36', '2018-06-30 03:28:36', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('52', 'Hithuwak Kari', '/core/storage/uploads/audio/audio-20180629154718.mp3', '2018-06-30 04:17:21', '2018-07-05 22:24:14', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('53', 'Aththama Kiyannam', '/core/storage/uploads/audio/audio-20180629161402.mp3', '2018-06-30 04:44:02', '2018-06-30 04:44:02', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('54', 'Unmada Rathriye', '/core/storage/uploads/audio/audio-20180630142307.mp3', '2018-07-01 02:53:08', '2018-07-01 02:53:08', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('55', 'Parana Hithuwakkari', '/core/storage/uploads/audio/audio-20180630152102.mp3', '2018-07-01 03:51:02', '2018-07-01 03:51:02', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('56', 'Dukak Denenna Epa', '/core/storage/uploads/audio/audio-20180704133150.mp3', '2018-07-05 02:01:56', '2018-07-05 02:01:56', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('57', 'Mage Asurin', '/core/storage/uploads/audio/audio-20180704165913.mp3', '2018-07-05 05:29:17', '2018-07-05 05:29:17', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('58', 'Therum Giya', '/core/storage/uploads/audio/audio-20180704174150.mp3', '2018-07-05 06:11:56', '2018-07-05 06:11:56', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('59', 'Heen Sare', '/core/storage/uploads/audio/audio-20180704181708.mp3', '2018-07-05 06:47:08', '2018-07-05 06:47:08', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('60', 'Man Vindina', '/core/storage/uploads/audio/audio-20180704191313.mp3', '2018-07-05 07:43:14', '2018-07-05 07:43:14', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('61', 'Nodakapu Dewani Budun', '/core/storage/uploads/audio/audio-20180704195539.mp3', '2018-07-05 08:25:43', '2018-07-05 08:25:43', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('62', 'Sudu Manika', '/core/storage/uploads/audio/audio-20180705150320.mp3', '2018-07-06 03:33:23', '2018-07-06 03:33:23', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('63', 'Mata Wetahuna', '/core/storage/uploads/audio/audio-20180705161131.mp3', '2018-07-06 04:41:32', '2018-07-06 04:41:32', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('64', 'Salli Na', '/core/storage/uploads/audio/audio-20180705171130.mp3', '2018-07-06 05:41:36', '2018-07-06 05:41:36', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('65', 'Rawatuna Nowe', '/core/storage/uploads/audio/audio-20180707125941.mp3', '2018-07-08 01:29:44', '2018-07-08 01:29:44', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('66', 'Aswaha Wadune', '/core/storage/uploads/audio/audio-20180708102159.mp3', '2018-07-08 22:52:05', '2018-07-08 22:52:05', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('67', ' Oba Warade Pataleddi', '/core/storage/uploads/audio/audio-20180710181520.mp3', '2018-07-11 06:45:25', '2018-07-11 06:45:25', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('68', ' Purudu Thanikama', '/core/storage/uploads/audio/audio-20180712160415.mp3', '2018-07-13 04:34:18', '2018-07-13 04:34:18', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('69', 'Nodaka Oya', '/core/storage/uploads/audio/audio-20180713135737.mp3', '2018-07-14 02:27:40', '2018-07-14 02:27:40', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('70', 'Adare Hodi Pothe', '/core/storage/uploads/audio/audio-20180713141734.mp3', '2018-07-14 02:47:37', '2018-07-14 02:47:37', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('71', 'Labimai Me Ape', '/core/storage/uploads/audio/audio-20180716153156.mp3', '2018-07-17 04:01:58', '2018-07-18 11:16:01', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('72', 'Wen Wenna', '/core/storage/uploads/audio/audio-20180719171850.mp3', '2018-07-20 05:48:54', '2018-07-20 05:48:54', '9', null, null, null, null, null);
INSERT INTO `audio` VALUES ('73', 'Manamaliye', '/core/storage/uploads/audio/audio-20180725095557.mp3', '2018-07-25 22:25:58', '2018-07-25 22:25:58', '9', null, null, null, null, null);

-- ----------------------------
-- Table structure for dynamic_pages
-- ----------------------------
DROP TABLE IF EXISTS `dynamic_pages`;
CREATE TABLE `dynamic_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dynamic_pages
-- ----------------------------
INSERT INTO `dynamic_pages` VALUES ('1', 'Privacy Policy Page', '<p>Introduction<br />This privacy policy (&ldquo;Policy&rdquo;) and this site&rsquo;s Terms of Service (together the &ldquo;Terms&rdquo;) govern all use of https://desawana.com and that site&rsquo;s services (together the &ldquo;Site&rdquo; or &ldquo;Services&rdquo;). The owners and contributors to the Site will be referred to as &ldquo;we,&rdquo; &ldquo;us,&rdquo; or &ldquo;our&rdquo; in this Policy. By using the Site or its Services, and/or by clicking anywhere on this Site to agree to the Terms and this Policy, you are deemed to be a &ldquo;user&rdquo; for purposes of this Policy. You and every other user (&ldquo;you&rdquo; or &ldquo;User&rdquo; as applicable) are subject to this Policy. You and each user also agree to the Terms by using the Services. In these Terms, the word &ldquo;Site&rdquo; includes the site referenced above, its owner(s), contributors, suppliers, licensors, and other related parties.</p>\r\n<p>We provide this privacy statement explaining our online information practices, so that you can decide whether and how to interact with the Site and the Services.</p>\r\n<p>We may release your information when we deem it appropriate to comply with the law, enforce our site policies, or protect ours or others&rsquo; rights, property, or safety.</p>\r\n<p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>\r\n<p>Please also review our Terms of Use section that governs the use and the users of the Site.</p>\r\n<p>By using our site, you consent to our privacy policy.</p>\r\n<p>If we decide to change our privacy policy, we will post those changes on this page. If we have your email address, we may also send an email notifying you of any changes.</p>\r\n<p>Contact Data and Other Identifiable Information<br />This site collects certain user information, which may include a username and password, contact information, or any other data that you type in to the site. It may also identify your IP address to help identify you on future visits to the site. At our discretion, the Site may use this data to:</p>\r\n<p>Personalize the user experience and/or customer service<br />Improve the site<br />To process transactions<br />Administer a contest, promotion, survey or other site feature or function<br />Send email to users<br />Mobile Device Privacy<br />The following applies to our site, when viewed on a mobile device:<br />When accessed with a mobile deivce, our site may collect information automatically, such as the type of mobile device you have, device identifiers, and information about your use of the site. Regardless of the device you use to access the site, it will also collect information you provide, as well as information about your interaction with the site and its content.<br />If location services are activated on your mobile device, our site may collect information about the location of your device. Your mobile network service providers may collect device-specific information, such as a device identifier, when you use our website or one of our mobile applications. This information collected by your mobile network service will not be associated with your user account with us, or with your personally identifiable information.</p>\r\n<p>Protection of user information<br />We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you share with the site.</p>\r\n<p>Advertising Network<br />We use one or more third party vendors to serve ads on the Site. To serve ads and determine how our users use the Site, these services use cookies, or small pieces of code to serve ads to Site users based on users&rsquo; visits to the Site and others. Users may adjust their browser settings to disallow the use of cookies. With cookies turned off, certain features of the Site may work less efficiently or not at all.</p>\r\n<p>We use Google as an advertising services provider for the Site. Users may opt out of Google&rsquo;s use of the DART use-tracking cookie by visiting the Google advertising Policies &amp; Principles page. If you opt out of ad tailoring, you will still see ads, but they will not be based on your browsing history, and they may appear in other languages.</p>\r\n<p>Cookies<br />This site uses cookies. Cookies are small pieces of code that the Site or a service provider will put on your computer if your Web browser allows it. The Site uses cookies to recognize and keep certain information. On the Site, that information may be used to recognize your computer and browser from current or past visits to the Site or related sites. We may use this cookie-captured information to improve our service to you, to aggregate information about visitors to the Site and their behavior, to remember and process items in your shopping cart, to understand and save user preferences, or to keep track of advertising. We may contract with third-party service providers to assist us in better understanding our site visitors.<br />In most Internet browsers, you can change your settings so that you will be warned each time a cookie is being sent, or so that cookies will be turned off. With cookies blocked, some functions of the Site may not operate properly.</p>\r\n<p>Usernames, Passwords, and Profiles<br />If prompted, Users must provide a valid email address to the Site, at which the User can receive messages. User must also update the Site if that email address changes. The Site reserves the right to terminate any User account if a valid email is requested but is not provided by the User.<br />If the Site prompts or allows a User to create a username or profile, Users agree not to pick a username or provide any profile information that would impersonate someone or that is likely to cause confusion with any other person or entity. The Site reserves the right to cancel a User account or change a username or profile data at any time. Similarly, if the Site prompts or allows a User to create an avatar or upload a picture, User agrees not to use any image that impersonates some other person or entity, or that is otherwise likely to cause confusion.<br />You are responsible for protecting your username and password for the Site, and you agree not to disclose it to any third party. We recommend that you use a passwork that is more than eight characters long. You are responsible for all activity on your account, whether or not you authorized it. You agree to inform us of unauthorized use of your account, by email to Admin@Desawana.Com. You acknowledge that if you wish to protect your interactions the Site, it is your responsibility to use a secure encrypted connection, virtual private network, or other appropriate measures.</p>\r\n<p>Disputes<br />We are based in Moratuwa and you are contracting to use our Site. This Policy and all matters arising from your use of the Site are governed by and will be construed according to the laws of Moratuwa, without regard to any choice of laws rules of any jurisdiction. The federal courts and state courts that have geographical jurisdiction over disputes arising at our office location in Moratuwa will be the only permissible venues for any and all disputes arising out of or in connection with this Policy or the Site and Service.</p>\r\n<p>ARBITRATION<br />Notwithstanding anything that may be contrary within the &ldquo;Disputes&rdquo; provisions above, all matters, and all claims within a multi-claim matter, that are arbitrable, including all claims for monetary damages, shall be decided by a single arbitrator to be selected by us, who shall hold hearings in or near Moratuwa, under the rules of the American Arbitration Association.</p>\r\n<p>Terms Contact<br />If you have any questions about these Terms, please address them to Admin@Desawana.Com.</p>\r\n<p>Last Updated<br />These terms were last updated on May 30, 2018</p>', '0000-00-00 00:00:00', '2018-05-31 05:22:58');
INSERT INTO `dynamic_pages` VALUES ('2', 'Terms Of Service Page', '<p>Introduction<br />Site Terms of Service, an Enforceable Legal Agreement.<br />As of May 30, 2018</p>\r\n<p>These Terms of Service and our privacy policy (together the &ldquo;Terms&rdquo;) govern all use of https://desawana.com and that site&rsquo;s services (together the &ldquo;Site&rdquo; or &ldquo;Services&rdquo;). The Site is owned by Kasun Madushanka, a individual.</p>\r\n<p>The owners and contributors to the Site will be referred to as &ldquo;we,&rdquo; &ldquo;us,&rdquo; or &ldquo;our&rdquo; in these Terms. By using the Site or its Services, and/or by clicking anywhere on this Site to agree to these Terms, you are deemed to be a &ldquo;user&rdquo; for purposes of the Terms. You and every other user (&ldquo;you&rdquo; or &ldquo;User&rdquo; as applicable) are bound by these Terms. You and each user also agree to the Terms by using the Services. If any User does not agree to the Terms or the Privacy Policy, such User may not access the Site or use the Services. In these Terms, the word &ldquo;Site&rdquo; includes the site referenced above, its owner(s), contributors, suppliers, licensors, and other related parties.</p>\r\n<p>User Prohibited From Illegal Uses<br />User shall not use, and shall not allow any person to use, the Site or Services in any way that violates a federal, state, or local law, regulation, or ordinance, or for any disruptive, tortious, or illegal purpose, including but not limited to harassment, slander, defamation, data theft or inappropriate dissemination, or improper surveillance of any person.</p>\r\n<p>User represents and warrants that:<br />User will use the Services only as provided in these Terms;<br />User is at least 18 years old and has all right, authority, and capacity to agree to these Terms;<br />User will provide accurate, complete, and current information to the Site and its owner(s);<br />User will notify the Site and its owner(s) regarding any material change to information User provides, either by updating and correcting the information, or by alerting the Site and its owner(s) via the functions of the Site or the email address provided below.<br />Disclaimer of Warranties<br />TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SITE PROVIDES THE SERVICES &ldquo;AS IS,&rdquo; WITH ALL FAULTS. THE SITE DOES NOT WARRANT UNINTERRUPTED USE OR OPERATION OF THE SERVICES, OR THAT ANY DATA WILL BE TRANSMITTED IN A MANNER THAT IS TIMELY, UNCORRUPTED, FREE OF INTERFERENCE, OR SECURE. THE SITE DISCLAIMS REPRESENTATIONS, WARRANTIES, AND CONDITIONS OF ANY KIND, WHETHER EXPRESS, IMPLIED, WRITTEN, ORAL, CONTRACTUAL, COMMON LAW, OR STATUTORY, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES, DUTIES, OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, OR THAT MAY ARISE FROM A COURSE OF DEALING OR USAGE OF TRADE.</p>\r\n<p>Liability Is Limited<br />THE SITE SHALL NOT BE LIABLE FOR INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY, OR PUNITIVE DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOST PROFITS (REGARDLESS OF WHETHER WE HAVE BEEN NOTIFIED THAT SUCH LOSS MAY OCCUR) OR EXPOSURE TO ANY THIRD PARTY CLAIMS BY REASON OF ANY ACT OR OMISSION. THE SITE SHALL NOT BE LIABLE FOR ANY ACT OR OMISSION OF ANY THIRD PARTY INVOLVED WITH THE SERVICES, SITE OFFERS, OR ANY ACT BY SITE USERS. THE SITE SHALL NOT BE LIABLE FOR ANY DAMAGES THAT RESULT FROM ANY SERVICE PROVIDED BY, OR PRODUCT OR DEVICE MANUFACTURED BY, THIRD PARTIES.</p>\r\n<p>NOTWITHSTANDING ANY DAMAGES THAT USER MAY SUFFER FOR ANY REASON, THE ENTIRE LIABILITY OF THE SITE IN CONNECTION WITH THE SITE OR SERVICES, AND ANY PARTY&rsquo;S EXCLUSIVE REMEDY, SHALL BE LIMITED TO THE AMOUNT, IF ANY, ACTUALLY PAID BY USER TO THE SITE OWNER DURING THE 12 MONTHS PRIOR TO THE EVENT THAT USER CLAIMS CAUSED THE DAMAGES.</p>\r\n<p>The Site shall not be liable for any damages incurred as a result of any loss, disclosure, or third party use of information, regardless of whether such disclosure or use is with or without User&rsquo;s knowledge or consent. The Site shall have no liability for any damages related to: User&rsquo;s actions or failures to act, the acts or omissions of any third party, including but not limited to any telecommunications service provider, or events or causes beyond the Site&rsquo;s reasonable control. The Site has no obligations whatever, and shall have no liability to, any third party who is not a User bound by these Terms. Limitations, exclusions, and disclaimers in these Terms shall apply to the maximum extent permitted by applicable law, even if any remedy fails its essential purpose.</p>\r\n<p>Third party products, links, and actions<br />The Site may include or offer third party products or services. The Site may also have other users or members who interact with each other, through the Site, elsewhere online, or in person. These third party products and any linked sites have separate and independent terms of service and privacy policies. We have no control or responsibility for the content and activities of these linked sites, sellers, and third parties in general, regardless of whether you first were introduced or interacted with such businesses, services, products, and people through the Site, and therefore you agree that we are not liable for any of them. We do, however, welcome any feedback about these sites, sellers, other users or members, and third parties.</p>\r\n<p>Changes to the Site and the Services<br />The owners and contributors to the Site will work to improve the Site for our users, and to further our business interests in the Site. We reserve the right to add, change, and remove features, content, and data, including the right to add or change any pricing terms. You agree that we will not be liable for any such changes. Neither your use of the Site nor these terms give you any right, title, or protectable legal interest in the Site or its content.</p>\r\n<p>Indemnity<br />If your activity or any activity on your behalf creates potential or actual liability for us, or for any of our users, partners, or contributors, you agree to indemnify and hold us and any such user, partner, contributor, or any agent harmless from and against all claims, costs of defense and judgment, liabilities, legal fees, damages, losses, and other expenses in relation to any claims or actions arising out of or relating to your use of the Site, or any breach by you of these Terms of Use.</p>\r\n<p>Intellectual Property<br />This site and some delivery modes of our product are built on the WordPress platform. For information about intellectual property rights, including General Public License (&ldquo;GPL&rdquo;) terms under which the WordPress software is licensed, see here http://wordpress.org/about/gpl/</p>\r\n<p>The Site grants User a revocable, non-transferable, and non-exclusive license to use the Site solely in connection with the Site and the Services, under these Terms.</p>\r\n<p>Copyright in all content and works of authorship included in the Site are the property of the Site or its licensors. Apart from links which lead to the Site, accurately attributed social media references, and de minimus text excerpts with links returning to the Site, no text, images, video or audio recording, or any other content from the Site shall be copied without explicit and detailed, written permission from the Site&rsquo;s owner. User shall not sublicense or otherwise transfer any rights or access to the Site or related Services to any other person.</p>\r\n<p>The names and logos used by the Site, and all other trademarks, service marks, and trade names used in connection with the Services are owned by the Site or its licensors and may not be used by User without written consent of the rights owners. Use of the Site does not in itself give any user any license, consent, or permission, unless and then only to the extent granted explicitly in these Terms.</p>\r\n<p>All rights not expressly granted in these Terms are reserved by the Site.</p>\r\n<p>Privacy<br />Any information that you provide to the Site is subject to the Site&rsquo;s Privacy Policy, which governs our collection and use of User information. User understands that through his or her use of the Site and its Services, User consents to the collection and use (as set forth in the Privacy Policy) of the information, including the transfer of this information to the United States and/or other countries for storage, processing and use by the Site. The Site may make certain communications to some or all Users, such as service announcements and administrative messages. These communications are considered part of the Services and a User&rsquo;s account with the Site, and Users are not able to opt out of all of them.</p>\r\n<p>Usernames, Passwords, and Profiles<br />If prompted, Users must provide a valid email address to the Site, at which email address the User can receive messages. User must also update the Site if that email address changes. The Site reserves the right to terminate any User account and/or User access to the Site if a valid email is requested but is not provided by the User.</p>\r\n<p>If the Site prompts or allows a User to create a username or profile, Users agree not to pick a username or provide any profile information that would impersonate someone else or that is likely to cause confusion with any other person or entity. The Site reserves the right to cancel a User account or to change a username or profile data at any time. Similarly, if the Site allows comments or user input, or prompts or allows a User to create an avatar or upload a picture, User agrees not to use any image that impersonates some other person or entity, or that is otherwise likely to cause confusion.</p>\r\n<p>You are responsible for protecting your username and password for the Site, and you agree not to disclose it to any third party. We recommend that you use a password that is more than eight characters long. You are responsible for all activity on your account, whether or not you authorized it. You agree to inform us of unauthorized use of your account, by email to Admin@Desawana.Com. You acknowledge that if you wish to protect your interactions with the Site, it is your responsibility to use a secure encrypted connection, virtual private network, or other appropriate measures. The Site&rsquo;s own security measures are reasonable in terms of their level of protection, but are not helpful if the interactions of you or any other User with Site are not secure or private.</p>\r\n<p>Disputes<br />We are based in Moratuwa and you are contracting to use our Site. These Terms and all matters arising from your use of the Site are governed by and will be construed according to the laws of Moratuwa, without regard to any choice of laws rules of any jurisdiction. The federal courts and state courts that have geographical jurisdiction over disputes arising at our office location in the Moratuwa will be the only permissible venues for any and all disputes arising out of or in connection with these Terms or the Site and Service.</p>\r\n<p>ARBITRATION<br />Notwithstanding anything that may be contrary within the &ldquo;Disputes&rdquo; provisions above, all matters, and all arbitrable claims within a multi-claim matter, including all claims for monetary damages, shall be decided by a single arbitrator to be selected by us, which arbitrator shall hold hearings in or near Moratuwa, under the rules of the American Arbitration Association.</p>\r\n<p>Advertising<br />The Site may include advertisements, which may be targeted for relevance to the Site, queries made, or other information to improve relevance to the Site&rsquo;s users. The types and extent of advertising on the Site will change over time. In consideration for User access to and use of the Site, User agrees that the Site and third party providers and partners may place advertising anywhere on the Site. For the remaining terms that will apply to our advertising practices, including use of your information, see our Privacy Policy.</p>\r\n<p>General<br />These Terms, including the incorporated Privacy Policy, supersede all oral or written communications and understandings between User and the Site.</p>\r\n<p>Any cause of action User may have relating to the Site or the Services must be commenced within one (1) year after the claim or cause of action arises.</p>\r\n<p>Both parties waive the right to a jury trial in any dispute relating to the Terms, the Site, or the Services.</p>\r\n<p>If for any reason a court of competent jurisdiction finds any aspect of the Terms to be unenforceable, the Terms shall be enforced to the maximum extent permissible, to give effect to the intent of the Terms, and the remainder of the Terms shall continue in full force and effect.</p>\r\n<p>User may not assign his or her rights or delegate his or her responsibilities under these Terms or otherwise relating to the Site or its Services.</p>\r\n<p>There shall be no third party beneficiaries under these Terms, except for the Site&rsquo;s affiliates, suppliers, and licensors, or as required by law.</p>\r\n<p>Use of the Site and its Services is unauthorized in any jurisdiction that does not give effect to all provisions of these Terms, including without limitation this paragraph.</p>\r\n<p>The failure of the Site to exercise or enforce any right or provision of these Terms shall not constitute a waiver of that right or provision.</p>\r\n<p>Terms Contact<br />If you have any questions about these Terms, please address them to Admin@Desawana.Com.</p>\r\n<p>Last Updated<br />These terms were last updated on May 30, 2018</p>', '0000-00-00 00:00:00', '2018-05-31 05:23:38');

-- ----------------------------
-- Table structure for fonts-list
-- ----------------------------
DROP TABLE IF EXISTS `fonts-list`;
CREATE TABLE `fonts-list` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `unicode` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fonts-list
-- ----------------------------
INSERT INTO `fonts-list` VALUES ('1', 'fa', 'fa-adjust', '&#xf042;');
INSERT INTO `fonts-list` VALUES ('2', 'fa', 'fa-adn', '&#xf170;');
INSERT INTO `fonts-list` VALUES ('3', 'fa', 'fa-align-center', '&#xf037;');
INSERT INTO `fonts-list` VALUES ('4', 'fa', 'fa-align-justify', '&#xf039;');
INSERT INTO `fonts-list` VALUES ('5', 'fa', 'fa-align-left', '&#xf036;');
INSERT INTO `fonts-list` VALUES ('6', 'fa', 'fa-align-right', '&#xf038;');
INSERT INTO `fonts-list` VALUES ('7', 'fa', 'fa-ambulance', '&#xf0f9;');
INSERT INTO `fonts-list` VALUES ('8', 'fa', 'fa-anchor', '&#xf13d;');
INSERT INTO `fonts-list` VALUES ('9', 'fa', 'fa-android', '&#xf17b;');
INSERT INTO `fonts-list` VALUES ('10', 'fa', 'fa-angellist', '&#xf209;');
INSERT INTO `fonts-list` VALUES ('11', 'fa', 'fa-angle-double-down', '&#xf103;');
INSERT INTO `fonts-list` VALUES ('12', 'fa', 'fa-angle-double-left', '&#xf100;');
INSERT INTO `fonts-list` VALUES ('13', 'fa', 'fa-angle-double-right', '&#xf101;');
INSERT INTO `fonts-list` VALUES ('14', 'fa', 'fa-angle-double-up', '&#xf102;');
INSERT INTO `fonts-list` VALUES ('15', 'fa', 'fa-angle-down', '&#xf107;');
INSERT INTO `fonts-list` VALUES ('16', 'fa', 'fa-angle-left', '&#xf104;');
INSERT INTO `fonts-list` VALUES ('17', 'fa', 'fa-angle-right', '&#xf105;');
INSERT INTO `fonts-list` VALUES ('18', 'fa', 'fa-angle-up', '&#xf106;');
INSERT INTO `fonts-list` VALUES ('19', 'fa', 'fa-apple', '&#xf179;');
INSERT INTO `fonts-list` VALUES ('20', 'fa', 'fa-archive', '&#xf187;');
INSERT INTO `fonts-list` VALUES ('21', 'fa', 'fa-area-chart', '&#xf1fe;');
INSERT INTO `fonts-list` VALUES ('22', 'fa', 'fa-arrow-circle-down', '&#xf0ab;');
INSERT INTO `fonts-list` VALUES ('23', 'fa', 'fa-arrow-circle-left', '&#xf0a8;');
INSERT INTO `fonts-list` VALUES ('24', 'fa', 'fa-arrow-circle-o-down', '&#xf01a;');
INSERT INTO `fonts-list` VALUES ('25', 'fa', 'fa-arrow-circle-o-left', '&#xf190;');
INSERT INTO `fonts-list` VALUES ('26', 'fa', 'fa-arrow-circle-o-right', '&#xf18e;');
INSERT INTO `fonts-list` VALUES ('27', 'fa', 'fa-arrow-circle-o-up', '&#xf01b;');
INSERT INTO `fonts-list` VALUES ('28', 'fa', 'fa-arrow-circle-right', '&#xf0a9;');
INSERT INTO `fonts-list` VALUES ('29', 'fa', 'fa-arrow-circle-up', '&#xf0aa;');
INSERT INTO `fonts-list` VALUES ('30', 'fa', 'fa-arrow-down', '&#xf063;');
INSERT INTO `fonts-list` VALUES ('31', 'fa', 'fa-arrow-left', '&#xf060;');
INSERT INTO `fonts-list` VALUES ('32', 'fa', 'fa-arrow-right', '&#xf061;');
INSERT INTO `fonts-list` VALUES ('33', 'fa', 'fa-arrow-up', '&#xf062;');
INSERT INTO `fonts-list` VALUES ('34', 'fa', 'fa-arrows', '&#xf047;');
INSERT INTO `fonts-list` VALUES ('35', 'fa', 'fa-arrows-alt', '&#xf0b2;');
INSERT INTO `fonts-list` VALUES ('36', 'fa', 'fa-arrows-h', '&#xf07e;');
INSERT INTO `fonts-list` VALUES ('37', 'fa', 'fa-arrows-v', '&#xf07d;');
INSERT INTO `fonts-list` VALUES ('38', 'fa', 'fa-asterisk', '&#xf069;');
INSERT INTO `fonts-list` VALUES ('39', 'fa', 'fa-at', '&#xf1fa;');
INSERT INTO `fonts-list` VALUES ('40', 'fa', 'fa-automobile(alias)', '&#xf1b9;');
INSERT INTO `fonts-list` VALUES ('41', 'fa', 'fa-backward', '&#xf04a;');
INSERT INTO `fonts-list` VALUES ('42', 'fa', 'fa-ban', '&#xf05e;');
INSERT INTO `fonts-list` VALUES ('43', 'fa', 'fa-bank(alias)', '&#xf19c;');
INSERT INTO `fonts-list` VALUES ('44', 'fa', 'fa-bar-chart', '&#xf080;');
INSERT INTO `fonts-list` VALUES ('45', 'fa', 'fa-bar-chart-o(alias)', '&#xf080;');
INSERT INTO `fonts-list` VALUES ('46', 'fa', 'fa-barcode', '&#xf02a;');
INSERT INTO `fonts-list` VALUES ('47', 'fa', 'fa-bars', '&#xf0c9;');
INSERT INTO `fonts-list` VALUES ('48', 'fa', 'fa-bed', '&#xf236;');
INSERT INTO `fonts-list` VALUES ('49', 'fa', 'fa-beer', '&#xf0fc;');
INSERT INTO `fonts-list` VALUES ('50', 'fa', 'fa-behance', '&#xf1b4;');
INSERT INTO `fonts-list` VALUES ('51', 'fa', 'fa-behance-square', '&#xf1b5;');
INSERT INTO `fonts-list` VALUES ('52', 'fa', 'fa-bell', '&#xf0f3;');
INSERT INTO `fonts-list` VALUES ('53', 'fa', 'fa-bell-o', '&#xf0a2;');
INSERT INTO `fonts-list` VALUES ('54', 'fa', 'fa-bell-slash', '&#xf1f6;');
INSERT INTO `fonts-list` VALUES ('55', 'fa', 'fa-bell-slash-o', '&#xf1f7;');
INSERT INTO `fonts-list` VALUES ('56', 'fa', 'fa-bicycle', '&#xf206;');
INSERT INTO `fonts-list` VALUES ('57', 'fa', 'fa-binoculars', '&#xf1e5;');
INSERT INTO `fonts-list` VALUES ('58', 'fa', 'fa-birthday-cake', '&#xf1fd;');
INSERT INTO `fonts-list` VALUES ('59', 'fa', 'fa-bitbucket', '&#xf171;');
INSERT INTO `fonts-list` VALUES ('60', 'fa', 'fa-bitbucket-square', '&#xf172;');
INSERT INTO `fonts-list` VALUES ('61', 'fa', 'fa-bitcoin(alias)', '&#xf15a;');
INSERT INTO `fonts-list` VALUES ('62', 'fa', 'fa-bold', '&#xf032;');
INSERT INTO `fonts-list` VALUES ('63', 'fa', 'fa-bolt', '&#xf0e7;');
INSERT INTO `fonts-list` VALUES ('64', 'fa', 'fa-bomb', '&#xf1e2;');
INSERT INTO `fonts-list` VALUES ('65', 'fa', 'fa-book', '&#xf02d;');
INSERT INTO `fonts-list` VALUES ('66', 'fa', 'fa-bookmark', '&#xf02e;');
INSERT INTO `fonts-list` VALUES ('67', 'fa', 'fa-bookmark-o', '&#xf097;');
INSERT INTO `fonts-list` VALUES ('68', 'fa', 'fa-briefcase', '&#xf0b1;');
INSERT INTO `fonts-list` VALUES ('69', 'fa', 'fa-btc', '&#xf15a;');
INSERT INTO `fonts-list` VALUES ('70', 'fa', 'fa-bug', '&#xf188;');
INSERT INTO `fonts-list` VALUES ('71', 'fa', 'fa-building', '&#xf1ad;');
INSERT INTO `fonts-list` VALUES ('72', 'fa', 'fa-building-o', '&#xf0f7;');
INSERT INTO `fonts-list` VALUES ('73', 'fa', 'fa-bullhorn', '&#xf0a1;');
INSERT INTO `fonts-list` VALUES ('74', 'fa', 'fa-bullseye', '&#xf140;');
INSERT INTO `fonts-list` VALUES ('75', 'fa', 'fa-bus', '&#xf207;');
INSERT INTO `fonts-list` VALUES ('76', 'fa', 'fa-buysellads', null);
INSERT INTO `fonts-list` VALUES ('77', 'fa', 'fa-cab(alias)', null);
INSERT INTO `fonts-list` VALUES ('78', 'fa', 'fa-calculator', null);
INSERT INTO `fonts-list` VALUES ('79', 'fa', 'fa-calendar', null);
INSERT INTO `fonts-list` VALUES ('80', 'fa', 'fa-calendar-o', null);
INSERT INTO `fonts-list` VALUES ('81', 'fa', 'fa-camera', null);
INSERT INTO `fonts-list` VALUES ('82', 'fa', 'fa-camera-retro', null);
INSERT INTO `fonts-list` VALUES ('83', 'fa', 'fa-car', null);
INSERT INTO `fonts-list` VALUES ('84', 'fa', 'fa-caret-down', null);
INSERT INTO `fonts-list` VALUES ('85', 'fa', 'fa-caret-left', null);
INSERT INTO `fonts-list` VALUES ('86', 'fa', 'fa-caret-right', null);
INSERT INTO `fonts-list` VALUES ('87', 'fa', 'fa-caret-square-o-down', null);
INSERT INTO `fonts-list` VALUES ('88', 'fa', 'fa-caret-square-o-left', null);
INSERT INTO `fonts-list` VALUES ('89', 'fa', 'fa-caret-square-o-right', null);
INSERT INTO `fonts-list` VALUES ('90', 'fa', 'fa-caret-square-o-up', null);
INSERT INTO `fonts-list` VALUES ('91', 'fa', 'fa-caret-up', null);
INSERT INTO `fonts-list` VALUES ('92', 'fa', 'fa-cart-arrow-down', null);
INSERT INTO `fonts-list` VALUES ('93', 'fa', 'fa-cart-plus', null);
INSERT INTO `fonts-list` VALUES ('94', 'fa', 'fa-cc', null);
INSERT INTO `fonts-list` VALUES ('95', 'fa', 'fa-cc-amex', null);
INSERT INTO `fonts-list` VALUES ('96', 'fa', 'fa-cc-discover', null);
INSERT INTO `fonts-list` VALUES ('97', 'fa', 'fa-cc-mastercard', null);
INSERT INTO `fonts-list` VALUES ('98', 'fa', 'fa-cc-paypal', null);
INSERT INTO `fonts-list` VALUES ('99', 'fa', 'fa-cc-stripe', null);
INSERT INTO `fonts-list` VALUES ('100', 'fa', 'fa-cc-visa', null);
INSERT INTO `fonts-list` VALUES ('101', 'fa', 'fa-certificate', null);
INSERT INTO `fonts-list` VALUES ('102', 'fa', 'fa-chain(alias)', null);
INSERT INTO `fonts-list` VALUES ('103', 'fa', 'fa-chain-broken', null);
INSERT INTO `fonts-list` VALUES ('104', 'fa', 'fa-check', null);
INSERT INTO `fonts-list` VALUES ('105', 'fa', 'fa-check-circle', null);
INSERT INTO `fonts-list` VALUES ('106', 'fa', 'fa-check-circle-o', null);
INSERT INTO `fonts-list` VALUES ('107', 'fa', 'fa-check-square', null);
INSERT INTO `fonts-list` VALUES ('108', 'fa', 'fa-check-square-o', null);
INSERT INTO `fonts-list` VALUES ('109', 'fa', 'fa-chevron-circle-down', null);
INSERT INTO `fonts-list` VALUES ('110', 'fa', 'fa-chevron-circle-left', null);
INSERT INTO `fonts-list` VALUES ('111', 'fa', 'fa-chevron-circle-right', null);
INSERT INTO `fonts-list` VALUES ('112', 'fa', 'fa-chevron-circle-up', null);
INSERT INTO `fonts-list` VALUES ('113', 'fa', 'fa-chevron-down', null);
INSERT INTO `fonts-list` VALUES ('114', 'fa', 'fa-chevron-left', null);
INSERT INTO `fonts-list` VALUES ('115', 'fa', 'fa-chevron-right', null);
INSERT INTO `fonts-list` VALUES ('116', 'fa', 'fa-chevron-up', null);
INSERT INTO `fonts-list` VALUES ('117', 'fa', 'fa-child', null);
INSERT INTO `fonts-list` VALUES ('118', 'fa', 'fa-circle', null);
INSERT INTO `fonts-list` VALUES ('119', 'fa', 'fa-circle-o', null);
INSERT INTO `fonts-list` VALUES ('120', 'fa', 'fa-circle-o-notch', null);
INSERT INTO `fonts-list` VALUES ('121', 'fa', 'fa-circle-thin', null);
INSERT INTO `fonts-list` VALUES ('122', 'fa', 'fa-clipboard', null);
INSERT INTO `fonts-list` VALUES ('123', 'fa', 'fa-clock-o', null);
INSERT INTO `fonts-list` VALUES ('124', 'fa', 'fa-close(alias)', null);
INSERT INTO `fonts-list` VALUES ('125', 'fa', 'fa-cloud', null);
INSERT INTO `fonts-list` VALUES ('126', 'fa', 'fa-cloud-download', null);
INSERT INTO `fonts-list` VALUES ('127', 'fa', 'fa-cloud-upload', null);
INSERT INTO `fonts-list` VALUES ('128', 'fa', 'fa-cny(alias)', null);
INSERT INTO `fonts-list` VALUES ('129', 'fa', 'fa-code', null);
INSERT INTO `fonts-list` VALUES ('130', 'fa', 'fa-code-fork', null);
INSERT INTO `fonts-list` VALUES ('131', 'fa', 'fa-codepen', null);
INSERT INTO `fonts-list` VALUES ('132', 'fa', 'fa-coffee', null);
INSERT INTO `fonts-list` VALUES ('133', 'fa', 'fa-cog', null);
INSERT INTO `fonts-list` VALUES ('134', 'fa', 'fa-cogs', null);
INSERT INTO `fonts-list` VALUES ('135', 'fa', 'fa-columns', null);
INSERT INTO `fonts-list` VALUES ('136', 'fa', 'fa-comment', null);
INSERT INTO `fonts-list` VALUES ('137', 'fa', 'fa-comment-o', null);
INSERT INTO `fonts-list` VALUES ('138', 'fa', 'fa-comments', null);
INSERT INTO `fonts-list` VALUES ('139', 'fa', 'fa-comments-o', null);
INSERT INTO `fonts-list` VALUES ('140', 'fa', 'fa-compass', null);
INSERT INTO `fonts-list` VALUES ('141', 'fa', 'fa-compress', null);
INSERT INTO `fonts-list` VALUES ('142', 'fa', 'fa-connectdevelop', null);
INSERT INTO `fonts-list` VALUES ('143', 'fa', 'fa-copy(alias)', null);
INSERT INTO `fonts-list` VALUES ('144', 'fa', 'fa-copyright', null);
INSERT INTO `fonts-list` VALUES ('145', 'fa', 'fa-credit-card', null);
INSERT INTO `fonts-list` VALUES ('146', 'fa', 'fa-crop', null);
INSERT INTO `fonts-list` VALUES ('147', 'fa', 'fa-crosshairs', null);
INSERT INTO `fonts-list` VALUES ('148', 'fa', 'fa-css3', null);
INSERT INTO `fonts-list` VALUES ('149', 'fa', 'fa-cube', null);
INSERT INTO `fonts-list` VALUES ('150', 'fa', 'fa-cubes', null);
INSERT INTO `fonts-list` VALUES ('151', 'fa', 'fa-cut(alias)', null);
INSERT INTO `fonts-list` VALUES ('152', 'fa', 'fa-cutlery', null);
INSERT INTO `fonts-list` VALUES ('153', 'fa', 'fa-dashboard(alias)', null);
INSERT INTO `fonts-list` VALUES ('154', 'fa', 'fa-dashcube', null);
INSERT INTO `fonts-list` VALUES ('155', 'fa', 'fa-database', null);
INSERT INTO `fonts-list` VALUES ('156', 'fa', 'fa-dedent(alias)', null);
INSERT INTO `fonts-list` VALUES ('157', 'fa', 'fa-delicious', null);
INSERT INTO `fonts-list` VALUES ('158', 'fa', 'fa-desktop', null);
INSERT INTO `fonts-list` VALUES ('159', 'fa', 'fa-deviantart', null);
INSERT INTO `fonts-list` VALUES ('160', 'fa', 'fa-diamond', null);
INSERT INTO `fonts-list` VALUES ('161', 'fa', 'fa-digg', null);
INSERT INTO `fonts-list` VALUES ('162', 'fa', 'fa-dollar(alias)', null);
INSERT INTO `fonts-list` VALUES ('163', 'fa', 'fa-dot-circle-o', null);
INSERT INTO `fonts-list` VALUES ('164', 'fa', 'fa-download', null);
INSERT INTO `fonts-list` VALUES ('165', 'fa', 'fa-dribbble', null);
INSERT INTO `fonts-list` VALUES ('166', 'fa', 'fa-dropbox', null);
INSERT INTO `fonts-list` VALUES ('167', 'fa', 'fa-drupal', null);
INSERT INTO `fonts-list` VALUES ('168', 'fa', 'fa-edit(alias)', null);
INSERT INTO `fonts-list` VALUES ('169', 'fa', 'fa-eject', null);
INSERT INTO `fonts-list` VALUES ('170', 'fa', 'fa-ellipsis-h', null);
INSERT INTO `fonts-list` VALUES ('171', 'fa', 'fa-ellipsis-v', null);
INSERT INTO `fonts-list` VALUES ('172', 'fa', 'fa-empire', null);
INSERT INTO `fonts-list` VALUES ('173', 'fa', 'fa-envelope', null);
INSERT INTO `fonts-list` VALUES ('174', 'fa', 'fa-envelope-o', null);
INSERT INTO `fonts-list` VALUES ('175', 'fa', 'fa-envelope-square', null);
INSERT INTO `fonts-list` VALUES ('176', 'fa', 'fa-eraser', null);
INSERT INTO `fonts-list` VALUES ('177', 'fa', 'fa-eur', null);
INSERT INTO `fonts-list` VALUES ('178', 'fa', 'fa-euro(alias)', null);
INSERT INTO `fonts-list` VALUES ('179', 'fa', 'fa-exchange', null);
INSERT INTO `fonts-list` VALUES ('180', 'fa', 'fa-exclamation', null);
INSERT INTO `fonts-list` VALUES ('181', 'fa', 'fa-exclamation-circle', null);
INSERT INTO `fonts-list` VALUES ('182', 'fa', 'fa-exclamation-triangle', null);
INSERT INTO `fonts-list` VALUES ('183', 'fa', 'fa-expand', null);
INSERT INTO `fonts-list` VALUES ('184', 'fa', 'fa-external-link', null);
INSERT INTO `fonts-list` VALUES ('185', 'fa', 'fa-external-link-square', null);
INSERT INTO `fonts-list` VALUES ('186', 'fa', 'fa-eye', null);
INSERT INTO `fonts-list` VALUES ('187', 'fa', 'fa-eye-slash', null);
INSERT INTO `fonts-list` VALUES ('188', 'fa', 'fa-eyedropper', null);
INSERT INTO `fonts-list` VALUES ('189', 'fa', 'fa-facebook', null);
INSERT INTO `fonts-list` VALUES ('190', 'fa', 'fa-facebook-f(alias)', null);
INSERT INTO `fonts-list` VALUES ('191', 'fa', 'fa-facebook-official', null);
INSERT INTO `fonts-list` VALUES ('192', 'fa', 'fa-facebook-square', null);
INSERT INTO `fonts-list` VALUES ('193', 'fa', 'fa-fast-backward', null);
INSERT INTO `fonts-list` VALUES ('194', 'fa', 'fa-fast-forward', null);
INSERT INTO `fonts-list` VALUES ('195', 'fa', 'fa-fax', null);
INSERT INTO `fonts-list` VALUES ('196', 'fa', 'fa-female', null);
INSERT INTO `fonts-list` VALUES ('197', 'fa', 'fa-fighter-jet', null);
INSERT INTO `fonts-list` VALUES ('198', 'fa', 'fa-file', null);
INSERT INTO `fonts-list` VALUES ('199', 'fa', 'fa-file-archive-o', null);
INSERT INTO `fonts-list` VALUES ('200', 'fa', 'fa-file-audio-o', null);
INSERT INTO `fonts-list` VALUES ('201', 'fa', 'fa-file-code-o', null);
INSERT INTO `fonts-list` VALUES ('202', 'fa', 'fa-file-excel-o', null);
INSERT INTO `fonts-list` VALUES ('203', 'fa', 'fa-file-image-o', null);
INSERT INTO `fonts-list` VALUES ('204', 'fa', 'fa-file-movie-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('205', 'fa', 'fa-file-o', null);
INSERT INTO `fonts-list` VALUES ('206', 'fa', 'fa-file-pdf-o', null);
INSERT INTO `fonts-list` VALUES ('207', 'fa', 'fa-file-photo-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('208', 'fa', 'fa-file-picture-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('209', 'fa', 'fa-file-powerpoint-o', null);
INSERT INTO `fonts-list` VALUES ('210', 'fa', 'fa-file-sound-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('211', 'fa', 'fa-file-text', null);
INSERT INTO `fonts-list` VALUES ('212', 'fa', 'fa-file-text-o', null);
INSERT INTO `fonts-list` VALUES ('213', 'fa', 'fa-file-video-o', null);
INSERT INTO `fonts-list` VALUES ('214', 'fa', 'fa-file-word-o', null);
INSERT INTO `fonts-list` VALUES ('215', 'fa', 'fa-file-zip-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('216', 'fa', 'fa-files-o', null);
INSERT INTO `fonts-list` VALUES ('217', 'fa', 'fa-film', null);
INSERT INTO `fonts-list` VALUES ('218', 'fa', 'fa-filter', null);
INSERT INTO `fonts-list` VALUES ('219', 'fa', 'fa-fire', null);
INSERT INTO `fonts-list` VALUES ('220', 'fa', 'fa-fire-extinguisher', null);
INSERT INTO `fonts-list` VALUES ('221', 'fa', 'fa-flag', null);
INSERT INTO `fonts-list` VALUES ('222', 'fa', 'fa-flag-checkered', null);
INSERT INTO `fonts-list` VALUES ('223', 'fa', 'fa-flag-o', null);
INSERT INTO `fonts-list` VALUES ('224', 'fa', 'fa-flash(alias)', null);
INSERT INTO `fonts-list` VALUES ('225', 'fa', 'fa-flask', null);
INSERT INTO `fonts-list` VALUES ('226', 'fa', 'fa-flickr', null);
INSERT INTO `fonts-list` VALUES ('227', 'fa', 'fa-floppy-o', null);
INSERT INTO `fonts-list` VALUES ('228', 'fa', 'fa-folder', null);
INSERT INTO `fonts-list` VALUES ('229', 'fa', 'fa-folder-o', null);
INSERT INTO `fonts-list` VALUES ('230', 'fa', 'fa-folder-open', null);
INSERT INTO `fonts-list` VALUES ('231', 'fa', 'fa-folder-open-o', null);
INSERT INTO `fonts-list` VALUES ('232', 'fa', 'fa-font', null);
INSERT INTO `fonts-list` VALUES ('233', 'fa', 'fa-forumbee', null);
INSERT INTO `fonts-list` VALUES ('234', 'fa', 'fa-forward', null);
INSERT INTO `fonts-list` VALUES ('235', 'fa', 'fa-foursquare', null);
INSERT INTO `fonts-list` VALUES ('236', 'fa', 'fa-frown-o', null);
INSERT INTO `fonts-list` VALUES ('237', 'fa', 'fa-futbol-o', null);
INSERT INTO `fonts-list` VALUES ('238', 'fa', 'fa-gamepad', null);
INSERT INTO `fonts-list` VALUES ('239', 'fa', 'fa-gavel', null);
INSERT INTO `fonts-list` VALUES ('240', 'fa', 'fa-gbp', null);
INSERT INTO `fonts-list` VALUES ('241', 'fa', 'fa-ge(alias)', null);
INSERT INTO `fonts-list` VALUES ('242', 'fa', 'fa-gear(alias)', null);
INSERT INTO `fonts-list` VALUES ('243', 'fa', 'fa-gears(alias)', null);
INSERT INTO `fonts-list` VALUES ('244', 'fa', 'fa-genderless(alias)', null);
INSERT INTO `fonts-list` VALUES ('245', 'fa', 'fa-gift', null);
INSERT INTO `fonts-list` VALUES ('246', 'fa', 'fa-git', null);
INSERT INTO `fonts-list` VALUES ('247', 'fa', 'fa-git-square', null);
INSERT INTO `fonts-list` VALUES ('248', 'fa', 'fa-github', null);
INSERT INTO `fonts-list` VALUES ('249', 'fa', 'fa-github-alt', null);
INSERT INTO `fonts-list` VALUES ('250', 'fa', 'fa-github-square', null);
INSERT INTO `fonts-list` VALUES ('251', 'fa', 'fa-gittip(alias)', null);
INSERT INTO `fonts-list` VALUES ('252', 'fa', 'fa-glass', null);
INSERT INTO `fonts-list` VALUES ('253', 'fa', 'fa-globe', null);
INSERT INTO `fonts-list` VALUES ('254', 'fa', 'fa-google', null);
INSERT INTO `fonts-list` VALUES ('255', 'fa', 'fa-google-plus', null);
INSERT INTO `fonts-list` VALUES ('256', 'fa', 'fa-google-plus-square', null);
INSERT INTO `fonts-list` VALUES ('257', 'fa', 'fa-google-wallet', null);
INSERT INTO `fonts-list` VALUES ('258', 'fa', 'fa-graduation-cap', null);
INSERT INTO `fonts-list` VALUES ('259', 'fa', 'fa-gratipay', null);
INSERT INTO `fonts-list` VALUES ('260', 'fa', 'fa-group(alias)', null);
INSERT INTO `fonts-list` VALUES ('261', 'fa', 'fa-h-square', null);
INSERT INTO `fonts-list` VALUES ('262', 'fa', 'fa-hacker-news', null);
INSERT INTO `fonts-list` VALUES ('263', 'fa', 'fa-hand-o-down', null);
INSERT INTO `fonts-list` VALUES ('264', 'fa', 'fa-hand-o-left', null);
INSERT INTO `fonts-list` VALUES ('265', 'fa', 'fa-hand-o-right', null);
INSERT INTO `fonts-list` VALUES ('266', 'fa', 'fa-hand-o-up', null);
INSERT INTO `fonts-list` VALUES ('267', 'fa', 'fa-hdd-o', null);
INSERT INTO `fonts-list` VALUES ('268', 'fa', 'fa-header', null);
INSERT INTO `fonts-list` VALUES ('269', 'fa', 'fa-headphones', null);
INSERT INTO `fonts-list` VALUES ('270', 'fa', 'fa-heart', null);
INSERT INTO `fonts-list` VALUES ('271', 'fa', 'fa-heart-o', null);
INSERT INTO `fonts-list` VALUES ('272', 'fa', 'fa-heartbeat', null);
INSERT INTO `fonts-list` VALUES ('273', 'fa', 'fa-history', null);
INSERT INTO `fonts-list` VALUES ('274', 'fa', 'fa-home', null);
INSERT INTO `fonts-list` VALUES ('275', 'fa', 'fa-hospital-o', null);
INSERT INTO `fonts-list` VALUES ('276', 'fa', 'fa-hotel(alias)', null);
INSERT INTO `fonts-list` VALUES ('277', 'fa', 'fa-html5', null);
INSERT INTO `fonts-list` VALUES ('278', 'fa', 'fa-ils', null);
INSERT INTO `fonts-list` VALUES ('279', 'fa', 'fa-image(alias)', null);
INSERT INTO `fonts-list` VALUES ('280', 'fa', 'fa-inbox', null);
INSERT INTO `fonts-list` VALUES ('281', 'fa', 'fa-indent', null);
INSERT INTO `fonts-list` VALUES ('282', 'fa', 'fa-info', null);
INSERT INTO `fonts-list` VALUES ('283', 'fa', 'fa-info-circle', null);
INSERT INTO `fonts-list` VALUES ('284', 'fa', 'fa-inr', null);
INSERT INTO `fonts-list` VALUES ('285', 'fa', 'fa-instagram', null);
INSERT INTO `fonts-list` VALUES ('286', 'fa', 'fa-institution(alias)', null);
INSERT INTO `fonts-list` VALUES ('287', 'fa', 'fa-ioxhost', null);
INSERT INTO `fonts-list` VALUES ('288', 'fa', 'fa-italic', null);
INSERT INTO `fonts-list` VALUES ('289', 'fa', 'fa-joomla', null);
INSERT INTO `fonts-list` VALUES ('290', 'fa', 'fa-jpy', null);
INSERT INTO `fonts-list` VALUES ('291', 'fa', 'fa-jsfiddle', null);
INSERT INTO `fonts-list` VALUES ('292', 'fa', 'fa-key', null);
INSERT INTO `fonts-list` VALUES ('293', 'fa', 'fa-keyboard-o', null);
INSERT INTO `fonts-list` VALUES ('294', 'fa', 'fa-krw', null);
INSERT INTO `fonts-list` VALUES ('295', 'fa', 'fa-language', null);
INSERT INTO `fonts-list` VALUES ('296', 'fa', 'fa-laptop', null);
INSERT INTO `fonts-list` VALUES ('297', 'fa', 'fa-lastfm', null);
INSERT INTO `fonts-list` VALUES ('298', 'fa', 'fa-lastfm-square', null);
INSERT INTO `fonts-list` VALUES ('299', 'fa', 'fa-leaf', null);
INSERT INTO `fonts-list` VALUES ('300', 'fa', 'fa-leanpub', null);
INSERT INTO `fonts-list` VALUES ('301', 'fa', 'fa-legal(alias)', null);
INSERT INTO `fonts-list` VALUES ('302', 'fa', 'fa-lemon-o', null);
INSERT INTO `fonts-list` VALUES ('303', 'fa', 'fa-level-down', null);
INSERT INTO `fonts-list` VALUES ('304', 'fa', 'fa-level-up', null);
INSERT INTO `fonts-list` VALUES ('305', 'fa', 'fa-life-bouy(alias)', null);
INSERT INTO `fonts-list` VALUES ('306', 'fa', 'fa-life-buoy(alias)', null);
INSERT INTO `fonts-list` VALUES ('307', 'fa', 'fa-life-ring', null);
INSERT INTO `fonts-list` VALUES ('308', 'fa', 'fa-life-saver(alias)', null);
INSERT INTO `fonts-list` VALUES ('309', 'fa', 'fa-lightbulb-o', null);
INSERT INTO `fonts-list` VALUES ('310', 'fa', 'fa-line-chart', null);
INSERT INTO `fonts-list` VALUES ('311', 'fa', 'fa-link', null);
INSERT INTO `fonts-list` VALUES ('312', 'fa', 'fa-linkedin', null);
INSERT INTO `fonts-list` VALUES ('313', 'fa', 'fa-linkedin-square', null);
INSERT INTO `fonts-list` VALUES ('314', 'fa', 'fa-linux', null);
INSERT INTO `fonts-list` VALUES ('315', 'fa', 'fa-list', null);
INSERT INTO `fonts-list` VALUES ('316', 'fa', 'fa-list-alt', null);
INSERT INTO `fonts-list` VALUES ('317', 'fa', 'fa-list-ol', null);
INSERT INTO `fonts-list` VALUES ('318', 'fa', 'fa-list-ul', null);
INSERT INTO `fonts-list` VALUES ('319', 'fa', 'fa-location-arrow', null);
INSERT INTO `fonts-list` VALUES ('320', 'fa', 'fa-lock', null);
INSERT INTO `fonts-list` VALUES ('321', 'fa', 'fa-long-arrow-down', null);
INSERT INTO `fonts-list` VALUES ('322', 'fa', 'fa-long-arrow-left', null);
INSERT INTO `fonts-list` VALUES ('323', 'fa', 'fa-long-arrow-right', null);
INSERT INTO `fonts-list` VALUES ('324', 'fa', 'fa-long-arrow-up', null);
INSERT INTO `fonts-list` VALUES ('325', 'fa', 'fa-magic', null);
INSERT INTO `fonts-list` VALUES ('326', 'fa', 'fa-magnet', null);
INSERT INTO `fonts-list` VALUES ('327', 'fa', 'fa-mail-forward(alias)', null);
INSERT INTO `fonts-list` VALUES ('328', 'fa', 'fa-mail-reply(alias)', null);
INSERT INTO `fonts-list` VALUES ('329', 'fa', 'fa-mail-reply-all(alias)', null);
INSERT INTO `fonts-list` VALUES ('330', 'fa', 'fa-male', null);
INSERT INTO `fonts-list` VALUES ('331', 'fa', 'fa-map-marker', null);
INSERT INTO `fonts-list` VALUES ('332', 'fa', 'fa-mars', null);
INSERT INTO `fonts-list` VALUES ('333', 'fa', 'fa-mars-double', null);
INSERT INTO `fonts-list` VALUES ('334', 'fa', 'fa-mars-stroke', null);
INSERT INTO `fonts-list` VALUES ('335', 'fa', 'fa-mars-stroke-h', null);
INSERT INTO `fonts-list` VALUES ('336', 'fa', 'fa-mars-stroke-v', null);
INSERT INTO `fonts-list` VALUES ('337', 'fa', 'fa-maxcdn', null);
INSERT INTO `fonts-list` VALUES ('338', 'fa', 'fa-meanpath', null);
INSERT INTO `fonts-list` VALUES ('339', 'fa', 'fa-medium', null);
INSERT INTO `fonts-list` VALUES ('340', 'fa', 'fa-medkit', null);
INSERT INTO `fonts-list` VALUES ('341', 'fa', 'fa-meh-o', null);
INSERT INTO `fonts-list` VALUES ('342', 'fa', 'fa-mercury', null);
INSERT INTO `fonts-list` VALUES ('343', 'fa', 'fa-microphone', null);
INSERT INTO `fonts-list` VALUES ('344', 'fa', 'fa-microphone-slash', null);
INSERT INTO `fonts-list` VALUES ('345', 'fa', 'fa-minus', null);
INSERT INTO `fonts-list` VALUES ('346', 'fa', 'fa-minus-circle', null);
INSERT INTO `fonts-list` VALUES ('347', 'fa', 'fa-minus-square', null);
INSERT INTO `fonts-list` VALUES ('348', 'fa', 'fa-minus-square-o', null);
INSERT INTO `fonts-list` VALUES ('349', 'fa', 'fa-mobile', null);
INSERT INTO `fonts-list` VALUES ('350', 'fa', 'fa-mobile-phone(alias)', null);
INSERT INTO `fonts-list` VALUES ('351', 'fa', 'fa-money', null);
INSERT INTO `fonts-list` VALUES ('352', 'fa', 'fa-moon-o', null);
INSERT INTO `fonts-list` VALUES ('353', 'fa', 'fa-mortar-board(alias)', null);
INSERT INTO `fonts-list` VALUES ('354', 'fa', 'fa-motorcycle', null);
INSERT INTO `fonts-list` VALUES ('355', 'fa', 'fa-music', null);
INSERT INTO `fonts-list` VALUES ('356', 'fa', 'fa-navicon(alias)', null);
INSERT INTO `fonts-list` VALUES ('357', 'fa', 'fa-neuter', null);
INSERT INTO `fonts-list` VALUES ('358', 'fa', 'fa-newspaper-o', null);
INSERT INTO `fonts-list` VALUES ('359', 'fa', 'fa-openid', null);
INSERT INTO `fonts-list` VALUES ('360', 'fa', 'fa-outdent', null);
INSERT INTO `fonts-list` VALUES ('361', 'fa', 'fa-pagelines', null);
INSERT INTO `fonts-list` VALUES ('362', 'fa', 'fa-paint-brush', null);
INSERT INTO `fonts-list` VALUES ('363', 'fa', 'fa-paper-plane', null);
INSERT INTO `fonts-list` VALUES ('364', 'fa', 'fa-paper-plane-o', null);
INSERT INTO `fonts-list` VALUES ('365', 'fa', 'fa-paperclip', null);
INSERT INTO `fonts-list` VALUES ('366', 'fa', 'fa-paragraph', null);
INSERT INTO `fonts-list` VALUES ('367', 'fa', 'fa-paste(alias)', null);
INSERT INTO `fonts-list` VALUES ('368', 'fa', 'fa-pause', null);
INSERT INTO `fonts-list` VALUES ('369', 'fa', 'fa-paw', null);
INSERT INTO `fonts-list` VALUES ('370', 'fa', 'fa-paypal', null);
INSERT INTO `fonts-list` VALUES ('371', 'fa', 'fa-pencil', null);
INSERT INTO `fonts-list` VALUES ('372', 'fa', 'fa-pencil-square', null);
INSERT INTO `fonts-list` VALUES ('373', 'fa', 'fa-pencil-square-o', null);
INSERT INTO `fonts-list` VALUES ('374', 'fa', 'fa-phone', null);
INSERT INTO `fonts-list` VALUES ('375', 'fa', 'fa-phone-square', null);
INSERT INTO `fonts-list` VALUES ('376', 'fa', 'fa-photo(alias)', null);
INSERT INTO `fonts-list` VALUES ('377', 'fa', 'fa-picture-o', null);
INSERT INTO `fonts-list` VALUES ('378', 'fa', 'fa-pie-chart', null);
INSERT INTO `fonts-list` VALUES ('379', 'fa', 'fa-pied-piper', null);
INSERT INTO `fonts-list` VALUES ('380', 'fa', 'fa-pied-piper-alt', null);
INSERT INTO `fonts-list` VALUES ('381', 'fa', 'fa-pinterest', null);
INSERT INTO `fonts-list` VALUES ('382', 'fa', 'fa-pinterest-p', null);
INSERT INTO `fonts-list` VALUES ('383', 'fa', 'fa-pinterest-square', null);
INSERT INTO `fonts-list` VALUES ('384', 'fa', 'fa-plane', null);
INSERT INTO `fonts-list` VALUES ('385', 'fa', 'fa-play', null);
INSERT INTO `fonts-list` VALUES ('386', 'fa', 'fa-play-circle', null);
INSERT INTO `fonts-list` VALUES ('387', 'fa', 'fa-play-circle-o', null);
INSERT INTO `fonts-list` VALUES ('388', 'fa', 'fa-plug', null);
INSERT INTO `fonts-list` VALUES ('389', 'fa', 'fa-plus', null);
INSERT INTO `fonts-list` VALUES ('390', 'fa', 'fa-plus-circle', null);
INSERT INTO `fonts-list` VALUES ('391', 'fa', 'fa-plus-square', null);
INSERT INTO `fonts-list` VALUES ('392', 'fa', 'fa-plus-square-o', null);
INSERT INTO `fonts-list` VALUES ('393', 'fa', 'fa-power-off', null);
INSERT INTO `fonts-list` VALUES ('394', 'fa', 'fa-print', null);
INSERT INTO `fonts-list` VALUES ('395', 'fa', 'fa-puzzle-piece', null);
INSERT INTO `fonts-list` VALUES ('396', 'fa', 'fa-qq', null);
INSERT INTO `fonts-list` VALUES ('397', 'fa', 'fa-qrcode', null);
INSERT INTO `fonts-list` VALUES ('398', 'fa', 'fa-question', null);
INSERT INTO `fonts-list` VALUES ('399', 'fa', 'fa-question-circle', null);
INSERT INTO `fonts-list` VALUES ('400', 'fa', 'fa-quote-left', null);
INSERT INTO `fonts-list` VALUES ('401', 'fa', 'fa-quote-right', null);
INSERT INTO `fonts-list` VALUES ('402', 'fa', 'fa-ra(alias)', null);
INSERT INTO `fonts-list` VALUES ('403', 'fa', 'fa-random', null);
INSERT INTO `fonts-list` VALUES ('404', 'fa', 'fa-rebel', null);
INSERT INTO `fonts-list` VALUES ('405', 'fa', 'fa-recycle', null);
INSERT INTO `fonts-list` VALUES ('406', 'fa', 'fa-reddit', null);
INSERT INTO `fonts-list` VALUES ('407', 'fa', 'fa-reddit-square', null);
INSERT INTO `fonts-list` VALUES ('408', 'fa', 'fa-refresh', null);
INSERT INTO `fonts-list` VALUES ('409', 'fa', 'fa-remove(alias)', null);
INSERT INTO `fonts-list` VALUES ('410', 'fa', 'fa-renren', null);
INSERT INTO `fonts-list` VALUES ('411', 'fa', 'fa-reorder(alias)', null);
INSERT INTO `fonts-list` VALUES ('412', 'fa', 'fa-repeat', null);
INSERT INTO `fonts-list` VALUES ('413', 'fa', 'fa-reply', null);
INSERT INTO `fonts-list` VALUES ('414', 'fa', 'fa-reply-all', null);
INSERT INTO `fonts-list` VALUES ('415', 'fa', 'fa-retweet', null);
INSERT INTO `fonts-list` VALUES ('416', 'fa', 'fa-rmb(alias)', null);
INSERT INTO `fonts-list` VALUES ('417', 'fa', 'fa-road', null);
INSERT INTO `fonts-list` VALUES ('418', 'fa', 'fa-rocket', null);
INSERT INTO `fonts-list` VALUES ('419', 'fa', 'fa-rotate-left(alias)', null);
INSERT INTO `fonts-list` VALUES ('420', 'fa', 'fa-rotate-right(alias)', null);
INSERT INTO `fonts-list` VALUES ('421', 'fa', 'fa-rouble(alias)', null);
INSERT INTO `fonts-list` VALUES ('422', 'fa', 'fa-rss', null);
INSERT INTO `fonts-list` VALUES ('423', 'fa', 'fa-rss-square', null);
INSERT INTO `fonts-list` VALUES ('424', 'fa', 'fa-rub', null);
INSERT INTO `fonts-list` VALUES ('425', 'fa', 'fa-ruble(alias)', null);
INSERT INTO `fonts-list` VALUES ('426', 'fa', 'fa-rupee(alias)', null);
INSERT INTO `fonts-list` VALUES ('427', 'fa', 'fa-save(alias)', null);
INSERT INTO `fonts-list` VALUES ('428', 'fa', 'fa-scissors', null);
INSERT INTO `fonts-list` VALUES ('429', 'fa', 'fa-search', null);
INSERT INTO `fonts-list` VALUES ('430', 'fa', 'fa-search-minus', null);
INSERT INTO `fonts-list` VALUES ('431', 'fa', 'fa-search-plus', null);
INSERT INTO `fonts-list` VALUES ('432', 'fa', 'fa-sellsy', null);
INSERT INTO `fonts-list` VALUES ('433', 'fa', 'fa-send(alias)', null);
INSERT INTO `fonts-list` VALUES ('434', 'fa', 'fa-send-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('435', 'fa', 'fa-server', null);
INSERT INTO `fonts-list` VALUES ('436', 'fa', 'fa-share', null);
INSERT INTO `fonts-list` VALUES ('437', 'fa', 'fa-share-alt', null);
INSERT INTO `fonts-list` VALUES ('438', 'fa', 'fa-share-alt-square', null);
INSERT INTO `fonts-list` VALUES ('439', 'fa', 'fa-share-square', null);
INSERT INTO `fonts-list` VALUES ('440', 'fa', 'fa-share-square-o', null);
INSERT INTO `fonts-list` VALUES ('441', 'fa', 'fa-shekel(alias)', null);
INSERT INTO `fonts-list` VALUES ('442', 'fa', 'fa-sheqel(alias)', null);
INSERT INTO `fonts-list` VALUES ('443', 'fa', 'fa-shield', null);
INSERT INTO `fonts-list` VALUES ('444', 'fa', 'fa-ship', null);
INSERT INTO `fonts-list` VALUES ('445', 'fa', 'fa-shirtsinbulk', null);
INSERT INTO `fonts-list` VALUES ('446', 'fa', 'fa-shopping-cart', null);
INSERT INTO `fonts-list` VALUES ('447', 'fa', 'fa-sign-in', null);
INSERT INTO `fonts-list` VALUES ('448', 'fa', 'fa-sign-out', null);
INSERT INTO `fonts-list` VALUES ('449', 'fa', 'fa-signal', null);
INSERT INTO `fonts-list` VALUES ('450', 'fa', 'fa-simplybuilt', null);
INSERT INTO `fonts-list` VALUES ('451', 'fa', 'fa-sitemap', null);
INSERT INTO `fonts-list` VALUES ('452', 'fa', 'fa-skyatlas', null);
INSERT INTO `fonts-list` VALUES ('453', 'fa', 'fa-skype', null);
INSERT INTO `fonts-list` VALUES ('454', 'fa', 'fa-slack', null);
INSERT INTO `fonts-list` VALUES ('455', 'fa', 'fa-sliders', null);
INSERT INTO `fonts-list` VALUES ('456', 'fa', 'fa-slideshare', null);
INSERT INTO `fonts-list` VALUES ('457', 'fa', 'fa-smile-o', null);
INSERT INTO `fonts-list` VALUES ('458', 'fa', 'fa-soccer-ball-o(alias)', null);
INSERT INTO `fonts-list` VALUES ('459', 'fa', 'fa-sort', null);
INSERT INTO `fonts-list` VALUES ('460', 'fa', 'fa-sort-alpha-asc', null);
INSERT INTO `fonts-list` VALUES ('461', 'fa', 'fa-sort-alpha-desc', null);
INSERT INTO `fonts-list` VALUES ('462', 'fa', 'fa-sort-amount-asc', null);
INSERT INTO `fonts-list` VALUES ('463', 'fa', 'fa-sort-amount-desc', null);
INSERT INTO `fonts-list` VALUES ('464', 'fa', 'fa-sort-asc', null);
INSERT INTO `fonts-list` VALUES ('465', 'fa', 'fa-sort-desc', null);
INSERT INTO `fonts-list` VALUES ('466', 'fa', 'fa-sort-down(alias)', null);
INSERT INTO `fonts-list` VALUES ('467', 'fa', 'fa-sort-numeric-asc', null);
INSERT INTO `fonts-list` VALUES ('468', 'fa', 'fa-sort-numeric-desc', null);
INSERT INTO `fonts-list` VALUES ('469', 'fa', 'fa-sort-up(alias)', null);
INSERT INTO `fonts-list` VALUES ('470', 'fa', 'fa-soundcloud', null);
INSERT INTO `fonts-list` VALUES ('471', 'fa', 'fa-space-shuttle', null);
INSERT INTO `fonts-list` VALUES ('472', 'fa', 'fa-spinner', null);
INSERT INTO `fonts-list` VALUES ('473', 'fa', 'fa-spoon', null);
INSERT INTO `fonts-list` VALUES ('474', 'fa', 'fa-spotify', null);
INSERT INTO `fonts-list` VALUES ('475', 'fa', 'fa-square', null);
INSERT INTO `fonts-list` VALUES ('476', 'fa', 'fa-square-o', null);
INSERT INTO `fonts-list` VALUES ('477', 'fa', 'fa-stack-exchange', null);
INSERT INTO `fonts-list` VALUES ('478', 'fa', 'fa-stack-overflow', null);
INSERT INTO `fonts-list` VALUES ('479', 'fa', 'fa-star', null);
INSERT INTO `fonts-list` VALUES ('480', 'fa', 'fa-star-half', null);
INSERT INTO `fonts-list` VALUES ('481', 'fa', 'fa-star-half-empty(alias)', null);
INSERT INTO `fonts-list` VALUES ('482', 'fa', 'fa-star-half-full(alias)', null);
INSERT INTO `fonts-list` VALUES ('483', 'fa', 'fa-star-half-o', null);
INSERT INTO `fonts-list` VALUES ('484', 'fa', 'fa-star-o', null);
INSERT INTO `fonts-list` VALUES ('485', 'fa', 'fa-steam', null);
INSERT INTO `fonts-list` VALUES ('486', 'fa', 'fa-steam-square', null);
INSERT INTO `fonts-list` VALUES ('487', 'fa', 'fa-step-backward', null);
INSERT INTO `fonts-list` VALUES ('488', 'fa', 'fa-step-forward', null);
INSERT INTO `fonts-list` VALUES ('489', 'fa', 'fa-stethoscope', null);
INSERT INTO `fonts-list` VALUES ('490', 'fa', 'fa-stop', null);
INSERT INTO `fonts-list` VALUES ('491', 'fa', 'fa-street-view', null);
INSERT INTO `fonts-list` VALUES ('492', 'fa', 'fa-strikethrough', null);
INSERT INTO `fonts-list` VALUES ('493', 'fa', 'fa-stumbleupon', null);
INSERT INTO `fonts-list` VALUES ('494', 'fa', 'fa-stumbleupon-circle', null);
INSERT INTO `fonts-list` VALUES ('495', 'fa', 'fa-subscript', null);
INSERT INTO `fonts-list` VALUES ('496', 'fa', 'fa-subway', null);
INSERT INTO `fonts-list` VALUES ('497', 'fa', 'fa-suitcase', null);
INSERT INTO `fonts-list` VALUES ('498', 'fa', 'fa-sun-o', null);
INSERT INTO `fonts-list` VALUES ('499', 'fa', 'fa-superscript', null);
INSERT INTO `fonts-list` VALUES ('500', 'fa', 'fa-support(alias)', null);
INSERT INTO `fonts-list` VALUES ('501', 'fa', 'fa-table', null);
INSERT INTO `fonts-list` VALUES ('502', 'fa', 'fa-tablet', null);
INSERT INTO `fonts-list` VALUES ('503', 'fa', 'fa-tachometer', null);
INSERT INTO `fonts-list` VALUES ('504', 'fa', 'fa-tag', null);
INSERT INTO `fonts-list` VALUES ('505', 'fa', 'fa-tags', null);
INSERT INTO `fonts-list` VALUES ('506', 'fa', 'fa-tasks', null);
INSERT INTO `fonts-list` VALUES ('507', 'fa', 'fa-taxi', null);
INSERT INTO `fonts-list` VALUES ('508', 'fa', 'fa-tencent-weibo', null);
INSERT INTO `fonts-list` VALUES ('509', 'fa', 'fa-terminal', null);
INSERT INTO `fonts-list` VALUES ('510', 'fa', 'fa-text-height', null);
INSERT INTO `fonts-list` VALUES ('511', 'fa', 'fa-text-width', null);
INSERT INTO `fonts-list` VALUES ('512', 'fa', 'fa-th', null);
INSERT INTO `fonts-list` VALUES ('513', 'fa', 'fa-th-large', null);
INSERT INTO `fonts-list` VALUES ('514', 'fa', 'fa-th-list', null);
INSERT INTO `fonts-list` VALUES ('515', 'fa', 'fa-thumb-tack', null);
INSERT INTO `fonts-list` VALUES ('516', 'fa', 'fa-thumbs-down', null);
INSERT INTO `fonts-list` VALUES ('517', 'fa', 'fa-thumbs-o-down', null);
INSERT INTO `fonts-list` VALUES ('518', 'fa', 'fa-thumbs-o-up', null);
INSERT INTO `fonts-list` VALUES ('519', 'fa', 'fa-thumbs-up', null);
INSERT INTO `fonts-list` VALUES ('520', 'fa', 'fa-ticket', null);
INSERT INTO `fonts-list` VALUES ('521', 'fa', 'fa-times', null);
INSERT INTO `fonts-list` VALUES ('522', 'fa', 'fa-times-circle', null);
INSERT INTO `fonts-list` VALUES ('523', 'fa', 'fa-times-circle-o', null);
INSERT INTO `fonts-list` VALUES ('524', 'fa', 'fa-tint', null);
INSERT INTO `fonts-list` VALUES ('525', 'fa', 'fa-toggle-down(alias)', null);
INSERT INTO `fonts-list` VALUES ('526', 'fa', 'fa-toggle-left(alias)', null);
INSERT INTO `fonts-list` VALUES ('527', 'fa', 'fa-toggle-off', null);
INSERT INTO `fonts-list` VALUES ('528', 'fa', 'fa-toggle-on', null);
INSERT INTO `fonts-list` VALUES ('529', 'fa', 'fa-toggle-right(alias)', null);
INSERT INTO `fonts-list` VALUES ('530', 'fa', 'fa-toggle-up(alias)', null);
INSERT INTO `fonts-list` VALUES ('531', 'fa', 'fa-train', null);
INSERT INTO `fonts-list` VALUES ('532', 'fa', 'fa-transgender', null);
INSERT INTO `fonts-list` VALUES ('533', 'fa', 'fa-transgender-alt', null);
INSERT INTO `fonts-list` VALUES ('534', 'fa', 'fa-trash', null);
INSERT INTO `fonts-list` VALUES ('535', 'fa', 'fa-trash-o', null);
INSERT INTO `fonts-list` VALUES ('536', 'fa', 'fa-tree', null);
INSERT INTO `fonts-list` VALUES ('537', 'fa', 'fa-trello', null);
INSERT INTO `fonts-list` VALUES ('538', 'fa', 'fa-trophy', null);
INSERT INTO `fonts-list` VALUES ('539', 'fa', 'fa-truck', null);
INSERT INTO `fonts-list` VALUES ('540', 'fa', 'fa-try', null);
INSERT INTO `fonts-list` VALUES ('541', 'fa', 'fa-tty', null);
INSERT INTO `fonts-list` VALUES ('542', 'fa', 'fa-tumblr', null);
INSERT INTO `fonts-list` VALUES ('543', 'fa', 'fa-tumblr-square', null);
INSERT INTO `fonts-list` VALUES ('544', 'fa', 'fa-turkish-lira(alias)', null);
INSERT INTO `fonts-list` VALUES ('545', 'fa', 'fa-twitch', null);
INSERT INTO `fonts-list` VALUES ('546', 'fa', 'fa-twitter', null);
INSERT INTO `fonts-list` VALUES ('547', 'fa', 'fa-twitter-square', null);
INSERT INTO `fonts-list` VALUES ('548', 'fa', 'fa-umbrella', null);
INSERT INTO `fonts-list` VALUES ('549', 'fa', 'fa-underline', null);
INSERT INTO `fonts-list` VALUES ('550', 'fa', 'fa-undo', null);
INSERT INTO `fonts-list` VALUES ('551', 'fa', 'fa-university', null);
INSERT INTO `fonts-list` VALUES ('552', 'fa', 'fa-unlink(alias)', null);
INSERT INTO `fonts-list` VALUES ('553', 'fa', 'fa-unlock', null);
INSERT INTO `fonts-list` VALUES ('554', 'fa', 'fa-unlock-alt', null);
INSERT INTO `fonts-list` VALUES ('555', 'fa', 'fa-unsorted(alias)', null);
INSERT INTO `fonts-list` VALUES ('556', 'fa', 'fa-upload', null);
INSERT INTO `fonts-list` VALUES ('557', 'fa', 'fa-usd', null);
INSERT INTO `fonts-list` VALUES ('558', 'fa', 'fa-user', null);
INSERT INTO `fonts-list` VALUES ('559', 'fa', 'fa-user-md', null);
INSERT INTO `fonts-list` VALUES ('560', 'fa', 'fa-user-plus', null);
INSERT INTO `fonts-list` VALUES ('561', 'fa', 'fa-user-secret', null);
INSERT INTO `fonts-list` VALUES ('562', 'fa', 'fa-user-times', null);
INSERT INTO `fonts-list` VALUES ('563', 'fa', 'fa-users', null);
INSERT INTO `fonts-list` VALUES ('564', 'fa', 'fa-venus', null);
INSERT INTO `fonts-list` VALUES ('565', 'fa', 'fa-venus-double', null);
INSERT INTO `fonts-list` VALUES ('566', 'fa', 'fa-venus-mars', null);
INSERT INTO `fonts-list` VALUES ('567', 'fa', 'fa-viacoin', null);
INSERT INTO `fonts-list` VALUES ('568', 'fa', 'fa-video-camera', null);
INSERT INTO `fonts-list` VALUES ('569', 'fa', 'fa-vimeo-square', null);
INSERT INTO `fonts-list` VALUES ('570', 'fa', 'fa-vine', null);
INSERT INTO `fonts-list` VALUES ('571', 'fa', 'fa-vk', null);
INSERT INTO `fonts-list` VALUES ('572', 'fa', 'fa-volume-down', null);
INSERT INTO `fonts-list` VALUES ('573', 'fa', 'fa-volume-off', null);
INSERT INTO `fonts-list` VALUES ('574', 'fa', 'fa-volume-up', null);
INSERT INTO `fonts-list` VALUES ('575', 'fa', 'fa-warning(alias)', null);
INSERT INTO `fonts-list` VALUES ('576', 'fa', 'fa-wechat(alias)', null);
INSERT INTO `fonts-list` VALUES ('577', 'fa', 'fa-weibo', null);
INSERT INTO `fonts-list` VALUES ('578', 'fa', 'fa-weixin', null);
INSERT INTO `fonts-list` VALUES ('579', 'fa', 'fa-whatsapp', null);
INSERT INTO `fonts-list` VALUES ('580', 'fa', 'fa-wheelchair', null);
INSERT INTO `fonts-list` VALUES ('581', 'fa', 'fa-wifi', null);
INSERT INTO `fonts-list` VALUES ('582', 'fa', 'fa-windows', null);
INSERT INTO `fonts-list` VALUES ('583', 'fa', 'fa-won(alias)', null);
INSERT INTO `fonts-list` VALUES ('584', 'fa', 'fa-wordpress', null);
INSERT INTO `fonts-list` VALUES ('585', 'fa', 'fa-wrench', null);
INSERT INTO `fonts-list` VALUES ('586', 'fa', 'fa-xing', null);
INSERT INTO `fonts-list` VALUES ('587', 'fa', 'fa-xing-square', null);
INSERT INTO `fonts-list` VALUES ('588', 'fa', 'fa-yahoo', null);
INSERT INTO `fonts-list` VALUES ('589', 'fa', 'fa-yelp', null);
INSERT INTO `fonts-list` VALUES ('590', 'fa', 'fa-yen(alias)', null);
INSERT INTO `fonts-list` VALUES ('591', 'fa', 'fa-youtube', null);
INSERT INTO `fonts-list` VALUES ('592', 'fa', 'fa-youtube-play', null);
INSERT INTO `fonts-list` VALUES ('593', 'fa', 'fa-youtube-square', null);

-- ----------------------------
-- Table structure for lyrics
-- ----------------------------
DROP TABLE IF EXISTS `lyrics`;
CREATE TABLE `lyrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lyrics
-- ----------------------------
INSERT INTO `lyrics` VALUES ('1', 'Man Pathanawa (Samu Aran Ya Yuthui)', '<p>man pathanawa test</p>', '/core/storage/uploads/images/lyrics/lyrics-20180513105417.jpg', '2018-05-13 23:24:17', '2018-05-13 23:24:17');
INSERT INTO `lyrics` VALUES ('2', 'Dahata Nopeni Inna Lyrics', '<p>Song | Dahata Nopeni Inna<br />Artist | Prasanna Thakshila<br />Music | Rusiru Rupasinghe<br />Lyrics | Yasith Dulshan<br />Directed By | Ravindu Abeyrathna<br />Video | Golden Film House</p>', '/core/storage/uploads/images/lyrics/lyrics-20180601165135.jpg', '2018-06-02 05:21:35', '2018-06-30 05:42:47');
INSERT INTO `lyrics` VALUES ('3', 'Oyata Vitharak Lyrics', '<p><strong>Oyata Vitharak - Ishan Priyasanka</strong></p>', '/core/storage/uploads/images/lyrics/lyrics-20180602145824.jpg', '2018-06-03 03:28:25', '2018-06-03 03:28:25');
INSERT INTO `lyrics` VALUES ('4', 'Mathaka Mawee Lyrics', '<p><strong>Song Title: Mathaka Mewee</strong><br /><strong>Artist: Shenu Kalpa</strong><br /><strong>Music: Hashan Thilina</strong><br /><strong>Lyrics: Prasanna Kumara Dammalage</strong></p>', '/core/storage/uploads/images/lyrics/lyrics-20180606120048.jpg', '2018-06-07 00:30:48', '2018-06-30 05:42:30');
INSERT INTO `lyrics` VALUES ('5', 'Pem Karala Lyrics', '<p>Pem Karala - Udesh Nilanga<br />Music &amp; Melody | DilShan L Silva<br />Lyrics | Lasitha Jayaneththi Arachchige</p>', '/core/storage/uploads/images/lyrics/lyrics-20180606214438.jpg', '2018-06-07 10:14:39', '2018-06-30 05:42:16');
INSERT INTO `lyrics` VALUES ('6', 'Obe Adare Lyrics', '<p><em>Song Title: Obe Adare</em><br /><em>Artist: Sameera Chathuranga</em><br /><em>Music: Amila Muthugala</em><br /><em>Lyrics: Supun Ravishan</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180606214627.jpg', '2018-06-07 10:16:28', '2018-06-30 05:41:57');
INSERT INTO `lyrics` VALUES ('7', 'Wirasakawee Lyrics', '<p>Song Title | Wirasakawee<br />Artist | Buddika Krishan<br />Music | Hashan Thilina @ H AudioLab<br />Lyrics | Prasanna Kumara Dammalage<br />Mixed &amp; Masterd | Hashan Thilina @ H Audiolab</p>', '/core/storage/uploads/images/lyrics/lyrics-20180607210504.jpg', '2018-06-08 09:35:04', '2018-06-30 05:41:38');
INSERT INTO `lyrics` VALUES ('8', 'Sammatheta Pitu Pe Lyrics', '<p><em>Songs - Sammatheta Pitupe</em><br /><em>Artist - Jude Rogans</em><br /><em>Music - Chandana Walpola</em><br /><em>Lyrics - Reru</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180619194138.jpg', '2018-06-20 08:11:38', '2018-06-30 05:41:14');
INSERT INTO `lyrics` VALUES ('9', 'Seya (Nabara Wu) Lyrics', '<p><em>Artist : Roony (Indika Ruwan)</em><br /><em>Lyrics : Roshan Samarawickrama</em><br /><em>Music : Nimesh Kulasinghe</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180629170959.jpg', '2018-06-30 05:39:59', '2018-06-30 05:39:59');
INSERT INTO `lyrics` VALUES ('10', 'Unmada Rathriye Lyrics', '<p><em>Artist : Nimash Fernando</em><br /><em>Lyrics : Nisal Fernando</em><br /><em>Music : Ashan Fernando</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180630153159.jpg', '2018-07-01 04:01:59', '2018-07-01 04:06:37');
INSERT INTO `lyrics` VALUES ('11', 'Parana Hithuwakkari Lyrics', '<p><em>Artist : Manoj Dewarajage</em><br /><em>Music : Prageeth Perera</em><br /><em>Lyrics : Manoj Dewarajage</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180630164224.jpg', '2018-07-01 05:12:24', '2018-07-01 05:12:24');
INSERT INTO `lyrics` VALUES ('12', 'Sudu Manika Lyrics', '<p><em>Song : Sudu Manika</em><br /><em>Artist By : Nalinda Ranasinghe</em><br /><em>Lyrics By : Lasitha Jayaneththi Arachchige</em><br /><em>Music By : DilShan L Sillva</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180706162855.jpg', '2018-07-07 04:58:56', '2018-07-07 04:58:56');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `lft` int(11) NOT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Root Menu', '#', null, null, null, '1', '40', '0', '1', '2016-09-21 00:00:00', '2018-07-15 23:06:45');
INSERT INTO `menu` VALUES ('2', 'MENU MANAGEMENT', 'menu/list', null, '1', '[\"menu.add\",\"admin\"]', '2', '3', '1', '1', '2016-09-21 00:00:00', '2017-12-24 16:39:01');
INSERT INTO `menu` VALUES ('4', 'USER MANAGEMENT', '#', null, '1', '[\"user.add\",\"user.edit\",\"user.delete\",\"user.list\",\"admin\"]', '4', '13', '1', '1', '2016-09-21 10:56:25', '2018-04-21 09:19:10');
INSERT INTO `menu` VALUES ('5', 'PERMISSION', 'permission/list', null, '4', '[\"admin\"]', '5', '6', '2', '1', '2016-09-21 10:56:51', '2018-04-21 09:19:10');
INSERT INTO `menu` VALUES ('6', 'ROLE', 'user/role/list', null, '4', '[\"admin\"]', '7', '8', '2', '1', '2016-09-21 10:57:15', '2018-04-21 09:19:10');
INSERT INTO `menu` VALUES ('7', 'USER', 'user/list', null, '4', '[\"user.list\",\"user.add\",\"user.edit\",\"user.delete\",\"admin\"]', '9', '10', '2', '1', '2016-09-21 10:57:51', '2018-04-21 09:19:10');
INSERT INTO `menu` VALUES ('43', 'publication', 'publication/list', null, '1', '[\"publication.list\",\"publication.add\",\"publication.edt\",\"publication.delete\",\"admin\"]', '40', '41', '1', '1', null, '2018-07-15 23:06:45');
INSERT INTO `menu` VALUES ('52', 'Coupon Management', 'coupon/list', null, '1', '[\"admin\"]', '40', '41', '1', '1', '2017-10-17 22:29:22', '2018-07-15 23:06:45');
INSERT INTO `menu` VALUES ('57', 'Artists', 'admin/artist/list', null, '1', '[\"admin\"]', '20', '21', '1', '1', '2018-04-21 09:14:31', '2018-04-21 09:37:52');
INSERT INTO `menu` VALUES ('58', 'Audios', 'admin/audio/list', null, '1', '[\"admin\"]', '16', '17', '1', '1', '2018-04-21 09:15:31', '2018-04-21 09:37:19');
INSERT INTO `menu` VALUES ('59', 'Contact Us', 'admin/contact-us/list', null, '1', '[\"admin\"]', '36', '37', '1', '1', '2018-04-21 09:16:28', '2018-07-15 23:06:45');
INSERT INTO `menu` VALUES ('60', 'Dynamic Pages', 'admin/dynamic-pages/list', null, '1', '[\"admin\"]', '32', '33', '1', '1', '2018-04-21 09:18:39', '2018-07-15 23:06:45');
INSERT INTO `menu` VALUES ('61', 'Lyrics', 'admin/lyrics/list', null, '1', '[\"admin\"]', '22', '23', '1', '1', '2018-04-21 09:36:31', '2018-04-21 09:37:52');
INSERT INTO `menu` VALUES ('62', 'Songs', 'admin/song/list', null, '1', '[\"admin\"]', '14', '15', '1', '1', '2018-04-21 09:37:18', '2018-04-21 09:37:19');
INSERT INTO `menu` VALUES ('63', 'Videos', 'admin/video/list', null, '1', '[\"admin\"]', '18', '19', '1', '1', '2018-04-21 09:37:52', '2018-04-21 09:37:52');
INSERT INTO `menu` VALUES ('64', 'Image slider', 'admin/slider/list', null, '1', '[\"admin\"]', '24', '25', '1', '1', '2018-06-16 01:48:29', '2018-06-16 01:48:29');
INSERT INTO `menu` VALUES ('65', 'Banner Ads', '#', null, '1', '[\"admin\"]', '26', '31', '1', '1', '2018-07-15 23:03:45', '2018-07-15 23:06:45');
INSERT INTO `menu` VALUES ('66', 'Ads details', 'admin/ads/ads', null, '65', '[\"admin\"]', '27', '28', '2', '1', '2018-07-15 23:04:26', '2018-07-15 23:05:08');
INSERT INTO `menu` VALUES ('67', 'Ads Locations', 'admin/ads/locations', null, '65', '[\"admin\"]', '29', '30', '2', '1', '2018-07-15 23:06:45', '2018-07-15 23:06:45');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2018_02_25_122523_change_galley_image_table', '1');
INSERT INTO `migrations` VALUES ('2018_02_25_142445_change_blog_table', '2');
INSERT INTO `migrations` VALUES ('2018_02_28_213250_create_destination_images_table', '2');
INSERT INTO `migrations` VALUES ('2018_03_01_114809_create_destination_table', '2');
INSERT INTO `migrations` VALUES ('2018_03_01_141028_create_hotel_table', '3');
INSERT INTO `migrations` VALUES ('2018_03_01_141256_create_hotel_images_table', '3');
INSERT INTO `migrations` VALUES ('2018_03_03_095133_create_package_table', '4');
INSERT INTO `migrations` VALUES ('2018_03_03_095849_create_hotel_package_table', '5');
INSERT INTO `migrations` VALUES ('2018_03_03_100347_create_destination_package_table', '6');
INSERT INTO `migrations` VALUES ('2018_03_03_132245_add_offer_column_to_package_table', '7');
INSERT INTO `migrations` VALUES ('2018_03_03_145154_add_duration_column_to_package_table', '8');
INSERT INTO `migrations` VALUES ('2018_03_04_220824_create_table_reviews', '9');
INSERT INTO `migrations` VALUES ('2018_03_13_215110_add_contact_no_to_reviews_table', '10');
INSERT INTO `migrations` VALUES ('2018_03_13_221501_create_feedbacks_table', '11');
INSERT INTO `migrations` VALUES ('2018_03_13_221841_add_contcat_no_to_feedback_table', '12');
INSERT INTO `migrations` VALUES ('2018_04_03_201900_create_lyrics_table', '13');
INSERT INTO `migrations` VALUES ('2018_04_04_212315_create_table_artists', '14');
INSERT INTO `migrations` VALUES ('2018_04_08_215209_create_audio_table', '15');
INSERT INTO `migrations` VALUES ('2018_04_08_225237_create_video_table', '16');
INSERT INTO `migrations` VALUES ('2018_04_08_233441_create_song_table', '17');
INSERT INTO `migrations` VALUES ('2018_04_08_235040_create_table_artist_song', '18');
INSERT INTO `migrations` VALUES ('2018_04_08_124701_create_contact_us', '19');
INSERT INTO `migrations` VALUES ('2018_04_08_130439_create_dynamic_pages_table', '19');
INSERT INTO `migrations` VALUES ('2018_04_25_202527_add_artist_imge', '19');
INSERT INTO `migrations` VALUES ('2018_05_01_211410_create_song_lyrics_artist_table', '20');
INSERT INTO `migrations` VALUES ('2018_05_02_045001_create_advertisement_locations_table', '21');
INSERT INTO `migrations` VALUES ('2018_05_02_045224_create_advertisements_table', '22');
INSERT INTO `migrations` VALUES ('2018_05_02_211107_create_song_music_artist_table', '23');
INSERT INTO `migrations` VALUES ('2018_05_05_133028_add_old_column_to_song_table', '23');
INSERT INTO `migrations` VALUES ('2018_05_05_135013_add_bio_to_artists_table', '24');
INSERT INTO `migrations` VALUES ('2018_05_05_142237_add_image_to_videos_table', '25');
INSERT INTO `migrations` VALUES ('2018_05_07_125024_add_user_id_column_to_table', '26');
INSERT INTO `migrations` VALUES ('2018_05_07_143116_add_meta_to_song_table', '27');
INSERT INTO `migrations` VALUES ('2018_07_26_222528_add_tune_colums_to_audio_table', '28');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'user', 'Normal Registered User', '1', '1', '2015-07-25 12:30:00', '2015-12-02 17:24:39');
INSERT INTO `permissions` VALUES ('2', 'menu.add', null, '1', '1', '2015-07-25 12:30:00', '2015-12-03 20:32:41');
INSERT INTO `permissions` VALUES ('3', 'menu.list', null, '1', '1', '2015-07-25 12:30:00', '2015-12-02 17:24:54');
INSERT INTO `permissions` VALUES ('4', 'menu.edit', null, '1', '1', '2015-07-25 12:30:00', '2015-12-02 17:24:57');
INSERT INTO `permissions` VALUES ('5', 'menu.status', null, '1', '1', '2015-07-25 12:30:00', '2015-12-02 17:25:01');
INSERT INTO `permissions` VALUES ('6', 'admin', 'Super Admin Permission', '1', '1', '2015-07-25 12:30:00', '2015-07-25 12:30:00');
INSERT INTO `permissions` VALUES ('7', 'index', 'Home Page Permission', '1', '1', '2015-07-25 12:30:00', '2015-12-02 17:25:03');
INSERT INTO `permissions` VALUES ('8', 'menu.delete', null, '1', '1', '2015-09-07 03:00:06', '2015-09-07 03:00:09');
INSERT INTO `permissions` VALUES ('9', 'user.add', null, '1', '1', '2015-10-16 12:30:00', '2015-10-16 12:30:00');
INSERT INTO `permissions` VALUES ('10', 'user.edit', null, '1', '1', '2015-10-16 12:30:00', '2015-10-16 12:30:00');
INSERT INTO `permissions` VALUES ('11', 'user.delete', null, '1', '1', '2015-10-16 12:30:00', '2015-10-16 12:30:00');
INSERT INTO `permissions` VALUES ('12', 'user.list', null, '1', '1', '2015-10-20 12:30:00', '2015-10-21 08:31:57');
INSERT INTO `permissions` VALUES ('13', 'user.role.add', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('14', 'user.role.edit', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('15', 'user.role.list', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('16', 'user.role.delete', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('17', 'permission.add', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('18', 'permission.edit', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('19', 'permission.delete', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('20', 'permission.list', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('21', 'permission.group.add', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('22', 'permission.group.edit', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('23', 'permission.group.list', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('24', 'permission.group.delete', null, '1', '1', '2015-10-22 12:30:00', '2015-10-22 12:30:00');
INSERT INTO `permissions` VALUES ('25', 'user.status', null, '1', '1', '2015-12-19 12:30:47', '2015-12-19 12:30:47');
INSERT INTO `permissions` VALUES ('26', 'device.list', 'DEVICE LIST', '1', '1', '2017-01-24 07:14:34', '2017-03-13 11:23:58');
INSERT INTO `permissions` VALUES ('27', 'dashboard', 'DASHBOARD', '1', '1', '2017-01-24 07:18:39', '2017-01-24 07:18:39');
INSERT INTO `permissions` VALUES ('28', 'device.add', 'DEVICE ADD', '1', '1', '2017-01-24 07:19:46', '2017-01-24 07:19:46');
INSERT INTO `permissions` VALUES ('29', 'device.edit', 'DEVICE EDIT', '1', '1', '2017-01-24 07:20:02', '2017-01-24 07:20:02');
INSERT INTO `permissions` VALUES ('30', 'device.delete', 'DEVICE DELETE', '1', '1', '2017-01-24 07:20:24', '2017-01-24 07:20:24');
INSERT INTO `permissions` VALUES ('31', 'gallery.list', 'GALLERY LIST', '1', '1', '2017-01-24 07:21:05', '2017-01-24 07:21:05');
INSERT INTO `permissions` VALUES ('32', 'gallery.add', 'Gallery ADD', '1', '1', '2017-01-24 07:21:23', '2017-01-24 07:21:23');
INSERT INTO `permissions` VALUES ('33', 'gallery.edit', 'GALLERY EDIT', '1', '1', '2017-01-24 07:21:36', '2017-01-24 07:21:36');
INSERT INTO `permissions` VALUES ('34', 'gallery.delete', 'GALLERY DELETE', '1', '1', '2017-01-24 07:21:53', '2017-01-24 07:21:53');
INSERT INTO `permissions` VALUES ('35', 'reservation.list', 'RESERVATION LIST', '1', '1', '2017-01-28 23:34:27', '2017-01-28 23:34:27');
INSERT INTO `permissions` VALUES ('36', 'product.list', 'PRODUCT LIST', '1', '1', '2017-02-21 03:07:26', '2017-02-21 03:07:26');
INSERT INTO `permissions` VALUES ('37', 'product.add', 'PRODUCT ADD', '1', '1', '2017-02-21 03:07:49', '2017-02-21 03:07:49');
INSERT INTO `permissions` VALUES ('38', 'product.edit', 'PRODUCT EDIT', '1', '1', '2017-02-21 03:08:08', '2017-02-21 03:08:08');
INSERT INTO `permissions` VALUES ('39', 'product.delete', 'PRODUCT DELETE', '1', '1', '2017-03-15 09:24:22', '2017-03-15 09:24:22');
INSERT INTO `permissions` VALUES ('40', 'series.list', 'SERIES LIST', '1', '1', '2017-03-15 09:31:49', '2017-03-15 09:31:49');
INSERT INTO `permissions` VALUES ('41', 'series.add', 'SERIES ADD', '1', '1', '2017-03-15 09:32:03', '2017-03-15 09:32:03');
INSERT INTO `permissions` VALUES ('42', 'series.edit', 'SERIES EDIT', '1', '1', '2017-03-15 09:32:17', '2017-03-15 09:32:17');
INSERT INTO `permissions` VALUES ('43', 'series.delete', 'SERIES DELETE', '1', '1', '2017-03-15 09:32:33', '2017-03-15 09:32:33');
INSERT INTO `permissions` VALUES ('44', 'feature.add', 'ADD FEATURE', '1', '1', '2017-03-26 22:12:51', '2017-03-26 22:12:51');
INSERT INTO `permissions` VALUES ('45', 'feature.edit', 'EDIT FEATURE', '1', '1', '2017-03-26 22:13:08', '2017-03-26 22:13:08');
INSERT INTO `permissions` VALUES ('46', 'feature.list', 'LIST FEATURE', '1', '1', '2017-03-26 22:13:23', '2017-03-26 22:13:23');
INSERT INTO `permissions` VALUES ('47', 'feature.delete', 'DELETE FEATURE', '1', '1', '2017-03-26 22:13:41', '2017-03-26 22:13:41');
INSERT INTO `permissions` VALUES ('48', 'blog', 'BLOG', '1', '1', '2017-04-04 11:32:46', '2017-04-04 11:32:46');
INSERT INTO `permissions` VALUES ('49', 'blog.add', 'ADD BLOG', '1', '1', '2017-05-09 10:17:46', '2017-05-09 10:17:46');
INSERT INTO `permissions` VALUES ('50', 'blog.edit', 'EDIT BLOG', '1', '1', '2017-05-09 10:18:01', '2017-05-09 10:18:01');
INSERT INTO `permissions` VALUES ('51', 'blog.list', 'LIST BLOG', '1', '1', '2017-05-09 10:18:13', '2017-05-09 10:18:13');
INSERT INTO `permissions` VALUES ('52', 'blog.delete', 'DELETE BLOG', '1', '1', '2017-05-09 10:18:28', '2017-05-09 10:18:28');
INSERT INTO `permissions` VALUES ('53', 'destination.list', '', '1', '1', '2018-02-26 04:46:36', '2018-02-26 04:46:36');
INSERT INTO `permissions` VALUES ('54', 'destination.add', '', '1', '1', '2018-02-26 04:46:43', '2018-02-26 04:46:43');
INSERT INTO `permissions` VALUES ('55', 'destination.edit', '', '1', '1', '2018-02-26 04:47:04', '2018-02-26 04:47:04');
INSERT INTO `permissions` VALUES ('56', 'destination.delete', '', '1', '1', '2018-02-26 04:47:12', '2018-02-26 04:47:12');
INSERT INTO `permissions` VALUES ('57', 'hotel.add', '', '1', '1', '2018-03-02 03:15:53', '2018-03-02 03:15:53');
INSERT INTO `permissions` VALUES ('58', 'hotel.edit', '', '1', '1', '2018-03-02 03:16:01', '2018-03-02 03:16:01');
INSERT INTO `permissions` VALUES ('59', 'hotel.list', '', '1', '1', '2018-03-02 03:16:50', '2018-03-02 03:16:50');

-- ----------------------------
-- Table structure for persistences
-- ----------------------------
DROP TABLE IF EXISTS `persistences`;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=774 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of persistences
-- ----------------------------
INSERT INTO `persistences` VALUES ('1', '1', '0q0ygX6vhRbhWLfDVMQjCpuVVi4Uv21m', '2015-07-12 00:09:55', '2015-07-12 00:09:55');
INSERT INTO `persistences` VALUES ('2', '1', 'efBjwoN42yjE5Pbbbn3NOvAQMh6Hc47p', '2015-07-13 22:42:45', '2015-07-13 22:42:45');
INSERT INTO `persistences` VALUES ('4', '1', 'dkYUwD816i7YeZLaLmENn7b7qXyRV6jE', '2015-07-14 03:43:07', '2015-07-14 03:43:07');
INSERT INTO `persistences` VALUES ('5', '1', 'fsVkzYpy5e5SIno5317Viix318Ipevum', '2015-07-15 03:12:12', '2015-07-15 03:12:12');
INSERT INTO `persistences` VALUES ('7', '1', 'BYdFxgUBhE9H2BqP6PEg5tQXjQvapGkk', '2015-07-15 07:23:52', '2015-07-15 07:23:52');
INSERT INTO `persistences` VALUES ('8', '1', 'UuheCz7Zmb1WM2zsJVnJv3yMtHrCZXZP', '2015-07-19 08:05:42', '2015-07-19 08:05:42');
INSERT INTO `persistences` VALUES ('9', '1', 'ugwMinnLII8pAvdX48uibN4tCfFncKxL', '2015-07-26 01:14:39', '2015-07-26 01:14:39');
INSERT INTO `persistences` VALUES ('10', '1', '6sv5Vmes7x5zzF5kp9BIiF0e3J7uatEA', '2015-07-26 10:02:42', '2015-07-26 10:02:42');
INSERT INTO `persistences` VALUES ('12', '1', 'AAPH0gv7ueGmg1GHT3CDDb8CuqliETTr', '2015-07-27 21:30:05', '2015-07-27 21:30:05');
INSERT INTO `persistences` VALUES ('17', '1', 'EvOMH9hGBkW3nS9TJjtnZOajBh8b2nH8', '2015-08-14 09:29:00', '2015-08-14 09:29:00');
INSERT INTO `persistences` VALUES ('18', '1', 'sgY1xcvVRC3q8s2Qe5IgY6k4NVdw8Bw2', '2015-08-14 09:30:50', '2015-08-14 09:30:50');
INSERT INTO `persistences` VALUES ('19', '1', 'smRBCmpzbBL4RTXJZcaAW5Wr4quWzvnA', '2015-08-14 22:38:36', '2015-08-14 22:38:36');
INSERT INTO `persistences` VALUES ('20', '1', 'C1xwzvvNREHhtx6JqQEZSTEPMbzY7J6F', '2015-08-14 22:43:16', '2015-08-14 22:43:16');
INSERT INTO `persistences` VALUES ('23', '1', '0XUcBHFDSxUgWDZsdPt9Oigx5cIfQ5KR', '2015-08-15 04:19:04', '2015-08-15 04:19:04');
INSERT INTO `persistences` VALUES ('24', '1', 'LeqsntX9HwB19oOzfqFICR3TB2xHZt5c', '2015-08-15 09:46:49', '2015-08-15 09:46:49');
INSERT INTO `persistences` VALUES ('25', '1', 'ZRilkZu1axOqoPqkAbNzA4ZPQccDHLEf', '2015-08-16 02:49:49', '2015-08-16 02:49:49');
INSERT INTO `persistences` VALUES ('26', '1', 'fHHSkcipQkFfpeTYIcxmFqKS2IKL0ozJ', '2015-08-17 22:11:53', '2015-08-17 22:11:53');
INSERT INTO `persistences` VALUES ('27', '1', 'Gev3KH0ERZX7UFA6QASlHd4wW9Tm47cz', '2015-08-18 01:52:08', '2015-08-18 01:52:08');
INSERT INTO `persistences` VALUES ('28', '1', 'Pu0bSNJwO7XEhfmr8Ubg28c5HWp2BCfN', '2015-08-18 21:31:21', '2015-08-18 21:31:21');
INSERT INTO `persistences` VALUES ('29', '1', 'FkavnjEGjcidC4hGsJwf5JepGpglq09u', '2015-08-18 23:33:49', '2015-08-18 23:33:49');
INSERT INTO `persistences` VALUES ('30', '1', 'GfVyVFbRB7yBaekFtBu3l16pB95iDYyv', '2015-08-19 06:41:28', '2015-08-19 06:41:28');
INSERT INTO `persistences` VALUES ('32', '1', '7Qa9ckHVYsRkTgnZNSQ9szqJ7ORRwqqP', '2015-08-20 08:13:25', '2015-08-20 08:13:25');
INSERT INTO `persistences` VALUES ('34', '1', '7gzkQXDRziKCU3V6YN3os6oty3ZYmm3V', '2015-08-20 23:10:32', '2015-08-20 23:10:32');
INSERT INTO `persistences` VALUES ('35', '1', 'PPni5OZ4t4Ukf7GqJhJTowQUbG1BKTCO', '2015-08-25 05:32:19', '2015-08-25 05:32:19');
INSERT INTO `persistences` VALUES ('39', '1', '1Vq5gtTDFk3uhKn9AHMOnWwQxiNq8eNm', '2015-08-25 09:02:07', '2015-08-25 09:02:07');
INSERT INTO `persistences` VALUES ('40', '1', '9tal1AP1HQ4usfTnI3Q0kXSrPzScKtnz', '2015-08-26 01:26:55', '2015-08-26 01:26:55');
INSERT INTO `persistences` VALUES ('41', '1', 'FDkLOdsEvF0HzB1XvBQ8GKAQZ9oq80xS', '2015-08-26 07:20:00', '2015-08-26 07:20:00');
INSERT INTO `persistences` VALUES ('42', '1', 'LW0maecSnWGHR1uvR1ev67CmiePNeYQb', '2015-08-26 21:13:11', '2015-08-26 21:13:11');
INSERT INTO `persistences` VALUES ('43', '1', 'c7UCeZhd2AdNOrNIrzFeP8Z2xHSPBOat', '2015-08-27 01:51:39', '2015-08-27 01:51:39');
INSERT INTO `persistences` VALUES ('44', '1', 'DmiuJhBD8ndKrxeQzYUgKJ0qdr5VfgEp', '2015-08-27 01:53:05', '2015-08-27 01:53:05');
INSERT INTO `persistences` VALUES ('45', '1', 'EvLSeVbZ4LpQHpIwn3ESkPmKiHZYqChR', '2015-08-27 08:13:08', '2015-08-27 08:13:08');
INSERT INTO `persistences` VALUES ('46', '1', 'ZaFHszTHVJxdXVHyYyDUiVrjDsVLF2os', '2015-08-27 21:20:53', '2015-08-27 21:20:53');
INSERT INTO `persistences` VALUES ('47', '1', 'E26Ajf3IuoSAQBDRGUnJxBXuLWf45qTE', '2015-08-28 23:30:55', '2015-08-28 23:30:55');
INSERT INTO `persistences` VALUES ('48', '1', '6gvIBqAaNqIHUC1GXGnvpobgXDqyGFWV', '2015-08-29 02:52:35', '2015-08-29 02:52:35');
INSERT INTO `persistences` VALUES ('49', '1', 'C9iOcAwSX8vcFZVUkRrnb7BTEmFfb1lW', '2015-08-29 05:24:30', '2015-08-29 05:24:30');
INSERT INTO `persistences` VALUES ('50', '1', 'm8HlkBppqVOmVtvm8WXn1gflqnpxZ7ZX', '2015-08-29 09:43:47', '2015-08-29 09:43:47');
INSERT INTO `persistences` VALUES ('51', '1', 'Ga2L52MViQQHOjQmC0B3NIwKoFoeC5LU', '2015-08-30 00:56:46', '2015-08-30 00:56:46');
INSERT INTO `persistences` VALUES ('52', '1', 'pEnesSMPpp8XwoXBGc0AQIxG8a0NlJL7', '2015-08-30 04:11:51', '2015-08-30 04:11:51');
INSERT INTO `persistences` VALUES ('53', '1', 'DOyCW9LubRxZlUZI1VM9VWmwEHFqEtuy', '2015-08-31 02:15:08', '2015-08-31 02:15:08');
INSERT INTO `persistences` VALUES ('54', '1', 'hQnXdF66aT6GWv1bjMfdQlrFzXCKzPaD', '2015-09-02 06:43:49', '2015-09-02 06:43:49');
INSERT INTO `persistences` VALUES ('55', '1', 'SofbScd28GADuBXJkBe02t4wNesj1pyc', '2015-09-02 21:46:21', '2015-09-02 21:46:21');
INSERT INTO `persistences` VALUES ('58', '1', 'S9zBLvLOtfn86jn2YZ7o4cDDIXir5odO', '2015-09-07 05:03:49', '2015-09-07 05:03:49');
INSERT INTO `persistences` VALUES ('59', '1', 'jhnFLwdIIq3akmuFjwNT5hXRbskFN8Cv', '2015-09-07 21:30:41', '2015-09-07 21:30:41');
INSERT INTO `persistences` VALUES ('65', '1', 'Yzp3Ic2rJbT21gHfSHQAyNH9sySMzNx8', '2015-09-18 01:08:02', '2015-09-18 01:08:02');
INSERT INTO `persistences` VALUES ('66', '1', 'FPxtt7aeMIpDuoEEIrFeXfS289y6YyrI', '2015-09-18 01:10:28', '2015-09-18 01:10:28');
INSERT INTO `persistences` VALUES ('67', '1', 'DE0xeUB6zv2l9VC7hlbHeVKZdNVgba2k', '2015-09-18 01:12:20', '2015-09-18 01:12:20');
INSERT INTO `persistences` VALUES ('68', '1', 'VSigkljclaPzU2jbUTnEPb7uAqbq7OO4', '2015-09-18 01:16:25', '2015-09-18 01:16:25');
INSERT INTO `persistences` VALUES ('69', '1', 'u1u64fxr5E8cVetPE2xfwXXlZg8ETdMt', '2015-09-18 01:18:29', '2015-09-18 01:18:29');
INSERT INTO `persistences` VALUES ('70', '1', 'IsPKSyDwN4yyBDpHDCQTFIA9cb9tDvho', '2015-09-18 01:20:56', '2015-09-18 01:20:56');
INSERT INTO `persistences` VALUES ('71', '1', 'Mqwwg7Ji564GosYUs8ew6p56lVqTNggm', '2015-09-18 01:21:06', '2015-09-18 01:21:06');
INSERT INTO `persistences` VALUES ('72', '1', 'ReDZfYlXcI3vckn50feKIYFukwEIaJxW', '2015-09-18 01:25:47', '2015-09-18 01:25:47');
INSERT INTO `persistences` VALUES ('73', '1', '1zPiFaCd2KentN0BEVayuoIqOfxi3lMS', '2015-09-18 01:26:15', '2015-09-18 01:26:15');
INSERT INTO `persistences` VALUES ('75', '1', 'FiS9P7LCFRk2i3wnNHZJqODlZaBMPBsH', '2015-09-18 01:27:16', '2015-09-18 01:27:16');
INSERT INTO `persistences` VALUES ('77', '1', 'w6afABnpTAwYkETiZ1LpLEG31RUMR9J9', '2015-09-18 01:44:14', '2015-09-18 01:44:14');
INSERT INTO `persistences` VALUES ('80', '1', 'zEQJ08oNoKnzyenYvSBPlGNkYrQRb9iU', '2015-09-18 03:38:48', '2015-09-18 03:38:48');
INSERT INTO `persistences` VALUES ('81', '1', 'ngBYUNkxL2QkLY5Lj7EHCsAY8VRUPIss', '2015-09-18 04:37:24', '2015-09-18 04:37:24');
INSERT INTO `persistences` VALUES ('82', '1', 'qPnddeucFgMvXEyCDsm99LGWgtiKQJI0', '2015-09-18 05:07:58', '2015-09-18 05:07:58');
INSERT INTO `persistences` VALUES ('83', '1', 'y8KppplOMITXfiYtuU10r96MmBw2fn4Z', '2015-09-18 05:24:17', '2015-09-18 05:24:17');
INSERT INTO `persistences` VALUES ('87', '1', '1O0lJH2OdlZ5JYPb7OzU95hN9DD9D08f', '2015-09-18 23:47:44', '2015-09-18 23:47:44');
INSERT INTO `persistences` VALUES ('91', '1', 'F2EASmRV02xRfOorilcer2HPJR0ff10w', '2015-09-21 21:23:43', '2015-09-21 21:23:43');
INSERT INTO `persistences` VALUES ('93', '1', 'XtZHW8aWJtnVDHLcFpJjfzuFQCI4BarH', '2015-09-21 21:31:03', '2015-09-21 21:31:03');
INSERT INTO `persistences` VALUES ('94', '1', '4MvhjQuFQMa3d3kTpaMJIhrHIqrpd0Ud', '2015-09-21 21:34:16', '2015-09-21 21:34:16');
INSERT INTO `persistences` VALUES ('95', '1', 'wIeSvojnWxVdqc8Ko2g4wSlgadyT46HX', '2015-09-22 07:16:41', '2015-09-22 07:16:41');
INSERT INTO `persistences` VALUES ('96', '1', 'IfNOgaDeoIHRgr1wMMQvzGO2zyedyHzQ', '2015-10-16 22:25:25', '2015-10-16 22:25:25');
INSERT INTO `persistences` VALUES ('97', '1', 'xb49Au965edKpfxust8QyV2A1ZtN1Jir', '2015-10-17 01:59:38', '2015-10-17 01:59:38');
INSERT INTO `persistences` VALUES ('98', '1', '8KKqFXos72faML2g6w1gZK8GecSR2A7B', '2015-10-17 02:25:28', '2015-10-17 02:25:28');
INSERT INTO `persistences` VALUES ('99', '1', 'IrxeYFpjWjhZdazrZz7tp253etqZlkuh', '2015-10-17 04:39:25', '2015-10-17 04:39:25');
INSERT INTO `persistences` VALUES ('100', '1', 'iuuX2Q6nvfyKfiNLDjOHJ0Jnmd3ZHLEM', '2015-10-18 00:28:06', '2015-10-18 00:28:06');
INSERT INTO `persistences` VALUES ('102', '1', 'vQW13aw7kJoovALydds8W9175IqZq7sh', '2015-10-21 02:45:41', '2015-10-21 02:45:41');
INSERT INTO `persistences` VALUES ('103', '1', 'kznRgyUivNAxj1LolOUloaV52kxc1TOg', '2015-10-21 08:19:12', '2015-10-21 08:19:12');
INSERT INTO `persistences` VALUES ('104', '1', 'UMqlxsaazj72vwRmCVfv5qB5oujn5eO1', '2015-10-21 08:20:38', '2015-10-21 08:20:38');
INSERT INTO `persistences` VALUES ('105', '1', 'xpjJT8wJEAA4xe0WNP9UbGPlEbwgZi7L', '2015-10-21 21:28:35', '2015-10-21 21:28:35');
INSERT INTO `persistences` VALUES ('106', '1', 'PMf4A3yNtiDOJzYEA2Pi58STBrJpYzi9', '2015-10-21 21:36:54', '2015-10-21 21:36:54');
INSERT INTO `persistences` VALUES ('107', '1', 'ix4iaApoo9ohGbwkBNcYyHv3nSkYMuOJ', '2015-10-22 08:51:23', '2015-10-22 08:51:23');
INSERT INTO `persistences` VALUES ('108', '1', 'klm8QxxO7mIG6Ljj5liFWR1qikUv3y1H', '2015-10-22 20:47:41', '2015-10-22 20:47:41');
INSERT INTO `persistences` VALUES ('109', '1', 'TMTs9AXiVGg3Dn8AmaXt5EZS4Z4WeVgp', '2015-10-26 21:48:24', '2015-10-26 21:48:24');
INSERT INTO `persistences` VALUES ('110', '1', 'gihggqvhu0ngRD3ZMkeBhMLMNW6uKXHJ', '2015-10-28 22:39:40', '2015-10-28 22:39:40');
INSERT INTO `persistences` VALUES ('111', '1', 'KDNzGA42AWdXV63AG90IhJF6Q36pmtpE', '2015-10-29 07:38:36', '2015-10-29 07:38:36');
INSERT INTO `persistences` VALUES ('112', '1', '35yTDDYO1IYhs9ujWXGWfAS9cxXIs4Hj', '2015-11-04 21:13:06', '2015-11-04 21:13:06');
INSERT INTO `persistences` VALUES ('113', '1', 'FE2s4HBsv9RoBO7oUsUT7KxFTaXqdpUs', '2015-11-07 03:06:44', '2015-11-07 03:06:44');
INSERT INTO `persistences` VALUES ('115', '1', 'a35iqeHc7BvGEbgIx9t357tFXklT9bqO', '2015-11-07 04:38:06', '2015-11-07 04:38:06');
INSERT INTO `persistences` VALUES ('116', '1', 'eE6SkzCzg1FtCi2E24UNmFm7OBVLWORs', '2015-11-08 05:02:42', '2015-11-08 05:02:42');
INSERT INTO `persistences` VALUES ('117', '1', 't71JCTd8yKBblCePz7bWznuSpkU2XqDv', '2015-11-08 05:27:44', '2015-11-08 05:27:44');
INSERT INTO `persistences` VALUES ('119', '1', 'ECMvGKmUgYm2qtMN0OMrPqpaDn5jx08d', '2015-11-19 11:33:17', '2015-11-19 11:33:17');
INSERT INTO `persistences` VALUES ('120', '1', 'PtyWimBS6rsnKlRxj96BDq9L7SDKG2rk', '2015-11-19 12:13:11', '2015-11-19 12:13:11');
INSERT INTO `persistences` VALUES ('121', '1', 'l9FIuW4AIkJij7BhzeiSEfzU4VyTWHMv', '2015-11-19 12:56:41', '2015-11-19 12:56:41');
INSERT INTO `persistences` VALUES ('122', '1', 'bFb85D3SNEMRAoWExKagngxrR9mpWG9a', '2015-11-19 15:26:32', '2015-11-19 15:26:32');
INSERT INTO `persistences` VALUES ('123', '1', 'SXL5MRTMSjhUxM2hHtJlZTUCbqgFF5dp', '2015-11-19 15:36:15', '2015-11-19 15:36:15');
INSERT INTO `persistences` VALUES ('124', '1', 'xe3efdBNBGu5Ftyzf6SBg1IpUjzMiODv', '2015-11-19 17:47:47', '2015-11-19 17:47:47');
INSERT INTO `persistences` VALUES ('125', '1', 'H0KI9TFvutyqksX9WFdo7ltXlIdi6LBO', '2015-11-20 00:31:04', '2015-11-20 00:31:04');
INSERT INTO `persistences` VALUES ('126', '1', 'b5U0MlNhXnSybtPBfB6OOmx2UD0jp3rK', '2015-11-20 11:26:33', '2015-11-20 11:26:33');
INSERT INTO `persistences` VALUES ('127', '1', 'aY5cfsCbK3SIOhyIfqn5swgMpi7DdOaH', '2015-11-20 12:18:16', '2015-11-20 12:18:16');
INSERT INTO `persistences` VALUES ('128', '1', 'LFMf9yFQYkV1iEc4MPSmy2cPyAlIBI7e', '2015-11-20 19:26:45', '2015-11-20 19:26:45');
INSERT INTO `persistences` VALUES ('129', '1', 'RzLW8COzh7iRkoCA0sY3VAejSmQbAFGS', '2015-11-21 11:13:07', '2015-11-21 11:13:07');
INSERT INTO `persistences` VALUES ('130', '1', 'QyjXCs2H1KLaiSveWaBenK05uG6Z0S8D', '2015-11-21 15:58:30', '2015-11-21 15:58:30');
INSERT INTO `persistences` VALUES ('131', '1', '9wrIGqqTTScbdXWxrxsu4mPGI30xTTJn', '2015-11-21 17:52:41', '2015-11-21 17:52:41');
INSERT INTO `persistences` VALUES ('132', '1', 'bBc9L5jexsFXL5iGZV8sIlgiyiEpvCo1', '2015-11-24 11:24:24', '2015-11-24 11:24:24');
INSERT INTO `persistences` VALUES ('134', '1', 'MVm2uIPKGoClnBOtPnZLeMne0cxqm79A', '2015-11-24 13:06:16', '2015-11-24 13:06:16');
INSERT INTO `persistences` VALUES ('135', '1', 'UdEhb1HUGGFeNUAHSzSEAd28LBtML6ti', '2015-11-24 18:54:58', '2015-11-24 18:54:58');
INSERT INTO `persistences` VALUES ('136', '1', 'CZGAahPMZICfT9C2pz8DKf3N9XLZpzwT', '2015-11-24 19:14:32', '2015-11-24 19:14:32');
INSERT INTO `persistences` VALUES ('138', '1', 'ZS8C30f5fheTscPSyKKcwnHFzwJi5YUg', '2015-11-25 10:47:44', '2015-11-25 10:47:44');
INSERT INTO `persistences` VALUES ('139', '1', 'hviLwIVE77jiFjpRBx6DmB29nDCTiEoa', '2015-11-25 11:51:06', '2015-11-25 11:51:06');
INSERT INTO `persistences` VALUES ('140', '1', 'k4YjDklmjCK3XOheyFj5tZRgkgQLcvPZ', '2015-11-25 12:44:04', '2015-11-25 12:44:04');
INSERT INTO `persistences` VALUES ('141', '1', 'Q0pgzUMaw1jbbjbJ76ZEoiPNHbvEaGhb', '2015-11-25 14:39:15', '2015-11-25 14:39:15');
INSERT INTO `persistences` VALUES ('142', '1', 'j1Ll3vyCfxk8WYrfGRWbLcFlmmB5gvuf', '2015-11-25 19:27:11', '2015-11-25 19:27:11');
INSERT INTO `persistences` VALUES ('143', '1', '36OdiItmKntFhhusCsOxRDRUmRLi3aIk', '2015-11-27 10:40:45', '2015-11-27 10:40:45');
INSERT INTO `persistences` VALUES ('144', '1', 'EY00OQByVxbkksjXJeB779sTw0PVBwQo', '2015-11-27 11:01:33', '2015-11-27 11:01:33');
INSERT INTO `persistences` VALUES ('145', '1', 'wFYXrcHs2VT8JxpVeu2nkYpEDIiyveCi', '2015-11-27 16:06:39', '2015-11-27 16:06:39');
INSERT INTO `persistences` VALUES ('146', '1', 'fnPGwFNPFaup09Pi8h3HRZkW7g2o89UB', '2015-11-27 16:40:49', '2015-11-27 16:40:49');
INSERT INTO `persistences` VALUES ('147', '1', 'EoByZX0seoP71TO3mh5YkAiMw41dQjfI', '2015-11-28 10:56:43', '2015-11-28 10:56:43');
INSERT INTO `persistences` VALUES ('148', '1', 'N4O3wiGd7DlOZPPsiXQnU3GaIUDVs0pk', '2015-11-28 11:05:49', '2015-11-28 11:05:49');
INSERT INTO `persistences` VALUES ('149', '1', 'wvrtqymHww86JtFR4ySTB7PRMVe6CYyZ', '2015-12-01 12:59:39', '2015-12-01 12:59:39');
INSERT INTO `persistences` VALUES ('150', '1', '0c1cjjzAmVVAFRwhzEBmL2fBR2a0cZ47', '2015-12-01 13:04:17', '2015-12-01 13:04:17');
INSERT INTO `persistences` VALUES ('151', '1', 'wXdvBHnlVA6VxrKWojj4MPaQ15WIDALo', '2015-12-01 14:14:11', '2015-12-01 14:14:11');
INSERT INTO `persistences` VALUES ('152', '1', '6KgSa5WB6Fcd1RhTQe4bpnc3NLRc4jKs', '2015-12-01 22:01:40', '2015-12-01 22:01:40');
INSERT INTO `persistences` VALUES ('153', '1', 'b9DvbCSyCVGeHps05XTN3kDLjG9i6UID', '2015-12-02 10:50:21', '2015-12-02 10:50:21');
INSERT INTO `persistences` VALUES ('154', '1', 'V1ebekY9wLdVXEXYaDXiDKizKIwV2YoZ', '2015-12-02 13:05:33', '2015-12-02 13:05:33');
INSERT INTO `persistences` VALUES ('155', '1', 'U1TDvsp3l7S50HKZJilwD8kolj3oKMZY', '2015-12-02 14:14:31', '2015-12-02 14:14:31');
INSERT INTO `persistences` VALUES ('156', '1', 'tWGlxczBI5ISqJsyb8i4t1GAGLVmCxdg', '2015-12-03 11:57:48', '2015-12-03 11:57:48');
INSERT INTO `persistences` VALUES ('157', '1', 'sX9s0vE8zk12ZJkKZOj53is0FOSzm3h5', '2015-12-03 12:36:14', '2015-12-03 12:36:14');
INSERT INTO `persistences` VALUES ('158', '1', 'YZ1T4vrGkxVOS91okt3lSiz7OFyibEWY', '2015-12-03 12:45:44', '2015-12-03 12:45:44');
INSERT INTO `persistences` VALUES ('160', '1', '0asRo8XZo9ictz6mNc941GyW81j7mqnY', '2015-12-03 19:12:18', '2015-12-03 19:12:18');
INSERT INTO `persistences` VALUES ('161', '1', 'FVal7sFDex7QqJibHPmQ3BqJOjMxY4zG', '2015-12-03 19:57:27', '2015-12-03 19:57:27');
INSERT INTO `persistences` VALUES ('162', '1', 'R82HDrmisEFi9HDBpF4Cb6Ffg8r9o3RI', '2015-12-03 20:32:32', '2015-12-03 20:32:32');
INSERT INTO `persistences` VALUES ('163', '1', 'bYRhi5W5Z5sShoOXorpyq9PTzruV4ISR', '2015-12-04 11:43:08', '2015-12-04 11:43:08');
INSERT INTO `persistences` VALUES ('164', '1', '6wFdYiPTSZW4ldZn62NgTYaTQrAyMRFW', '2015-12-04 13:40:42', '2015-12-04 13:40:42');
INSERT INTO `persistences` VALUES ('165', '1', '7Zpc8lp11v5CtWyMWGlonBNEHCsQCWhi', '2015-12-08 11:34:48', '2015-12-08 11:34:48');
INSERT INTO `persistences` VALUES ('166', '1', '9vbkG1yJuNrxJbPccH6inExHABpHtQMU', '2015-12-08 12:56:07', '2015-12-08 12:56:07');
INSERT INTO `persistences` VALUES ('167', '1', 'O61lttpy7l77lwEm2vQBpYftms5BbuWz', '2015-12-08 12:57:47', '2015-12-08 12:57:47');
INSERT INTO `persistences` VALUES ('168', '1', 'z1BKohUNT9iUIx0QDiNHknohKyQLxgwR', '2015-12-08 15:21:57', '2015-12-08 15:21:57');
INSERT INTO `persistences` VALUES ('169', '1', 'AutjPb5lGuWESM9dFfzsSpFyDH6vJifT', '2015-12-08 18:42:57', '2015-12-08 18:42:57');
INSERT INTO `persistences` VALUES ('170', '1', 'IjjDqcWuwLtABfWU4FeKbmfFnYVZeo81', '2015-12-09 13:51:17', '2015-12-09 13:51:17');
INSERT INTO `persistences` VALUES ('171', '1', 'MvJ9bEq3DNsBfz6eBPpbvweOEqZUZTZZ', '2015-12-09 16:29:56', '2015-12-09 16:29:56');
INSERT INTO `persistences` VALUES ('172', '1', 'YE0xTDpxGhMKoxuRYEqODQdhjsg0HBWN', '2015-12-09 19:14:15', '2015-12-09 19:14:15');
INSERT INTO `persistences` VALUES ('173', '1', 'qtPfV4NGQUHK8nPilb3zMSkPtOFa5fA7', '2015-12-09 19:18:06', '2015-12-09 19:18:06');
INSERT INTO `persistences` VALUES ('174', '1', 'bRiBCAqEMyoZMFGcVh0ppiCe7jJQtmL6', '2015-12-10 11:09:45', '2015-12-10 11:09:45');
INSERT INTO `persistences` VALUES ('175', '1', 'kvxZtGoLNIrPws0jng0hSky09B4w52xo', '2015-12-10 11:24:21', '2015-12-10 11:24:21');
INSERT INTO `persistences` VALUES ('177', '1', 'CYcO33TQxApc3BsrIxKhA1TG2XfjvjuE', '2015-12-10 22:01:48', '2015-12-10 22:01:48');
INSERT INTO `persistences` VALUES ('178', '1', 'ch00cMtPi9ZXy38Y8xsVKkeQT4Tdyz0F', '2015-12-11 11:02:28', '2015-12-11 11:02:28');
INSERT INTO `persistences` VALUES ('179', '1', 'ucujCYid3Ed1fWRzUc33bszW0Da5WpoR', '2015-12-11 11:52:35', '2015-12-11 11:52:35');
INSERT INTO `persistences` VALUES ('180', '1', 'NQRHc1Aw4NGYpvd8UJHdacDUcMoN6CWT', '2015-12-11 11:58:07', '2015-12-11 11:58:07');
INSERT INTO `persistences` VALUES ('181', '1', 'lNch1fxa3m3v7w7QaQKOxJSAq8MM1oWV', '2015-12-11 13:42:57', '2015-12-11 13:42:57');
INSERT INTO `persistences` VALUES ('182', '1', 'AhU6Q1goojDW2QHqZtWGrOFIJ5yvyN29', '2015-12-11 14:46:19', '2015-12-11 14:46:19');
INSERT INTO `persistences` VALUES ('183', '1', 'HlNikcVYXQjaM4RTiuBaXpUVHvz6BVRD', '2015-12-12 17:23:54', '2015-12-12 17:23:54');
INSERT INTO `persistences` VALUES ('184', '1', 'aNFY4tCtnikY3dkpYL5GCTNiU3uil6dK', '2015-12-15 13:46:12', '2015-12-15 13:46:12');
INSERT INTO `persistences` VALUES ('185', '1', 'uFP0pEF4i8hpkuXI3XbN5HyyL45IEg9d', '2015-12-15 13:53:42', '2015-12-15 13:53:42');
INSERT INTO `persistences` VALUES ('186', '1', 'vHVcNsAZ5cHqBBCBesd8a4O5bWERDwta', '2015-12-15 14:30:17', '2015-12-15 14:30:17');
INSERT INTO `persistences` VALUES ('187', '1', 'ChWOSdh7XwdgcCY811PGvll0OvNwuijO', '2015-12-15 14:30:39', '2015-12-15 14:30:39');
INSERT INTO `persistences` VALUES ('188', '1', 'HmekPBJ7ICRGCVPXebGjrXKaeqFxAsUz', '2015-12-15 21:40:11', '2015-12-15 21:40:11');
INSERT INTO `persistences` VALUES ('189', '1', '3Y7IXo5Fav5lMC18YJOBaw3LyGm3Zqti', '2015-12-16 11:18:39', '2015-12-16 11:18:39');
INSERT INTO `persistences` VALUES ('190', '1', 'VWfHAS0MK3w8urby0C1O5gBydkw5nRXt', '2015-12-16 12:07:52', '2015-12-16 12:07:52');
INSERT INTO `persistences` VALUES ('192', '1', 'x18zuJwn8SteKHNbt4URG9Uq1XR0f2Pi', '2015-12-17 10:45:37', '2015-12-17 10:45:37');
INSERT INTO `persistences` VALUES ('193', '1', 'mf0llY1RIuyVIBH7UZnwayL1EaVgaFFX', '2015-12-17 11:56:48', '2015-12-17 11:56:48');
INSERT INTO `persistences` VALUES ('194', '1', 'z9DhpWh9V8MpvWZSwFjx0hKFxoI2DGgK', '2015-12-17 13:04:29', '2015-12-17 13:04:29');
INSERT INTO `persistences` VALUES ('195', '1', 'CR87O4WkLFp06iAR7lqniAlqjmh1k8we', '2015-12-17 15:33:19', '2015-12-17 15:33:19');
INSERT INTO `persistences` VALUES ('196', '1', 'cPDJLvk735GPW0ra0gW4t56wJ5pDmMRk', '2015-12-18 11:26:23', '2015-12-18 11:26:23');
INSERT INTO `persistences` VALUES ('197', '1', 'oVn2YFQRwd2gAWhlclpXyDrhjeVppsLU', '2015-12-18 12:06:31', '2015-12-18 12:06:31');
INSERT INTO `persistences` VALUES ('198', '1', 'vuNBEvFcd18udu3JQnsW0ofs2KzferFr', '2015-12-18 17:18:06', '2015-12-18 17:18:06');
INSERT INTO `persistences` VALUES ('199', '1', 'Ywq2iUj3JoQ0Nm3RfYTVYVEHmaQdebk9', '2015-12-18 18:17:28', '2015-12-18 18:17:28');
INSERT INTO `persistences` VALUES ('201', '1', 'qQwfqZPYXB5z1AGj6X0sA564nSQFxfAK', '2015-12-19 13:29:07', '2015-12-19 13:29:07');
INSERT INTO `persistences` VALUES ('205', '1', 'vcFhrXNme0lqyD9NHvhq70lohOEjKwXm', '2015-12-19 15:17:59', '2015-12-19 15:17:59');
INSERT INTO `persistences` VALUES ('221', '1', 'v8PGTwhcVg9tbCprRcnz7O51wzOd7u6E', '2015-12-19 19:32:04', '2015-12-19 19:32:04');
INSERT INTO `persistences` VALUES ('222', '1', 'NqZN9jtcJEUBarUEcjhBHW3WEKeC7Y4U', '2015-12-22 14:36:49', '2015-12-22 14:36:49');
INSERT INTO `persistences` VALUES ('223', '1', 'JCXWsqKC99RPpa6ETVbmnuEwv6bBuQdL', '2015-12-29 10:16:56', '2015-12-29 10:16:56');
INSERT INTO `persistences` VALUES ('224', '1', 'oDVXJBM8w9YRFKWb8HbGYD8TGtlt3X4b', '2015-12-30 11:31:35', '2015-12-30 11:31:35');
INSERT INTO `persistences` VALUES ('225', '1', 'Ao8BnmQ433VKsCngYlkBA3XwmdG6KxtH', '2015-12-31 16:31:50', '2015-12-31 16:31:50');
INSERT INTO `persistences` VALUES ('226', '1', 'iRxZ1afS4VTBy5VwpG5NQxQISj8EnNVP', '2015-12-31 17:40:39', '2015-12-31 17:40:39');
INSERT INTO `persistences` VALUES ('227', '1', 'k37znZ2CQPln2OLinYELHQ2Tkw8j56ht', '2016-01-01 10:14:34', '2016-01-01 10:14:34');
INSERT INTO `persistences` VALUES ('228', '1', '3PjJ5RyRe8ZPBHfQ9ro1UI3iVhG49rGR', '2016-01-01 10:41:05', '2016-01-01 10:41:05');
INSERT INTO `persistences` VALUES ('229', '1', 'BLBuxGAaqABxtg08U0SO5L8RYf3YTlrJ', '2016-01-05 11:23:26', '2016-01-05 11:23:26');
INSERT INTO `persistences` VALUES ('230', '1', '3GezeUSlnh72asykrBlhVqlOZW9dVBgc', '2016-01-07 11:33:28', '2016-01-07 11:33:28');
INSERT INTO `persistences` VALUES ('233', '1', 'U04AcwG7T17Ky9gy72tThaE5pa7cMKRb', '2016-01-08 14:26:55', '2016-01-08 14:26:55');
INSERT INTO `persistences` VALUES ('234', '1', 'NKfuqj7oiybLSyVEn4gfvgzdlrTIYXJh', '2016-01-08 16:30:23', '2016-01-08 16:30:23');
INSERT INTO `persistences` VALUES ('236', '1', 'HIJ6IMqSV3EybIZFiNu3cxpbabgBbqWy', '2016-01-09 19:35:32', '2016-01-09 19:35:32');
INSERT INTO `persistences` VALUES ('238', '1', 'sWxeUSntPPSkt6pQp0llxmHLbkEaChYD', '2016-01-13 20:52:14', '2016-01-13 20:52:14');
INSERT INTO `persistences` VALUES ('239', '1', 'ZJD3jkIsNXOfl3Ns3h7Zdc1c8YTWZszk', '2016-01-20 16:21:02', '2016-01-20 16:21:02');
INSERT INTO `persistences` VALUES ('240', '1', 'IYTXwaz9POTqnhccszxP5xrsVS6pVfsT', '2016-01-20 17:47:09', '2016-01-20 17:47:09');
INSERT INTO `persistences` VALUES ('247', '1', 'sK03UtbWuendDySxW4QHNwFk7oUfGGQh', '2016-01-22 16:51:02', '2016-01-22 16:51:02');
INSERT INTO `persistences` VALUES ('248', '1', 'cEupKusIH32KPfSFxsgrNsbyPtOrAKmJ', '2016-01-25 00:25:37', '2016-01-25 00:25:37');
INSERT INTO `persistences` VALUES ('249', '1', 'rh3YgfuzOeuR1XTHcv6C8va0IeF6suBG', '2016-01-27 15:27:20', '2016-01-27 15:27:20');
INSERT INTO `persistences` VALUES ('251', '1', 'O43TYM85iDgTel2AL9Vx9R0XfCQBkLVi', '2016-01-29 11:10:07', '2016-01-29 11:10:07');
INSERT INTO `persistences` VALUES ('252', '1', 'PwS9qjcqbOWH0Vfmi3r2Rsyt6DD4a0Zu', '2016-01-30 11:20:25', '2016-01-30 11:20:25');
INSERT INTO `persistences` VALUES ('253', '1', 'zPd7scLweBBjdsNU4z9W1ypNHkJXUluh', '2016-02-02 14:01:41', '2016-02-02 14:01:41');
INSERT INTO `persistences` VALUES ('254', '1', 'Bs7DtcBQghw6pIARDwkUTAQlFNDmBoJH', '2016-02-03 11:10:54', '2016-02-03 11:10:54');
INSERT INTO `persistences` VALUES ('255', '1', 'pHG2p2qNOyL7yGHs64ZEIlMi3gWIKoqA', '2016-02-03 15:17:42', '2016-02-03 15:17:42');
INSERT INTO `persistences` VALUES ('256', '1', '98beL2w2JCdq6tPhm8WI8aJhOn1rImR4', '2016-02-06 10:42:46', '2016-02-06 10:42:46');
INSERT INTO `persistences` VALUES ('257', '1', 'T5TIVTIWDNAQJmGWyidQyREJ5I4FzgZR', '2016-02-06 10:45:01', '2016-02-06 10:45:01');
INSERT INTO `persistences` VALUES ('258', '1', 'EnBjEdYhWPx3Oy15aRAWG4DKqhwRv5ov', '2016-02-06 13:57:20', '2016-02-06 13:57:20');
INSERT INTO `persistences` VALUES ('259', '1', 'CsMADZrehJhqSi5ZLjW818I2w4CfHDSr', '2016-02-06 14:03:35', '2016-02-06 14:03:35');
INSERT INTO `persistences` VALUES ('260', '1', 'azAaPIxSHpYhnaoX0dJSDMLNqc8lcmJc', '2016-02-09 10:22:54', '2016-02-09 10:22:54');
INSERT INTO `persistences` VALUES ('261', '1', 'HKxd27kS2kMSM2HrTAKv43R3KbDrswhA', '2016-02-09 12:49:08', '2016-02-09 12:49:08');
INSERT INTO `persistences` VALUES ('262', '1', '48Ubx2II5G9sO0mFRXvuDZ2Y7nPfD6GL', '2016-02-09 15:30:34', '2016-02-09 15:30:34');
INSERT INTO `persistences` VALUES ('263', '1', 'vT3WOvD54FuXTHFUCCGDZJTMESQGIyLm', '2016-02-10 10:07:06', '2016-02-10 10:07:06');
INSERT INTO `persistences` VALUES ('264', '1', 'wbqdkltHpAMPJBwCArq8ofEOJM78ijA8', '2016-02-11 11:26:27', '2016-02-11 11:26:27');
INSERT INTO `persistences` VALUES ('265', '1', 'rg7fD0sVCXY1tKniF72qcMC2yMT7qiIW', '2016-02-11 11:29:18', '2016-02-11 11:29:18');
INSERT INTO `persistences` VALUES ('266', '1', 'TiysVzWSHW41jwJVXICfb2HKf8SbffrA', '2016-02-11 12:27:37', '2016-02-11 12:27:37');
INSERT INTO `persistences` VALUES ('267', '1', '8W8ebwDb7e6y09mrOLNIbK1NRau4COMD', '2016-02-11 15:28:24', '2016-02-11 15:28:24');
INSERT INTO `persistences` VALUES ('268', '1', 'ZiFrNARlSVBjkIbrjZ7onhY0tJzQz507', '2016-02-12 12:45:09', '2016-02-12 12:45:09');
INSERT INTO `persistences` VALUES ('270', '1', 'mP5f65vcNaweLd9PJCf3Pk9IVnME5RBf', '2016-02-13 13:26:40', '2016-02-13 13:26:40');
INSERT INTO `persistences` VALUES ('271', '1', 'tdu6OzlLAyNW4sluvHCEWds5ZxP0bxS4', '2016-02-14 06:50:12', '2016-02-14 06:50:12');
INSERT INTO `persistences` VALUES ('275', '1', 'ztbunZDOv1s9QIiNz7RFxluWuSYbg8Fa', '2016-03-01 00:35:19', '2016-03-01 00:35:19');
INSERT INTO `persistences` VALUES ('276', '1', 'hLmFelaXvujhsSGwNlU7lfcyJpmYLnGk', '2016-03-02 21:47:03', '2016-03-02 21:47:03');
INSERT INTO `persistences` VALUES ('277', '1', 'XB9vR14I8dis89PH6B87A7TLhZKXaXJm', '2016-03-03 00:24:12', '2016-03-03 00:24:12');
INSERT INTO `persistences` VALUES ('278', '1', 'kQeWQK85H1BrXV1SCJfc27Fmw6Wp3oyN', '2016-03-03 01:00:33', '2016-03-03 01:00:33');
INSERT INTO `persistences` VALUES ('279', '1', 'WIQSxD6QHb4PFG2aU4Ha6jEj3uBub0cs', '2016-03-04 04:41:41', '2016-03-04 04:41:41');
INSERT INTO `persistences` VALUES ('280', '1', 'AEVdOFQIJrN97odGu8wIzsxDq2ZXvdp0', '2016-03-07 22:11:17', '2016-03-07 22:11:17');
INSERT INTO `persistences` VALUES ('281', '1', '94wd5Y0M2Kps5m7ie7cPccjEjOKpMQNy', '2016-03-07 22:16:38', '2016-03-07 22:16:38');
INSERT INTO `persistences` VALUES ('282', '1', 'DPVodfpPkU8Kz5mWCt3JAlqo6fwGbuxo', '2016-03-07 23:12:12', '2016-03-07 23:12:12');
INSERT INTO `persistences` VALUES ('283', '1', 'VGvZq0gH0YS1imjFqXLlvrG0WX7SOAcU', '2016-03-08 22:10:20', '2016-03-08 22:10:20');
INSERT INTO `persistences` VALUES ('284', '1', 'ANi2cc4LPzquSUcuvQpVzYcRuVOAutaZ', '2016-03-09 01:21:06', '2016-03-09 01:21:06');
INSERT INTO `persistences` VALUES ('285', '1', 'ilHKy865x9mgPlXMJP4l5fMJrkOqZLZc', '2016-03-09 03:09:59', '2016-03-09 03:09:59');
INSERT INTO `persistences` VALUES ('286', '1', 'QsNTMqkvHRecWfEJ6H1NWcaxSs1oNvfw', '2016-03-09 08:48:58', '2016-03-09 08:48:58');
INSERT INTO `persistences` VALUES ('287', '1', '37SnOgQXkjLfYjP4izY4X1UR1iXQ3Ssp', '2016-03-09 22:12:10', '2016-03-09 22:12:10');
INSERT INTO `persistences` VALUES ('288', '1', 'mpppkJaenoQAVtCIfkY36F35y1HlT86S', '2016-03-10 00:58:57', '2016-03-10 00:58:57');
INSERT INTO `persistences` VALUES ('289', '1', 'Ftyf3YMVMk5d5O0KjxdIyH0vTheyr7DR', '2016-03-10 22:20:23', '2016-03-10 22:20:23');
INSERT INTO `persistences` VALUES ('290', '1', '0LUVNJqQ667EUhMzzza8qehmqdvwRD4V', '2016-03-11 02:27:26', '2016-03-11 02:27:26');
INSERT INTO `persistences` VALUES ('291', '1', 'W2l6Ih8MTSrv0teq6J7rx42EH4oFCXy6', '2016-03-11 21:59:30', '2016-03-11 21:59:30');
INSERT INTO `persistences` VALUES ('292', '1', '2WDNZ8YPZQBPvCjkqaMLcrG2bHNpiSx2', '2016-03-14 22:53:02', '2016-03-14 22:53:02');
INSERT INTO `persistences` VALUES ('293', '1', 'sIzDhM5WZMUPdch0erMFTXt6uNaxcn7G', '2016-03-15 21:41:22', '2016-03-15 21:41:22');
INSERT INTO `persistences` VALUES ('294', '1', 'vS1UwLmFUAIsNtD3bxL7mmzkFnwEybSF', '2016-03-15 23:23:33', '2016-03-15 23:23:33');
INSERT INTO `persistences` VALUES ('295', '1', 'im30jVklOWR7TySKNe21naGEanVatJYL', '2016-03-16 21:26:31', '2016-03-16 21:26:31');
INSERT INTO `persistences` VALUES ('296', '1', 'PT6rUzFfOsrLbETkHyRUxfTxgVLIHgIE', '2016-03-16 21:50:49', '2016-03-16 21:50:49');
INSERT INTO `persistences` VALUES ('297', '1', 'Hy47UymE9xA7JsfjGCjefolnkVOOMTwY', '2016-03-17 04:53:41', '2016-03-17 04:53:41');
INSERT INTO `persistences` VALUES ('298', '1', 'paA1vDiDZxGtFx61LWWMV7tZqeNrCUW1', '2016-03-17 04:59:10', '2016-03-17 04:59:10');
INSERT INTO `persistences` VALUES ('299', '1', 'x5gISkPux4CamFmVoaeoYsPImFfBGpef', '2016-03-17 06:37:05', '2016-03-17 06:37:05');
INSERT INTO `persistences` VALUES ('300', '1', 'yjn0mOUVW3y7UVevY9DKSECOggs96j6r', '2016-03-18 23:49:44', '2016-03-18 23:49:44');
INSERT INTO `persistences` VALUES ('301', '1', 'GoIQf7DmVGs40YbFL7T8BVtbiQ8R8uBh', '2016-03-23 22:14:19', '2016-03-23 22:14:19');
INSERT INTO `persistences` VALUES ('302', '1', 'DvjAiVcASbqobvNMqzIhVBdm6jofQdt1', '2016-03-23 22:47:21', '2016-03-23 22:47:21');
INSERT INTO `persistences` VALUES ('303', '1', 'QBOZsaTD6i22spqeC2vgG2yIgCh8dwnG', '2016-03-23 23:00:56', '2016-03-23 23:00:56');
INSERT INTO `persistences` VALUES ('304', '1', 'u1lErExOOF3HRLDmIx6hruoN94qS9Dsn', '2016-03-23 23:29:47', '2016-03-23 23:29:47');
INSERT INTO `persistences` VALUES ('305', '1', 'cbIVuXZn5wyzsTGZHKxVDjLB7SLOPSYD', '2016-03-29 03:33:00', '2016-03-29 03:33:00');
INSERT INTO `persistences` VALUES ('307', '1', 'CpIqZbEGoqDFJKxHET5h3C0pzDZdvnKn', '2016-03-29 23:51:21', '2016-03-29 23:51:21');
INSERT INTO `persistences` VALUES ('308', '1', 'byerYGBIbsMg4CfY7aidC1ZNYsnmj1E6', '2016-03-31 03:13:20', '2016-03-31 03:13:20');
INSERT INTO `persistences` VALUES ('310', '1', 'ICBM7yj3By39IICV1jkoIKJxh1FFpOzg', '2016-03-31 05:37:58', '2016-03-31 05:37:58');
INSERT INTO `persistences` VALUES ('311', '1', 'hYZ8uFrbW213bFWW2IIBXFQOq5B04Kbj', '2016-03-31 21:40:55', '2016-03-31 21:40:55');
INSERT INTO `persistences` VALUES ('312', '1', 'nFv4bXOYipFv5Ix6zhKSpR1wyNhobj99', '2016-04-02 03:37:40', '2016-04-02 03:37:40');
INSERT INTO `persistences` VALUES ('313', '1', 'KHu9yIGB16YvrDV2FEWOq8BQ6PvMbKl8', '2016-04-25 22:22:04', '2016-04-25 22:22:04');
INSERT INTO `persistences` VALUES ('315', '1', '7FoQgzabeFRIWL1M7ZPsm3z9PG8F1Ugo', '2016-04-26 23:01:21', '2016-04-26 23:01:21');
INSERT INTO `persistences` VALUES ('316', '1', 'OdnZDFviqrfXC8WrQ2Cb6ZDvYz8nPgRx', '2016-04-28 03:37:40', '2016-04-28 03:37:40');
INSERT INTO `persistences` VALUES ('317', '1', 'kOebcJr1JJG5wUk5fhOfGhHxHnLOwSvc', '2016-05-02 12:18:45', '2016-05-02 12:18:45');
INSERT INTO `persistences` VALUES ('320', '1', 'ggE3UJheRV8uDVSOh95l2o0aefxiuPKt', '2016-05-02 22:04:11', '2016-05-02 22:04:11');
INSERT INTO `persistences` VALUES ('321', '1', '53mvW0wlHIMVAW0H00HVg9zkesJ4OhXD', '2016-05-02 22:27:17', '2016-05-02 22:27:17');
INSERT INTO `persistences` VALUES ('322', '1', 'hRCPvsEyyjmKVeHCXisqCoaXBYCxaScN', '2016-05-03 06:52:32', '2016-05-03 06:52:32');
INSERT INTO `persistences` VALUES ('323', '1', 'cTlcwjYOxTY6ZJCrkMNEnO5b5TzMmEGB', '2016-05-03 22:34:46', '2016-05-03 22:34:46');
INSERT INTO `persistences` VALUES ('324', '1', 'vmqEkzTG3IbAneq5OOjHzSqucV35AmnD', '2016-11-02 04:17:58', '2016-11-02 04:17:58');
INSERT INTO `persistences` VALUES ('325', '1', 'BW01QCJOOLoZIO8VUJnF52vIbu2rc11g', '2016-11-02 21:09:37', '2016-11-02 21:09:37');
INSERT INTO `persistences` VALUES ('326', '1', 'FzWFz0LRPiRYtqrp0ZYERRRuAbG1yYiV', '2016-11-09 07:18:50', '2016-11-09 07:18:50');
INSERT INTO `persistences` VALUES ('327', '1', '0xAj653k46fiqUDh19Ckozg3ZyytnCH0', '2016-11-10 00:23:05', '2016-11-10 00:23:05');
INSERT INTO `persistences` VALUES ('328', '1', '6OwwOXig9X0lFtXXVrYgJ8SrkoLgzEvg', '2016-11-10 22:51:43', '2016-11-10 22:51:43');
INSERT INTO `persistences` VALUES ('329', '1', 'FnxCrE4ava77Pf646SgsJhZZcMIfODMy', '2016-11-11 03:07:29', '2016-11-11 03:07:29');
INSERT INTO `persistences` VALUES ('330', '1', 'hXKEok3YM5YjXZjMQuL7T14hAmz43BJH', '2016-11-17 05:20:10', '2016-11-17 05:20:10');
INSERT INTO `persistences` VALUES ('331', '1', 'WlPtomsC2L3hnU7xIpW81oFQENGiNNaB', '2016-11-17 21:03:54', '2016-11-17 21:03:54');
INSERT INTO `persistences` VALUES ('332', '1', 'liZqeZ1qON6xfJK7g2oCtLjbxcjkUBCS', '2016-11-18 21:19:34', '2016-11-18 21:19:34');
INSERT INTO `persistences` VALUES ('333', '1', '0eTUyc0uXgk1fRS0dDaGQUdXVVapU8ts', '2016-11-19 02:34:38', '2016-11-19 02:34:38');
INSERT INTO `persistences` VALUES ('334', '1', 'nUEqpH5Mf6sLxlaTi9sULwKMMgVLYp2a', '2016-11-21 21:29:03', '2016-11-21 21:29:03');
INSERT INTO `persistences` VALUES ('335', '1', '88kA0cgf81T7zClaZff6EnvhJNcUtaSf', '2016-11-22 02:37:54', '2016-11-22 02:37:54');
INSERT INTO `persistences` VALUES ('336', '1', 'VepF1pmmoT2kFPHwKDIFkF6kb34GMdNs', '2016-11-22 20:54:59', '2016-11-22 20:54:59');
INSERT INTO `persistences` VALUES ('337', '1', 'nPKUHcWR3obf890hffnSkhzBH7IGFrlp', '2016-11-23 00:30:15', '2016-11-23 00:30:15');
INSERT INTO `persistences` VALUES ('338', '1', 'l4KFahczm29zpo7TakPGJM9qHJWAxOAQ', '2016-11-24 01:04:22', '2016-11-24 01:04:22');
INSERT INTO `persistences` VALUES ('339', '1', 'luOKV6kdrS9iGE56uw85la2ddiCvN67F', '2016-11-24 21:11:20', '2016-11-24 21:11:20');
INSERT INTO `persistences` VALUES ('341', '1', 'RIo4FfgIOAjDS1jjYUvtUNGBH9wjhuKY', '2016-11-28 21:05:34', '2016-11-28 21:05:34');
INSERT INTO `persistences` VALUES ('342', '1', 'OnbCVCBgNJdKh2zqX3kDgNYi4SCfSzur', '2016-11-29 21:07:22', '2016-11-29 21:07:22');
INSERT INTO `persistences` VALUES ('343', '1', 'MEYNvMqzpb5SvUki1HoGiebSnkOV6uOw', '2016-11-30 21:15:37', '2016-11-30 21:15:37');
INSERT INTO `persistences` VALUES ('344', '1', 'HmqRCpKWJ37CB8sDmMaSzn2JCE22ogQP', '2016-12-01 21:10:55', '2016-12-01 21:10:55');
INSERT INTO `persistences` VALUES ('345', '1', 'Dpbs7U9zUKm1747oicIGoLGugCMFRJXu', '2016-12-05 21:42:14', '2016-12-05 21:42:14');
INSERT INTO `persistences` VALUES ('346', '1', 'IxBT0XnquRWKn6LdI08QGNWZlGpILa1m', '2016-12-06 21:30:17', '2016-12-06 21:30:17');
INSERT INTO `persistences` VALUES ('347', '1', 'J0GWjCrIGjcxAeezuVTwr23G0tX8z9FA', '2016-12-07 01:10:21', '2016-12-07 01:10:21');
INSERT INTO `persistences` VALUES ('348', '1', '40aMIDL7SIahLW5qCDogsFDB5jrjSrAl', '2016-12-07 21:16:01', '2016-12-07 21:16:01');
INSERT INTO `persistences` VALUES ('349', '1', 'UWX5assFv0BqZHtAYvNLjj3vZBeS4F8S', '2016-12-08 21:38:25', '2016-12-08 21:38:25');
INSERT INTO `persistences` VALUES ('350', '1', 'YxETIww2Np3mmklNscNLHuro63gm80n6', '2016-12-09 02:40:45', '2016-12-09 02:40:45');
INSERT INTO `persistences` VALUES ('351', '1', 'arQP0rVbLNbuSAhBMdvb8d708jPVs7TS', '2016-12-09 21:49:26', '2016-12-09 21:49:26');
INSERT INTO `persistences` VALUES ('352', '1', 'xFAbEiz775LC7qKnr3Hnhrk6CnFicY4N', '2016-12-10 03:23:24', '2016-12-10 03:23:24');
INSERT INTO `persistences` VALUES ('353', '1', '2zXNR8a9dPx6Yh6jhqkdlXMH2uAdvWq2', '2016-12-14 22:01:01', '2016-12-14 22:01:01');
INSERT INTO `persistences` VALUES ('354', '1', 'U6yuAZYyUpFDYdIUh3ttI034vFbyOOtF', '2016-12-15 03:53:47', '2016-12-15 03:53:47');
INSERT INTO `persistences` VALUES ('355', '1', 'rn1PCB8xyinSDZzM6FveYtdRkwEYv7or', '2016-12-15 22:02:12', '2016-12-15 22:02:12');
INSERT INTO `persistences` VALUES ('356', '1', '89O8WwMadX6nZsPQVwVI5amH0KfcBG69', '2016-12-16 02:09:59', '2016-12-16 02:09:59');
INSERT INTO `persistences` VALUES ('357', '1', 'ODUR6IbIZjp6m0WlUkRBJCxWBmYy2uCP', '2016-12-28 21:39:32', '2016-12-28 21:39:32');
INSERT INTO `persistences` VALUES ('358', '1', 'PRhfdm4BXCA6VVTmJmJp2A8cck1fmQOs', '2016-12-30 02:48:03', '2016-12-30 02:48:03');
INSERT INTO `persistences` VALUES ('359', '1', 'MX2BtaL7IbVo0M9cOjHgGKtv0W3qsfvN', '2016-12-30 22:19:48', '2016-12-30 22:19:48');
INSERT INTO `persistences` VALUES ('360', '1', 'URDmiIwst9p7AH752meBRy5jnmmn12iQ', '2016-12-31 03:32:32', '2016-12-31 03:32:32');
INSERT INTO `persistences` VALUES ('368', '1', 'Xgq5p1wpwa6nMCh71oawtjYyaDBqFF0Z', '2017-01-15 17:18:10', '2017-01-15 17:18:10');
INSERT INTO `persistences` VALUES ('380', '1', 'YTOyGClrQ6j10dPZK2sEsgVdSq2z1KXp', '2017-01-16 17:07:15', '2017-01-16 17:07:15');
INSERT INTO `persistences` VALUES ('385', '1', '30rPT91aUiZggV1KuNyKH3qmhZKYUzBO', '2017-01-19 13:27:49', '2017-01-19 13:27:49');
INSERT INTO `persistences` VALUES ('386', '1', 'NMo6ESSw8QVNQcewukGFL6Wnt2tJv7FB', '2017-01-19 19:12:11', '2017-01-19 19:12:11');
INSERT INTO `persistences` VALUES ('389', '1', 'EYdjNai3eG7Yd6zYQBvUiUkI3yfj6YCk', '2017-01-20 14:01:39', '2017-01-20 14:01:39');
INSERT INTO `persistences` VALUES ('393', '1', 'XG5ZK9wWhKtftpzeVlFOyotpfn72STI4', '2017-01-23 02:23:01', '2017-01-23 02:23:01');
INSERT INTO `persistences` VALUES ('400', '1', 'vQp8uS1ljqj4UhabgjDjcP7rW0vSoXeJ', '2017-01-24 07:08:00', '2017-01-24 07:08:00');
INSERT INTO `persistences` VALUES ('401', '9', 'SWyNFqRVA8qiuWgNylGD5deH9FPDjdrk', '2017-01-24 07:15:43', '2017-01-24 07:15:43');
INSERT INTO `persistences` VALUES ('402', '1', 'dfnpIy8kp33r1SzHMChUkh2JZHe5dGRO', '2017-01-25 00:00:38', '2017-01-25 00:00:38');
INSERT INTO `persistences` VALUES ('403', '1', '33EGJXIDAduqWCSifVsPmVuMTJyLnquR', '2017-01-25 00:52:19', '2017-01-25 00:52:19');
INSERT INTO `persistences` VALUES ('406', '1', 'ZG0iCyctnYgcnkTIGV9H07fXE8s33bkd', '2017-01-26 03:05:52', '2017-01-26 03:05:52');
INSERT INTO `persistences` VALUES ('407', '9', 'bvZmNj56MW8nev4UoSExXEAjHnNtIYd7', '2017-01-26 04:38:45', '2017-01-26 04:38:45');
INSERT INTO `persistences` VALUES ('408', '1', 'ikDhS95qhS3n3tJo6foqD3TNlJ0hTMky', '2017-01-27 03:17:07', '2017-01-27 03:17:07');
INSERT INTO `persistences` VALUES ('410', '1', 'P4KSBwXBVdxIdzqUiEgofSv1K8YnWyUt', '2017-01-28 21:05:11', '2017-01-28 21:05:11');
INSERT INTO `persistences` VALUES ('415', '9', 'fHy1CJ8ZNAENNJRR2AbRxO1q3NgwDSya', '2017-01-28 23:36:57', '2017-01-28 23:36:57');
INSERT INTO `persistences` VALUES ('416', '1', 'vmnV6viMKGospBjxZN5KeJlr2JfieB80', '2017-02-08 10:30:34', '2017-02-08 10:30:34');
INSERT INTO `persistences` VALUES ('417', '1', '6FNWIuuJK9UlpO1Fic0Q0yTcYX7oRXfx', '2017-02-21 00:25:15', '2017-02-21 00:25:15');
INSERT INTO `persistences` VALUES ('418', '1', 'q9AXtBPt6ECX67wEusA6AAPWzSgSafLj', '2017-02-21 00:26:40', '2017-02-21 00:26:40');
INSERT INTO `persistences` VALUES ('420', '9', 'hS7zYXzFL2iFSdSWDW7ykwKc4khfQe2L', '2017-02-21 01:36:22', '2017-02-21 01:36:22');
INSERT INTO `persistences` VALUES ('421', '1', '9hjq43wKRKSBmxY2t3ZOejwktrvjSMEH', '2017-02-21 01:44:42', '2017-02-21 01:44:42');
INSERT INTO `persistences` VALUES ('422', '1', 'uLoVafnA4TgQrblXUJBhfA89DUWnD9Vq', '2017-02-23 01:37:53', '2017-02-23 01:37:53');
INSERT INTO `persistences` VALUES ('423', '1', '7OV8S0oBOY4rGatHhiaAzypR1DGDaZNa', '2017-02-24 01:41:41', '2017-02-24 01:41:41');
INSERT INTO `persistences` VALUES ('424', '1', 'UYUmFphBxnfpBAQyy5Uy2O60clf2v5uy', '2017-03-13 11:16:13', '2017-03-13 11:16:13');
INSERT INTO `persistences` VALUES ('425', '1', 'nVM4Mqyd6CqQyiK66KDV2RcpFlqC7iHK', '2017-03-13 11:53:52', '2017-03-13 11:53:52');
INSERT INTO `persistences` VALUES ('426', '1', 'h3pToQYIaz2jvgU4AMwUNjEc1EyTDj1Q', '2017-03-15 09:21:17', '2017-03-15 09:21:17');
INSERT INTO `persistences` VALUES ('427', '1', 'rmia1KYiKPpTgCceikY640kRsRQL24Uc', '2017-03-25 23:45:45', '2017-03-25 23:45:45');
INSERT INTO `persistences` VALUES ('428', '1', 'RkCnVMzn79IghbqS6Tol1iXpMovbLeym', '2017-03-26 22:09:41', '2017-03-26 22:09:41');
INSERT INTO `persistences` VALUES ('429', '1', '2RDU3tFlFxsv7L2cTLSgcpRGVbwjCW8G', '2017-03-28 02:49:51', '2017-03-28 02:49:51');
INSERT INTO `persistences` VALUES ('430', '1', 'A3NKM0u6WIRHFIPKWVrpRChKOWenNmNz', '2017-03-29 21:58:16', '2017-03-29 21:58:16');
INSERT INTO `persistences` VALUES ('431', '1', 'JjBwd9vjdA5lkf7Egmo2XDWkCm4wM3S6', '2017-03-30 02:35:37', '2017-03-30 02:35:37');
INSERT INTO `persistences` VALUES ('432', '1', 'TvgVvDLvHaMuIbcV5lvteo65rkYWTuzy', '2017-03-30 09:20:46', '2017-03-30 09:20:46');
INSERT INTO `persistences` VALUES ('433', '1', 'xXXlnxXtruorIKPg2zFRLQdNAbOPgulY', '2017-03-31 07:57:47', '2017-03-31 07:57:47');
INSERT INTO `persistences` VALUES ('435', '1', 'GfPMoIyrNnVSA43DaXYUoo5oJeEnaIKl', '2017-04-02 10:35:38', '2017-04-02 10:35:38');
INSERT INTO `persistences` VALUES ('436', '1', 'cln4DtiKEDuQxodFRDw4Lcra8cGQeIMz', '2017-04-02 21:17:12', '2017-04-02 21:17:12');
INSERT INTO `persistences` VALUES ('437', '1', 'gDbjgjlpQr6KOjlMh9pB4bHKi5cp1Wrc', '2017-04-04 09:40:08', '2017-04-04 09:40:08');
INSERT INTO `persistences` VALUES ('438', '1', 'FrI0pkXa4NwLpsn3Pi1PH3A0otWeGruU', '2017-04-04 11:19:27', '2017-04-04 11:19:27');
INSERT INTO `persistences` VALUES ('439', '1', 'beJtoHPg0Ht1NsKFVakpduMdidW8GpFm', '2017-04-05 10:27:48', '2017-04-05 10:27:48');
INSERT INTO `persistences` VALUES ('440', '1', '9ltPoQR91755nbeUztRi9xx8BY3cqz2J', '2017-04-06 10:39:20', '2017-04-06 10:39:20');
INSERT INTO `persistences` VALUES ('441', '1', 'UPyLjrn0wB0zzQ6zVKlxWCRPo9n73Vw1', '2017-04-06 11:29:11', '2017-04-06 11:29:11');
INSERT INTO `persistences` VALUES ('442', '1', 'YlEMN7x1ZBG3yPGYX2RVjd0iiwoPNWk4', '2017-04-10 02:15:13', '2017-04-10 02:15:13');
INSERT INTO `persistences` VALUES ('443', '9', '5pZReIfyiIz7EIjW3qfbRrmRWsMCV2xD', '2017-04-11 00:09:34', '2017-04-11 00:09:34');
INSERT INTO `persistences` VALUES ('447', '1', 'VrEBinxz1hTXXmiPVeefL138I4iSWQ52', '2017-04-11 22:43:34', '2017-04-11 22:43:34');
INSERT INTO `persistences` VALUES ('448', '1', 'kDXffNhbdLtlC2lNLnlf4HST4FI6kdal', '2017-04-13 00:37:44', '2017-04-13 00:37:44');
INSERT INTO `persistences` VALUES ('449', '1', 'lK9nPuYw3iWpNs3x5MbByORZxlWm8fim', '2017-04-13 00:51:37', '2017-04-13 00:51:37');
INSERT INTO `persistences` VALUES ('450', '9', '0aY6n6847RBlgl4dZT6e3IdzDL1sq0Xq', '2017-04-13 00:52:51', '2017-04-13 00:52:51');
INSERT INTO `persistences` VALUES ('452', '9', 'TABN9b6WT0CYjifUjMsRam5aERlRbjzZ', '2017-04-13 02:55:33', '2017-04-13 02:55:33');
INSERT INTO `persistences` VALUES ('453', '9', 'buJbqy4uYLHDQ3RdZGgliHXVjLAlAj8h', '2017-04-14 00:52:36', '2017-04-14 00:52:36');
INSERT INTO `persistences` VALUES ('455', '9', 'WZcJMFiKO3ZRhVgqVjLweteggAmdW7Tt', '2017-04-14 06:57:48', '2017-04-14 06:57:48');
INSERT INTO `persistences` VALUES ('456', '9', 'eBjaIKb8G8ugqfKIY9n5ArRJtHmu3Gsm', '2017-04-17 21:25:58', '2017-04-17 21:25:58');
INSERT INTO `persistences` VALUES ('457', '9', 'WS5SwpOuTeZAnYhQilFL0Xb0Y4b77N2h', '2017-04-17 21:33:45', '2017-04-17 21:33:45');
INSERT INTO `persistences` VALUES ('458', '9', '411nM5xC2dt3oMeSCiEl5I0lhz2Aqj8D', '2017-04-18 04:34:03', '2017-04-18 04:34:03');
INSERT INTO `persistences` VALUES ('460', '9', 'Djxgc5Jgai6TbvOk2rjzsNAPZWuTaKeH', '2017-04-19 00:55:58', '2017-04-19 00:55:58');
INSERT INTO `persistences` VALUES ('461', '9', 'qqD2PlrD3qJLRsYty7FHpDyrJQxIhiLO', '2017-04-19 11:09:04', '2017-04-19 11:09:04');
INSERT INTO `persistences` VALUES ('464', '9', 'xSxreVLlbTYg46jkRqTskBIPW89IDwpS', '2017-04-25 00:52:41', '2017-04-25 00:52:41');
INSERT INTO `persistences` VALUES ('465', '9', '14gp3LskVafODeQwTDw6h6hxgunp8mHf', '2017-04-25 01:45:52', '2017-04-25 01:45:52');
INSERT INTO `persistences` VALUES ('468', '9', 'kQ30zVbjLum6rqBwhy90Up4mcm6NcFSi', '2017-04-27 01:28:26', '2017-04-27 01:28:26');
INSERT INTO `persistences` VALUES ('469', '9', '3bqBJ0wnW7HOeeEfWLF2JoqeQ2PBzCHz', '2017-04-27 05:29:15', '2017-04-27 05:29:15');
INSERT INTO `persistences` VALUES ('472', '9', 'Z4INZw1dg8oynP4OD3tZmAY5hzOjPTEh', '2017-05-02 22:09:03', '2017-05-02 22:09:03');
INSERT INTO `persistences` VALUES ('478', '9', 'EdRqaQPyavSjqIcaq82t1vmc9f7mYcuu', '2017-05-09 10:20:40', '2017-05-09 10:20:40');
INSERT INTO `persistences` VALUES ('484', '9', 'Ri9Ea0mfx2LagbGD8BSyLt6fkgxJbfcr', '2017-05-10 04:20:23', '2017-05-10 04:20:23');
INSERT INTO `persistences` VALUES ('485', '9', '8Vj5ePjyRAICMixCletCXCDtP4HR2JBX', '2017-05-13 03:04:38', '2017-05-13 03:04:38');
INSERT INTO `persistences` VALUES ('488', '9', 'jmk6mdzwom7URjlYx9GHsb1JlZifHPSv', '2017-05-16 01:11:51', '2017-05-16 01:11:51');
INSERT INTO `persistences` VALUES ('489', '9', 'NTNI9SUXhMFjsw65hdWMpBoP07BV3y1q', '2017-05-24 00:42:10', '2017-05-24 00:42:10');
INSERT INTO `persistences` VALUES ('491', '1', 'd6f8zbo1u9lGbzYPuTFcQ4jwHm1d82KV', '2017-05-24 01:28:55', '2017-05-24 01:28:55');
INSERT INTO `persistences` VALUES ('492', '9', '90SqsYXsn0Vo2t3j0X3bqiLvjbvfJHPd', '2017-05-24 01:44:23', '2017-05-24 01:44:23');
INSERT INTO `persistences` VALUES ('493', '9', 'HbYU4JGhFiXXGwA7nharo80hoOg8xuC4', '2017-05-24 01:54:44', '2017-05-24 01:54:44');
INSERT INTO `persistences` VALUES ('494', '9', 'eB0gnJuRxuRFiBhjLfDFByoinXons3xZ', '2017-05-24 01:57:50', '2017-05-24 01:57:50');
INSERT INTO `persistences` VALUES ('495', '9', 'CuU8HSHiMhmMvb6qVDmcNsL0chZp6SWy', '2017-05-24 06:16:22', '2017-05-24 06:16:22');
INSERT INTO `persistences` VALUES ('496', '9', 'PWTkpZeVGSdtwt9IC0g0icmk1gFSlfGv', '2017-06-15 11:35:43', '2017-06-15 11:35:43');
INSERT INTO `persistences` VALUES ('497', '9', 's49fFnklRpF5OOS2jYXRoutH5VqEFewt', '2017-09-03 02:44:41', '2017-09-03 02:44:41');
INSERT INTO `persistences` VALUES ('499', '9', 'ZACd7QKPIDrMopxrvsAFCULxDo7PX2br', '2017-09-03 10:11:27', '2017-09-03 10:11:27');
INSERT INTO `persistences` VALUES ('500', '1', 'xVHdxpCRdZF2nDHP9lLeIoHpX2PXj5Yc', '2017-09-08 11:15:27', '2017-09-08 11:15:27');
INSERT INTO `persistences` VALUES ('501', '1', 'bW88sq8eaaw6u1QvEngEq5YVVQ2qhFzX', '2017-09-08 11:37:13', '2017-09-08 11:37:13');
INSERT INTO `persistences` VALUES ('502', '1', 'PlIhwjZkcOwqngtSzdRZIbtR0UGwXv6o', '2017-09-09 08:55:32', '2017-09-09 08:55:32');
INSERT INTO `persistences` VALUES ('503', '1', 'OyWtzOT01xuQEizw8ooxrqggOskOXisu', '2017-09-10 01:20:15', '2017-09-10 01:20:15');
INSERT INTO `persistences` VALUES ('504', '1', 'Z3KJTkiaygVFz1RpG5YGGHdpLCwPSzZI', '2017-09-10 09:39:51', '2017-09-10 09:39:51');
INSERT INTO `persistences` VALUES ('508', '9', 'BT1p2Bt9UqNx2JF1uBjJUQXTKiL3lejO', '2017-09-12 01:39:19', '2017-09-12 01:39:19');
INSERT INTO `persistences` VALUES ('514', '9', 'BKOgFtE1cNnrnCCR299Xh2ZRpa0L5iao', '2017-09-18 07:31:18', '2017-09-18 07:31:18');
INSERT INTO `persistences` VALUES ('515', '9', 'gdtOIPY5rI05ZHILzGZr0m4tkGAfCvcq', '2017-09-18 20:22:26', '2017-09-18 20:22:26');
INSERT INTO `persistences` VALUES ('516', '9', 'LlMhuZP8B47nJ4ORhq7uACPtaXdOv2yo', '2017-09-18 22:57:54', '2017-09-18 22:57:54');
INSERT INTO `persistences` VALUES ('517', '9', 'YMVZKtIuDyVFhS7v29nO48C5WsL8JjZF', '2017-09-19 03:08:13', '2017-09-19 03:08:13');
INSERT INTO `persistences` VALUES ('518', '9', 'BvJ12OugoJhgJs9isQL9br24C7YyTD8j', '2017-09-19 06:07:29', '2017-09-19 06:07:29');
INSERT INTO `persistences` VALUES ('519', '9', '8qcp6wNVPRBaCMpXkNQvolSpnNLDcUB7', '2017-09-19 22:46:13', '2017-09-19 22:46:13');
INSERT INTO `persistences` VALUES ('520', '9', 'TqibeIg2QSEsQDyC7LcK59VAKKMipkpn', '2017-09-26 04:12:02', '2017-09-26 04:12:02');
INSERT INTO `persistences` VALUES ('521', '9', 'a6R8PaA9vwIpGxN7MlLJuVUveYS1LpPH', '2017-09-26 07:46:18', '2017-09-26 07:46:18');
INSERT INTO `persistences` VALUES ('522', '9', 'WmzWXCLtADZ2X8xk6dEBRciyq1Ua7yiU', '2017-09-26 22:50:13', '2017-09-26 22:50:13');
INSERT INTO `persistences` VALUES ('523', '9', 'S2ojiPFjhkcqLoQ7Oi5R7lY4FGrmTJVN', '2017-09-26 22:55:28', '2017-09-26 22:55:28');
INSERT INTO `persistences` VALUES ('524', '9', 'kAl7L3eJgJYVHpbAl6WhEi8xMNnYPpnr', '2017-09-26 23:46:23', '2017-09-26 23:46:23');
INSERT INTO `persistences` VALUES ('525', '1', 'xLk8EQyXw0Sust6JfEFjOVGUuTQySRte', '2017-09-29 12:51:45', '2017-09-29 12:51:45');
INSERT INTO `persistences` VALUES ('526', '9', 'vdahqrLd0RHiCr5b8h6iKGDJFPdFAOe8', '2017-09-29 21:57:13', '2017-09-29 21:57:13');
INSERT INTO `persistences` VALUES ('527', '9', 'WS1ZNJrVSOndMKv6IynLlgtKzsSf8hKA', '2017-09-29 22:00:17', '2017-09-29 22:00:17');
INSERT INTO `persistences` VALUES ('528', '9', 'IxRsRJOY2jDlbgw5sWeiAjCvOa3kR1Ac', '2017-09-29 22:37:18', '2017-09-29 22:37:18');
INSERT INTO `persistences` VALUES ('529', '9', 'gwwpv2D4cG911t35wH4YvCcyoVQvRElK', '2017-09-29 22:39:02', '2017-09-29 22:39:02');
INSERT INTO `persistences` VALUES ('530', '9', 'G76hJghdimBSj1PHvhLiQPvcBZ22VJyo', '2017-09-29 22:57:20', '2017-09-29 22:57:20');
INSERT INTO `persistences` VALUES ('531', '9', 'pIl45I3RQuHsbg9DrTifyp7FmOFvXAbI', '2017-09-30 08:35:59', '2017-09-30 08:35:59');
INSERT INTO `persistences` VALUES ('532', '9', 'Pke5IPK4IxTUkF6XqVjnZFDPsw6wbQnB', '2017-09-30 08:38:08', '2017-09-30 08:38:08');
INSERT INTO `persistences` VALUES ('533', '9', 'pfGDyVgBbQCoyWNf2Ejn4xZwTozIkv3B', '2017-09-30 08:38:41', '2017-09-30 08:38:41');
INSERT INTO `persistences` VALUES ('534', '9', 'seMjDAG5MVSZnFzYleDHHSC9NaLUolGe', '2017-09-30 09:11:47', '2017-09-30 09:11:47');
INSERT INTO `persistences` VALUES ('535', '9', 'T33Id6bhSDDM44aYkwA9qnrqtNlJQXP4', '2017-09-30 12:24:47', '2017-09-30 12:24:47');
INSERT INTO `persistences` VALUES ('536', '9', 'LSn1x97nqG5yVzXBEWISLl1ja6m5Q08X', '2017-10-01 00:04:39', '2017-10-01 00:04:39');
INSERT INTO `persistences` VALUES ('537', '9', 'vdRsiiXnYqZbCxQWcaLCzthap7hU23dp', '2017-10-01 00:10:34', '2017-10-01 00:10:34');
INSERT INTO `persistences` VALUES ('538', '9', 'fhBlMrOEYQDO18asgB6gkwykQ8bZeRtX', '2017-10-02 01:58:05', '2017-10-02 01:58:05');
INSERT INTO `persistences` VALUES ('539', '9', 'zvuEqTNzSvCLChO6DIs4EfS52N7OWFff', '2017-10-02 01:59:07', '2017-10-02 01:59:07');
INSERT INTO `persistences` VALUES ('540', '9', 'FkpaUFI9FUh4gYyAjtfoh7bH5APYRovj', '2017-10-02 02:12:16', '2017-10-02 02:12:16');
INSERT INTO `persistences` VALUES ('541', '9', 'RzQufcstJuydJLBpgzhlqJXDsjDwFc6d', '2017-10-02 08:33:09', '2017-10-02 08:33:09');
INSERT INTO `persistences` VALUES ('542', '9', 'RfkaWBmwUmem7zCfjuAsmZLAbXkp5Ig2', '2017-10-02 12:09:40', '2017-10-02 12:09:40');
INSERT INTO `persistences` VALUES ('543', '9', 'Lwu6hoQfWQw6j8oLZqxP0ZjiaKquushe', '2017-10-03 12:22:31', '2017-10-03 12:22:31');
INSERT INTO `persistences` VALUES ('544', '9', 'QjjlkcEEKa1OdtyoQm4Pv9NzvrjWf6ex', '2017-10-05 06:58:43', '2017-10-05 06:58:43');
INSERT INTO `persistences` VALUES ('545', '9', 'uoSHV0CE9x4cyKbZ2SHst8VusHh9yI2W', '2017-10-05 12:44:15', '2017-10-05 12:44:15');
INSERT INTO `persistences` VALUES ('546', '9', 'yWRBL2TfCaOD3lTV0NZpbtIRzZmz3E2X', '2017-10-05 22:48:06', '2017-10-05 22:48:06');
INSERT INTO `persistences` VALUES ('547', '9', '1JtgEc5RNqJTEGA9PRmjYVA14l6i0HkO', '2017-10-05 22:50:34', '2017-10-05 22:50:34');
INSERT INTO `persistences` VALUES ('548', '9', 'T7VDc2KEcM6TzknmM9uUz34rjLDypGTU', '2017-10-06 05:30:44', '2017-10-06 05:30:44');
INSERT INTO `persistences` VALUES ('549', '9', 'mBN5iNDSOWFAby7SuxyrnjyhlBmREhzI', '2017-10-06 05:32:50', '2017-10-06 05:32:50');
INSERT INTO `persistences` VALUES ('550', '9', 'dk1YRLRE1JUo291Q3tirAMHuXLoYef2B', '2017-10-06 23:57:30', '2017-10-06 23:57:30');
INSERT INTO `persistences` VALUES ('551', '9', 'ry2Bxvz3m6tnd1obdzz14I87nk0v9S3q', '2017-10-07 04:56:29', '2017-10-07 04:56:29');
INSERT INTO `persistences` VALUES ('552', '9', '7oI0DCpaw0fKfqw4Yi1LCcNHn9md25Y7', '2017-10-07 09:36:25', '2017-10-07 09:36:25');
INSERT INTO `persistences` VALUES ('553', '9', 'UWLFqE78A4roavDtn4MaMU2UkEB6ZONU', '2017-10-13 22:28:45', '2017-10-13 22:28:45');
INSERT INTO `persistences` VALUES ('554', '9', 'AvgDO5mzCwq8ljBGPwU3OViHsRmk5LJJ', '2017-10-14 12:02:52', '2017-10-14 12:02:52');
INSERT INTO `persistences` VALUES ('555', '9', 'YPKsclCuQwF09CaFt44LeootzbaOt1SI', '2017-10-14 22:29:43', '2017-10-14 22:29:43');
INSERT INTO `persistences` VALUES ('556', '9', 'IZitIuzqvCI0OawtFT6lKAp06Gs7qp6C', '2017-10-16 10:52:06', '2017-10-16 10:52:06');
INSERT INTO `persistences` VALUES ('557', '9', 'XLCgtKFdE1gMAb9l9eDGeZgPh6abkk3V', '2017-10-17 06:27:45', '2017-10-17 06:27:45');
INSERT INTO `persistences` VALUES ('558', '9', 'Yi7Zx70bcJUU7vpps5VJvQBtZT4foCpr', '2017-10-17 13:25:09', '2017-10-17 13:25:09');
INSERT INTO `persistences` VALUES ('559', '9', 'HYRmxSGdoDTRPMqCP8n2yfBNUG5mXns2', '2017-10-17 13:31:39', '2017-10-17 13:31:39');
INSERT INTO `persistences` VALUES ('560', '9', 'ahqLDDxwrfdCKCFYObRnRVevKcJJNzL9', '2017-10-17 22:42:25', '2017-10-17 22:42:25');
INSERT INTO `persistences` VALUES ('561', '9', 'ck2Mn5nPgvkoYz3YARZ5Pn6rA051QDIo', '2017-10-18 10:52:50', '2017-10-18 10:52:50');
INSERT INTO `persistences` VALUES ('562', '9', 'yHN07TYMOwVTw2uatqPnJkJCw22AqyTo', '2017-10-19 00:55:04', '2017-10-19 00:55:04');
INSERT INTO `persistences` VALUES ('563', '9', 'I01jFrfGjSCS4oWyv8zKu4V97CRWFkgA', '2017-10-20 05:39:07', '2017-10-20 05:39:07');
INSERT INTO `persistences` VALUES ('564', '9', 'UvPDFfNlJNCkH2BocvFt7mBjIlECD0yM', '2017-10-23 23:57:55', '2017-10-23 23:57:55');
INSERT INTO `persistences` VALUES ('565', '9', 'VlEihMHC1c4hBsHkXQtjplZ7iAfe2F5U', '2017-10-24 02:57:47', '2017-10-24 02:57:47');
INSERT INTO `persistences` VALUES ('566', '9', 'o9yhTxKN6OWSh615hlWUbDR5qNlVzNs7', '2017-10-24 13:56:56', '2017-10-24 13:56:56');
INSERT INTO `persistences` VALUES ('567', '9', 'nOMjhVqxiujlsCCuCeS6w4jUBOi84mmX', '2017-10-24 14:20:12', '2017-10-24 14:20:12');
INSERT INTO `persistences` VALUES ('568', '9', '2fPEfJZsHVc6rqP4Y8MCLNtzRWBcQhxT', '2017-10-25 07:37:09', '2017-10-25 07:37:09');
INSERT INTO `persistences` VALUES ('569', '9', 'zvR46vim4xH94YWmNpFV6G6zgT2yuGrg', '2017-10-25 23:10:21', '2017-10-25 23:10:21');
INSERT INTO `persistences` VALUES ('570', '9', '2zTswKmr9G9dnDMGjWqS08aJLvyqWX1x', '2017-10-27 23:33:09', '2017-10-27 23:33:09');
INSERT INTO `persistences` VALUES ('571', '9', 'gbjNHN3Rbfa7BpmESzLF2YldvN72HBeM', '2017-10-28 09:38:57', '2017-10-28 09:38:57');
INSERT INTO `persistences` VALUES ('572', '9', '2UuMu7Jk3XOr0Lp1qJ8i1tcw5QGQahgO', '2017-10-31 10:27:16', '2017-10-31 10:27:16');
INSERT INTO `persistences` VALUES ('574', '9', 'K9Qr6Vgbtn8twa9ebWjJvNNkkhPWatYS', '2017-12-26 03:19:59', '2017-12-26 03:19:59');
INSERT INTO `persistences` VALUES ('576', '9', 'Bps39WAJHf6x9nEP2IBkuplsBjM6gLit', '2017-12-27 12:35:51', '2017-12-27 12:35:51');
INSERT INTO `persistences` VALUES ('577', '9', 'DUkEdTKANrqTCybnyP27nEfZsDcRCsU4', '2017-12-28 06:58:11', '2017-12-28 06:58:11');
INSERT INTO `persistences` VALUES ('579', '1', 'gwEPEK5AUypx8EW4cTqxYN6BHGvfJ9yc', '2017-12-28 08:03:21', '2017-12-28 08:03:21');
INSERT INTO `persistences` VALUES ('584', '9', 'cesY9MkupQ6U145waWYQ4HVS8DVwkvDd', '2017-12-28 08:46:57', '2017-12-28 08:46:57');
INSERT INTO `persistences` VALUES ('587', '10', 'FQAjzYxo6Q1jwUtnC4U9pkaaskqxOf3g', '2017-12-28 11:48:03', '2017-12-28 11:48:03');
INSERT INTO `persistences` VALUES ('589', '10', 'htDxNPzOHgsoYxfAg1vioBhbmlzlKBTz', '2017-12-28 11:50:30', '2017-12-28 11:50:30');
INSERT INTO `persistences` VALUES ('590', '9', '8kQrsqpSyTu4lrv3x1eC5gLvmDnMBfhK', '2017-12-29 11:57:16', '2017-12-29 11:57:16');
INSERT INTO `persistences` VALUES ('591', '9', 'f5FDNKxKnlMeQYGEb5odqUpIp1tRUFop', '2017-12-29 12:00:05', '2017-12-29 12:00:05');
INSERT INTO `persistences` VALUES ('592', '9', 'QdJRzz9eqOMX9teYtyiuKLWXOE4n4mOA', '2017-12-30 11:39:07', '2017-12-30 11:39:07');
INSERT INTO `persistences` VALUES ('594', '9', '0hJ5edWa6kmHkzFzaYJtxoC80BUGFKGy', '2017-12-31 03:27:13', '2017-12-31 03:27:13');
INSERT INTO `persistences` VALUES ('595', '9', 'wLZQm4WFgdA8Xtgx0Rt7GzyaJYif8RrC', '2017-12-31 06:50:45', '2017-12-31 06:50:45');
INSERT INTO `persistences` VALUES ('596', '9', 'idpTmk6y0wE6drnjltGHnC0DZYYsCCMC', '2017-12-31 11:12:20', '2017-12-31 11:12:20');
INSERT INTO `persistences` VALUES ('597', '9', 'mcg6PSzSCT4k02zp7pcNA2KhxI4CEM5B', '2017-12-31 20:27:47', '2017-12-31 20:27:47');
INSERT INTO `persistences` VALUES ('598', '9', 'FytzqGsNCSAxXCV4nmRTGFP4WmMv46sS', '2018-01-01 22:26:16', '2018-01-01 22:26:16');
INSERT INTO `persistences` VALUES ('599', '9', 'lWina6s3EU3UH2HLShe9Fcdg2xj63nzN', '2018-01-02 06:52:09', '2018-01-02 06:52:09');
INSERT INTO `persistences` VALUES ('600', '9', 'QUhe666JPKzaVV4Ur4PthXTALLU4BKN0', '2018-01-03 08:42:49', '2018-01-03 08:42:49');
INSERT INTO `persistences` VALUES ('601', '9', 'CdmkVmYAtYQTpZ1OkncQ4DTBAfMzKUtA', '2018-01-03 10:59:52', '2018-01-03 10:59:52');
INSERT INTO `persistences` VALUES ('602', '9', 'tH17GHidNSKwVEPkxS60FuZB9VYjBZN5', '2018-01-04 12:14:28', '2018-01-04 12:14:28');
INSERT INTO `persistences` VALUES ('603', '9', 'R41vAyFzCHyiqOXvuj111H0pYwHQ72Ze', '2018-01-05 03:36:01', '2018-01-05 03:36:01');
INSERT INTO `persistences` VALUES ('604', '9', 'rqC20hVOikfpNtFrBvtcEAWJtJQupz7M', '2018-01-05 03:38:38', '2018-01-05 03:38:38');
INSERT INTO `persistences` VALUES ('605', '9', '5JqvznBkQk3ELAM2bTMHd0FBeAHORt4V', '2018-01-05 10:18:59', '2018-01-05 10:18:59');
INSERT INTO `persistences` VALUES ('606', '9', 'xxWqOS7WFuSR7agyGsOIXOkLQdQlqUNV', '2018-01-06 07:41:00', '2018-01-06 07:41:00');
INSERT INTO `persistences` VALUES ('608', '9', 'mDVGqa9Xj9whlnASFINfJ8dMiQeS4d2D', '2018-01-07 08:45:37', '2018-01-07 08:45:37');
INSERT INTO `persistences` VALUES ('609', '1', '6bTxEgNcrge2qeymYxGeDl4i53ActgRh', '2018-01-14 06:54:30', '2018-01-14 06:54:30');
INSERT INTO `persistences` VALUES ('610', '1', 'fodEY90PIYVilzSRmUzgMavLnx8ot1Bf', '2018-01-16 07:59:51', '2018-01-16 07:59:51');
INSERT INTO `persistences` VALUES ('611', '1', 'TGyyV1URyXhCd37SaQJMHZbz0FSQqfwn', '2018-01-19 09:54:09', '2018-01-19 09:54:09');
INSERT INTO `persistences` VALUES ('612', '1', 'if2agsgCopVBHmkABT9whV5J84L9FzUw', '2018-01-26 07:59:13', '2018-01-26 07:59:13');
INSERT INTO `persistences` VALUES ('613', '1', 'MBt0gHt0sXx9MTH2yRMi3A7eO5QIxdW2', '2018-01-26 09:30:18', '2018-01-26 09:30:18');
INSERT INTO `persistences` VALUES ('614', '1', 'os3P8bBS3GDxFE5kP3ggpnjmYLStjvng', '2017-01-15 13:58:55', '2017-01-15 13:58:55');
INSERT INTO `persistences` VALUES ('615', '1', 'ppiMMLK3g1Ak0JfPJ4aUxmWhyErzttaS', '2018-01-27 23:18:09', '2018-01-27 23:18:09');
INSERT INTO `persistences` VALUES ('616', '1', 'Tr8rnTdLegAVeZnsSsRrEAYrZYlQdfdf', '2018-02-21 08:44:30', '2018-02-21 08:44:30');
INSERT INTO `persistences` VALUES ('617', '1', 'jo6vBtQvtvRwnk1iTv6wNXyjAJfOTs5S', '2018-02-26 00:21:15', '2018-02-26 00:21:15');
INSERT INTO `persistences` VALUES ('618', '1', 'WnDOCo50idwCpQXE1lRvebtzSO2XJAaI', '2018-03-01 09:58:41', '2018-03-01 09:58:41');
INSERT INTO `persistences` VALUES ('619', '1', 'K6J9Z4loo5KKXZRSv5c09yMeH11Qpyjo', '2018-03-02 00:10:40', '2018-03-02 00:10:40');
INSERT INTO `persistences` VALUES ('620', '1', 'V6fEIn9T7a038N9Bx8brRwO0sGD0dMmg', '2018-03-02 03:16:27', '2018-03-02 03:16:27');
INSERT INTO `persistences` VALUES ('621', '1', 'knEEyZDYYYJdO5wgu9DUHFhuULBmw8r3', '2018-03-03 22:18:01', '2018-03-03 22:18:01');
INSERT INTO `persistences` VALUES ('622', '1', 'WFE6YoykfG26I8n2tnGILMhqRsTpCARA', '2018-03-04 01:03:37', '2018-03-04 01:03:37');
INSERT INTO `persistences` VALUES ('623', '1', 'adbFy3Pf5ShFg8H4e7V4ahdAgC4F6e69', '2018-03-14 08:09:00', '2018-03-14 08:09:00');
INSERT INTO `persistences` VALUES ('624', '1', 'tl4XO2h7ZpqJnGF8wf7HqwsuMHONlU2A', '2018-03-14 09:17:28', '2018-03-14 09:17:28');
INSERT INTO `persistences` VALUES ('625', '1', 'jn71Dku2NJkXZM0IcAF88ky3xnWf5tHZ', '2018-03-16 08:22:14', '2018-03-16 08:22:14');
INSERT INTO `persistences` VALUES ('626', '1', 'CQJdcAA0Dt3B3Lqgd4DWqukaOgNeLn5P', '2018-03-16 08:43:03', '2018-03-16 08:43:03');
INSERT INTO `persistences` VALUES ('627', '1', 'c4XlSEfmCmA2gbYM9zurdLj2Vg67laoq', '2018-03-16 08:56:22', '2018-03-16 08:56:22');
INSERT INTO `persistences` VALUES ('628', '1', 't67f4XNitYSSlORNzyOZoAMvhHa1ofu4', '2018-03-16 09:04:50', '2018-03-16 09:04:50');
INSERT INTO `persistences` VALUES ('629', '1', '8vQXrOOkV4dF4GgjTGO3zQxSlOpo16XO', '2018-04-01 00:39:25', '2018-04-01 00:39:25');
INSERT INTO `persistences` VALUES ('630', '1', 'ReBQA7zXwfey5TM1rXoYPA6djL9Yr1yi', '2018-04-01 04:19:01', '2018-04-01 04:19:01');
INSERT INTO `persistences` VALUES ('631', '1', 'v5oUaXclOgmi4Agm1HIkVwhTBlMopS9y', '2018-04-01 10:53:47', '2018-04-01 10:53:47');
INSERT INTO `persistences` VALUES ('632', '1', 'xdz9C7ZWvUCHafNVa3eoetu91dyujEk3', '2018-04-01 20:41:24', '2018-04-01 20:41:24');
INSERT INTO `persistences` VALUES ('633', '1', 'u3upwHiHyMWxzYW1pt5vZmtPBy20trCc', '2018-04-04 08:13:46', '2018-04-04 08:13:46');
INSERT INTO `persistences` VALUES ('634', '1', 'ux616Cg3ZQwUOQPR19AlKEOPzcAoaR5d', '2018-04-04 08:17:05', '2018-04-04 08:17:05');
INSERT INTO `persistences` VALUES ('635', '1', 'X8uRZdGfuty8dJhJmmqJpoDVEe8qTSpm', '2018-04-05 08:44:55', '2018-04-05 08:44:55');
INSERT INTO `persistences` VALUES ('636', '1', 'VdsyWXkn0A9INY4uKC8m37VTQA24dkrQ', '2018-04-09 08:57:21', '2018-04-09 08:57:21');
INSERT INTO `persistences` VALUES ('637', '1', 'KwstnH2z1JiLqNA02CLkRJk7wGb3Kjya', '2018-04-10 08:57:59', '2018-04-10 08:57:59');
INSERT INTO `persistences` VALUES ('638', '1', 'L6LAtQAg7CUDNFVerYCaYj01P6pcBucf', '2018-04-18 09:46:09', '2018-04-18 09:46:09');
INSERT INTO `persistences` VALUES ('639', '1', 'tYtIfTpz1g9CazngFGqbnqN5JKoXBiQy', '2018-04-26 08:49:44', '2018-04-26 08:49:44');
INSERT INTO `persistences` VALUES ('640', '1', 'xQzzoBqnNJEsEfePYqLpEnkySBFDmO1k', '2018-04-26 08:54:22', '2018-04-26 08:54:22');
INSERT INTO `persistences` VALUES ('641', '1', 'UWPJsvsp78QapukNXbiOa7cDUr9m5Kcm', '2018-04-26 09:49:22', '2018-04-26 09:49:22');
INSERT INTO `persistences` VALUES ('642', '1', 'r3KWjYZnUBc6wYYnYpVz9CytqIj3ErI6', '2018-05-02 09:40:17', '2018-05-02 09:40:17');
INSERT INTO `persistences` VALUES ('643', '1', '4ZBatf2E8Rr6F9mIVuDj7tNDSDi8w0ye', '2018-05-03 09:48:30', '2018-05-03 09:48:30');
INSERT INTO `persistences` VALUES ('644', '1', 'iYRuq4HVbM7AiSAJXT0vRoEDN1tqemAe', '2018-05-05 13:19:30', '2018-05-05 13:19:30');
INSERT INTO `persistences` VALUES ('645', '1', 'K1BJ0oKdq1YkKgxFgCRBRUYyvG4LaiSd', '2018-05-06 02:05:19', '2018-05-06 02:05:19');
INSERT INTO `persistences` VALUES ('647', '1', '0fmvIweHpH4ahxu5ppZT7HFGWEPWGwyU', '2018-05-08 00:56:38', '2018-05-08 00:56:38');
INSERT INTO `persistences` VALUES ('648', '1', 'YQ9FVFgV747fInT39QkIdEzZhVXFF1Ci', '2018-05-08 01:57:43', '2018-05-08 01:57:43');
INSERT INTO `persistences` VALUES ('649', '1', 'nZ1nEuHVI3zsAaPwVHlhDHbBw0BEQQmr', '2018-05-11 08:14:35', '2018-05-11 08:14:35');
INSERT INTO `persistences` VALUES ('650', '9', 'X4WKWpLuJQbNd8yF9DVGH0NnVpQFoLCc', '2018-05-12 08:11:24', '2018-05-12 08:11:24');
INSERT INTO `persistences` VALUES ('651', '9', 'WmGbJ3EHxPsXQrXm6boaeQMH8nIuQVXO', '2018-05-12 09:00:08', '2018-05-12 09:00:08');
INSERT INTO `persistences` VALUES ('652', '9', 'qZmC462cld5WmJGTiiq6oxSFJs4uCW9s', '2018-05-13 08:37:56', '2018-05-13 08:37:56');
INSERT INTO `persistences` VALUES ('653', '9', 'odvFTAYb1dUTpofL0vSaL5ODG3EtsAUZ', '2018-05-13 08:52:58', '2018-05-13 08:52:58');
INSERT INTO `persistences` VALUES ('654', '9', 'fyQYhO0wM7pf77kUpAEotCTxmhvbBi94', '2018-05-13 23:17:13', '2018-05-13 23:17:13');
INSERT INTO `persistences` VALUES ('657', '9', '19PDnbxEC2xuwrF08lpZg4tj1iN2qRmT', '2018-05-14 09:27:53', '2018-05-14 09:27:53');
INSERT INTO `persistences` VALUES ('658', '9', 'W2PoE5yjnaVTiKhrHuBsMMP6ZExp1lO4', '2018-05-14 09:28:32', '2018-05-14 09:28:32');
INSERT INTO `persistences` VALUES ('659', '9', 'SvYvqwUHcQsulbk2akFR3spjXSLH3ioI', '2018-05-14 11:58:47', '2018-05-14 11:58:47');
INSERT INTO `persistences` VALUES ('660', '9', 'YIH90n5pDhU0aWA9UZdYRXFuqQlvqjDM', '2018-05-15 01:06:23', '2018-05-15 01:06:23');
INSERT INTO `persistences` VALUES ('661', '9', 'lBSOxAZ3DcnHy2KdZ9BlpLmTlAcBx2PK', '2018-05-15 02:13:22', '2018-05-15 02:13:22');
INSERT INTO `persistences` VALUES ('662', '9', 'aarLsARyIwokEiYhECnfvu1d43OViGwD', '2018-05-15 02:27:21', '2018-05-15 02:27:21');
INSERT INTO `persistences` VALUES ('663', '9', 'Ymrtm0waiwiao1kcLlBH0LolfaeXubWc', '2018-05-16 23:17:51', '2018-05-16 23:17:51');
INSERT INTO `persistences` VALUES ('664', '9', 'hItXTOWB3UvHzk7dqpFzJ8pZ4vcQOrj9', '2018-05-17 06:08:09', '2018-05-17 06:08:09');
INSERT INTO `persistences` VALUES ('665', '9', 'FQVoM2aV0br9g5O1ZSaM7JLSGRtifxfi', '2018-05-17 21:51:15', '2018-05-17 21:51:15');
INSERT INTO `persistences` VALUES ('666', '9', '6vC9Pzte395u8w5tIICbGxM3GRLfzZlp', '2018-05-18 23:44:03', '2018-05-18 23:44:03');
INSERT INTO `persistences` VALUES ('667', '9', 'ixPPGOTTzF3OMs8pm0M8OpHXiEf4LNsE', '2018-05-19 05:51:33', '2018-05-19 05:51:33');
INSERT INTO `persistences` VALUES ('668', '9', 'tIvJ4wpZPCl8FibxoJbN2gijxFUWmpYu', '2018-05-21 06:11:39', '2018-05-21 06:11:39');
INSERT INTO `persistences` VALUES ('669', '9', '1xo9OTVpamBdjaQtF5uLyPscSSaBkSZS', '2018-05-21 06:25:30', '2018-05-21 06:25:30');
INSERT INTO `persistences` VALUES ('670', '9', 'X5jubthq2cf0cbSmexaaESmAPcKbFq5B', '2018-05-22 03:31:16', '2018-05-22 03:31:16');
INSERT INTO `persistences` VALUES ('672', '9', 'zGhmobmZ5QzaMxrTigeteLFffOY7oQre', '2018-05-22 04:23:29', '2018-05-22 04:23:29');
INSERT INTO `persistences` VALUES ('674', '9', '8Sd6Fqd4fy9BMZnEF7BiUFpGVn95ltKB', '2018-05-22 06:13:08', '2018-05-22 06:13:08');
INSERT INTO `persistences` VALUES ('675', '9', 'nSd4SZfp5wGvOENikWxr8cm0f4WJqqhN', '2018-05-22 06:16:08', '2018-05-22 06:16:08');
INSERT INTO `persistences` VALUES ('676', '9', 'mo73auPuBe1WLm5HJQff1KxxvYPquJYX', '2018-05-22 06:19:42', '2018-05-22 06:19:42');
INSERT INTO `persistences` VALUES ('677', '9', 'VYVWyhduIxOMHCOov9YQxXNhamwsPC21', '2018-05-22 07:43:32', '2018-05-22 07:43:32');
INSERT INTO `persistences` VALUES ('678', '9', 'lUYSSggsqMyldyFqMzyeT8bKl3Lv2iRW', '2018-05-22 23:18:00', '2018-05-22 23:18:00');
INSERT INTO `persistences` VALUES ('679', '9', 'iaFHDDr4kel4fcE66KdAcOBeWxru0dxf', '2018-05-23 07:55:31', '2018-05-23 07:55:31');
INSERT INTO `persistences` VALUES ('680', '9', '428rtZ1u02VdfSJuJPeHa5FrzlcDHik5', '2018-05-23 21:39:26', '2018-05-23 21:39:26');
INSERT INTO `persistences` VALUES ('681', '9', 'xgldfcNSWhbIbMY9rSllJBVuSsfKrFEN', '2018-05-24 23:32:28', '2018-05-24 23:32:28');
INSERT INTO `persistences` VALUES ('682', '9', 'Xth92hCzYOAqIvoEHKEVIXaD1YWuS9D1', '2018-05-26 00:54:38', '2018-05-26 00:54:38');
INSERT INTO `persistences` VALUES ('683', '9', 'MVLkWtRsbzdONodFSo5CxUMrZ2idw5vn', '2018-05-26 08:24:29', '2018-05-26 08:24:29');
INSERT INTO `persistences` VALUES ('684', '9', 'Uz9BNtKLCNWILdsDEBUWRejSDtWV5ACV', '2018-05-26 23:27:01', '2018-05-26 23:27:01');
INSERT INTO `persistences` VALUES ('685', '9', 'UHUTUFzsC8dPJj3bx9clJUGEwMuHV4Un', '2018-05-27 00:31:52', '2018-05-27 00:31:52');
INSERT INTO `persistences` VALUES ('686', '9', 'e1mVn7Huepy6zVo9VAVVvqNS5xi6Masf', '2018-05-27 02:53:45', '2018-05-27 02:53:45');
INSERT INTO `persistences` VALUES ('687', '9', 'hqOmEdrJwHErhxQqQ3gZIOYtnkgvMd5F', '2018-05-27 06:19:04', '2018-05-27 06:19:04');
INSERT INTO `persistences` VALUES ('688', '9', '9SZS2EzXqzFUtMikd4tPDuyo2QLlrx6R', '2018-05-27 22:40:36', '2018-05-27 22:40:36');
INSERT INTO `persistences` VALUES ('689', '9', '05dECTp7TQawfbBKiOS4ToUOJVYmXmZ8', '2018-05-27 22:43:51', '2018-05-27 22:43:51');
INSERT INTO `persistences` VALUES ('690', '1', 'bObk9AyDsFjTWkxRfzbwCZwtconp2eli', '2018-05-27 22:56:28', '2018-05-27 22:56:28');
INSERT INTO `persistences` VALUES ('691', '1', 'iD79sTjzwi5HpI3BLncPl4LWVRSn5yfZ', '2018-05-28 00:39:50', '2018-05-28 00:39:50');
INSERT INTO `persistences` VALUES ('692', '9', 'weDYYpbXkrnZkYliPigqYgwmliUn0e3c', '2018-05-28 06:43:57', '2018-05-28 06:43:57');
INSERT INTO `persistences` VALUES ('693', '9', 'Wdxh7FvR5i9NsEqIt7JLrcXUVizLJYNW', '2018-05-28 11:25:51', '2018-05-28 11:25:51');
INSERT INTO `persistences` VALUES ('694', '9', 'fDF6oyGmWSBk7jaQlrEdsrOexE1p5B2W', '2018-05-29 23:46:57', '2018-05-29 23:46:57');
INSERT INTO `persistences` VALUES ('695', '9', '7TP0m2GRRhNlDIqmopo8MYhjC4Cl9lqR', '2018-05-30 12:00:08', '2018-05-30 12:00:08');
INSERT INTO `persistences` VALUES ('696', '9', 'aQtOiC0dkGgsOW8AzFgg02A61VZgVdDW', '2018-05-31 02:38:48', '2018-05-31 02:38:48');
INSERT INTO `persistences` VALUES ('697', '9', 'Hepu1wO9SBhNLTC0DeTrWHm3iBP8m4CH', '2018-05-31 05:14:48', '2018-05-31 05:14:48');
INSERT INTO `persistences` VALUES ('698', '9', 'JBztphQfzY7Ob1p3CEroikpP2KLCn7Gc', '2018-05-31 07:54:10', '2018-05-31 07:54:10');
INSERT INTO `persistences` VALUES ('699', '9', 'NMyDteJoHFLbjleMJ6iF4vqDWs0EXYUT', '2018-06-02 03:33:28', '2018-06-02 03:33:28');
INSERT INTO `persistences` VALUES ('700', '9', 'tsXSCxUfKjITxwX3p5Uc9nytIkNCklhu', '2018-06-02 05:23:27', '2018-06-02 05:23:27');
INSERT INTO `persistences` VALUES ('701', '9', 'VpLMNzKawgRdyilHngtKGVkeUDAogznm', '2018-06-03 01:57:27', '2018-06-03 01:57:27');
INSERT INTO `persistences` VALUES ('702', '9', 'RiBh7S1YpifxjYij9PZ39Y8eS14xIqkd', '2018-06-05 07:08:57', '2018-06-05 07:08:57');
INSERT INTO `persistences` VALUES ('703', '9', 'OWEpKarC3t21pLVc3hxuEf3HzgWqOQgS', '2018-06-06 22:13:05', '2018-06-06 22:13:05');
INSERT INTO `persistences` VALUES ('704', '9', 'q4iYVAh8sB5IAXuzH5vnYVYWrb0kZHNV', '2018-06-07 10:12:39', '2018-06-07 10:12:39');
INSERT INTO `persistences` VALUES ('705', '9', 'GuP3XEOedz158VfTG3UQ3gyrMUkX9sIO', '2018-06-08 00:52:51', '2018-06-08 00:52:51');
INSERT INTO `persistences` VALUES ('706', '9', 'w2EsppaK0MjgbLcEgii14O2XEsxtvRWt', '2018-06-08 09:33:38', '2018-06-08 09:33:38');
INSERT INTO `persistences` VALUES ('707', '9', 'UrYCzEYNC5JprWAXiIAtN1GOYrmY4XoT', '2018-06-09 02:35:14', '2018-06-09 02:35:14');
INSERT INTO `persistences` VALUES ('708', '9', '8GCPdcoCKfLnMPE2I64EllPgmvlEEY0w', '2018-06-09 03:16:13', '2018-06-09 03:16:13');
INSERT INTO `persistences` VALUES ('711', '1', 'HHT5Pta6BNDdUTPW9k6ynxfX0rbTSPyo', '2018-06-10 20:03:25', '2018-06-10 20:03:25');
INSERT INTO `persistences` VALUES ('712', '9', 'N6zv71eF2asEzX4uCLRHuCjN9BWR64LT', '2018-06-10 20:30:54', '2018-06-10 20:30:54');
INSERT INTO `persistences` VALUES ('713', '9', 'cteKGZ4iq7epmjqcrlUqp2aH3SeRGDJu', '2018-06-12 00:06:27', '2018-06-12 00:06:27');
INSERT INTO `persistences` VALUES ('714', '9', '783IXgjDrkdI7xLseccnDcVRluHPgbNL', '2018-06-12 00:06:45', '2018-06-12 00:06:45');
INSERT INTO `persistences` VALUES ('715', '9', 'Ve2QksBjrFnwx6HCIXG7gMbeDd0UdWiP', '2018-06-12 22:25:45', '2018-06-12 22:25:45');
INSERT INTO `persistences` VALUES ('716', '9', 'Uh4GsL2ZTcf3qQ3dWy7nwFERx4QzkTVq', '2018-06-13 23:23:20', '2018-06-13 23:23:20');
INSERT INTO `persistences` VALUES ('717', '9', '5gFKbiZ4uRCcrhcsHQgYS19wfqkq5VxD', '2018-06-14 02:39:09', '2018-06-14 02:39:09');
INSERT INTO `persistences` VALUES ('718', '9', 'MG88uqBc2Sme4mehom1cdWlIA1TPrAqm', '2018-06-14 05:42:55', '2018-06-14 05:42:55');
INSERT INTO `persistences` VALUES ('719', '9', 'thyCOCAqUykA6GQbXHtEEdjsanIrdTHO', '2018-06-15 00:54:45', '2018-06-15 00:54:45');
INSERT INTO `persistences` VALUES ('720', '9', '7Dd7Qun2cH7LlgtJMBHJuwCsYCzLZHfS', '2018-06-15 04:57:50', '2018-06-15 04:57:50');
INSERT INTO `persistences` VALUES ('721', '9', 'YP1vXguusMJCB7OOAvzSSeE9Lzyo6LcZ', '2018-06-15 11:19:26', '2018-06-15 11:19:26');
INSERT INTO `persistences` VALUES ('723', '9', 'svLMfQYP1DNUuL0zXikMSNGyUSKFL8WT', '2018-06-16 14:48:28', '2018-06-16 14:48:28');
INSERT INTO `persistences` VALUES ('724', '9', '4ExNzyejkrEkmnlpOpKN4Du23xuKCEwG', '2018-06-16 19:21:28', '2018-06-16 19:21:28');
INSERT INTO `persistences` VALUES ('725', '9', 'p7lMF8KwjQvtsLqMQCgEIWe5OkidiJ9s', '2018-06-16 22:46:18', '2018-06-16 22:46:18');
INSERT INTO `persistences` VALUES ('726', '9', 'cSHcRNXFcwmu0nVQaISBmPnZ4Qbt5ucB', '2018-06-17 03:34:50', '2018-06-17 03:34:50');
INSERT INTO `persistences` VALUES ('727', '9', 'BLkjmnSoZ9uErNCDThNbBmBQZMrow08a', '2018-06-20 06:51:14', '2018-06-20 06:51:14');
INSERT INTO `persistences` VALUES ('728', '9', 'OHg1H1MU9puJnywl5l25hg1xTaMxNHVk', '2018-06-20 07:57:24', '2018-06-20 07:57:24');
INSERT INTO `persistences` VALUES ('729', '9', 'xS5paetAA7lhryAJnOmqcs2AfGXNfl6N', '2018-06-22 12:55:17', '2018-06-22 12:55:17');
INSERT INTO `persistences` VALUES ('730', '9', 'gmiURBI5Zyuk2iXP3tqVcyrtTVEQCnxw', '2018-06-22 23:07:42', '2018-06-22 23:07:42');
INSERT INTO `persistences` VALUES ('732', '9', 'HUZmdlQzYnw4JsB0GNxSUY0gehgyC86m', '2018-06-23 20:48:34', '2018-06-23 20:48:34');
INSERT INTO `persistences` VALUES ('733', '9', 'MpgfBxJqYCmGsFXteuygas8VE0Qw6lDx', '2018-06-26 08:24:12', '2018-06-26 08:24:12');
INSERT INTO `persistences` VALUES ('734', '9', 'V9EMPAFjrYt7uaKE3WB2LfaA87ISdoPk', '2018-06-26 20:21:08', '2018-06-26 20:21:08');
INSERT INTO `persistences` VALUES ('735', '9', 'spjdn5MAWSctQHdEqlFm0wjC2e5WKYVh', '2018-06-28 03:42:56', '2018-06-28 03:42:56');
INSERT INTO `persistences` VALUES ('736', '9', 'UMpyojtjErdM7na15jViPJWzY2tNiRim', '2018-06-30 02:15:56', '2018-06-30 02:15:56');
INSERT INTO `persistences` VALUES ('737', '9', 'E8oOrxYC7qrhqDOWnTWeGx0BP2tk06fg', '2018-06-30 03:13:59', '2018-06-30 03:13:59');
INSERT INTO `persistences` VALUES ('738', '9', 'NkrljapelkeHlR8A1DZvwD1L3jDcejtl', '2018-06-30 09:25:02', '2018-06-30 09:25:02');
INSERT INTO `persistences` VALUES ('739', '9', 'XhGj2beootX7tMkt7chryC6noey9XF0z', '2018-06-30 10:03:19', '2018-06-30 10:03:19');
INSERT INTO `persistences` VALUES ('740', '9', '2QyyqdZJOwSzBXraywOPszYvIirinBTX', '2018-07-01 02:01:27', '2018-07-01 02:01:27');
INSERT INTO `persistences` VALUES ('741', '9', 'OMcBHDy2jA5myDkLmeN7TjgPWGJdE0Sr', '2018-07-05 01:51:58', '2018-07-05 01:51:58');
INSERT INTO `persistences` VALUES ('742', '9', 'xHdh0mtKZf7AHK1fNRATSHo7QARsWLWY', '2018-07-05 05:28:21', '2018-07-05 05:28:21');
INSERT INTO `persistences` VALUES ('744', '9', 'SLr1hAJKv04hHU0uJIvPOGhL0nBXgRHC', '2018-07-05 21:01:29', '2018-07-05 21:01:29');
INSERT INTO `persistences` VALUES ('745', '9', 'BnwFTb5NvfivEUWfTS8zbe86M2FrCKut', '2018-07-05 23:11:53', '2018-07-05 23:11:53');
INSERT INTO `persistences` VALUES ('746', '9', 'Lts5n545MrA3Gu6PnYBcsDJummp4Y9ly', '2018-07-06 02:28:28', '2018-07-06 02:28:28');
INSERT INTO `persistences` VALUES ('748', '9', 'Ab30o5DzoAEkHFqZEebtzQoBCzL1U366', '2018-07-06 12:21:24', '2018-07-06 12:21:24');
INSERT INTO `persistences` VALUES ('749', '9', 'ibUg3tU5wz8UKrUBkxna0goHSp9SQegi', '2018-07-07 04:54:10', '2018-07-07 04:54:10');
INSERT INTO `persistences` VALUES ('751', '9', 'Uji4LEjHlMom7ILfqJU8Bq06HXWaVTo3', '2018-07-07 23:52:59', '2018-07-07 23:52:59');
INSERT INTO `persistences` VALUES ('752', '9', 'BsGOnv1kUxJXqBYTfqeREuBJRNpr24Wh', '2018-07-08 22:30:03', '2018-07-08 22:30:03');
INSERT INTO `persistences` VALUES ('753', '9', 'ajJSTukt3kLHx9dDxwL9kv9uwFr8cAlg', '2018-07-08 22:41:48', '2018-07-08 22:41:48');
INSERT INTO `persistences` VALUES ('754', '9', 't106WBCLGyTSsUv69l1EqMKguDox6uGI', '2018-07-11 06:15:12', '2018-07-11 06:15:12');
INSERT INTO `persistences` VALUES ('755', '9', 'razjjQLsVXpefUAPBtHmsZxLlCZg4iRn', '2018-07-12 21:57:29', '2018-07-12 21:57:29');
INSERT INTO `persistences` VALUES ('756', '9', 'RVleoA78hXko8ZyzXHacu7YnEAlqnlWL', '2018-07-13 03:46:59', '2018-07-13 03:46:59');
INSERT INTO `persistences` VALUES ('757', '9', 'aRwOyVS3pETVTNXnEF8m8xKTzk21LNLE', '2018-07-13 22:31:57', '2018-07-13 22:31:57');
INSERT INTO `persistences` VALUES ('758', '9', 'hitHa96dMFUajABLkoubrB1wZLRRWZVI', '2018-07-14 01:43:05', '2018-07-14 01:43:05');
INSERT INTO `persistences` VALUES ('759', '9', 'ljFggJZ2pPmnFFaa9WKc8yDEAONNFgeD', '2018-07-17 03:54:48', '2018-07-17 03:54:48');
INSERT INTO `persistences` VALUES ('760', '9', 'eQOCKaUftgrzcq8rD97EvkTZl975OxXs', '2018-07-18 11:14:27', '2018-07-18 11:14:27');
INSERT INTO `persistences` VALUES ('761', '9', 'ShSiqWME5yKt9xlMjLk60N7MQRKd72Nb', '2018-07-19 02:02:28', '2018-07-19 02:02:28');
INSERT INTO `persistences` VALUES ('762', '9', 'S8yzNyWFB9fhTugTgFHR7MhujCXTORf2', '2018-07-19 20:44:41', '2018-07-19 20:44:41');
INSERT INTO `persistences` VALUES ('763', '9', '69msAqNyEoONo5RfNvEWpjvYxkmtykHS', '2018-07-19 22:06:14', '2018-07-19 22:06:14');
INSERT INTO `persistences` VALUES ('764', '9', 'gZU95o6HdO2ZgHZg5bmQMUVVp3xGiBHq', '2018-07-20 00:21:46', '2018-07-20 00:21:46');
INSERT INTO `persistences` VALUES ('765', '9', 'FNt2rCkE9ccoUcsF5bRC0tNstcSskUgO', '2018-07-20 04:29:40', '2018-07-20 04:29:40');
INSERT INTO `persistences` VALUES ('766', '9', 'RQqxkaiCsBefNRYG9dhagN3owiBYEufE', '2018-07-21 04:29:04', '2018-07-21 04:29:04');
INSERT INTO `persistences` VALUES ('767', '9', 'QXzoqTnOtEX9Eb4lwfv2mOmdZUD8RFc1', '2018-07-21 21:19:25', '2018-07-21 21:19:25');
INSERT INTO `persistences` VALUES ('768', '9', 'RrN6UAl6soolzVQvfljNBVvDHYRe4jvV', '2018-07-24 11:24:16', '2018-07-24 11:24:16');
INSERT INTO `persistences` VALUES ('769', '9', 'jGzZDUnvYlyHa3V5HzZJwW3aPhJrug5n', '2018-07-25 00:10:01', '2018-07-25 00:10:01');
INSERT INTO `persistences` VALUES ('770', '9', 'ZKP2N9NpwHGwcsUjjocjNZhrFjMh33RY', '2018-07-25 09:05:52', '2018-07-25 09:05:52');
INSERT INTO `persistences` VALUES ('771', '9', 'lINtYOads2tj03oO5ldJ0Q9W3ifzLNth', '2018-07-25 21:36:38', '2018-07-25 21:36:38');
INSERT INTO `persistences` VALUES ('773', '1', 'cT1nEPd9ZR1H4mmAqxMx7Kq07fIM3GMW', '2018-07-26 22:22:21', '2018-07-26 22:22:21');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'registered', '    Registered    ', '{\"front\":true,\"my-account\":true}', '1', '2015-10-26 22:16:31', '2015-12-19 15:54:02');
INSERT INTO `roles` VALUES ('2', 'administrator', '      Administrator      ', '{\"admin\":true,\"my-account\":true}', '1', '2015-12-19 12:30:47', '2015-12-19 15:57:50');
INSERT INTO `roles` VALUES ('3', 'sdc-admin', 'SDC ADMIN', '{\"admin\":true}', '1', '2017-01-24 01:04:57', '2017-09-12 01:38:11');

-- ----------------------------
-- Table structure for role_users
-- ----------------------------
DROP TABLE IF EXISTS `role_users`;
CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_users
-- ----------------------------
INSERT INTO `role_users` VALUES ('9', '2', '2018-05-14 08:28:58', '2018-05-14 08:28:58');
INSERT INTO `role_users` VALUES ('9', '3', '2018-05-14 08:28:58', '2018-05-14 08:28:58');
INSERT INTO `role_users` VALUES ('10', '1', '2017-12-28 08:10:24', '2017-12-28 08:10:24');
INSERT INTO `role_users` VALUES ('11', '1', '2018-04-04 08:22:12', '2018-04-04 08:22:12');
INSERT INTO `role_users` VALUES ('11', '3', '2018-04-04 08:22:13', '2018-04-04 08:22:13');

-- ----------------------------
-- Table structure for sliders
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `expire_date` date NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `sliders_user_id_foreign` (`user_id`),
  CONSTRAINT `sliders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sliders
-- ----------------------------
INSERT INTO `sliders` VALUES ('2', 'Dahata Nopeni Inna - Prasanna Thakshila', '/core/storage/uploads/images/slider/slider-20180616015109.jpg', 'http://www.desawana.com/video/30', '<p>-</p>', '2018-09-08', '9', '2018-06-16 01:51:09', '2018-06-16 01:51:09');
INSERT INTO `sliders` VALUES ('5', 'Aulak Nane - Nalin Wijayasinghe Official Music Video', '/core/storage/uploads/images/slider/slider-20180616150810.jpg', 'http://www.desawana.com/video/34', '', '2018-06-30', '9', '2018-06-17 03:38:10', '2018-07-05 12:34:49');
INSERT INTO `sliders` VALUES ('6', 'Cybertech Int Advertisement', '/core/storage/uploads/images/slider/slider-20180622003226.png', 'https://www.facebook.com/cybertechInt.lk/', '', '2018-12-31', '9', '2018-06-22 13:02:26', '2018-06-22 13:02:26');
INSERT INTO `sliders` VALUES ('7', 'Obe Adare - Sameera Chathuranga Official Music Video', '/core/storage/uploads/images/slider/slider-20180625200340.jpg', 'http://www.desawana.com/video/36', '', '2018-07-07', '9', '2018-06-26 08:33:40', '2018-06-26 08:36:37');

-- ----------------------------
-- Table structure for slider_song
-- ----------------------------
DROP TABLE IF EXISTS `slider_song`;
CREATE TABLE `slider_song` (
  `slider_id` int(10) unsigned NOT NULL,
  `song_id` int(10) unsigned NOT NULL,
  KEY `slider_song_slider_id_foreign` (`slider_id`),
  KEY `slider_song_song_id_foreign` (`song_id`),
  CONSTRAINT `slider_song_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `slider_song_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slider_song
-- ----------------------------
INSERT INTO `slider_song` VALUES ('2', '36');
INSERT INTO `slider_song` VALUES ('5', '46');
INSERT INTO `slider_song` VALUES ('6', '11');
INSERT INTO `slider_song` VALUES ('7', '40');

-- ----------------------------
-- Table structure for songs
-- ----------------------------
DROP TABLE IF EXISTS `songs`;
CREATE TABLE `songs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `album_art` text COLLATE utf8_unicode_ci NOT NULL,
  `video_id` int(10) unsigned DEFAULT NULL,
  `lyrics_id` int(10) unsigned DEFAULT NULL,
  `audio_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `audio_download` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `songs_video_id_foreign` (`video_id`),
  KEY `songs_lyrics_id_foreign` (`lyrics_id`),
  KEY `songs_audio_id_foreign` (`audio_id`),
  KEY `songs_user_id_foreign` (`user_id`),
  CONSTRAINT `songs_audio_id_foreign` FOREIGN KEY (`audio_id`) REFERENCES `audio` (`id`),
  CONSTRAINT `songs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of songs
-- ----------------------------
INSERT INTO `songs` VALUES ('1', 'Man Pathanawa (Samu Aran Ya Yuthui)', 'core/storage/uploads/images/album_art/album_art-20180511195140.png', '1', '0', '3', '2018-05-12 08:21:40', '2018-07-27 02:49:13', '1', '9', '', '', '61');
INSERT INTO `songs` VALUES ('2', 'Heenayakda Me', 'core/storage/uploads/images/album_art/album_art-20180511204012.png', '0', '0', '4', '2018-05-12 09:10:12', '2018-07-26 19:53:06', '0', '9', '', '', '78');
INSERT INTO `songs` VALUES ('3', 'Saththai Oya (Man Witharada Adare Kale)', 'core/storage/uploads/images/album_art/album_art-20180511212003.png', '2', '0', '5', '2018-05-12 09:50:03', '2018-07-27 02:49:43', '0', '9', '', '', '82');
INSERT INTO `songs` VALUES ('6', 'Me Nihanda Bhawaye', 'core/storage/uploads/images/album_art/album_art-20180513204121.png', '0', '0', '8', '2018-05-14 09:11:21', '2018-07-26 21:43:19', '0', '9', '', '', '88');
INSERT INTO `songs` VALUES ('7', 'Nolabena Senehe New Version', 'core/storage/uploads/images/album_art/album_art-20180513204750.png', '0', '0', '9', '2018-05-14 09:17:50', '2018-07-27 02:51:15', '0', '9', '', '', '226');
INSERT INTO `songs` VALUES ('8', 'Kawadahari Wetahei', 'core/storage/uploads/images/album_art/album_art-20180513210702.png', '0', '0', '7', '2018-05-14 09:37:02', '2018-07-27 02:53:09', '0', '9', '', '', '90');
INSERT INTO `songs` VALUES ('9', 'Oya Nathuwa Dannam Mata', 'core/storage/uploads/images/album_art/album_art-20180513211328.png', '0', '0', '10', '2018-05-14 09:43:28', '2018-07-27 02:50:56', '0', '9', '', '', '190');
INSERT INTO `songs` VALUES ('10', 'Adarei Katawath Nathi Tharam Kiya', 'core/storage/uploads/images/album_art/album_art-20180514125657.png', '5', '0', '11', '2018-05-15 01:26:57', '2018-07-26 01:49:40', '0', '9', '', '', '56');
INSERT INTO `songs` VALUES ('11', 'Adarei Wasthu', 'core/storage/uploads/images/album_art/album_art-20180514131210.png', '4', '0', '12', '2018-05-15 01:42:10', '2018-07-23 16:18:27', '0', '9', '', '', '67');
INSERT INTO `songs` VALUES ('12', 'Sudu (Mathu Dineka)', 'core/storage/uploads/images/album_art/album_art-20180521170338.png', '6', '0', '15', '2018-05-22 05:33:38', '2018-07-26 22:35:41', '0', '9', '', '', '435');
INSERT INTO `songs` VALUES ('13', 'Sansara Sihine', 'core/storage/uploads/images/album_art/album_art-20180521181303.png', '0', '0', '16', '2018-05-22 06:43:03', '2018-07-23 15:27:27', '0', '9', '', '', '55');
INSERT INTO `songs` VALUES ('14', 'Saaritha', 'core/storage/uploads/images/album_art/album_art-20180521184134.png', '38', '0', '17', '2018-05-22 07:11:34', '2018-07-26 01:28:53', '0', '9', 'Saaritha Viraj Perera Official Music Video 2018', 'viraj perera, saritha, saaritha, viraj perera new song, mage adarema aparade, saritha viraj perera, sinhala new songs, new sinhala song, official music video, viraj new song, new music videos 2018, new sinhala songs 2018, saaritha viraj, saritha viraj perera video song download, saritha viraj perera, saritha viraj perera mp4, saritha viraj perera lyrics, saritha viraj perera live, saritha viraj perera song lyrics, saritha viraj perera dj mp3,', '252');
INSERT INTO `songs` VALUES ('15', 'Saragaye (Niya Rata Mawanawa)', 'core/storage/uploads/images/album_art/album_art-20180521191255.png', '7', '0', '18', '2018-05-22 07:42:55', '2018-07-26 01:46:45', '0', '9', '', 'sanuka wickramasinghe mp3 download, sansara sihine download mp3, Saragaye mp3,', '114');
INSERT INTO `songs` VALUES ('17', 'Akeekaruma Man Udura', 'core/storage/uploads/images/album_art/album_art-20180525124022.png', '0', '0', '13', '2018-05-26 01:10:22', '2018-07-22 19:34:08', '1', '9', '', '', '64');
INSERT INTO `songs` VALUES ('18', 'Waradak Kiyanne Na', 'core/storage/uploads/images/album_art/album_art-20180526144125.png', '9', '0', '14', '2018-05-27 03:11:25', '2018-07-23 15:36:06', '0', '9', '', '', '63');
INSERT INTO `songs` VALUES ('19', 'Ma Hara Giya Dine', 'core/storage/uploads/images/album_art/album_art-20180529115202.png', '0', '0', '19', '2018-05-30 00:22:02', '2018-07-17 04:32:11', '1', '9', '', 'dimanka wellalage, sajith v chathuranga, dimanka, radeesh vandabona, dimanka wellalage songs, music video, dimanka wellalage songs youtube, dimanka wellalage songs lyrics, dimanka wellalage songs chords, dimanka wellalage songs ananmanan.lk, dimanka wellalage video songs, dimanka wellalage new video songs, dimanka wellalage dj songs, dimanka wellalage new video 2017, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show,', '10');
INSERT INTO `songs` VALUES ('20', 'Amathaka Karanna Ba', 'core/storage/uploads/images/album_art/album_art-20180529115805.png', '10', '0', '20', '2018-05-30 00:28:06', '2018-07-18 23:11:21', '1', '9', '', '', '20');
INSERT INTO `songs` VALUES ('21', 'Arayum Karanne', 'core/storage/uploads/images/album_art/album_art-20180529120634.png', '28', '0', '21', '2018-05-30 00:36:34', '2018-06-30 10:16:35', '1', '9', '', '', '3');
INSERT INTO `songs` VALUES ('22', 'Awasan Pema Mage', 'core/storage/uploads/images/album_art/album_art-20180529125613.png', '11', '0', '22', '2018-05-30 01:26:13', '2018-07-23 15:39:41', '1', '9', '', '', '12');
INSERT INTO `songs` VALUES ('23', 'Durin Hinda Ma', 'core/storage/uploads/images/album_art/album_art-20180529130225.png', '12', '0', '23', '2018-05-30 01:32:25', '2018-07-23 15:39:14', '1', '9', '', '', '13');
INSERT INTO `songs` VALUES ('24', 'Kiyabu Lathawe', 'core/storage/uploads/images/album_art/album_art-20180529131455.png', '13', '0', '24', '2018-05-30 01:44:55', '2018-07-25 22:22:21', '0', '9', '', '', '65');
INSERT INTO `songs` VALUES ('25', 'Ma Me Tharam Handawala', 'core/storage/uploads/images/album_art/album_art-20180529132514.png', '15', '0', '26', '2018-05-30 01:55:15', '2018-07-17 04:33:46', '1', '9', '', 'Ma Me Tharam Handawala, Ma Me Tharam Handawala mp3, Ma Me Tharam Handawala mp3 download, Ma Me Tharam Handawala song, ', '7');
INSERT INTO `songs` VALUES ('26', 'Me Hitha Langa', 'core/storage/uploads/images/album_art/album_art-20180529132958.png', '16', '0', '27', '2018-05-30 01:59:58', '2018-06-30 11:06:13', '1', '9', '', 'Me Hitha Langa, Me Hitha Langa song, Me Hitha Langa song download, Me Hitha Langa mp3, Me Hitha Langa mp3 download,  ', '16');
INSERT INTO `songs` VALUES ('27', 'Nidukin Inu Mana', 'core/storage/uploads/images/album_art/album_art-20180529133205.png', '17', '0', '28', '2018-05-30 02:02:05', '2018-07-08 04:10:04', '1', '9', '', '', '4');
INSERT INTO `songs` VALUES ('28', 'Oba Ekka Mama', 'core/storage/uploads/images/album_art/album_art-20180529134744.png', '18', '0', '29', '2018-05-30 02:17:44', '2018-06-30 11:00:37', '1', '9', '', 'Oba Ekka Mama, Oba Ekka Mama song, Oba Ekka Mama song download, Oba Ekka Mama song mp3, Oba Ekka Mama song download, ', '5');
INSERT INTO `songs` VALUES ('29', 'Obamada Me Hithata Mage', 'core/storage/uploads/images/album_art/album_art-20180529134946.png', '19', '0', '30', '2018-05-30 02:19:46', '2018-06-30 10:58:38', '1', '9', '', 'Obamada Me Hithata Mage, Obamada Me Hithata Mage song, Obamada Me Hithata Mage mp3, Obamada Me Hithata Mage song mp3, Obamada Me Hithata Mage song mp3 download, ', '3');
INSERT INTO `songs` VALUES ('30', 'Para Kiyana Tharukawi', 'core/storage/uploads/images/album_art/album_art-20180529135638.png', '20', '0', '31', '2018-05-30 02:26:38', '2018-06-30 10:46:09', '1', '9', '', 'Para Kiyana Tharukawi, Para Kiyana Tharukawi song, Para Kiyana Tharukawi mp3, Para Kiyana Tharukawi song download, Para Kiyana Tharukawi mp3 song download, ', '7');
INSERT INTO `songs` VALUES ('31', 'Senehasa Bidunath', 'core/storage/uploads/images/album_art/album_art-20180529135741.png', '21', '0', '32', '2018-05-30 02:27:41', '2018-06-30 10:38:54', '1', '9', '', 'Senehasa Bidunath, Senehasa Bidunath song, Senehasa Bidunath mp3,', '5');
INSERT INTO `songs` VALUES ('32', 'Awasana Premayai Mage', 'core/storage/uploads/images/album_art/album_art-20180529135849.png', '22', '0', '33', '2018-05-30 02:28:49', '2018-07-23 15:37:04', '0', '9', '', '', '33');
INSERT INTO `songs` VALUES ('33', 'Maa Nisa Pa Sina', 'core/storage/uploads/images/album_art/album_art-20180529140419.png', '0', '0', '34', '2018-05-30 02:34:19', '2018-07-05 22:57:32', '1', '9', '', 'Maa Nisa Pa Sina, Maa Nisa Pa Sina song, Maa Nisa Pa Sina mp3, Maa Nisa Pa Sina mp3 download, Maa Nisa Pa Sina song video, ', '15');
INSERT INTO `songs` VALUES ('34', 'Wenna Thiyena', 'core/storage/uploads/images/album_art/album_art-20180529140526.png', '24', '0', '35', '2018-05-30 02:35:26', '2018-06-30 10:32:56', '1', '9', '', '', '36');
INSERT INTO `songs` VALUES ('35', 'Diwranna Behe Neda', 'core/storage/uploads/images/album_art/album_art-20180530141724.png', '29', '0', '36', '2018-05-31 02:47:24', '2018-07-26 20:23:04', '0', '9', '', 'Diwranna Behe Neda Thushara Joshap, Diwranna Behe Neda Thushara Sandakelum, Diwranna Behe Neda (Hadawathe Niruwatha), Thushara Sandakelum New Song, Thushara New Songs, Sahara Flash, Diwranna Behe Neda Mp3, Diwranna Behe Neda Mp3 Download', '3730');
INSERT INTO `songs` VALUES ('36', 'Dahata Nopeni Inna', 'core/storage/uploads/images/album_art/album_art-20180601155328.png', '30', '2', '37', '2018-06-02 04:23:28', '2018-07-26 18:36:49', '0', '9', 'Kind of a song that will wet your tears sung by Prasanna Thakshila to a heart melting melody of Rusiru Dulshan and lyrics by Yasith Dulshan...', 'Dahata Nopeni Inna, Dahata Nopeni Inna song, Dahata Nopeni Inna song download, Dahata Nopeni Inna video song, Dahata Nopeni Inna song lyrics, Dahata Nopeni Inna song music, sinhala new songs, sinhala new songs 2018', '579');
INSERT INTO `songs` VALUES ('37', 'Oyata Vitharak', 'core/storage/uploads/images/album_art/album_art-20180602145937.png', '32', '3', '38', '2018-06-03 03:29:38', '2018-07-26 03:03:06', '0', '9', 'Oyata Vitharak - Ishan Priyasanka Official Music Video 2018', 'Oyata Vitharak, Oyata Vitharak song, Oyata Vitharak mp3, Oyata Vitharak song mp3 download, Oyata Vitharak song video, ', '780');
INSERT INTO `songs` VALUES ('38', 'Miya Yanna Sudanam', 'core/storage/uploads/images/album_art/album_art-20180602173433.png', '0', '0', '40', '2018-06-03 06:04:33', '2018-07-26 11:50:12', '0', '9', '', '', '375');
INSERT INTO `songs` VALUES ('39', 'Hitha Gawa Heena', 'core/storage/uploads/images/album_art/album_art-20180602174528.png', '31', '0', '39', '2018-06-03 06:15:28', '2018-07-26 01:18:39', '1', '9', '', '', '484');
INSERT INTO `songs` VALUES ('40', 'Obe Adare', 'core/storage/uploads/images/album_art/album_art-20180606102518.png', '36', '6', '41', '2018-06-06 22:55:18', '2018-07-23 20:13:37', '0', '9', '', 'Obe Adare Sameera Chathuranga mp3, Obe Adare Sameera Chathuranga mp3 download, Sameera Chathuranga mp3, Obe Adare mp3, Obe Adare mp3 download, ahimi u obe adare song, ahimi u obe adare song download, ahimi u obe adare song mp3 download, ', '301');
INSERT INTO `songs` VALUES ('41', 'Mathaka Mawee', 'core/storage/uploads/images/album_art/album_art-20180606120259.png', '33', '4', '42', '2018-06-07 00:33:00', '2018-07-26 17:54:38', '0', '9', '', '', '901');
INSERT INTO `songs` VALUES ('42', 'Pem Karala', 'core/storage/uploads/images/album_art/album_art-20180606160604.png', '0', '0', '43', '2018-06-07 04:36:04', '2018-07-24 04:49:45', '0', '9', '', 'Pem Karala Udesh Nilanga song download, Pem Karala Udesh Nilanga mp3 song download, Pem Karala song download, Pem Karala Udesh mp3 download', '264');
INSERT INTO `songs` VALUES ('43', 'Anthima Mohothedi', 'core/storage/uploads/images/album_art/album_art-20180607132655.png', '40', '0', '45', '2018-06-08 01:56:55', '2018-07-26 23:46:19', '0', '9', 'Anthima Mohothedi - Nilan Hettiarachchi Official Music Video & Audio 2018', 'antima mohothedi nilan hettiarachchi mp3, antima mohothedi nilan hettiarachchi mp3 download, antima mohothedi song, antima mohothedi song download, antima mohothedi mp3 song download, antima mohothedi video song,', '187');
INSERT INTO `songs` VALUES ('44', 'Wirasakawee', 'core/storage/uploads/images/album_art/album_art-20180608141544.png', '0', '7', '44', '2018-06-09 02:45:45', '2018-07-23 20:16:31', '0', '9', '', 'Wirasakawee song, Wirasakawee song mp3, Wirasakawee song download, ', '262');
INSERT INTO `songs` VALUES ('45', 'Pemwathiye', 'core/storage/uploads/images/album_art/album_art-20180614230911.png', '0', '0', '46', '2018-06-15 11:39:11', '2018-07-25 19:12:43', '0', '9', '', 'Pemwathiye song, Pemwathiye song mp3, Pemwathiye song download, Pemwathiye mp3 song download, Pemwathiye song video, Asanjaya Imashath songs, Asanjaya Imashath Pemwathiye, Pemwathiye Asanjaya mp3,', '234');
INSERT INTO `songs` VALUES ('46', 'Aulak Nane', 'core/storage/uploads/images/album_art/album_art-20180614232833.png', '34', '0', '47', '2018-06-15 11:58:33', '2018-07-26 01:50:13', '0', '9', '', 'Aulak Nane song, Aulak Nane song mp3, Aulak Nane song download, Aulak Nane mp3 download, awlak nane song, awlak nane song download, ', '154');
INSERT INTO `songs` VALUES ('47', 'Sulan Podak Wee', 'core/storage/uploads/images/album_art/album_art-20180614235834.png', '35', '0', '48', '2018-06-15 12:28:34', '2018-07-23 14:20:34', '0', '9', '', '', '64');
INSERT INTO `songs` VALUES ('48', 'Sammatheta Pitupe', 'core/storage/uploads/images/album_art/album_art-20180619183209.png', '0', '8', '49', '2018-06-20 07:02:09', '2018-07-27 02:56:07', '0', '9', '', 'Sammatheta Pitupe, Sammatheta Pitupe mp3, Sammatheta Pitupe Mp3 Download, Sammatheta Pitupe Song Mp3, Sammatheta Pitupe song download, Sammatheta Pitupe Jude Rogans, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show, Sammatheta Pitupe Jude Rogans mp3 download, Sammatheta Pitu pa, Sammatheta Pitu pa Jude Rogans, Sammatheta Pitu pa Jude Rogans song, Sammatheta Pitu pa Jude Rogans song download, Sammatheta Pitu pa Jude Rogans mp3, Sammatheta Pitu pa Jude Rogans mp3 download, Sammatheta Pitu pa mp3 download', '178');
INSERT INTO `songs` VALUES ('49', 'Nindath Hora Gaththa Oya', 'core/storage/uploads/images/album_art/album_art-20180622105705.png', '0', '0', '50', '2018-06-22 23:27:05', '2018-07-26 20:41:38', '0', '9', '', 'Nindath Hora Gaththa Oya song, Nindath Hora Gaththa Oya song download, Nindath Hora Gaththa Oya mp3 download, Nindath Hora Gaththa Oya song mp3 download, Ajith Sanjeewa song, Ajith Sanjeewa song download,', '97');
INSERT INTO `songs` VALUES ('50', 'Seya (Nabara Wu)', 'core/storage/uploads/images/album_art/album_art-20180629150142.png', '0', '9', '51', '2018-06-30 03:31:43', '2018-07-26 01:48:03', '0', '9', '', '', '141');
INSERT INTO `songs` VALUES ('51', 'Hithuwak Kari', 'core/storage/uploads/images/album_art/album_art-20180629155045.png', '0', '0', '52', '2018-06-30 04:20:45', '2018-07-26 22:15:33', '0', '9', '', '', '160');
INSERT INTO `songs` VALUES ('52', 'Aththama Kiyannam', 'core/storage/uploads/images/album_art/album_art-20180629162159.png', '0', '0', '53', '2018-06-30 04:51:59', '2018-07-26 05:31:34', '0', '9', '', '', '186');
INSERT INTO `songs` VALUES ('53', 'Unmada Rathriye', 'core/storage/uploads/images/album_art/album_art-20180630153418.png', '0', '10', '54', '2018-07-01 04:04:18', '2018-07-26 09:25:08', '0', '9', 'Unmada Rathriye Nimash Frenando Official Audio', 'unmada rathriye, unmada rathriye song, unmada rathriye song download, unmada rathriye mp3 song download, unmada rathriye mp3 download,', '99');
INSERT INTO `songs` VALUES ('54', 'Parana Hithuwakkari', 'core/storage/uploads/images/album_art/album_art-20180630161208.png', '0', '11', '55', '2018-07-01 04:42:09', '2018-07-27 02:54:41', '0', '9', 'Parana Hithuwakkari Official Audio', 'parana hithuwakkari, parana hithuwakkari song, parana hithuwakkari song video, parana hithuwakkari manoj, parana hithuwakkari mp3 song, parana hithuwakkari new song, parana hithuwakkari official video,', '100');
INSERT INTO `songs` VALUES ('55', 'Dukak Denenna Epa', 'core/storage/uploads/images/album_art/album_art-20180704133524.png', '0', '0', '56', '2018-07-05 02:05:24', '2018-07-23 15:07:54', '1', '9', 'Dukak Denenna Epa Official Audio Sadun Perera', 'sandun perera, dukak denenna epa, sandun perera new song, new sinhala mp3, sandun perera new, chandana walpola, dukak denenna epa sandun perera, dukak denenna epa mp3, dukak denenna epa video, dukak denenna epa lyrics, dukak denenna epa dj, dukak denenna epa mp3 song, dukak denenna epa download, sajith v chathuranga, dukak danenna epa obata nam kisida, sandun perera new song download mp3, ', '10');
INSERT INTO `songs` VALUES ('56', 'Mage Asurin', 'core/storage/uploads/images/album_art/album_art-20180704171253.png', '0', '0', '57', '2018-07-05 05:42:53', '2018-07-08 03:08:36', '1', '9', 'Mage Asurin Official Audio Shalinda Fernando', 'shalinda fernando, mage asurin, mage asurin song, mage asurin live, mage asurin video download, mage asurin lyrics, mage asurin mideela, mage asurin midila, mage asurin live show mp3, mage asurin shalinda fernando, mage asurin video download, mage asurin live show mp3, mage asurin dj, mage asurin shalinda fernando (all right), shalinda fernando songs, ', '4');
INSERT INTO `songs` VALUES ('57', 'Therum Giya', 'core/storage/uploads/images/album_art/album_art-20180704175055.png', '0', '0', '58', '2018-07-05 06:20:55', '2018-07-23 15:05:34', '1', '9', 'Therum Giya (Giya De Giyaden) Shahil Himansa Official Audio', 'therum giya, shahil himansa, chandana walpola, therum giya video song download, therum giya dj song, therum giya lyrics, therum giya nuba wenas wee kiya, therum giya chords, therum giya song lyrics, therum giya video, therum giya mp3, ', '9');
INSERT INTO `songs` VALUES ('58', 'Heen Sare', 'core/storage/uploads/images/album_art/album_art-20180704182147.png', '0', '0', '59', '2018-07-05 06:51:47', '2018-07-22 19:33:39', '1', '9', 'Heen Sare Thushara Subasinghe Official Audio', 'heen sare, heen sare video, heen sare video download, thushara subasinghe new song, thushara subasinha new song, thushara subasingha new song, oxygen leader, oxygen sri lanka, heen sare hithata mage, heen sare song, heen sare thushara, heen sare song thushara, ', '32');
INSERT INTO `songs` VALUES ('59', 'Man Vindina', 'core/storage/uploads/images/album_art/album_art-20180704191626.png', '0', '0', '60', '2018-07-05 07:46:26', '2018-07-08 07:28:05', '1', '9', 'Man Vindina Udesh Indula Official Audio', 'udesh indula, new song, man vindina mey duk ada, best sinhala songs, sinhala new song, udesh indula new song, udesh indula mp3 song, vindina duk ada official audio, vindina duk ada indula,', '2');
INSERT INTO `songs` VALUES ('60', 'Nodakapu Dewani Budun', 'core/storage/uploads/images/album_art/album_art-20180704200102.png', '37', '0', '61', '2018-07-05 08:31:02', '2018-07-27 02:48:02', '1', '9', 'Nodakapu Dewani Budun Jude Rogans Official Music Video', 'jude rogans, nodakapu dewani budun, nodakapu deweni budun jude rogans, jude rogans new song, sinhala songs, nodakapu deweni budun video, jude rogans new song video, nodakapu dewani budun song video, nodakapu dewani budun song lyrics, nodakapu dewani budun song free download, nodakapu dewani budun dj song, nodakapu jude, ', '14');
INSERT INTO `songs` VALUES ('61', 'Mata Wetahuna', 'core/storage/uploads/images/album_art/album_art-20180705161854.png', '0', '0', '63', '2018-07-06 04:48:54', '2018-07-26 06:50:03', '0', '9', '', 'mata wetahuna, mata wetahuna song, mata wetahuna mp3 song, mata wetahuna song download, mata wetahuna mp3 song download, ', '123');
INSERT INTO `songs` VALUES ('62', 'Salli Na', 'core/storage/uploads/images/album_art/album_art-20180705172859.png', '0', '0', '64', '2018-07-06 05:59:00', '2018-07-26 23:12:46', '0', '9', 'Salli Na Manoj V Official Audio', 'salli na, salli na song, salli na song download, salli na mp3 song download, salli na manoj, ', '75');
INSERT INTO `songs` VALUES ('63', 'Sudu Manika', 'core/storage/uploads/images/album_art/album_art-20180705174513.png', '0', '12', '62', '2018-07-06 06:15:13', '2018-07-26 22:44:07', '0', '9', 'Sudu Manika Nalinda Ranasinghe Official Audio', 'sudu manika, sudu manika song, sudu manika song nalinda, sudu manika song download, sudu manika mp3, sudu manika mp3 song download, ', '1163');
INSERT INTO `songs` VALUES ('64', 'Rawatuna Nowe', 'core/storage/uploads/images/album_art/album_art-20180707130659.png', '0', '0', '65', '2018-07-08 01:36:59', '2018-07-27 02:54:20', '0', '9', 'Rawatuna Nowe Thushara Joshap Official Audio 2018', 'rawatuna nowe, rawatuna nowe song, rawatuna nowe song mp3, rawatuna nowe thushara, rawatuna nowe mp3, rawatuna nowe mp3 download, rawatuna nowe song download, ', '3962');
INSERT INTO `songs` VALUES ('65', 'Aswaha Wadune', 'core/storage/uploads/images/album_art/album_art-20180708102657.png', '0', '0', '66', '2018-07-08 22:56:57', '2018-07-19 23:55:14', '0', '9', 'Aswaha Wadune Mithila Randika Official Audio 2018', 'aswaha wadune, aswaha wadune song, aswaha wadune song download, aswaha wadune mithila randika, aswaha wedune, aswaha wedune song, aswaha wedune song download, aswaha wedune song mp3, aswaha wedune song video, ', '140');
INSERT INTO `songs` VALUES ('66', 'Oba Warade Pataleddi', 'core/storage/uploads/images/album_art/album_art-20180710181907.png', '0', '0', '67', '2018-07-11 06:49:07', '2018-07-26 09:46:49', '0', '9', 'Oba Warade Pataleddi Saman Pushpakumara Official Audio 2018', '', '386');
INSERT INTO `songs` VALUES ('67', 'Purudu Thanikama', 'core/storage/uploads/images/album_art/album_art-20180712161157.png', '0', '0', '68', '2018-07-13 04:41:57', '2018-07-26 01:43:18', '0', '9', 'Purudu Thanikama Errol Jayawardena Official Audio 2018', 'purudu thanikama, purudu thanikama song, purudu thanikama mp3, purudu thanikama song mp3, purudu thanikama song video, purudu thanikama song download, purudu thanikama song free download, purudu thanikama free download, ', '143');
INSERT INTO `songs` VALUES ('68', 'Nodaka Oya', 'core/storage/uploads/images/album_art/album_art-20180713141026.png', '0', '0', '69', '2018-07-14 02:40:26', '2018-07-26 17:56:35', '0', '9', 'Nodaka Oya Janaka Katipearachchige Official Audio 2018', 'nodaka oya, nodaka oya mp3, nodaka oya song, nodaka oya song download, nodaka oya mp3 download, nodaka oya song free download, nodaka oya song janaka, ', '344');
INSERT INTO `songs` VALUES ('69', 'Adare Hodi Pothe', 'core/storage/uploads/images/album_art/album_art-20180713145750.png', '0', '0', '70', '2018-07-14 03:27:50', '2018-07-26 05:42:21', '0', '9', 'Adare Hodi Pothe Prageeth Vidana Pathirana Official Audio 2018', 'adare hodi pothe, adare hodi pothe song, adare hodi pothe mp3, adare hodi pothe song mp3, adare hodi pothe song download, adare hodi pothe mp3 free download, adare hodi pothe prageeth, prageeth new songs, ', '656');
INSERT INTO `songs` VALUES ('70', 'Labimai Me Ape ', 'core/storage/uploads/images/album_art/album_art-20180716153353.png', '0', '0', '71', '2018-07-17 04:03:53', '2018-07-27 02:55:23', '0', '9', 'Labimai Me Ape Ajith Anthony Official Audio 2018', 'ajith anthony, ajith anthony song, ajith anthony songs, ajith anthony song mp3, ajith anthony mp3 song download,', '386');
INSERT INTO `songs` VALUES ('71', 'Wen Wenna', 'core/storage/uploads/images/album_art/album_art-20180719172057.png', '0', '0', '72', '2018-07-20 05:50:57', '2018-07-27 02:48:46', '0', '9', 'Wen Wenna Ashan Fernando Official Audio 2018', 'wen wenna, wen wenna song, wen wenna song ashan, wen wenna new song ashan, wen wenna song ashan mp3, wen wenna song ashan mp3 download, wen wenna mp3, wen wenna song ashan free download, ', '2382');
INSERT INTO `songs` VALUES ('72', 'Manamaliye', 'core/storage/uploads/images/album_art/album_art-20180725095936.png', '39', '0', '73', '2018-07-25 22:29:36', '2018-07-27 02:55:00', '0', '9', 'Manamaliye Tehan Perera ft. Hot Chocolate 2018', 'manamaliye, manamaliye song, manamaliye tehan, manamaliye new song tehan, manamaliye download, manamaliye mp3 download,', '29');

-- ----------------------------
-- Table structure for song_lyrics_artist
-- ----------------------------
DROP TABLE IF EXISTS `song_lyrics_artist`;
CREATE TABLE `song_lyrics_artist` (
  `artist_id` int(10) unsigned DEFAULT NULL,
  `song_id` int(10) unsigned DEFAULT NULL,
  KEY `song_lyrics_artist_artist_id_foreign` (`artist_id`),
  KEY `song_lyrics_artist_song_id_foreign` (`song_id`),
  CONSTRAINT `song_lyrics_artist_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `song_lyrics_artist_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of song_lyrics_artist
-- ----------------------------
INSERT INTO `song_lyrics_artist` VALUES ('2', '2');
INSERT INTO `song_lyrics_artist` VALUES ('2', '3');
INSERT INTO `song_lyrics_artist` VALUES ('2', '6');
INSERT INTO `song_lyrics_artist` VALUES ('2', '7');
INSERT INTO `song_lyrics_artist` VALUES ('2', '8');
INSERT INTO `song_lyrics_artist` VALUES ('18', '9');
INSERT INTO `song_lyrics_artist` VALUES ('19', '10');
INSERT INTO `song_lyrics_artist` VALUES ('21', '11');
INSERT INTO `song_lyrics_artist` VALUES ('23', '12');
INSERT INTO `song_lyrics_artist` VALUES ('27', '13');
INSERT INTO `song_lyrics_artist` VALUES ('21', '14');
INSERT INTO `song_lyrics_artist` VALUES ('28', '15');
INSERT INTO `song_lyrics_artist` VALUES ('33', '23');
INSERT INTO `song_lyrics_artist` VALUES ('23', '27');
INSERT INTO `song_lyrics_artist` VALUES ('47', '38');
INSERT INTO `song_lyrics_artist` VALUES ('64', '49');
INSERT INTO `song_lyrics_artist` VALUES ('66', '50');
INSERT INTO `song_lyrics_artist` VALUES ('68', '51');
INSERT INTO `song_lyrics_artist` VALUES ('69', '52');
INSERT INTO `song_lyrics_artist` VALUES ('71', '40');
INSERT INTO `song_lyrics_artist` VALUES ('53', '44');
INSERT INTO `song_lyrics_artist` VALUES ('72', '45');
INSERT INTO `song_lyrics_artist` VALUES ('53', '41');
INSERT INTO `song_lyrics_artist` VALUES ('23', '43');
INSERT INTO `song_lyrics_artist` VALUES ('21', '42');
INSERT INTO `song_lyrics_artist` VALUES ('44', '36');
INSERT INTO `song_lyrics_artist` VALUES ('62', '46');
INSERT INTO `song_lyrics_artist` VALUES ('73', '35');
INSERT INTO `song_lyrics_artist` VALUES ('2', '1');
INSERT INTO `song_lyrics_artist` VALUES ('2', '32');
INSERT INTO `song_lyrics_artist` VALUES ('23', '18');
INSERT INTO `song_lyrics_artist` VALUES ('31', '19');
INSERT INTO `song_lyrics_artist` VALUES ('23', '20');
INSERT INTO `song_lyrics_artist` VALUES ('23', '22');
INSERT INTO `song_lyrics_artist` VALUES ('34', '24');
INSERT INTO `song_lyrics_artist` VALUES ('47', '39');
INSERT INTO `song_lyrics_artist` VALUES ('73', '48');
INSERT INTO `song_lyrics_artist` VALUES ('2', '37');
INSERT INTO `song_lyrics_artist` VALUES ('23', '33');
INSERT INTO `song_lyrics_artist` VALUES ('74', '30');
INSERT INTO `song_lyrics_artist` VALUES ('23', '29');
INSERT INTO `song_lyrics_artist` VALUES ('38', '28');
INSERT INTO `song_lyrics_artist` VALUES ('36', '26');
INSERT INTO `song_lyrics_artist` VALUES ('76', '53');
INSERT INTO `song_lyrics_artist` VALUES ('77', '54');
INSERT INTO `song_lyrics_artist` VALUES ('23', '55');
INSERT INTO `song_lyrics_artist` VALUES ('79', '56');
INSERT INTO `song_lyrics_artist` VALUES ('73', '57');
INSERT INTO `song_lyrics_artist` VALUES ('21', '58');
INSERT INTO `song_lyrics_artist` VALUES ('83', '59');
INSERT INTO `song_lyrics_artist` VALUES ('76', '61');
INSERT INTO `song_lyrics_artist` VALUES ('21', '63');
INSERT INTO `song_lyrics_artist` VALUES ('36', '64');
INSERT INTO `song_lyrics_artist` VALUES ('90', '65');
INSERT INTO `song_lyrics_artist` VALUES ('91', '66');
INSERT INTO `song_lyrics_artist` VALUES ('92', '67');
INSERT INTO `song_lyrics_artist` VALUES ('62', '68');
INSERT INTO `song_lyrics_artist` VALUES ('96', '69');
INSERT INTO `song_lyrics_artist` VALUES ('64', '70');
INSERT INTO `song_lyrics_artist` VALUES ('2', '71');
INSERT INTO `song_lyrics_artist` VALUES ('100', '72');

-- ----------------------------
-- Table structure for song_music_artist
-- ----------------------------
DROP TABLE IF EXISTS `song_music_artist`;
CREATE TABLE `song_music_artist` (
  `artist_id` int(10) unsigned DEFAULT NULL,
  `song_id` int(10) unsigned DEFAULT NULL,
  KEY `music_artist_table_artist_id_foreign` (`artist_id`),
  KEY `music_artist_table_song_id_foreign` (`song_id`),
  CONSTRAINT `music_artist_table_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `music_artist_table_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of song_music_artist
-- ----------------------------
INSERT INTO `song_music_artist` VALUES ('1', '1');
INSERT INTO `song_music_artist` VALUES ('1', '2');
INSERT INTO `song_music_artist` VALUES ('1', '3');
INSERT INTO `song_music_artist` VALUES ('1', '6');
INSERT INTO `song_music_artist` VALUES ('1', '7');
INSERT INTO `song_music_artist` VALUES ('1', '8');
INSERT INTO `song_music_artist` VALUES ('1', '9');
INSERT INTO `song_music_artist` VALUES ('20', '10');
INSERT INTO `song_music_artist` VALUES ('22', '11');
INSERT INTO `song_music_artist` VALUES ('24', '12');
INSERT INTO `song_music_artist` VALUES ('17', '13');
INSERT INTO `song_music_artist` VALUES ('22', '14');
INSERT INTO `song_music_artist` VALUES ('17', '15');
INSERT INTO `song_music_artist` VALUES ('46', '38');
INSERT INTO `song_music_artist` VALUES ('63', '49');
INSERT INTO `song_music_artist` VALUES ('30', '48');
INSERT INTO `song_music_artist` VALUES ('50', '40');
INSERT INTO `song_music_artist` VALUES ('37', '50');
INSERT INTO `song_music_artist` VALUES ('63', '51');
INSERT INTO `song_music_artist` VALUES ('69', '52');
INSERT INTO `song_music_artist` VALUES ('52', '44');
INSERT INTO `song_music_artist` VALUES ('22', '45');
INSERT INTO `song_music_artist` VALUES ('52', '41');
INSERT INTO `song_music_artist` VALUES ('24', '43');
INSERT INTO `song_music_artist` VALUES ('22', '42');
INSERT INTO `song_music_artist` VALUES ('43', '36');
INSERT INTO `song_music_artist` VALUES ('22', '46');
INSERT INTO `song_music_artist` VALUES ('29', '35');
INSERT INTO `song_music_artist` VALUES ('30', '18');
INSERT INTO `song_music_artist` VALUES ('32', '19');
INSERT INTO `song_music_artist` VALUES ('5', '20');
INSERT INTO `song_music_artist` VALUES ('29', '22');
INSERT INTO `song_music_artist` VALUES ('33', '23');
INSERT INTO `song_music_artist` VALUES ('35', '24');
INSERT INTO `song_music_artist` VALUES ('48', '39');
INSERT INTO `song_music_artist` VALUES ('1', '37');
INSERT INTO `song_music_artist` VALUES ('41', '33');
INSERT INTO `song_music_artist` VALUES ('5', '32');
INSERT INTO `song_music_artist` VALUES ('40', '30');
INSERT INTO `song_music_artist` VALUES ('29', '29');
INSERT INTO `song_music_artist` VALUES ('37', '28');
INSERT INTO `song_music_artist` VALUES ('30', '26');
INSERT INTO `song_music_artist` VALUES ('1', '53');
INSERT INTO `song_music_artist` VALUES ('24', '54');
INSERT INTO `song_music_artist` VALUES ('30', '55');
INSERT INTO `song_music_artist` VALUES ('78', '56');
INSERT INTO `song_music_artist` VALUES ('30', '57');
INSERT INTO `song_music_artist` VALUES ('22', '58');
INSERT INTO `song_music_artist` VALUES ('82', '59');
INSERT INTO `song_music_artist` VALUES ('84', '60');
INSERT INTO `song_music_artist` VALUES ('1', '61');
INSERT INTO `song_music_artist` VALUES ('88', '62');
INSERT INTO `song_music_artist` VALUES ('22', '63');
INSERT INTO `song_music_artist` VALUES ('89', '64');
INSERT INTO `song_music_artist` VALUES ('24', '65');
INSERT INTO `song_music_artist` VALUES ('24', '66');
INSERT INTO `song_music_artist` VALUES ('93', '67');
INSERT INTO `song_music_artist` VALUES ('22', '68');
INSERT INTO `song_music_artist` VALUES ('97', '69');
INSERT INTO `song_music_artist` VALUES ('63', '70');
INSERT INTO `song_music_artist` VALUES ('1', '71');
INSERT INTO `song_music_artist` VALUES ('1', '72');

-- ----------------------------
-- Table structure for throttle
-- ----------------------------
DROP TABLE IF EXISTS `throttle`;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of throttle
-- ----------------------------
INSERT INTO `throttle` VALUES ('1', null, 'global', null, '2015-07-12 00:05:27', '2015-07-12 00:05:27');
INSERT INTO `throttle` VALUES ('2', null, 'ip', '127.0.0.1', '2015-07-12 00:05:28', '2015-07-12 00:05:28');
INSERT INTO `throttle` VALUES ('3', null, 'global', null, '2015-07-12 00:06:28', '2015-07-12 00:06:28');
INSERT INTO `throttle` VALUES ('4', null, 'ip', '127.0.0.1', '2015-07-12 00:06:28', '2015-07-12 00:06:28');
INSERT INTO `throttle` VALUES ('5', null, 'global', null, '2015-07-13 22:44:23', '2015-07-13 22:44:23');
INSERT INTO `throttle` VALUES ('6', null, 'ip', '127.0.0.1', '2015-07-13 22:44:23', '2015-07-13 22:44:23');
INSERT INTO `throttle` VALUES ('7', '1', 'user', null, '2015-07-13 22:44:23', '2015-07-13 22:44:23');
INSERT INTO `throttle` VALUES ('8', null, 'global', null, '2015-07-27 04:24:02', '2015-07-27 04:24:02');
INSERT INTO `throttle` VALUES ('9', null, 'ip', '127.0.0.1', '2015-07-27 04:24:02', '2015-07-27 04:24:02');
INSERT INTO `throttle` VALUES ('10', '1', 'user', null, '2015-07-27 04:24:02', '2015-07-27 04:24:02');
INSERT INTO `throttle` VALUES ('11', null, 'global', null, '2015-08-14 03:48:09', '2015-08-14 03:48:09');
INSERT INTO `throttle` VALUES ('12', null, 'ip', '127.0.0.1', '2015-08-14 03:48:09', '2015-08-14 03:48:09');
INSERT INTO `throttle` VALUES ('13', '1', 'user', null, '2015-08-14 03:48:09', '2015-08-14 03:48:09');
INSERT INTO `throttle` VALUES ('14', null, 'global', null, '2015-08-14 03:50:45', '2015-08-14 03:50:45');
INSERT INTO `throttle` VALUES ('15', null, 'ip', '127.0.0.1', '2015-08-14 03:50:45', '2015-08-14 03:50:45');
INSERT INTO `throttle` VALUES ('16', '1', 'user', null, '2015-08-14 03:50:45', '2015-08-14 03:50:45');
INSERT INTO `throttle` VALUES ('17', null, 'global', null, '2015-08-18 21:31:17', '2015-08-18 21:31:17');
INSERT INTO `throttle` VALUES ('18', null, 'ip', '127.0.0.1', '2015-08-18 21:31:17', '2015-08-18 21:31:17');
INSERT INTO `throttle` VALUES ('19', '1', 'user', null, '2015-08-18 21:31:17', '2015-08-18 21:31:17');
INSERT INTO `throttle` VALUES ('20', null, 'global', null, '2015-08-20 21:57:44', '2015-08-20 21:57:44');
INSERT INTO `throttle` VALUES ('21', null, 'ip', '127.0.0.1', '2015-08-20 21:57:44', '2015-08-20 21:57:44');
INSERT INTO `throttle` VALUES ('22', '1', 'user', null, '2015-08-20 21:57:45', '2015-08-20 21:57:45');
INSERT INTO `throttle` VALUES ('23', null, 'global', null, '2015-08-20 22:52:53', '2015-08-20 22:52:53');
INSERT INTO `throttle` VALUES ('24', null, 'ip', '127.0.0.1', '2015-08-20 22:52:53', '2015-08-20 22:52:53');
INSERT INTO `throttle` VALUES ('25', '1', 'user', null, '2015-08-20 22:52:54', '2015-08-20 22:52:54');
INSERT INTO `throttle` VALUES ('26', null, 'global', null, '2015-08-20 22:53:06', '2015-08-20 22:53:06');
INSERT INTO `throttle` VALUES ('27', null, 'ip', '127.0.0.1', '2015-08-20 22:53:06', '2015-08-20 22:53:06');
INSERT INTO `throttle` VALUES ('28', '1', 'user', null, '2015-08-20 22:53:06', '2015-08-20 22:53:06');
INSERT INTO `throttle` VALUES ('29', null, 'global', null, '2015-08-20 22:53:09', '2015-08-20 22:53:09');
INSERT INTO `throttle` VALUES ('30', null, 'ip', '127.0.0.1', '2015-08-20 22:53:09', '2015-08-20 22:53:09');
INSERT INTO `throttle` VALUES ('31', '1', 'user', null, '2015-08-20 22:53:09', '2015-08-20 22:53:09');
INSERT INTO `throttle` VALUES ('32', null, 'global', null, '2015-08-20 22:55:59', '2015-08-20 22:55:59');
INSERT INTO `throttle` VALUES ('33', null, 'ip', '127.0.0.1', '2015-08-20 22:55:59', '2015-08-20 22:55:59');
INSERT INTO `throttle` VALUES ('34', '1', 'user', null, '2015-08-20 22:55:59', '2015-08-20 22:55:59');
INSERT INTO `throttle` VALUES ('35', null, 'global', null, '2015-08-20 22:56:18', '2015-08-20 22:56:18');
INSERT INTO `throttle` VALUES ('36', null, 'ip', '127.0.0.1', '2015-08-20 22:56:19', '2015-08-20 22:56:19');
INSERT INTO `throttle` VALUES ('37', '1', 'user', null, '2015-08-20 22:56:19', '2015-08-20 22:56:19');
INSERT INTO `throttle` VALUES ('38', null, 'global', null, '2015-08-20 22:57:25', '2015-08-20 22:57:25');
INSERT INTO `throttle` VALUES ('39', null, 'ip', '127.0.0.1', '2015-08-20 22:57:25', '2015-08-20 22:57:25');
INSERT INTO `throttle` VALUES ('40', '1', 'user', null, '2015-08-20 22:57:25', '2015-08-20 22:57:25');
INSERT INTO `throttle` VALUES ('41', null, 'global', null, '2015-08-25 09:06:12', '2015-08-25 09:06:12');
INSERT INTO `throttle` VALUES ('42', null, 'ip', '127.0.0.1', '2015-08-25 09:06:12', '2015-08-25 09:06:12');
INSERT INTO `throttle` VALUES ('43', null, 'global', null, '2015-08-25 09:08:25', '2015-08-25 09:08:25');
INSERT INTO `throttle` VALUES ('44', null, 'ip', '127.0.0.1', '2015-08-25 09:08:25', '2015-08-25 09:08:25');
INSERT INTO `throttle` VALUES ('45', null, 'global', null, '2015-08-25 09:09:09', '2015-08-25 09:09:09');
INSERT INTO `throttle` VALUES ('46', null, 'ip', '127.0.0.1', '2015-08-25 09:09:09', '2015-08-25 09:09:09');
INSERT INTO `throttle` VALUES ('47', null, 'global', null, '2015-08-25 09:09:44', '2015-08-25 09:09:44');
INSERT INTO `throttle` VALUES ('48', null, 'ip', '127.0.0.1', '2015-08-25 09:09:44', '2015-08-25 09:09:44');
INSERT INTO `throttle` VALUES ('49', null, 'global', null, '2015-08-25 09:09:49', '2015-08-25 09:09:49');
INSERT INTO `throttle` VALUES ('50', null, 'ip', '127.0.0.1', '2015-08-25 09:09:50', '2015-08-25 09:09:50');
INSERT INTO `throttle` VALUES ('51', null, 'global', null, '2015-08-25 09:11:29', '2015-08-25 09:11:29');
INSERT INTO `throttle` VALUES ('52', null, 'ip', '127.0.0.1', '2015-08-25 09:11:29', '2015-08-25 09:11:29');
INSERT INTO `throttle` VALUES ('53', null, 'global', null, '2015-08-26 01:26:45', '2015-08-26 01:26:45');
INSERT INTO `throttle` VALUES ('54', null, 'ip', '127.0.0.1', '2015-08-26 01:26:45', '2015-08-26 01:26:45');
INSERT INTO `throttle` VALUES ('55', null, 'global', null, '2015-08-27 01:48:20', '2015-08-27 01:48:20');
INSERT INTO `throttle` VALUES ('56', null, 'ip', '192.168.1.35', '2015-08-27 01:48:21', '2015-08-27 01:48:21');
INSERT INTO `throttle` VALUES ('57', null, 'global', null, '2015-08-27 01:48:23', '2015-08-27 01:48:23');
INSERT INTO `throttle` VALUES ('58', null, 'ip', '192.168.1.35', '2015-08-27 01:48:23', '2015-08-27 01:48:23');
INSERT INTO `throttle` VALUES ('59', null, 'global', null, '2015-08-27 01:48:27', '2015-08-27 01:48:27');
INSERT INTO `throttle` VALUES ('60', null, 'ip', '192.168.1.35', '2015-08-27 01:48:27', '2015-08-27 01:48:27');
INSERT INTO `throttle` VALUES ('61', null, 'global', null, '2015-08-27 01:48:31', '2015-08-27 01:48:31');
INSERT INTO `throttle` VALUES ('62', null, 'ip', '192.168.1.35', '2015-08-27 01:48:31', '2015-08-27 01:48:31');
INSERT INTO `throttle` VALUES ('63', null, 'global', null, '2015-08-27 01:48:36', '2015-08-27 01:48:36');
INSERT INTO `throttle` VALUES ('64', null, 'ip', '192.168.1.35', '2015-08-27 01:48:36', '2015-08-27 01:48:36');
INSERT INTO `throttle` VALUES ('65', null, 'global', null, '2015-08-27 01:48:48', '2015-08-27 01:48:48');
INSERT INTO `throttle` VALUES ('66', null, 'global', null, '2015-08-27 21:20:50', '2015-08-27 21:20:50');
INSERT INTO `throttle` VALUES ('67', null, 'ip', '127.0.0.1', '2015-08-27 21:20:50', '2015-08-27 21:20:50');
INSERT INTO `throttle` VALUES ('68', '1', 'user', null, '2015-08-27 21:20:50', '2015-08-27 21:20:50');
INSERT INTO `throttle` VALUES ('69', null, 'global', null, '2015-08-31 00:42:57', '2015-08-31 00:42:57');
INSERT INTO `throttle` VALUES ('70', null, 'ip', '127.0.0.1', '2015-08-31 00:42:57', '2015-08-31 00:42:57');
INSERT INTO `throttle` VALUES ('71', null, 'global', null, '2015-08-31 00:51:13', '2015-08-31 00:51:13');
INSERT INTO `throttle` VALUES ('72', null, 'ip', '127.0.0.1', '2015-08-31 00:51:14', '2015-08-31 00:51:14');
INSERT INTO `throttle` VALUES ('73', null, 'global', null, '2015-09-07 05:03:36', '2015-09-07 05:03:36');
INSERT INTO `throttle` VALUES ('74', null, 'ip', '127.0.0.1', '2015-09-07 05:03:36', '2015-09-07 05:03:36');
INSERT INTO `throttle` VALUES ('75', null, 'global', null, '2015-09-18 23:45:18', '2015-09-18 23:45:18');
INSERT INTO `throttle` VALUES ('76', null, 'ip', '192.168.1.15', '2015-09-18 23:45:18', '2015-09-18 23:45:18');
INSERT INTO `throttle` VALUES ('77', null, 'global', null, '2015-09-18 23:45:22', '2015-09-18 23:45:22');
INSERT INTO `throttle` VALUES ('78', null, 'ip', '192.168.1.15', '2015-09-18 23:45:22', '2015-09-18 23:45:22');
INSERT INTO `throttle` VALUES ('79', null, 'global', null, '2015-09-18 23:45:30', '2015-09-18 23:45:30');
INSERT INTO `throttle` VALUES ('80', null, 'ip', '192.168.1.15', '2015-09-18 23:45:30', '2015-09-18 23:45:30');
INSERT INTO `throttle` VALUES ('81', null, 'global', null, '2015-09-18 23:45:34', '2015-09-18 23:45:34');
INSERT INTO `throttle` VALUES ('82', null, 'ip', '192.168.1.15', '2015-09-18 23:45:34', '2015-09-18 23:45:34');
INSERT INTO `throttle` VALUES ('83', null, 'global', null, '2015-09-18 23:45:40', '2015-09-18 23:45:40');
INSERT INTO `throttle` VALUES ('84', null, 'ip', '192.168.1.15', '2015-09-18 23:45:40', '2015-09-18 23:45:40');
INSERT INTO `throttle` VALUES ('85', null, 'global', null, '2015-10-30 07:37:26', '2015-10-30 07:37:26');
INSERT INTO `throttle` VALUES ('86', null, 'ip', '127.0.0.1', '2015-10-30 07:37:26', '2015-10-30 07:37:26');
INSERT INTO `throttle` VALUES ('87', '1', 'user', null, '2015-10-30 07:37:26', '2015-10-30 07:37:26');
INSERT INTO `throttle` VALUES ('88', null, 'global', null, '2015-10-30 07:37:30', '2015-10-30 07:37:30');
INSERT INTO `throttle` VALUES ('89', null, 'ip', '127.0.0.1', '2015-10-30 07:37:30', '2015-10-30 07:37:30');
INSERT INTO `throttle` VALUES ('90', '1', 'user', null, '2015-10-30 07:37:30', '2015-10-30 07:37:30');
INSERT INTO `throttle` VALUES ('91', null, 'global', null, '2015-10-30 07:37:34', '2015-10-30 07:37:34');
INSERT INTO `throttle` VALUES ('92', null, 'ip', '127.0.0.1', '2015-10-30 07:37:34', '2015-10-30 07:37:34');
INSERT INTO `throttle` VALUES ('93', '1', 'user', null, '2015-10-30 07:37:34', '2015-10-30 07:37:34');
INSERT INTO `throttle` VALUES ('94', null, 'global', null, '2015-10-30 07:37:41', '2015-10-30 07:37:41');
INSERT INTO `throttle` VALUES ('95', null, 'ip', '127.0.0.1', '2015-10-30 07:37:41', '2015-10-30 07:37:41');
INSERT INTO `throttle` VALUES ('96', '1', 'user', null, '2015-10-30 07:37:41', '2015-10-30 07:37:41');
INSERT INTO `throttle` VALUES ('97', null, 'global', null, '2015-10-30 07:37:48', '2015-10-30 07:37:48');
INSERT INTO `throttle` VALUES ('98', null, 'ip', '127.0.0.1', '2015-10-30 07:37:48', '2015-10-30 07:37:48');
INSERT INTO `throttle` VALUES ('99', '1', 'user', null, '2015-10-30 07:37:48', '2015-10-30 07:37:48');
INSERT INTO `throttle` VALUES ('100', null, 'global', null, '2015-10-30 07:37:52', '2015-10-30 07:37:52');
INSERT INTO `throttle` VALUES ('101', null, 'ip', '127.0.0.1', '2015-10-30 07:37:52', '2015-10-30 07:37:52');
INSERT INTO `throttle` VALUES ('102', '1', 'user', null, '2015-10-30 07:37:52', '2015-10-30 07:37:52');
INSERT INTO `throttle` VALUES ('103', null, 'global', null, '2015-11-04 21:13:02', '2015-11-04 21:13:02');
INSERT INTO `throttle` VALUES ('104', null, 'ip', '127.0.0.1', '2015-11-04 21:13:02', '2015-11-04 21:13:02');
INSERT INTO `throttle` VALUES ('105', '1', 'user', null, '2015-11-04 21:13:02', '2015-11-04 21:13:02');
INSERT INTO `throttle` VALUES ('106', null, 'global', null, '2015-11-08 05:26:55', '2015-11-08 05:26:55');
INSERT INTO `throttle` VALUES ('107', null, 'ip', '127.0.0.1', '2015-11-08 05:26:55', '2015-11-08 05:26:55');
INSERT INTO `throttle` VALUES ('108', null, 'global', null, '2015-11-08 05:27:01', '2015-11-08 05:27:01');
INSERT INTO `throttle` VALUES ('109', null, 'ip', '127.0.0.1', '2015-11-08 05:27:01', '2015-11-08 05:27:01');
INSERT INTO `throttle` VALUES ('110', null, 'global', null, '2015-11-19 12:09:36', '2015-11-19 12:09:36');
INSERT INTO `throttle` VALUES ('111', null, 'ip', '127.0.0.1', '2015-11-19 12:09:36', '2015-11-19 12:09:36');
INSERT INTO `throttle` VALUES ('112', null, 'global', null, '2015-11-19 12:09:43', '2015-11-19 12:09:43');
INSERT INTO `throttle` VALUES ('113', null, 'ip', '127.0.0.1', '2015-11-19 12:09:43', '2015-11-19 12:09:43');
INSERT INTO `throttle` VALUES ('114', null, 'global', null, '2015-11-19 12:12:14', '2015-11-19 12:12:14');
INSERT INTO `throttle` VALUES ('115', null, 'ip', '127.0.0.1', '2015-11-19 12:12:14', '2015-11-19 12:12:14');
INSERT INTO `throttle` VALUES ('116', null, 'global', null, '2015-11-19 12:12:27', '2015-11-19 12:12:27');
INSERT INTO `throttle` VALUES ('117', null, 'ip', '127.0.0.1', '2015-11-19 12:12:27', '2015-11-19 12:12:27');
INSERT INTO `throttle` VALUES ('118', null, 'global', null, '2015-11-19 12:12:36', '2015-11-19 12:12:36');
INSERT INTO `throttle` VALUES ('119', null, 'ip', '127.0.0.1', '2015-11-19 12:12:36', '2015-11-19 12:12:36');
INSERT INTO `throttle` VALUES ('120', null, 'global', null, '2015-11-19 14:27:03', '2015-11-19 14:27:03');
INSERT INTO `throttle` VALUES ('121', null, 'ip', '::1', '2015-11-19 14:27:03', '2015-11-19 14:27:03');
INSERT INTO `throttle` VALUES ('122', null, 'global', null, '2015-11-19 14:27:06', '2015-11-19 14:27:06');
INSERT INTO `throttle` VALUES ('123', null, 'ip', '::1', '2015-11-19 14:27:06', '2015-11-19 14:27:06');
INSERT INTO `throttle` VALUES ('124', null, 'global', null, '2015-11-19 15:26:24', '2015-11-19 15:26:24');
INSERT INTO `throttle` VALUES ('125', null, 'ip', '::1', '2015-11-19 15:26:24', '2015-11-19 15:26:24');
INSERT INTO `throttle` VALUES ('126', '1', 'user', null, '2015-11-19 15:26:24', '2015-11-19 15:26:24');
INSERT INTO `throttle` VALUES ('127', null, 'global', null, '2015-11-19 15:26:27', '2015-11-19 15:26:27');
INSERT INTO `throttle` VALUES ('128', null, 'ip', '::1', '2015-11-19 15:26:27', '2015-11-19 15:26:27');
INSERT INTO `throttle` VALUES ('129', '1', 'user', null, '2015-11-19 15:26:27', '2015-11-19 15:26:27');
INSERT INTO `throttle` VALUES ('130', null, 'global', null, '2015-11-24 12:13:05', '2015-11-24 12:13:05');
INSERT INTO `throttle` VALUES ('131', null, 'ip', '::1', '2015-11-24 12:13:05', '2015-11-24 12:13:05');
INSERT INTO `throttle` VALUES ('132', '1', 'user', null, '2015-11-24 12:13:05', '2015-11-24 12:13:05');
INSERT INTO `throttle` VALUES ('133', null, 'global', null, '2015-11-24 19:14:15', '2015-11-24 19:14:15');
INSERT INTO `throttle` VALUES ('134', null, 'ip', '::1', '2015-11-24 19:14:15', '2015-11-24 19:14:15');
INSERT INTO `throttle` VALUES ('135', null, 'global', null, '2015-12-03 19:12:12', '2015-12-03 19:12:12');
INSERT INTO `throttle` VALUES ('136', null, 'ip', '::1', '2015-12-03 19:12:12', '2015-12-03 19:12:12');
INSERT INTO `throttle` VALUES ('137', '1', 'user', null, '2015-12-03 19:12:12', '2015-12-03 19:12:12');
INSERT INTO `throttle` VALUES ('138', null, 'global', null, '2015-12-03 20:32:03', '2015-12-03 20:32:03');
INSERT INTO `throttle` VALUES ('139', null, 'ip', '::1', '2015-12-03 20:32:03', '2015-12-03 20:32:03');
INSERT INTO `throttle` VALUES ('140', '1', 'user', null, '2015-12-03 20:32:03', '2015-12-03 20:32:03');
INSERT INTO `throttle` VALUES ('141', null, 'global', null, '2015-12-04 11:42:59', '2015-12-04 11:42:59');
INSERT INTO `throttle` VALUES ('142', null, 'ip', '127.0.0.1', '2015-12-04 11:42:59', '2015-12-04 11:42:59');
INSERT INTO `throttle` VALUES ('143', '1', 'user', null, '2015-12-04 11:42:59', '2015-12-04 11:42:59');
INSERT INTO `throttle` VALUES ('144', null, 'global', null, '2015-12-04 11:43:02', '2015-12-04 11:43:02');
INSERT INTO `throttle` VALUES ('145', null, 'ip', '127.0.0.1', '2015-12-04 11:43:02', '2015-12-04 11:43:02');
INSERT INTO `throttle` VALUES ('146', '1', 'user', null, '2015-12-04 11:43:02', '2015-12-04 11:43:02');
INSERT INTO `throttle` VALUES ('147', null, 'global', null, '2015-12-19 16:28:50', '2015-12-19 16:28:50');
INSERT INTO `throttle` VALUES ('148', null, 'ip', '127.0.0.1', '2015-12-19 16:28:50', '2015-12-19 16:28:50');
INSERT INTO `throttle` VALUES ('149', '1', 'user', null, '2015-12-19 16:28:50', '2015-12-19 16:28:50');
INSERT INTO `throttle` VALUES ('150', null, 'global', null, '2016-01-09 18:28:55', '2016-01-09 18:28:55');
INSERT INTO `throttle` VALUES ('151', null, 'ip', '192.168.1.121', '2016-01-09 18:28:55', '2016-01-09 18:28:55');
INSERT INTO `throttle` VALUES ('153', null, 'global', null, '2016-01-09 18:29:26', '2016-01-09 18:29:26');
INSERT INTO `throttle` VALUES ('154', null, 'ip', '192.168.1.121', '2016-01-09 18:29:26', '2016-01-09 18:29:26');
INSERT INTO `throttle` VALUES ('156', null, 'global', null, '2016-01-09 18:29:41', '2016-01-09 18:29:41');
INSERT INTO `throttle` VALUES ('157', null, 'ip', '192.168.1.121', '2016-01-09 18:29:41', '2016-01-09 18:29:41');
INSERT INTO `throttle` VALUES ('159', null, 'global', null, '2016-01-09 18:30:07', '2016-01-09 18:30:07');
INSERT INTO `throttle` VALUES ('160', null, 'ip', '192.168.1.121', '2016-01-09 18:30:07', '2016-01-09 18:30:07');
INSERT INTO `throttle` VALUES ('162', null, 'global', null, '2016-01-12 11:50:41', '2016-01-12 11:50:41');
INSERT INTO `throttle` VALUES ('163', null, 'ip', '192.168.1.142', '2016-01-12 11:50:41', '2016-01-12 11:50:41');
INSERT INTO `throttle` VALUES ('165', null, 'global', null, '2016-01-12 11:50:50', '2016-01-12 11:50:50');
INSERT INTO `throttle` VALUES ('166', null, 'ip', '192.168.1.142', '2016-01-12 11:50:50', '2016-01-12 11:50:50');
INSERT INTO `throttle` VALUES ('168', null, 'global', null, '2016-01-12 11:51:03', '2016-01-12 11:51:03');
INSERT INTO `throttle` VALUES ('169', null, 'ip', '192.168.1.142', '2016-01-12 11:51:03', '2016-01-12 11:51:03');
INSERT INTO `throttle` VALUES ('171', null, 'global', null, '2016-01-12 11:51:16', '2016-01-12 11:51:16');
INSERT INTO `throttle` VALUES ('172', null, 'ip', '192.168.1.142', '2016-01-12 11:51:16', '2016-01-12 11:51:16');
INSERT INTO `throttle` VALUES ('174', null, 'global', null, '2016-01-12 11:52:23', '2016-01-12 11:52:23');
INSERT INTO `throttle` VALUES ('175', null, 'ip', '192.168.1.142', '2016-01-12 11:52:23', '2016-01-12 11:52:23');
INSERT INTO `throttle` VALUES ('177', null, 'global', null, '2016-01-12 11:52:58', '2016-01-12 11:52:58');
INSERT INTO `throttle` VALUES ('178', null, 'ip', '192.168.1.142', '2016-01-12 11:52:58', '2016-01-12 11:52:58');
INSERT INTO `throttle` VALUES ('180', null, 'global', null, '2016-01-12 12:06:27', '2016-01-12 12:06:27');
INSERT INTO `throttle` VALUES ('181', null, 'ip', '192.168.1.142', '2016-01-12 12:06:27', '2016-01-12 12:06:27');
INSERT INTO `throttle` VALUES ('183', null, 'global', null, '2016-01-12 12:06:33', '2016-01-12 12:06:33');
INSERT INTO `throttle` VALUES ('184', null, 'ip', '192.168.1.142', '2016-01-12 12:06:33', '2016-01-12 12:06:33');
INSERT INTO `throttle` VALUES ('186', null, 'global', null, '2016-01-12 12:06:36', '2016-01-12 12:06:36');
INSERT INTO `throttle` VALUES ('187', null, 'ip', '192.168.1.142', '2016-01-12 12:06:36', '2016-01-12 12:06:36');
INSERT INTO `throttle` VALUES ('189', null, 'global', null, '2016-01-12 12:06:41', '2016-01-12 12:06:41');
INSERT INTO `throttle` VALUES ('190', null, 'ip', '192.168.1.142', '2016-01-12 12:06:41', '2016-01-12 12:06:41');
INSERT INTO `throttle` VALUES ('192', null, 'global', null, '2016-01-12 12:18:58', '2016-01-12 12:18:58');
INSERT INTO `throttle` VALUES ('193', null, 'ip', '192.168.1.142', '2016-01-12 12:18:58', '2016-01-12 12:18:58');
INSERT INTO `throttle` VALUES ('195', null, 'global', null, '2016-01-25 00:25:32', '2016-01-25 00:25:32');
INSERT INTO `throttle` VALUES ('196', null, 'ip', '127.0.0.1', '2016-01-25 00:25:32', '2016-01-25 00:25:32');
INSERT INTO `throttle` VALUES ('197', null, 'global', null, '2016-01-27 15:27:09', '2016-01-27 15:27:09');
INSERT INTO `throttle` VALUES ('198', null, 'ip', '127.0.0.1', '2016-01-27 15:27:09', '2016-01-27 15:27:09');
INSERT INTO `throttle` VALUES ('200', null, 'global', null, '2016-01-29 11:10:03', '2016-01-29 11:10:03');
INSERT INTO `throttle` VALUES ('201', null, 'ip', '127.0.0.1', '2016-01-29 11:10:03', '2016-01-29 11:10:03');
INSERT INTO `throttle` VALUES ('202', '1', 'user', null, '2016-01-29 11:10:03', '2016-01-29 11:10:03');
INSERT INTO `throttle` VALUES ('203', null, 'global', null, '2016-02-11 11:29:12', '2016-02-11 11:29:12');
INSERT INTO `throttle` VALUES ('204', null, 'ip', '127.0.0.1', '2016-02-11 11:29:12', '2016-02-11 11:29:12');
INSERT INTO `throttle` VALUES ('205', '1', 'user', null, '2016-02-11 11:29:12', '2016-02-11 11:29:12');
INSERT INTO `throttle` VALUES ('206', null, 'global', null, '2016-02-25 00:45:41', '2016-02-25 00:45:41');
INSERT INTO `throttle` VALUES ('207', null, 'ip', '192.168.1.59', '2016-02-25 00:45:41', '2016-02-25 00:45:41');
INSERT INTO `throttle` VALUES ('209', null, 'global', null, '2016-02-25 00:47:03', '2016-02-25 00:47:03');
INSERT INTO `throttle` VALUES ('210', null, 'ip', '192.168.1.59', '2016-02-25 00:47:03', '2016-02-25 00:47:03');
INSERT INTO `throttle` VALUES ('212', null, 'global', null, '2016-02-25 00:47:06', '2016-02-25 00:47:06');
INSERT INTO `throttle` VALUES ('213', null, 'ip', '192.168.1.59', '2016-02-25 00:47:06', '2016-02-25 00:47:06');
INSERT INTO `throttle` VALUES ('215', null, 'global', null, '2016-03-02 21:47:00', '2016-03-02 21:47:00');
INSERT INTO `throttle` VALUES ('216', null, 'ip', '127.0.0.1', '2016-03-02 21:47:00', '2016-03-02 21:47:00');
INSERT INTO `throttle` VALUES ('217', '1', 'user', null, '2016-03-02 21:47:00', '2016-03-02 21:47:00');
INSERT INTO `throttle` VALUES ('218', null, 'global', null, '2016-03-29 03:32:49', '2016-03-29 03:32:49');
INSERT INTO `throttle` VALUES ('219', null, 'ip', '127.0.0.1', '2016-03-29 03:32:49', '2016-03-29 03:32:49');
INSERT INTO `throttle` VALUES ('220', null, 'global', null, '2016-03-29 03:32:56', '2016-03-29 03:32:56');
INSERT INTO `throttle` VALUES ('221', null, 'ip', '127.0.0.1', '2016-03-29 03:32:56', '2016-03-29 03:32:56');
INSERT INTO `throttle` VALUES ('222', '1', 'user', null, '2016-03-29 03:32:56', '2016-03-29 03:32:56');
INSERT INTO `throttle` VALUES ('223', null, 'global', null, '2016-03-31 05:37:40', '2016-03-31 05:37:40');
INSERT INTO `throttle` VALUES ('224', null, 'ip', '::1', '2016-03-31 05:37:41', '2016-03-31 05:37:41');
INSERT INTO `throttle` VALUES ('225', null, 'global', null, '2016-05-02 22:01:37', '2016-05-02 22:01:37');
INSERT INTO `throttle` VALUES ('226', null, 'ip', '192.168.1.41', '2016-05-02 22:01:37', '2016-05-02 22:01:37');
INSERT INTO `throttle` VALUES ('227', null, 'global', null, '2016-05-02 22:01:40', '2016-05-02 22:01:40');
INSERT INTO `throttle` VALUES ('228', null, 'ip', '192.168.1.41', '2016-05-02 22:01:40', '2016-05-02 22:01:40');
INSERT INTO `throttle` VALUES ('229', null, 'global', null, '2016-05-02 22:01:43', '2016-05-02 22:01:43');
INSERT INTO `throttle` VALUES ('230', null, 'ip', '192.168.1.41', '2016-05-02 22:01:43', '2016-05-02 22:01:43');
INSERT INTO `throttle` VALUES ('231', null, 'global', null, '2016-05-02 22:01:49', '2016-05-02 22:01:49');
INSERT INTO `throttle` VALUES ('232', null, 'ip', '192.168.1.41', '2016-05-02 22:01:49', '2016-05-02 22:01:49');
INSERT INTO `throttle` VALUES ('233', null, 'global', null, '2016-05-02 22:26:12', '2016-05-02 22:26:12');
INSERT INTO `throttle` VALUES ('234', null, 'ip', '192.168.1.41', '2016-05-02 22:26:12', '2016-05-02 22:26:12');
INSERT INTO `throttle` VALUES ('235', null, 'global', null, '2016-05-02 22:26:40', '2016-05-02 22:26:40');
INSERT INTO `throttle` VALUES ('236', null, 'ip', '192.168.1.41', '2016-05-02 22:26:40', '2016-05-02 22:26:40');
INSERT INTO `throttle` VALUES ('237', null, 'global', null, '2016-05-02 22:26:58', '2016-05-02 22:26:58');
INSERT INTO `throttle` VALUES ('238', null, 'ip', '192.168.1.41', '2016-05-02 22:26:58', '2016-05-02 22:26:58');
INSERT INTO `throttle` VALUES ('239', null, 'global', null, '2016-05-02 22:27:01', '2016-05-02 22:27:01');
INSERT INTO `throttle` VALUES ('240', null, 'ip', '192.168.1.41', '2016-05-02 22:27:01', '2016-05-02 22:27:01');
INSERT INTO `throttle` VALUES ('241', null, 'global', null, '2016-05-02 22:27:07', '2016-05-02 22:27:07');
INSERT INTO `throttle` VALUES ('242', null, 'ip', '192.168.1.41', '2016-05-02 22:27:07', '2016-05-02 22:27:07');
INSERT INTO `throttle` VALUES ('243', null, 'global', null, '2017-01-16 16:13:01', '2017-01-16 16:13:01');
INSERT INTO `throttle` VALUES ('244', null, 'ip', '::1', '2017-01-16 16:13:01', '2017-01-16 16:13:01');
INSERT INTO `throttle` VALUES ('245', null, 'global', null, '2017-01-16 16:53:12', '2017-01-16 16:53:12');
INSERT INTO `throttle` VALUES ('246', null, 'ip', '::1', '2017-01-16 16:53:12', '2017-01-16 16:53:12');
INSERT INTO `throttle` VALUES ('248', null, 'global', null, '2017-01-16 16:53:40', '2017-01-16 16:53:40');
INSERT INTO `throttle` VALUES ('249', null, 'ip', '::1', '2017-01-16 16:53:40', '2017-01-16 16:53:40');
INSERT INTO `throttle` VALUES ('251', null, 'global', null, '2017-01-19 13:05:37', '2017-01-19 13:05:37');
INSERT INTO `throttle` VALUES ('252', null, 'ip', '::1', '2017-01-19 13:05:37', '2017-01-19 13:05:37');
INSERT INTO `throttle` VALUES ('253', null, 'global', null, '2017-01-26 00:29:52', '2017-01-26 00:29:52');
INSERT INTO `throttle` VALUES ('254', null, 'ip', '::1', '2017-01-26 00:29:52', '2017-01-26 00:29:52');
INSERT INTO `throttle` VALUES ('255', null, 'global', null, '2017-04-13 02:55:26', '2017-04-13 02:55:26');
INSERT INTO `throttle` VALUES ('256', null, 'ip', '112.135.7.121', '2017-04-13 02:55:26', '2017-04-13 02:55:26');
INSERT INTO `throttle` VALUES ('257', '9', 'user', null, '2017-04-13 02:55:26', '2017-04-13 02:55:26');
INSERT INTO `throttle` VALUES ('258', null, 'global', null, '2017-04-25 00:17:15', '2017-04-25 00:17:15');
INSERT INTO `throttle` VALUES ('259', null, 'ip', '::1', '2017-04-25 00:17:15', '2017-04-25 00:17:15');
INSERT INTO `throttle` VALUES ('260', null, 'global', null, '2017-05-01 00:53:53', '2017-05-01 00:53:53');
INSERT INTO `throttle` VALUES ('261', null, 'ip', '123.231.108.3', '2017-05-01 00:53:53', '2017-05-01 00:53:53');
INSERT INTO `throttle` VALUES ('262', '9', 'user', null, '2017-05-01 00:53:53', '2017-05-01 00:53:53');
INSERT INTO `throttle` VALUES ('263', null, 'global', null, '2017-05-01 00:53:58', '2017-05-01 00:53:58');
INSERT INTO `throttle` VALUES ('264', null, 'ip', '123.231.108.3', '2017-05-01 00:53:58', '2017-05-01 00:53:58');
INSERT INTO `throttle` VALUES ('265', '9', 'user', null, '2017-05-01 00:53:58', '2017-05-01 00:53:58');
INSERT INTO `throttle` VALUES ('266', null, 'global', null, '2017-05-01 00:54:05', '2017-05-01 00:54:05');
INSERT INTO `throttle` VALUES ('267', null, 'ip', '123.231.108.3', '2017-05-01 00:54:05', '2017-05-01 00:54:05');
INSERT INTO `throttle` VALUES ('268', '9', 'user', null, '2017-05-01 00:54:05', '2017-05-01 00:54:05');
INSERT INTO `throttle` VALUES ('269', null, 'global', null, '2017-05-01 00:54:14', '2017-05-01 00:54:14');
INSERT INTO `throttle` VALUES ('270', null, 'ip', '123.231.108.3', '2017-05-01 00:54:14', '2017-05-01 00:54:14');
INSERT INTO `throttle` VALUES ('271', '9', 'user', null, '2017-05-01 00:54:14', '2017-05-01 00:54:14');
INSERT INTO `throttle` VALUES ('272', null, 'global', null, '2017-05-01 00:54:19', '2017-05-01 00:54:19');
INSERT INTO `throttle` VALUES ('273', null, 'ip', '123.231.108.3', '2017-05-01 00:54:19', '2017-05-01 00:54:19');
INSERT INTO `throttle` VALUES ('274', '9', 'user', null, '2017-05-01 00:54:19', '2017-05-01 00:54:19');
INSERT INTO `throttle` VALUES ('275', null, 'global', null, '2017-05-01 00:54:24', '2017-05-01 00:54:24');
INSERT INTO `throttle` VALUES ('276', null, 'ip', '123.231.108.3', '2017-05-01 00:54:24', '2017-05-01 00:54:24');
INSERT INTO `throttle` VALUES ('277', '9', 'user', null, '2017-05-01 00:54:24', '2017-05-01 00:54:24');
INSERT INTO `throttle` VALUES ('278', null, 'global', null, '2017-08-31 00:27:01', '2017-08-31 00:27:01');
INSERT INTO `throttle` VALUES ('279', null, 'ip', '113.59.213.196', '2017-08-31 00:27:01', '2017-08-31 00:27:01');
INSERT INTO `throttle` VALUES ('280', null, 'global', null, '2017-09-19 05:32:04', '2017-09-19 05:32:04');
INSERT INTO `throttle` VALUES ('281', null, 'ip', '::1', '2017-09-19 05:32:04', '2017-09-19 05:32:04');
INSERT INTO `throttle` VALUES ('282', null, 'global', null, '2017-09-26 23:46:17', '2017-09-26 23:46:17');
INSERT INTO `throttle` VALUES ('283', null, 'ip', '::1', '2017-09-26 23:46:17', '2017-09-26 23:46:17');
INSERT INTO `throttle` VALUES ('284', '9', 'user', null, '2017-09-26 23:46:17', '2017-09-26 23:46:17');
INSERT INTO `throttle` VALUES ('285', null, 'global', null, '2017-10-18 10:52:42', '2017-10-18 10:52:42');
INSERT INTO `throttle` VALUES ('286', null, 'ip', '::1', '2017-10-18 10:52:42', '2017-10-18 10:52:42');
INSERT INTO `throttle` VALUES ('287', null, 'global', null, '2018-01-16 07:59:41', '2018-01-16 07:59:41');
INSERT INTO `throttle` VALUES ('288', null, 'ip', '::1', '2018-01-16 07:59:41', '2018-01-16 07:59:41');
INSERT INTO `throttle` VALUES ('289', '1', 'user', null, '2018-01-16 07:59:41', '2018-01-16 07:59:41');
INSERT INTO `throttle` VALUES ('290', null, 'global', null, '2018-01-19 09:53:38', '2018-01-19 09:53:38');
INSERT INTO `throttle` VALUES ('291', null, 'ip', '::1', '2018-01-19 09:53:39', '2018-01-19 09:53:39');
INSERT INTO `throttle` VALUES ('292', '1', 'user', null, '2018-01-19 09:53:39', '2018-01-19 09:53:39');
INSERT INTO `throttle` VALUES ('293', null, 'global', null, '2018-01-26 07:58:56', '2018-01-26 07:58:56');
INSERT INTO `throttle` VALUES ('294', null, 'ip', '::1', '2018-01-26 07:58:56', '2018-01-26 07:58:56');
INSERT INTO `throttle` VALUES ('295', '1', 'user', null, '2018-01-26 07:58:56', '2018-01-26 07:58:56');
INSERT INTO `throttle` VALUES ('296', null, 'global', null, '2018-01-27 23:18:02', '2018-01-27 23:18:02');
INSERT INTO `throttle` VALUES ('297', null, 'ip', '::1', '2018-01-27 23:18:02', '2018-01-27 23:18:02');
INSERT INTO `throttle` VALUES ('298', '1', 'user', null, '2018-01-27 23:18:02', '2018-01-27 23:18:02');
INSERT INTO `throttle` VALUES ('299', null, 'global', null, '2018-02-21 08:40:48', '2018-02-21 08:40:48');
INSERT INTO `throttle` VALUES ('300', null, 'ip', '::1', '2018-02-21 08:40:48', '2018-02-21 08:40:48');
INSERT INTO `throttle` VALUES ('301', null, 'global', null, '2018-02-21 08:40:55', '2018-02-21 08:40:55');
INSERT INTO `throttle` VALUES ('302', null, 'ip', '::1', '2018-02-21 08:40:55', '2018-02-21 08:40:55');
INSERT INTO `throttle` VALUES ('303', null, 'global', null, '2018-02-21 08:44:06', '2018-02-21 08:44:06');
INSERT INTO `throttle` VALUES ('304', null, 'ip', '::1', '2018-02-21 08:44:07', '2018-02-21 08:44:07');
INSERT INTO `throttle` VALUES ('305', null, 'global', null, '2018-03-02 03:16:22', '2018-03-02 03:16:22');
INSERT INTO `throttle` VALUES ('306', null, 'ip', '::1', '2018-03-02 03:16:22', '2018-03-02 03:16:22');
INSERT INTO `throttle` VALUES ('307', '1', 'user', null, '2018-03-02 03:16:23', '2018-03-02 03:16:23');
INSERT INTO `throttle` VALUES ('308', null, 'global', null, '2018-03-14 09:17:06', '2018-03-14 09:17:06');
INSERT INTO `throttle` VALUES ('309', null, 'ip', '::1', '2018-03-14 09:17:07', '2018-03-14 09:17:07');
INSERT INTO `throttle` VALUES ('310', null, 'global', null, '2018-03-16 09:04:26', '2018-03-16 09:04:26');
INSERT INTO `throttle` VALUES ('311', null, 'ip', '::1', '2018-03-16 09:04:29', '2018-03-16 09:04:29');
INSERT INTO `throttle` VALUES ('312', '1', 'user', null, '2018-03-16 09:04:38', '2018-03-16 09:04:38');
INSERT INTO `throttle` VALUES ('313', null, 'global', null, '2018-04-01 00:22:11', '2018-04-01 00:22:11');
INSERT INTO `throttle` VALUES ('314', null, 'ip', '::1', '2018-04-01 00:22:11', '2018-04-01 00:22:11');
INSERT INTO `throttle` VALUES ('315', null, 'global', null, '2018-04-01 00:22:27', '2018-04-01 00:22:27');
INSERT INTO `throttle` VALUES ('316', null, 'ip', '::1', '2018-04-01 00:22:28', '2018-04-01 00:22:28');
INSERT INTO `throttle` VALUES ('317', null, 'global', null, '2018-04-01 00:24:03', '2018-04-01 00:24:03');
INSERT INTO `throttle` VALUES ('318', null, 'ip', '::1', '2018-04-01 00:24:03', '2018-04-01 00:24:03');
INSERT INTO `throttle` VALUES ('319', null, 'global', null, '2018-04-01 00:24:20', '2018-04-01 00:24:20');
INSERT INTO `throttle` VALUES ('320', null, 'ip', '::1', '2018-04-01 00:24:20', '2018-04-01 00:24:20');
INSERT INTO `throttle` VALUES ('321', null, 'global', null, '2018-04-04 08:13:38', '2018-04-04 08:13:38');
INSERT INTO `throttle` VALUES ('322', null, 'ip', '::1', '2018-04-04 08:13:39', '2018-04-04 08:13:39');
INSERT INTO `throttle` VALUES ('323', '1', 'user', null, '2018-04-04 08:13:39', '2018-04-04 08:13:39');
INSERT INTO `throttle` VALUES ('324', null, 'global', null, '2018-04-05 08:44:35', '2018-04-05 08:44:35');
INSERT INTO `throttle` VALUES ('325', null, 'ip', '::1', '2018-04-05 08:44:35', '2018-04-05 08:44:35');
INSERT INTO `throttle` VALUES ('326', '1', 'user', null, '2018-04-05 08:44:36', '2018-04-05 08:44:36');
INSERT INTO `throttle` VALUES ('327', null, 'global', null, '2018-04-10 08:55:44', '2018-04-10 08:55:44');
INSERT INTO `throttle` VALUES ('328', null, 'ip', '::1', '2018-04-10 08:55:45', '2018-04-10 08:55:45');
INSERT INTO `throttle` VALUES ('329', '1', 'user', null, '2018-04-10 08:55:45', '2018-04-10 08:55:45');
INSERT INTO `throttle` VALUES ('330', null, 'global', null, '2018-04-26 08:49:24', '2018-04-26 08:49:24');
INSERT INTO `throttle` VALUES ('331', null, 'ip', '::1', '2018-04-26 08:49:24', '2018-04-26 08:49:24');
INSERT INTO `throttle` VALUES ('332', null, 'global', null, '2018-04-26 08:54:07', '2018-04-26 08:54:07');
INSERT INTO `throttle` VALUES ('333', null, 'ip', '::1', '2018-04-26 08:54:07', '2018-04-26 08:54:07');
INSERT INTO `throttle` VALUES ('334', null, 'global', null, '2018-04-26 08:54:13', '2018-04-26 08:54:13');
INSERT INTO `throttle` VALUES ('335', null, 'ip', '::1', '2018-04-26 08:54:13', '2018-04-26 08:54:13');
INSERT INTO `throttle` VALUES ('336', null, 'global', null, '2018-05-08 00:19:15', '2018-05-08 00:19:15');
INSERT INTO `throttle` VALUES ('337', null, 'ip', '::1', '2018-05-08 00:19:16', '2018-05-08 00:19:16');
INSERT INTO `throttle` VALUES ('338', '1', 'user', null, '2018-05-08 00:19:16', '2018-05-08 00:19:16');
INSERT INTO `throttle` VALUES ('339', null, 'global', null, '2018-05-08 00:56:29', '2018-05-08 00:56:29');
INSERT INTO `throttle` VALUES ('340', null, 'ip', '::1', '2018-05-08 00:56:30', '2018-05-08 00:56:30');
INSERT INTO `throttle` VALUES ('341', null, 'global', null, '2018-05-14 08:30:36', '2018-05-14 08:30:36');
INSERT INTO `throttle` VALUES ('342', null, 'ip', '112.135.9.29', '2018-05-14 08:30:36', '2018-05-14 08:30:36');
INSERT INTO `throttle` VALUES ('343', null, 'global', null, '2018-05-14 08:31:53', '2018-05-14 08:31:53');
INSERT INTO `throttle` VALUES ('344', null, 'ip', '112.135.9.29', '2018-05-14 08:31:53', '2018-05-14 08:31:53');
INSERT INTO `throttle` VALUES ('345', null, 'global', null, '2018-05-14 08:32:54', '2018-05-14 08:32:54');
INSERT INTO `throttle` VALUES ('346', null, 'ip', '112.135.9.29', '2018-05-14 08:32:54', '2018-05-14 08:32:54');
INSERT INTO `throttle` VALUES ('347', null, 'global', null, '2018-05-14 08:33:24', '2018-05-14 08:33:24');
INSERT INTO `throttle` VALUES ('348', null, 'ip', '112.135.9.29', '2018-05-14 08:33:24', '2018-05-14 08:33:24');
INSERT INTO `throttle` VALUES ('349', null, 'global', null, '2018-05-14 08:33:34', '2018-05-14 08:33:34');
INSERT INTO `throttle` VALUES ('350', null, 'ip', '112.135.9.29', '2018-05-14 08:33:34', '2018-05-14 08:33:34');
INSERT INTO `throttle` VALUES ('351', null, 'global', null, '2018-05-14 08:37:08', '2018-05-14 08:37:08');
INSERT INTO `throttle` VALUES ('352', null, 'ip', '112.135.9.29', '2018-05-14 08:37:08', '2018-05-14 08:37:08');
INSERT INTO `throttle` VALUES ('353', null, 'global', null, '2018-05-14 08:50:37', '2018-05-14 08:50:37');
INSERT INTO `throttle` VALUES ('354', null, 'ip', '112.135.9.29', '2018-05-14 08:50:37', '2018-05-14 08:50:37');
INSERT INTO `throttle` VALUES ('355', null, 'global', null, '2018-05-14 08:50:42', '2018-05-14 08:50:42');
INSERT INTO `throttle` VALUES ('356', null, 'ip', '112.135.9.29', '2018-05-14 08:50:42', '2018-05-14 08:50:42');
INSERT INTO `throttle` VALUES ('357', null, 'global', null, '2018-05-14 08:50:46', '2018-05-14 08:50:46');
INSERT INTO `throttle` VALUES ('358', null, 'ip', '112.135.9.29', '2018-05-14 08:50:46', '2018-05-14 08:50:46');
INSERT INTO `throttle` VALUES ('359', null, 'global', null, '2018-05-14 09:27:30', '2018-05-14 09:27:30');
INSERT INTO `throttle` VALUES ('360', null, 'ip', '112.135.10.73', '2018-05-14 09:27:30', '2018-05-14 09:27:30');
INSERT INTO `throttle` VALUES ('361', null, 'global', null, '2018-05-22 04:16:34', '2018-05-22 04:16:34');
INSERT INTO `throttle` VALUES ('362', null, 'ip', '112.135.1.191', '2018-05-22 04:16:34', '2018-05-22 04:16:34');
INSERT INTO `throttle` VALUES ('363', null, 'global', null, '2018-05-22 23:17:39', '2018-05-22 23:17:39');
INSERT INTO `throttle` VALUES ('364', null, 'ip', '112.135.0.158', '2018-05-22 23:17:39', '2018-05-22 23:17:39');
INSERT INTO `throttle` VALUES ('365', null, 'global', null, '2018-05-22 23:17:42', '2018-05-22 23:17:42');
INSERT INTO `throttle` VALUES ('366', null, 'ip', '112.135.0.158', '2018-05-22 23:17:42', '2018-05-22 23:17:42');
INSERT INTO `throttle` VALUES ('367', null, 'global', null, '2018-05-22 23:17:49', '2018-05-22 23:17:49');
INSERT INTO `throttle` VALUES ('368', null, 'ip', '112.135.0.158', '2018-05-22 23:17:49', '2018-05-22 23:17:49');
INSERT INTO `throttle` VALUES ('369', null, 'global', null, '2018-05-22 23:17:53', '2018-05-22 23:17:53');
INSERT INTO `throttle` VALUES ('370', null, 'ip', '112.135.0.158', '2018-05-22 23:17:53', '2018-05-22 23:17:53');
INSERT INTO `throttle` VALUES ('371', null, 'global', null, '2018-05-27 22:43:45', '2018-05-27 22:43:45');
INSERT INTO `throttle` VALUES ('372', null, 'ip', '112.135.11.10', '2018-05-27 22:43:45', '2018-05-27 22:43:45');
INSERT INTO `throttle` VALUES ('373', '9', 'user', null, '2018-05-27 22:43:45', '2018-05-27 22:43:45');
INSERT INTO `throttle` VALUES ('374', null, 'global', null, '2018-05-30 11:59:54', '2018-05-30 11:59:54');
INSERT INTO `throttle` VALUES ('375', null, 'ip', '112.135.0.37', '2018-05-30 11:59:54', '2018-05-30 11:59:54');
INSERT INTO `throttle` VALUES ('376', null, 'global', null, '2018-06-10 20:03:18', '2018-06-10 20:03:18');
INSERT INTO `throttle` VALUES ('377', null, 'ip', '112.134.121.103', '2018-06-10 20:03:18', '2018-06-10 20:03:18');
INSERT INTO `throttle` VALUES ('378', '1', 'user', null, '2018-06-10 20:03:18', '2018-06-10 20:03:18');
INSERT INTO `throttle` VALUES ('379', null, 'global', null, '2018-06-16 14:48:07', '2018-06-16 14:48:07');
INSERT INTO `throttle` VALUES ('380', null, 'ip', '112.135.9.96', '2018-06-16 14:48:07', '2018-06-16 14:48:07');
INSERT INTO `throttle` VALUES ('381', '9', 'user', null, '2018-06-16 14:48:07', '2018-06-16 14:48:07');
INSERT INTO `throttle` VALUES ('382', null, 'global', null, '2018-07-12 21:56:18', '2018-07-12 21:56:18');
INSERT INTO `throttle` VALUES ('383', null, 'ip', '123.231.109.4', '2018-07-12 21:56:18', '2018-07-12 21:56:18');
INSERT INTO `throttle` VALUES ('384', null, 'global', null, '2018-07-12 21:57:21', '2018-07-12 21:57:21');
INSERT INTO `throttle` VALUES ('385', null, 'ip', '123.231.109.4', '2018-07-12 21:57:21', '2018-07-12 21:57:21');
INSERT INTO `throttle` VALUES ('386', '9', 'user', null, '2018-07-12 21:57:21', '2018-07-12 21:57:21');
INSERT INTO `throttle` VALUES ('387', null, 'global', null, '2018-07-26 22:22:12', '2018-07-26 22:22:12');
INSERT INTO `throttle` VALUES ('388', null, 'ip', '::1', '2018-07-26 22:22:12', '2018-07-26 22:22:12');
INSERT INTO `throttle` VALUES ('389', '1', 'user', null, '2018-07-26 22:22:12', '2018-07-26 22:22:12');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fb_id` text COLLATE utf8_unicode_ci NOT NULL,
  `g_id` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8_unicode_ci,
  `branch` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `supervisor_id` int(25) DEFAULT NULL,
  `lft` int(25) DEFAULT NULL,
  `rgt` int(25) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '', '', 'Super', 'Administrator', 'super.admin', 'admin@admin.lk', null, '0', '$2y$10$vJ0/.9l8qByoN/ZFJlzew.U3SUrYnsI6QoPezSfeo9qDQYLJSjC/O', '{\"admin\":true,\"index\":true}', '2018-07-26 22:22:21', null, '7', '16', '0', '1', '2015-07-12 00:09:31', '2018-07-26 22:22:21');
INSERT INTO `users` VALUES ('9', '', '', 'Desawana', 'Administrator', 'desawana.admin', 'developer@admin.com', null, '0', '$2y$10$ijimT7X0fQADncEnlwgRZelKPlCoLE81YVjgQlX25F64Ulf5JHiaG', null, '2018-07-25 23:45:36', '1', '12', '15', '1', '1', '2017-01-24 01:05:38', '2018-07-25 23:58:24');
INSERT INTO `users` VALUES ('10', '', '', null, null, 'lakshitham', 'rullzzm@gmail.comm', null, '0', '$2y$10$KYJLzG26TupSdCXfRh9XRO0VctPDiWMSfbr2ooltjOGpZqZvf5KSS', null, '2017-12-31 03:26:38', null, '17', '18', '0', null, '2017-12-28 08:10:24', '2018-04-04 08:19:48');
INSERT INTO `users` VALUES ('11', '', '', 'Test', 'test', 'test', '', null, '0', '$2y$10$dvRBZsUn6zr2aueDjT3hVOQOlFB3AlO67NQ7aQaO4ge89y447Qo2q', null, null, '9', '13', '14', '2', null, '2018-04-04 08:19:44', '2018-04-04 08:22:11');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `sbu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES ('2', '2', '1', '2016-03-24 03:03:10', '2016-03-24 03:03:10');
INSERT INTO `users_groups` VALUES ('3', '3', '2', '2016-03-24 03:26:12', '2016-03-24 03:26:12');

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube_link` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `img` text COLLATE utf8_unicode_ci,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `videos_user_id_foreign` (`user_id`),
  CONSTRAINT `videos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES ('1', 'Man Pathanawa (Samu Aran Ya Yuthui)', 'https://www.youtube.com/watch?v=ZCdO0Xr5oG8', '2018-05-13 23:37:10', '2018-05-13 23:37:10', 'core/storage/uploads/images/video/5af7cf06209bd.jpg', '9');
INSERT INTO `videos` VALUES ('2', 'Saththai Oya (Man Witharada Adare Kale)', 'https://www.youtube.com/watch?v=oE5MewJuEI8', '2018-05-13 23:38:26', '2018-05-22 04:55:13', null, '9');
INSERT INTO `videos` VALUES ('3', 'Heenayakda Me - Ashan Fernando ft Dilki Uresha', 'https://www.youtube.com/watch?v=cup44QvubSs', '2018-05-13 23:39:40', '2018-05-13 23:39:40', 'core/storage/uploads/images/video/5af7cf9c017ef.jpg', '9');
INSERT INTO `videos` VALUES ('4', 'Adarei Wasthu', 'https://www.youtube.com/watch?v=CL-Tz5yQ5SQ', '2018-05-15 01:44:32', '2018-06-14 02:50:21', null, '9');
INSERT INTO `videos` VALUES ('5', 'Adarei Katawath', 'https://www.youtube.com/watch?v=CKiN_dU9Yuo', '2018-05-15 02:01:00', '2018-07-05 22:59:16', null, '9');
INSERT INTO `videos` VALUES ('6', 'SUDU Official Music Video', 'https://www.youtube.com/watch?v=FLu5mM0h2z4', '2018-05-22 05:29:45', '2018-06-14 02:59:59', null, '9');
INSERT INTO `videos` VALUES ('7', 'Saragaye Official Music Video', 'https://www.youtube.com/watch?v=izKxgHWzQ80', '2018-05-22 07:34:48', '2018-06-14 02:59:37', null, '9');
INSERT INTO `videos` VALUES ('8', 'Amathaka Karanna Ba', 'https://www.youtube.com/watch?v=kFxfPK77AXA', '2018-05-27 00:11:08', '2018-06-14 02:59:20', null, '9');
INSERT INTO `videos` VALUES ('9', 'Waradak Kiyanne Na', 'https://www.youtube.com/watch?v=22T5m-wT14U', '2018-05-27 03:03:20', '2018-06-14 02:59:03', null, '9');
INSERT INTO `videos` VALUES ('10', 'Amathaka Karanna Ba', 'https://www.youtube.com/watch?v=0GZHKWi9KUM', '2018-05-30 00:27:09', '2018-06-14 02:58:49', null, '9');
INSERT INTO `videos` VALUES ('11', 'Awasan Pema Mage', 'https://www.youtube.com/watch?v=rv8UsNcI_M0', '2018-05-30 01:16:10', '2018-06-14 02:58:34', null, '9');
INSERT INTO `videos` VALUES ('12', 'Durin Hinda Ma', 'https://www.youtube.com/watch?v=nS5rS2lSRXI', '2018-05-30 01:17:00', '2018-06-14 02:58:13', null, '9');
INSERT INTO `videos` VALUES ('13', 'Kiyabu Lathawe', 'https://www.youtube.com/watch?v=02nz535ELr8', '2018-05-30 01:18:19', '2018-07-05 22:59:41', null, '9');
INSERT INTO `videos` VALUES ('14', 'Ma Hara Giya Dine', 'https://www.youtube.com/watch?v=tSqN9HfsiT8', '2018-05-30 01:18:57', '2018-06-14 02:57:26', null, '9');
INSERT INTO `videos` VALUES ('15', 'Ma Me Tharam Handawala', 'https://www.youtube.com/watch?v=EMNj7Adrj7Y', '2018-05-30 01:19:21', '2018-06-14 02:57:08', null, '9');
INSERT INTO `videos` VALUES ('16', 'Me Hitha Langa', 'https://www.youtube.com/watch?v=FYTMDSvfPd4', '2018-05-30 01:19:49', '2018-06-14 02:56:53', null, '9');
INSERT INTO `videos` VALUES ('17', 'Nidukin Inu Mana', 'https://www.youtube.com/watch?v=sN2AaSZ0ogY', '2018-05-30 01:20:18', '2018-06-14 02:56:38', null, '9');
INSERT INTO `videos` VALUES ('18', 'Oba Ekka Mama', 'https://www.youtube.com/watch?v=LDgphkZukOQ', '2018-05-30 01:21:04', '2018-06-14 02:56:24', null, '9');
INSERT INTO `videos` VALUES ('19', 'Obamada Me Hithata Mage', 'https://www.youtube.com/watch?v=206gpCGqa0o', '2018-05-30 01:21:29', '2018-06-14 02:56:10', null, '9');
INSERT INTO `videos` VALUES ('20', 'Para Kiyana Tharukawi', 'https://www.youtube.com/watch?v=FpaPGjiOldE', '2018-05-30 01:22:09', '2018-06-14 02:55:52', null, '9');
INSERT INTO `videos` VALUES ('21', 'Senehasa Bidunath', 'https://www.youtube.com/watch?v=nlxU2_yPzh0', '2018-05-30 01:22:39', '2018-06-14 02:55:35', null, '9');
INSERT INTO `videos` VALUES ('22', 'Awasana Premayai Mage', 'https://www.youtube.com/watch?v=CSqy1zTOzW0', '2018-05-30 01:23:25', '2018-06-14 02:55:11', null, '9');
INSERT INTO `videos` VALUES ('24', 'Wenna Thiyena', 'https://www.youtube.com/watch?v=QvOwg08pKU4', '2018-05-30 01:24:26', '2018-06-14 02:54:13', null, '9');
INSERT INTO `videos` VALUES ('25', 'Akeekaruma Man Udura', 'https://www.youtube.com/watch?v=zJQ8bXmzjz4', '2018-05-30 01:48:38', '2018-06-14 02:53:59', null, '9');
INSERT INTO `videos` VALUES ('26', 'Waradak Kiyanne Na', 'https://www.youtube.com/watch?v=22T5m-wT14U', '2018-05-30 01:49:22', '2018-06-14 02:53:39', null, '9');
INSERT INTO `videos` VALUES ('28', 'Arayum Karanne', 'https://www.youtube.com/watch?v=OYAzt5836ww', '2018-05-30 01:51:05', '2018-06-14 02:53:05', null, '9');
INSERT INTO `videos` VALUES ('29', 'Diwranna Behe Neda Music Video', 'https://www.youtube.com/watch?v=2cCIpSWHuDY', '2018-05-31 07:57:31', '2018-06-14 02:52:49', null, '9');
INSERT INTO `videos` VALUES ('30', 'Dahata Nopeni Inna Official Music Video', 'https://www.youtube.com/watch?v=W9aa4IdzFhI', '2018-06-02 05:09:05', '2018-06-14 02:52:02', null, '9');
INSERT INTO `videos` VALUES ('31', 'Hitha Gawa Heena Official Music Video', 'https://www.youtube.com/watch?v=470nK33PpHs', '2018-06-03 05:50:38', '2018-06-14 02:52:30', null, '9');
INSERT INTO `videos` VALUES ('32', 'Oyata Vitharak Music Video', 'https://www.youtube.com/watch?v=g1VIjPy4eEE', '2018-06-03 07:21:57', '2018-06-14 02:51:24', null, '9');
INSERT INTO `videos` VALUES ('33', 'Mathaka Mawee Official Music Video', 'https://www.youtube.com/watch?v=VAwKXPWNVvY', '2018-06-07 00:23:34', '2018-06-14 02:50:56', null, '9');
INSERT INTO `videos` VALUES ('34', 'Aulak Nane Official Music Video', 'https://www.youtube.com/watch?v=-fCPsxVYi4s', '2018-06-15 12:07:01', '2018-06-15 12:07:01', 'core/storage/uploads/images/video/5b22aec531eb4.jpg', '9');
INSERT INTO `videos` VALUES ('35', 'Sulan Podak Wee', 'https://www.youtube.com/watch?v=tQTRRe9Jt5U', '2018-06-15 12:33:32', '2018-06-15 12:33:32', 'core/storage/uploads/images/video/5b22b4fc32ae1.jpg', '9');
INSERT INTO `videos` VALUES ('36', 'Obe Adare Official Music Video', 'https://www.youtube.com/watch?v=doPvhVdkNA4', '2018-06-26 08:26:26', '2018-06-26 08:26:26', 'core/storage/uploads/images/video/5b30fb92e74a6.jpg', '9');
INSERT INTO `videos` VALUES ('37', 'Nodakapu Dewani Budun', 'https://www.youtube.com/watch?v=5T_sH5RRWqA', '2018-07-05 08:27:34', '2018-07-05 08:27:34', 'core/storage/uploads/images/video/5b3cd9560615d.jfif', '9');
INSERT INTO `videos` VALUES ('38', 'Sareetha - Viraj Perera Official Music Video', 'https://www.youtube.com/watch?v=AHmbpkg0PaA', '2018-07-24 11:35:25', '2018-07-24 11:35:25', 'core/storage/uploads/images/video/5b5611dd3236d.jpg', '9');
INSERT INTO `videos` VALUES ('39', 'Manamaliye - Tehan Perera ft. Hot Chocolate', 'https://www.youtube.com/watch?v=yCj-DbU-aUQ', '2018-07-25 21:58:56', '2018-07-25 21:58:56', 'core/storage/uploads/images/video/5b57f5807d318.jpg', '9');
INSERT INTO `videos` VALUES ('40', 'Anthima Mohothedi - Nilan Hettiarachchi Official Music Video', 'https://www.youtube.com/watch?v=WGY7xgGHlh0', '2018-07-25 22:33:17', '2018-07-25 22:33:17', 'core/storage/uploads/images/video/5b57fd8d213f4.jpg', '9');
