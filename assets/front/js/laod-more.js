
function getAudiosData() {
  $("#audiosLoadMoreBtn").html("<span>Please wait <i class='fa fa-spinner fa-spin'></i></span>")
  var formData = new FormData();
  formData.append("page", currrentAudiosPage);
  formData.append("_token", token);
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
  } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.addEventListener("load", function (data){
    if (xmlhttp.status == 200) {
      audioData = JSON.parse(xmlhttp.responseText);
      audioData = audioData.audioData;
      if (audioData == "") {
        $("#audiosLoadMoreBtn").hide()
      }else{
        $("#audiosLoadMoreBtn").html("<span>Load More</span>")
        $("#audoDetails").append(audioData)
        currrentAudiosPage++;
      }

    }else{
      $("#audiosLoadMoreBtn").html("<span>Load More</span>")
    }
  });
  xmlhttp.addEventListener("error", function (data){

      $("#audiosLoadMoreBtn").html("<span>Load More</span>")

  });
  xmlhttp.open('POST', getAudiosUrl, false);
  xmlhttp.send(formData);
}
function getMusicData() {
  $("#musicLoadMoreBtn").html("<span>Please wait <i class='fa fa-spinner fa-spin'></i></span>")
  var formData = new FormData();
  formData.append("page", currrentMusicPage);
  formData.append("_token", token);
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
  } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.addEventListener("load", function (data){
    if (xmlhttp.status == 200) {
      audioData = JSON.parse(xmlhttp.responseText);
      audioData = audioData.audioData;
      if (audioData == "") {
        $("#musicLoadMoreBtn").hide()
      }else{
        $("#musicLoadMoreBtn").html("<span>Load More</span>")
        $("#musicDetails").append(audioData)
        currrentMusicPage++;
      }

    }else{
      $("#musicLoadMoreBtn").html("<span>Load More</span>")
    }
  });
  xmlhttp.addEventListener("error", function (data){

      $("#musicLoadMoreBtn").html("<span>Load More</span>")

  });
  xmlhttp.open('POST', getMusicsUrl, false);
  xmlhttp.send(formData);
}
function getLyricsData() {
  $("#lyricsLoadMoreBtn").html("<span>Please wait <i class='fa fa-spinner fa-spin'></i></span>")
  var formData = new FormData();
  formData.append("page", currrentLyrcPage);
  formData.append("_token", token);
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
  } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.addEventListener("load", function (data){
    if (xmlhttp.status == 200) {
      audioData = JSON.parse(xmlhttp.responseText);
      audioData = audioData.audioData;
      if (audioData == "") {
        $("#lyricsLoadMoreBtn").hide()
      }else{
        $("#lyricsLoadMoreBtn").html("<span>Load More</span>")
        $("#lyricDetails").append(audioData)
        currrentLyrcPage++;
      }

    }else{
      $("#lyricsLoadMoreBtn").html("<span>Load More</span>")
    }
  });
  xmlhttp.addEventListener("error", function (data){

      $("#lyricsLoadMoreBtn").html("<span>Load More</span>")

  });
  xmlhttp.open('POST', getLyricsUrl, false);
  xmlhttp.send(formData);
}
function getVideosData() {
  $("#videosLoadMoreBtn").html("<span>Please wait <i class='fa fa-spinner fa-spin'></i></span>")
  var formData = new FormData();
  formData.append("page", currrentVideosPage);
  formData.append("_token", token);
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
  } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.addEventListener("load", function (data){
    if (xmlhttp.status == 200) {
      videoData = JSON.parse(xmlhttp.responseText);
      videoData = videoData.videoData;
      if (videoData == "") {
        $("#videosLoadMoreBtn").hide()
      }else{
        $("#videosLoadMoreBtn").html("<span>Load More</span>")
        $("#videoDetails").append(videoData)
        currrentVideosPage++;
      }
      setHeightVideos()

    }else{
      $("#videosLoadMoreBtn").html("<span>Load More</span>")
    }
  });
  xmlhttp.addEventListener("error", function (data){

      $("#videosLoadMoreBtn").html("<span>Load More</span>")

  });
  xmlhttp.open('POST', getVideosUrl,  false);
  xmlhttp.send(formData);
}
function setHeightVideos(){

  // $('.setHeight_2').matchHeight({
  //   byRow: true,
  //   property: 'height',
  // });
}
