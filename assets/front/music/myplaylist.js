
jQuery(document).ready(function ($) {
	$('.music-player-list').ttwMusicPlayer(myPlaylist, {
		currencySymbol:'',
		buyText:'<i class="fa fa-download" aria-hidden="true"></i>',
		tracksToShow:3,
		autoplay:false,

		jPlayer:{
			swfPath: "js/Jplayer.swf",
			supplied: "mp3",
			volume:  0.8,
			wmode:"window",
			solution: "html,flash",
			errorAlerts: true,
			warningAlerts: false
		}
	});
});
