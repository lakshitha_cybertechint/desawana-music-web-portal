/*
	For Edit This File Please Read Documentation
*/


jQuery(document).ready(function ($) {
	$('.music-single').ttwMusicPlayer(myPlaylist, {
		currencySymbol:'',
		buyText:'DOWNLOAD',
		tracksToShow:3,
		autoplay:false,
		 ratingCallback:null,
		// ratingCallback:function(index, playlistItem, rating){
		// 	//some logic to process the rating, perhaps through an ajax call
		// },
		jPlayer:{
			swfPath: "http://www.jplayer.org/2.7.0/js/",
			supplied: "mp3",
			volume:  0.8,
			wmode:"window",
			solution: "html,flash",
			errorAlerts: true,
			warningAlerts: true
		}
	});
});
