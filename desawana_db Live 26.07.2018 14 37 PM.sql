-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 26, 2018 at 03:07 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desawana_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'RCsr7Qt1bBMqFwlW4bBVvtu02UBBQ0hr', 1, '2015-07-11 18:39:31', '2015-07-11 18:39:31', '2015-07-11 18:39:31'),
(5, 7, 'KB71pV1DYudR9TzPPQnGxRXQBoo7jV0E', 1, '2016-01-07 06:05:57', '2016-01-07 06:05:57', '2016-01-07 06:05:57'),
(8, 10, 'Bs5wlBYLO997Z8lcLpO19pj40fx2qVuV', 1, '2016-01-08 06:50:38', '2016-01-08 06:50:38', '2016-01-08 06:50:38'),
(9, 11, '0dsYg0Ft0IZFKMP7qIs1nbpK0f8gif6y', 1, '2016-01-08 07:18:38', '2016-01-08 07:18:38', '2016-01-08 07:18:38'),
(10, 12, 'yfD7K4mMa3eend4vvBBPNVbyFeX3UGZF', 1, '2016-01-08 07:27:38', '2016-01-08 07:27:38', '2016-01-08 07:27:38'),
(14, 9, 'i7J1jS1CbeCsMb40JdWwQLSFfoOWdngc', 1, '2017-01-23 19:35:39', '2017-01-23 19:35:39', '2017-01-23 19:35:39'),
(15, 10, '8eiPuz7KrCZXYjiMEY1wmkLuPbmMrSwI', 1, '2017-12-28 02:40:24', '2017-12-28 02:40:24', '2017-12-28 02:40:24'),
(16, 11, 'htzbXZnJuH098fhWA5e8V5QLberPA1IA', 1, '2018-01-08 02:48:27', '2018-01-08 02:48:27', '2018-01-08 02:48:27'),
(18, 12, 'PnJ03QoBnJMegHXmGCXh6bcoXArycqr5', 1, '2018-01-30 04:45:58', '2018-01-30 04:45:58', '2018-01-30 04:45:58'),
(20, 15, 'ivsgJeobsGYHI1oHtQo5oTzkBOb6A9WL', 1, '2018-02-01 17:14:24', '2018-02-01 17:14:24', '2018-02-01 17:14:24'),
(21, 17, 'jDkUMCYv0VpNfdrlsxKEFa2wCI78R7DP', 1, '2018-02-01 17:15:53', '2018-02-01 17:15:53', '2018-02-01 17:15:53'),
(22, 18, 'WCbfDN6mTvtTfu2DHELZfkgDgd7B6phA', 1, '2018-02-01 17:28:20', '2018-02-01 17:28:20', '2018-02-01 17:28:20'),
(23, 19, '70ytUN9RjCsVU0qnwno2RseFZRulBXZk', 1, '2018-02-08 04:39:36', '2018-02-08 04:39:36', '2018-02-08 04:39:36'),
(24, 11, 'OTRmEEVvix8rTHBvWtEhFhcyp1DYba6G', 1, '2018-04-04 02:49:47', '2018-04-04 02:49:46', '2018-04-04 02:49:47');

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position_id` int(11) NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_at` date DEFAULT NULL,
  `expire_at` date DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_locations`
--

CREATE TABLE `advertisement_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad_count` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `google_ad_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `advertisement_locations`
--

INSERT INTO `advertisement_locations` (`id`, `position`, `width`, `height`, `ad_count`, `status`, `google_ad_status`, `created_at`, `updated_at`) VALUES
(1, 'Top image (Audios, Videos, Lyrics, Artists)', '928px', '70px', 2, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(2, 'Side panel(Audio,Video,Lyric)', '300px', '90px', 3, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(3, 'Pagination Top (Audios, Videos, Lyrics, Artists)', '728px', '90px', 1, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(4, 'Pagination Bottom(Audios, Videos, Lyrics, Artists)', '728px', '90px', 1, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(5, 'Side Panel (Home, Audios, Audio, Videos, Video, Lyrics, Lyric)', '300px', '250px', 3, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(6, 'Related Content Top (Audio, Video, Lyric)', '728px', '90px', 1, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(7, 'Related Content Middle (Audio, Video, Lyric)', '728px', '90px', 1, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(8, 'Related Content Bottom (Audio, Video, Lyric)', '728px', '90px', 1, 1, 0, '2018-07-15 18:01:58', '0000-00-00 00:00:00'),
(9, 'Home Floating Ad', '728px', '90px', 1, 1, 0, '2018-07-15 18:01:58', '2018-07-18 10:58:34');

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `fb` text COLLATE utf8_unicode_ci,
  `twitter` text COLLATE utf8_unicode_ci,
  `utube` text COLLATE utf8_unicode_ci,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `bio`, `fb`, `twitter`, `utube`, `img`, `user_id`, `created_at`, `updated_at`, `meta_description`, `meta_keywords`) VALUES
(1, 'Ashan Fernando', 'Ashan Fernando is a Sri Lankan music composer & singer. Ashan Fernando is a kind of an artist that is truly believes in youth social sense. And as a result he provides very sensational music to the social that will melt your heart.  ', 'https://www.facebook.com/ashanf123/', 'https://twitter.com/AshanFernandoo', 'https://www.youtube.com/channel/UCXYVkl6OYjWm0uJCyIacNJQ', 'core/storage/uploads/images/artists/artists-20180719115749.png', 9, '2018-05-12 02:41:54', '2018-07-19 18:57:50', 'Ashan Fernando is a Sri Lankan music composer & singer. Ashan Fernando is a kind of an artist that is truly believes in youth social sense. And as a result he provides very sensational music to the social that will melt your heart.  ', 'oba miriguwak kiya song download, Ashan Fernando song mp3 download, me nihanda bhawaye dj, nolabena senehe ashan fernando, me nihanda bhawaye lyrics, ashan fernando song list, ashan fernando ahas maliga, ahas maliga mp3, Ashan Fernando, Ashan Fernando songs, Ashan Fernando songs mp3, Ashan Fernando songs video, Ashan Fernando live show songs, Ashan Fernando song download, Ashan Fernando song chords, Ashan Fernando song lyrics, Ashan Fernando song Live'),
(2, 'Dilan Gamage', 'Dilan Gamage is a trending lyricist who is gathering more and more visionary, sensational facts and ideas into a one place to produce a kind of a song that will be a sense to human.', 'https://www.facebook.com/gamagedylan', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511194707.png', 9, '2018-05-12 02:47:08', '2018-07-06 07:49:31', 'Dilan Gamage is a trending lyricist who is gathering more and more visionary, sensational facts and ideas into a one place to produce a kind of a song that will be a sense to human.', 'dilan gamage, dilan gamage songs, dilan gamage contact number, new, sinhala, man pathanawa, dilan gamage lyrics, dilan gamage facebook, dilan gamage new song mp3, dilan gamage new songs, dilan gamage songs, dilan gama, sinhala songs, new sinhala songs, romantic songs, new songs, latest sinhala songa, gamage, top music video in sri lanka, new hit, ashan fernando new songs, song videos, videos, me nihada bawaye, thamath tharahin neda oya, saththai oya, sri lanka music, love song, '),
(3, 'Damith Asanka', 'Damith Asanka is a highly recommended singer by youth fans and also a trending artist in nowadays. His songs can bring tears to your eyes. ', 'https://www.facebook.com/DamithAsankaOfficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203226.png', 9, '2018-05-12 03:32:26', '2018-07-06 07:59:29', 'Damith Asanka is a highly recommended singer by youth fans and also a trending artist in nowadays. His songs can bring tears to your eyes. ', 'damith asanka, damith, damith asanka new songs, sinhala, damith asanka live show, damith asanka songs, damith asanka all songs, damith asanka album, damith asanka old songs, damith asanka best songs collection, best of damith, srilankan, damith asanka songs lyrics, damith asanka songs non stop, damith asanka songs mp3, mp3 free download, damith asanka mp3, me anantha raathriye, damith asanka video song, damith asanka live, pamawela, kachayak idiriye, me ananth raathriye video, '),
(4, 'Dilki Uresha', 'Dilki Uresha is a female singer that will be a kind of an female voice to the music social. ', 'https://www.facebook.com/Dilki-Uresha-204234943711424/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203246.png', 9, '2018-05-12 03:32:46', '2018-07-06 08:14:21', 'Dilki Uresha is a female singer that will be a kind of an female voice to the music social. ', ''),
(5, 'Dimanka Wellalage', 'Dimanka Wellalage is a well experienced singer, lyricist and a song producer who has made his own empire of music in the field. And also he is a very popular singer in sri lankan live music events. ', 'https://www.facebook.com/dimankaonline/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCIq8JDQZDa2ZjAGIu4LafxA', 'core/storage/uploads/images/artists/artists-20180511203307.png', 9, '2018-05-12 03:33:07', '2018-07-06 08:30:40', 'Dimanka Wellalage is a well experienced singer, lyricist and a song producer who has made his own empire of music in the field. And also he is a very popular singer in sri lankan live music events. ', 'dimanka wellalage, sajith v chathuranga, dimanka, radeesh vandabona, dimanka wellalage songs, music video, dimanka wellalage songs youtube, dimanka wellalage songs lyrics, dimanka wellalage songs chords, dimanka wellalage songs ananmanan.lk, dimanka wellalage video songs, dimanka wellalage new video songs, dimanka wellalage dj songs, dimanka wellalage new video 2017, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show,'),
(6, 'Jude Rogans', 'Jude Rogans is a well popular and panoplied singer for live singing and his singing can touch your heart by a word. ', 'https://www.facebook.com/%E0%B6%A2%E0%B7%96%E0%B6%A9%E0%B7%8A-%E0%B6%BB%E0%B7%9C%E0%B6%9C%E0%B6%B1%E0%B7%8A%E0%B7%83%E0%B7%8A-1948139895404796/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203338.png', 9, '2018-05-12 03:33:38', '2018-07-06 08:45:25', 'Jude Rogans is a well popular and panoplied singer for live singing and his singing can touch your heart by a word. ', 'jude rogans, sinhala, music, jude, jude rogans new song, live, horen bala, srilanka music, sri lanka, jude rogans new songs, jude rogans amma, jude rogans new song 2018 mp3, jude rogans new song mp3 download, jude rogans new song video, jude rogans nonstop, jude rogans, jude rogans mp3, jude rogans music, jude rogans all songs, jude rogans live, jude rogans live show, ජූඩ් රොගන්ස්, athmedi, new hit music videos, sammatheta pitu pe music video, sammatheta pitu pa adare, '),
(7, 'Milinda Sandaruwan', 'Milinda Sandaruwan is a opposition singer who has a very romantic and sensible voice. he is a rising star in the youth social nowadays.  ', 'https://www.facebook.com/milindasofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203356.png', 9, '2018-05-12 03:33:57', '2018-07-06 08:52:59', 'Milinda Sandaruwan is a opposition singer who has a very romantic and sensible voice. he is a rising star in the youth social nowadays.  ', 'milinda sandaruwan, new music video, sinhala music video, ashirwadaya, milinda, milinda sandaruwan new song, milinda sandaruwan sudu, mathu dineka, sinhala mp3, sinhala songs, sudu milinda sandaruwan, new sinhala songs, milinda sandaruwan live, milinda sandaruwan new songs, milinda sandaruwan new song 2018, milinda sandaruwan songs, milinda new video, sithin lanwela milinda, iraj, milinda new song bambarindu, rawatuwe obamane milinda sandaruwan, ashirwadaya dennam mp3, '),
(8, 'Nadeera Nonis', 'Nadeera Nonis is a musician in live music industry and also a part of a live streaming band. Nadeera Nonis is a well experienced singer and his singing techniques are very powerful to entertain your mind in a sensible manner.', 'https://www.facebook.com/pg/Nadeera-Nonis-611056125581229/about/?ref=page_internal', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203417.png', 9, '2018-05-12 03:34:17', '2018-07-06 09:01:22', 'Nadeera Nonis is a musician in live music industry and also a part of a live streaming band. Nadeera Nonis is a well experienced singer and his singing techniques are very powerful to entertain your mind in a sensible manner.', 'nadeera nonis, nadeera, nonis, oba noena karane, dawena duka, hithata hitha, nadeera nonis new song 2017, sinhala songs, nadeera nonis peradaka, nadeera nonis mp3, nadeera nonis new song download, nadeera nonis new song mp3 download, nadeera nonis band, nadeera nonis mp3 song, nadeera nonis new song, nadeera nonis new song mp3, nadeera nonis songs lyrics, nadeera nonis video songs, nadeera nonis new songs video, nadeera nonis latest songs, sinhala, mp3 free download, new song, '),
(9, 'Rahal Alwis', 'Rahal Alwis is a well known sri lankan singer and also a musician, producer and a lyricist in a top level sensible music. \r\nhis voice is a one of a kind and you all will love it. ', 'https://www.facebook.com/RahalAlwisOfficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203435.png', 9, '2018-05-12 03:34:35', '2018-07-06 09:11:27', 'Rahal Alwis is a well known sri lankan singer and also a musician, producer and a lyricist in a top level sensible music.  his voice is a one of a kind and you all will love it. ', 'rahal alwis, sinhala, video, rahal, alwis, sinhala mp3, rahal alwis new song 2018, rahal alwis new song mp3 download, rahal alwis songs, rahal alwis, new song, sithata hora man, rahal alwis mage so susum, rahal alwis shungari, rahal alwis sithata hora, rahal alwis songs, rahal alwis new song, rahal alwis song, rahal alwis new song 2018, rahal alwis new mp3, shrungari - rahal alwis official music video, hitha riduna hamadama, new sinhala songs, heenayak premayak official music video, '),
(10, 'Ruwan Hettiarachchi', 'Ruwan Hettiarachchi is an award winning singing artist who has huge collection of songs sung by him. There is a big gathering of fans for him.', 'https://www.facebook.com/Official.Ruwan.Hettiarachchi/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203454.png', 9, '2018-05-12 03:34:54', '2018-07-12 17:13:23', 'Ruwan Hettiarachchi is an award winning singing artist who has huge collection of songs sung by him. There is a big gathering of fans for him.', 'ruwan hettiarachchi, ruwan, sinhala, hettiarachchi, sinhala songs, music, new, nodaka, official, nodaka inna ba, alen weli, rawatuna tharam, ruwan hettiarachchi songs, ruwan hettiarachchi new songs, ruwan hettiarachchi new songs 2016, ruwan hettiarachchi song, ruwan hettiarachchi new song mp3, ruwan hettiarachchi and umariya new song, ruwan hettiarachchi amathumak, ruwan hettiarachchi wife, ruwan hettiarachchi new songs 2017, ruwan hettiarachchi live, ruwan hettiarachchi mp3, sheril dekker, '),
(11, 'Sadun Perera', 'Sandun perera is a top ranking singing artist who is also doing as a song lyricist. Nowadays sandun perera is a very popular singer amongst the youth.  ', 'https://www.facebook.com/sandunpereralive?lst=100005806563957%3A100002769006391%3A1531370660', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203517.png', 9, '2018-05-12 03:35:17', '2018-07-12 17:40:16', 'Sandun perera is a top ranking singing artist who is also doing as a song lyricist. Nowadays sandun perera is a very popular singer amongst the youth.  ', 'sandun perera, ayeth warak, pooja karannam, sinhala music, sinhala mp3, sinhala songs, sandun, warada piligannawa, sandun new song, sandun perera, sandun perera live show 2018, sandun perera new song lyrics, sandun perera video download, sandun perera new song, sandun perera songs, sandun perera live show, sandun perera mashup, sandun perera warada piligannawa, sandun perera with flashback, sandun perera new song, sandun perera mp3, sandun perera music, sandun perera, new sinhalasongs, '),
(12, 'Samith Sirimanna', 'samith sirimanna is an upcoming young singer who is willing to be a heart warming singer and of course he is very popular singer among the romantic music lovers.', 'https://www.facebook.com/officialsamith/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203534.png', 9, '2018-05-12 03:35:34', '2018-07-12 17:47:01', 'samith sirimanna is an upcoming young singer who is willing to be a heart warming singer and of course he is very popular singer among the romantic music lovers.', 'samith sirimanna, pin madida, new songs, rathnaththare pihitai, samith new songs, pin madida danne na, samith newsongs, samith sirimanna, samith sirimanna songs, samith sirimanna new song mp3, samith sirimanna new song 2017, samith sirimanna new video song download, samith sirimanna video, samith sirimanna new songs, penenathek mane, 2018 new songs, romantic, true story, new sinhala, sinhala videos, sinhalalanka, sandun, samith sirimanna saba adare live, new music video, rathnaththare, '),
(13, 'Shahil Himansa', 'Shahil Himansa is a Sri Lankan singer, musician and a music producer and During his musical career, Shahil has released five Music Videos. And also he is a upcoming popular singer in nowadays.', 'https://www.facebook.com/shahilhimansaofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203554.png', 9, '2018-05-12 03:35:54', '2018-07-12 17:53:58', 'Shahil Himansa is a Sri Lankan singer, musician and a music producer and During his musical career, Shahil has released five Music Videos. he is upcoming popular singer in nowadays.', 'shahil himansa, shahil new song, therum giya, new, sinhala, shahil himansa new song 2018, shahil himansa new song download, shahil himansa mp3, shahil himansa video song download, shahil himansa me hitha thawamath, shahil himansa aye noena, sinhala songs, music, shahil himansa, shahil himansa songs, shahil himansa mp3 free download, shahil himansa mp3 song download, shahil himansa new song 2018, shahil himansa mashup, shahil himansa live show, shahil himansa, shahil himansa new song video, '),
(14, 'Thushara Joshap', 'Thushara Joshap is a well popular upcoming young singer who is also work as a live performer of sri lankan band named \'sahara flash\'. He has a kind of a voice and a new way of performing and because of that everyone is willing to hear from him.', 'https://www.facebook.com/thushasf/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203614.png', 9, '2018-05-12 03:36:14', '2018-07-12 18:29:50', 'Thushara Joshap is a well popular upcoming young singer who is also work as a live performer of sri lankan band named \'sahara flash\'. He has a kind of a voice and a new way of performing and because of that everyone is willing to hear from him.', 'thushara joshap, new, sinhala, rawatuna nowe, thushara, thushara joshap new song, sahara flash, lyrics, 2018, sinhala songs, thushara sandakelum, sinhala new songs 2018, new song thushara sandakelum, thushara josap new song, rawatuna nowe thushara, rawatuna nowe thushara joshap, thushara joshap new song rawatuna nowe, rawatuna nowe song mp3, thushara sandakelum new song, mp3 free download, trending sinhala songs, monawathma wena aye, thushara joshap new, thushara joshap mp3, '),
(15, 'Viraj Perera', 'Viraj perera is a kind of a singer in nowadays and evryone love his voice. He is a upcoming musician that everyone is willing to hear his voice.', 'https://www.facebook.com/official.VIRAJ.Perera/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180511203649.png', 9, '2018-05-12 03:36:49', '2018-07-12 18:38:10', 'Viraj perera is a kind of a singer in nowadays and evryone love his voice. He is a upcoming musician that everyone is willing to hear his voice.', 'viraj perera, viraj perera new song, viraj perera songs, sinhala, thahanam dan, sinhala songs, thahanam, viraj perera new song 2017, viraj perera new video song download, viraj perera new song video, viraj perera new song, viraj perera hemin sare, viraj perera saritha, viraj perera mp3 download, viraj perera saritha mp3 song, thahanam dam mata oyage rupe, thahanam viraj perera, viraj perera latest songs, best song of 2017, තහනම් දැන්, srilankan, official full hd video, '),
(16, 'Chamara Weerasinghe', 'Chamara Weerasinghe is a popular artist who is a romantic and a heart warming singer and also work as a performer in live music band events. In a way he is a new era of music among youth fans.', 'https://www.facebook.com/l.m.chamara.weerasinghe/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180512201320.png', 9, '2018-05-13 03:13:20', '2018-07-12 19:41:20', 'Chamara Weerasinghe is a popular artist who is a romantic and a heart warming singer and also work as a performer in live music band events. In a way he is a new era of music among youth fans.', 'chamara weerasinghe, chamara, best of chamara weerasinghe, chamara weerasingha, sinhala, chamara weerasinghe best songs collection - 01, chamara weerasinghe songs, mage jeewithe pura, ma nowana mama, chamara weerasinghe best songs collection, songs, sinhala songs, weerasinghe, chamara weerasinghe thaththa, chamara weerasinghe wife, chamara weerasinghe live, chamara weerasinghe song, chamara weerasinghe 2018, chamara weerasinghe album free download, chamara weerasinghe live show 2018, '),
(17, 'Sanuka Wickramasinghe', 'Sanuka wickramasinghe is very popular sri lankan Singer and also works as a live performer, Lyricist, Producer. He is a expert in sound engineering too. Fans are going mad when he sings. And he has his own way performing and he is a bright star in the music field. ', 'https://www.facebook.com/sanukawick/', 'https://twitter.com/sanukawick', 'https://www.youtube.com/user/IamStriKING/', 'core/storage/uploads/images/artists/artists-20180513195806.png', 9, '2018-05-14 02:58:07', '2018-07-12 19:55:48', 'Sanuka wickramasinghe is very popular sri lankan Singer and also works as a live performer, Lyricist, Producer. He is a expert in sound engineering too. Fans are going mad when he sings. And he has his own way performing and he is a bright star in the music field. ', 'sanuka, sanuka wickramasinghe, saragaye, sansara sihine, sanuka wicky, mal wiyan, ciao malli, sanukawicky, sanuka wickramasinghe live, sanuka wickramasinghe, sanuka wickramasinghe youtube, sanuka wickramasinghe saragaye mp3, sanuka wickramasinghe, sanuka wickramasinghe new songs, sanuka wickramasinghe ma nowana mama, sanuka wickramasinghe songs, sanuka wickramasinghe saragaye, sanuka wickramasinghe sansara sihine, sanuka wickramasinghe songs download, sanuka wickramasinghe mp3, sanuka new song, '),
(18, 'Sydney Chandrasekara', 'Sudney Chandrasekara is a well known person as a experienced lyricist and also working as journalist, tele drama writer, director. ', 'https://www.facebook.com/iamsydneychandrasekara/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180513211135.png', 9, '2018-05-14 04:11:35', '2018-07-19 16:25:11', 'Sudney Chandrasekara is a well known person as a experienced lyricist and also working as journalist, tele drama writer, director. ', 'srilanka, reality, show, sydney chandrasekara, sydney, sydney chandrasekara, sydney chandrasekara songs, sydney chandrasekara new song, sydney chandrasekara lyrics, sydney chandrasekara teledrama, sydney chandrasekara songs, sundara birinda with sydney chandrasekara, '),
(19, 'Rachithaa Wakista', 'Rachithaa Wakista is a young female lyricist who blessed with the remarkable opportunity to make creations on behalf of Sri Lankan music industry. \r\n', 'https://www.facebook.com/Rachithaa-Wakista-1660354970960653/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514125219.png', 9, '2018-05-14 19:52:19', '2018-07-19 16:34:16', 'Rachithaa Wakista is a young female lyricist who blessed with the remarkable opportunity to make creations on behalf of Sri Lankan music industry. ', 'rachithaa wakista, new music videos, music video, bplus songs, bplus music video, sinhala music video, sinhala songs, new release, sinhala sindu, bplus music, rachithaa wakista, rachita wakista, new sinhala song, bplus video, rachithaa wakista jukebox, rachitha songs, sinhala top hits, jukebox songs, man numbe, ayemath adaren, sangeeth satharasinghe new song, sangeeth satharasinghe, '),
(20, 'Thilina Ruhunage', 'Thilina ruhunage is a upcoming young musician who is trying to build his own empire in in sri lankan music industry.   ', 'https://www.facebook.com/Thilina-Ruhunage-128818567265075/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514125534.png', 9, '2018-05-14 19:55:34', '2018-07-19 16:42:14', 'Thilina ruhunage is a upcoming young musician who is trying to build his empire in in sri lankan music industry.   ', 'thilina ruhunage, sinhala songs, hiru gossip, thilina, ruhunage, thilina r, hiru fm, es deka pura, sinhala new song, music, sinhala teledrama, sinhala music video, sinhala video songs, me hitha thaniyen, athma liyanage, thilina ruhunage, thilina ruhunage songs, thawa ridawanna, thilina ruhunage mp3 song, thilina ruhunage mp3, thilina ruhunage music, thilina ruhunage songs, thilina ruhunage new songs, thilina ruhunage es deka pura, thilina ruhunage es deka pura, love songs, music video, '),
(21, 'Lasitha Jayaneththi Arachchige', 'Lasitha Jayaneththi Arachchige is a well known upcoming lyricist who has a special ability to touch anyones heart by his lyrics. ', 'https://www.facebook.com/lasitha.jayaneththiarachchige?lst=100005806563957%3A100000871151867%3A1531973582', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514130614.png', 9, '2018-05-14 20:06:14', '2018-07-19 16:49:19', 'Lasitha Jayaneththi Arachchige is a well known upcoming lyricist who has a special ability to touch anyones heart by his lyrics. ', 'lasitha jayaneththi arachchige, viraj perera, udesh nilanga, sinhala, dilshan l silva, saaritha, youtube, jude rogans, new sinhala songs 2018, new sinhalasongs, new music videos 2018, new hit music videos, video mansala, lasitha jayaneththi arachchige, adare sanwedana, ashirwada soya, lasitha, new song, new, song, video, lyrics, official, mathudinaka, sudu, new hit music video, chamara weerasinghe new music video, best songs on 2017, yanada oya sajeewa erandith, sri lanka music video, '),
(22, 'DilShan L Silva', 'DilShan L Silva is a music composer, producer and a music engineer who is doing a great job on music industry to make an new era of sri lankan music field.  ', 'https://www.facebook.com/dilshan.lakshitha1?lst=100005806563957%3A100000454174581%3A1531974066', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng/featured', 'core/storage/uploads/images/artists/artists-20180514130947.png', 9, '2018-05-14 20:09:48', '2018-07-19 17:00:04', 'DilShan L Silva is a music composer, producer and a music engineer who is doing a great job on music industry to make an new era of sri lankan music field.  ', 'dilshan l silva, adare sanwedana, aware aware song, new song, pathanne na, viraj perera new song, udesh nilanga, new songs, sinhala, dilshan l silva songs, dilshan l silva, dilshan l silva new song, kaun tujhe, dilshan l silva new song, dilshan l silva, dilshan l silva facebook, bala innawane, adarei wasthu, hirumusic, jude rogans, coming soon, sameera, nodeka oya, 2018, srilankan, sinhala new songs 2018, music videos, video song, new sinhala song, music video, official music video, '),
(23, 'Sajith V Chathuranga', '', NULL, NULL, NULL, 'core/storage/uploads/images/artists/artists-20180514134231.png', 9, '2018-05-14 20:42:34', '2018-05-14 20:42:34', NULL, NULL),
(24, 'Prageeth Perera', 'music composer .. audio engineer', 'https://www.facebook.com/Prageeth-Perera-1822270981420746/', '', '', 'core/storage/uploads/images/artists/artists-20180521165251.png', 9, '2018-05-21 23:52:52', '2018-05-21 23:53:52', NULL, NULL),
(25, 'Uma Aseni', '', 'https://www.facebook.com/Uma-Aseni-Kavikari-902415469914173/', '', '', 'core/storage/uploads/images/artists/artists-20180521172620.png', 9, '2018-05-22 00:26:20', '2018-05-22 00:26:20', NULL, NULL),
(26, 'Sangeeth Wickramasinghe', '', 'https://www.facebook.com/sangeeth.wickramasinghe', '', '', 'core/storage/uploads/images/artists/artists-20180521174029.png', 9, '2018-05-22 00:40:29', '2018-05-22 00:40:29', NULL, NULL),
(27, 'Udari Amarathunga', '', 'https://www.facebook.com/Udaamarathunga', '', '', 'core/storage/uploads/images/artists/artists-20180521180357.png', 9, '2018-05-22 01:03:58', '2018-05-22 01:03:58', NULL, NULL),
(28, 'Manuranga Wijesekara', '', 'https://www.facebook.com/manuranga.wijesekara/', '', '', 'core/storage/uploads/images/artists/artists-20180521185815.png', 9, '2018-05-22 01:58:15', '2018-05-22 01:58:15', NULL, NULL),
(29, 'Radeesh Vandabona', '', 'https://www.facebook.com/RadeeshVandebona/', '', 'https://www.youtube.com/channel/UCJHzK5thXCTuL1JK-MBXzfA', 'core/storage/uploads/images/artists/artists-20180524125723.png', 9, '2018-05-24 19:57:25', '2018-05-24 19:57:25', '', 'ananthen piyaba awidin mp3 free download, radeesh vandebona photos, radeesh vandebona duka hithuna, radeesh vandebona mashup, Radeesh Vandabona, Radeesh Vandabona songs, Radeesh Vandabona songs mp3, Radeesh Vandabona songs video, Radeesh Vandabona live show songs, Radeesh Vandabona song download, Radeesh Vandabona song mp3 download, Radeesh Vandabona song chords, Radeesh Vandabona song lyrics, Radeesh Vandabona song Live, Radeesh Vandabona song guitar code, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show, album free download'),
(30, 'Chandana Walpola', '', 'https://www.facebook.com/chandana.walpolaflashback', '', '', 'core/storage/uploads/images/artists/artists-20180526144011.png', 9, '2018-05-26 21:40:11', '2018-05-26 21:40:11', '', ''),
(31, 'Shehan Galahitiyawa', '', 'https://www.facebook.com/shehan.galahitiyawa', '', '', 'core/storage/uploads/images/artists/artists-20180529114625.png', 9, '2018-05-29 18:46:26', '2018-05-29 18:46:26', '', ''),
(32, 'Priyantha Nawalage', '', 'https://www.facebook.com/priyantha.nawalage', '', '', 'core/storage/uploads/images/artists/artists-20180529114930.png', 9, '2018-05-29 18:49:30', '2018-05-29 18:49:30', '', ''),
(33, 'Sandaruwan Jayasinghe', '', 'https://www.facebook.com/sandaruwan.jayasinghe.35', '', '', 'core/storage/uploads/images/artists/artists-20180529130100.png', 9, '2018-05-29 20:01:01', '2018-05-29 20:01:01', '', ''),
(34, 'Kelum Srimal', '', 'https://www.facebook.com/The1nonlyKelumSrimal/', '', '', 'core/storage/uploads/images/artists/artists-20180529130950.png', 9, '2018-05-29 20:09:50', '2018-05-29 20:09:50', '', ''),
(35, 'Sadeeptha Gunawardana', '', 'https://www.facebook.com/sadeeptha.sadeeptha', '', '', 'core/storage/uploads/images/artists/artists-20180529131314.png', 9, '2018-05-29 20:13:15', '2018-05-29 20:13:15', '', ''),
(36, 'Sujith Priyan', '', 'https://www.facebook.com/sujee.sujith', '', '', 'core/storage/uploads/images/artists/artists-20180529132912.png', 9, '2018-05-29 20:29:13', '2018-05-29 20:29:13', '', ''),
(37, 'Nimesh Kulasinghe', '', 'https://www.facebook.com/nimeshKulasingheofficial/', '', '', 'core/storage/uploads/images/artists/artists-20180529134131.png', 9, '2018-05-29 20:41:31', '2018-05-29 20:41:31', '', ''),
(38, 'Athula Jayasinghe', '', 'https://www.facebook.com/athujaya', '', '', 'core/storage/uploads/images/artists/artists-20180529134657.png', 9, '2018-05-29 20:46:57', '2018-05-29 20:46:57', '', ''),
(40, 'Yasas Medagedara', '', 'https://www.facebook.com/yasasm', '', '', 'core/storage/uploads/images/artists/artists-20180529135556.png', 9, '2018-05-29 20:55:56', '2018-05-29 20:55:56', '', ''),
(41, 'Udara Samaraweera', 'Udara Samaraweera can be introduced as a talented young music director who has contributed many of unique and predominant creations to the Music Industry.', 'https://www.facebook.com/CanoeEntertainment/', '', '', 'core/storage/uploads/images/artists/artists-20180529140247.png', 9, '2018-05-29 21:02:47', '2018-05-29 21:02:47', '', ''),
(42, 'Prasanna Thakshila', '', 'https://www.facebook.com/prasannathakhila/', '', '', 'core/storage/uploads/images/artists/artists-20180601152312.png', 9, '2018-06-01 22:23:12', '2018-06-01 22:23:12', '', ''),
(43, 'Rusiru Rupasinghe', '', 'https://www.facebook.com/rusirur321624', '', '', 'core/storage/uploads/images/artists/artists-20180601152623.png', 9, '2018-06-01 22:26:23', '2018-06-01 22:26:23', '', ''),
(44, 'Yasith Dulshan', '', 'https://www.facebook.com/YasithDulRanasinghe/', '', '', 'core/storage/uploads/images/artists/artists-20180601152803.png', 9, '2018-06-01 22:28:03', '2018-06-01 22:28:03', '', ''),
(45, 'Ishan Priyasanka', '', 'https://www.facebook.com/ishan.priyashanka.3', '', '', 'core/storage/uploads/images/artists/artists-20180602133733.png', 9, '2018-06-02 20:37:34', '2018-06-02 20:37:34', '', ''),
(46, 'Tharindu Costa', '', 'https://www.facebook.com/tharindu.costha.1', '', '', 'core/storage/uploads/images/artists/artists-20180602173030.png', 9, '2018-06-03 00:30:30', '2018-06-03 00:30:30', '', ''),
(47, 'Uvindu Dayaratne', '', 'https://www.facebook.com/uvindu.dayaratne', '', '', 'core/storage/uploads/images/artists/artists-20180602173059.png', 9, '2018-06-03 00:31:00', '2018-06-03 00:31:00', '', ''),
(48, 'Harshana Prasad', '', 'https://www.facebook.com/Harshana-prasad-saharaflash-317634255331182/', '', '', 'core/storage/uploads/images/artists/artists-20180602174607.png', 9, '2018-06-03 00:46:08', '2018-06-03 00:46:08', '', ''),
(49, 'Sameera Chathuranga', '', 'https://www.facebook.com/Sameera-chathuranga-165980247195563/', '', 'https://www.youtube.com/channel/UCgaczbpQl7-n7LIMbWlMbDA', 'core/storage/uploads/images/artists/artists-20180606100322.png', 9, '2018-06-06 17:03:23', '2018-06-06 17:03:23', '', ''),
(50, 'Amila Muthugala', '', 'https://www.facebook.com/Amila-muthugala-studio-feel-364889150525359/', '', '', 'core/storage/uploads/images/artists/artists-20180606100857.png', 9, '2018-06-06 17:08:57', '2018-06-06 17:08:57', '', ''),
(51, 'Shenu Kalpa', '', 'https://www.facebook.com/kalpa.danu', '', '', 'core/storage/uploads/images/artists/artists-20180606111252.png', 9, '2018-06-06 18:12:52', '2018-06-06 18:12:52', '', ''),
(52, 'Hashan Thilina', '', 'https://www.facebook.com/hashanmusic', '', '', 'core/storage/uploads/images/artists/artists-20180606112834.png', 9, '2018-06-06 18:28:34', '2018-06-06 18:28:34', '', ''),
(53, 'Prasanna Kumara Dammalage', '', 'https://www.facebook.com/prasanna.kumaradammalage', '', '', 'core/storage/uploads/images/artists/artists-20180606114420.png', 9, '2018-06-06 18:44:20', '2018-06-06 18:44:20', '', ''),
(54, 'Udesh Nilanga', '', 'https://www.facebook.com/udesh.nilanga.7', '', '', 'core/storage/uploads/images/artists/artists-20180606121129.png', 9, '2018-06-06 19:11:29', '2018-06-06 19:11:29', '', ''),
(55, 'Buddika Krishan', '', 'https://www.facebook.com/buddika.krishan', '', '', 'core/storage/uploads/images/artists/artists-20180607123437.png', 9, '2018-06-07 19:34:38', '2018-06-07 19:34:38', '', ''),
(56, 'Nilan Hettiarachchi', '', 'https://www.facebook.com/NILANHETTIARACHCHIOFFICALFANPAGE/', '', '', 'core/storage/uploads/images/artists/artists-20180607131656.png', 9, '2018-06-07 20:16:57', '2018-06-07 20:16:57', '', 'Nilan Hettiarachchi songs, Nilan Hettiarachchi mp3 songs, Nilan Hettiarachchi video songs, Nilan Hettiarachchi video songs, Nilan Hettiarachchi song, Nilan Hettiarachchi songs download, Nilan Hettiarachchi mp3 songs download, '),
(57, 'Unknown Artist', '-Unknown artist default account', '', '', '', 'core/storage/uploads/images/artists/artists-20180612095806.png', 9, '2018-06-12 16:58:07', '2018-06-12 16:58:07', '', ''),
(58, 'Shahani Perera', '', '', '', '', 'core/storage/uploads/images/artists/artists-20180614163007.png', 9, '2018-06-14 23:30:08', '2018-06-14 23:30:08', '', 'Shahani Perera, Shahani Perera Song, Shahani Perera new song, Shahani Perera Songs, Shahani Perera new Songs, Shahani Perera song download'),
(59, 'Asanjaya Imashath', '', 'https://www.facebook.com/asanjayaimashath/', '', '', 'core/storage/uploads/images/artists/artists-20180614225602.png', 9, '2018-06-15 05:56:02', '2018-06-15 05:56:02', '', ''),
(61, 'Nalin Wijayasinghe', '', 'https://www.facebook.com/napwijayasinghe', '', '', 'core/storage/uploads/images/artists/artists-20180614232044.png', 9, '2018-06-15 06:20:44', '2018-06-15 06:20:44', '', ''),
(62, 'Sajeeka Jayasinghe', '', 'https://www.facebook.com/sajeekajayasinghe', '', '', 'core/storage/uploads/images/artists/artists-20180614232529.png', 9, '2018-06-15 06:25:29', '2018-06-15 06:25:29', '', ''),
(63, 'Ajith Sanjeewa Perera', '', 'https://www.facebook.com/cdsstudio7', '', '', 'core/storage/uploads/images/artists/artists-20180622104432.png', 9, '2018-06-22 17:44:32', '2018-06-22 17:44:32', '', ''),
(64, 'Roshan Perera', '', 'https://www.facebook.com/Roshan-Perera-780750018636211/', '', '', 'core/storage/uploads/images/artists/artists-20180622104907.png', 9, '2018-06-22 17:49:07', '2018-06-22 17:49:07', '', ''),
(65, 'Indika Ruwan (Roony)', '', 'https://www.facebook.com/roo.roony', '', '', 'core/storage/uploads/images/artists/artists-20180629144632.png', 9, '2018-06-29 21:46:33', '2018-06-29 21:46:33', '', ''),
(66, 'Roshan Samarawickrama', '', 'https://www.facebook.com/randika.roshan.5', '', '', 'core/storage/uploads/images/artists/artists-20180629145312.png', 9, '2018-06-29 21:53:12', '2018-06-29 21:53:12', '', ''),
(67, 'Marlon Jerom Alwis', '', 'https://www.facebook.com/marly.brando.75', '', '', 'core/storage/uploads/images/artists/artists-20180629153615.png', 9, '2018-06-29 22:36:15', '2018-06-29 22:36:15', '', ''),
(68, 'Hasitha Hemal', '', 'https://www.facebook.com/hasitha.hemal.731', '', '', 'core/storage/uploads/images/artists/artists-20180629153815.png', 9, '2018-06-29 22:38:15', '2018-06-29 22:38:15', '', ''),
(69, 'Chathura Warnasekara', '', 'https://www.facebook.com/chathura.warnasekara', '', '', 'core/storage/uploads/images/artists/artists-20180629161034.png', 9, '2018-06-29 23:10:34', '2018-06-29 23:10:34', '', ''),
(70, 'Ranga Indunil', '', 'https://www.facebook.com/profile.php?id=100008486551717', '', '', 'core/storage/uploads/images/artists/artists-20180629161722.png', 9, '2018-06-29 23:17:22', '2018-06-29 23:17:22', '', ''),
(71, 'Supun Ravishan', '', 'https://www.facebook.com/supun.ravishan', '', '', 'core/storage/uploads/images/artists/artists-20180629172444.png', 9, '2018-06-30 00:24:45', '2018-06-30 00:24:45', '', ''),
(72, 'Thilini Piyumali', '', 'https://www.facebook.com/thilini.piyumali.14', '', '', 'core/storage/uploads/images/artists/artists-20180629173716.png', 9, '2018-06-30 00:37:16', '2018-06-30 00:37:16', '', ''),
(73, 'Reru', '', 'https://www.facebook.com/profile.php?id=100012034291171', '', '', 'core/storage/uploads/images/artists/artists-20180629210612.png', 9, '2018-06-30 04:06:12', '2018-06-30 04:06:12', '', ''),
(74, 'Sajith Akmeemana', 'Singer , Lyrics Writer & Video Director\r\n\r\nI have done few Audio Tracks and a few Video and another one is yet to come.\r\nMy 1st song \"Tharu Kumari\" was created in 2007, and then after forming a band call \"SilentZ\" we produced another song named \"Adaraye...\"\r\nIn collaboration with Vishal Shenira we sang \"Adaraye Sihinayai\", which was produced by Yasas Medagedara & mixed by Nisal G @ Audio Lab Studios.\r\nI was a student of Avenue Of Audio (AOA) under Mr.Nisal Gangodage....', 'https://www.facebook.com/SajithDanushkaAkmeemana/', '', '', 'core/storage/uploads/images/artists/artists-20180629221502.png', 9, '2018-06-30 05:15:02', '2018-06-30 05:15:02', '', ''),
(75, 'Nimash Fernando', '', 'https://www.facebook.com/nimashlk/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180630134827.png', 9, '2018-06-30 20:48:27', '2018-06-30 20:48:27', '', 'nimash fernando, nimash fernando song, nimash fernando songs, nimash fernando mp3, nimash fernando mp3 songs, nimash fernando mp3 songs download, nimash fernando video songs download, nimash fernando video songs, '),
(76, 'Nisal Fernando', '', 'https://www.facebook.com/nisalfernandolive/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180630135458.png', 9, '2018-06-30 20:54:58', '2018-06-30 20:54:58', '', 'Nisal Fernando, Nisal Fernando songs,'),
(77, 'Manoj Dewarajage', '', 'https://www.facebook.com/manoj.prera', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180630151458.png', 9, '2018-06-30 22:14:58', '2018-06-30 22:14:58', '', 'Manoj Dewarajage, Manoj Dewarajage songs, Manoj Dewarajage songs download, '),
(78, 'Damitha Ayodya', '', 'https://www.facebook.com/dayodya', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704141732.png', 9, '2018-07-04 21:17:32', '2018-07-04 21:17:32', '', 'damitha ayodya, damitha ayodya songs, damitha ayodya songs mp3, damitha ayodya mp3 songs download, damitha ayodya songs download, '),
(79, 'Mahesh Uyanwatta', '', 'https://www.facebook.com/Mahesh-Uyanwatta-1411907875782169/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704142449.png', 9, '2018-07-04 21:24:49', '2018-07-04 21:24:49', '', ''),
(80, 'Shalinda Fernando', '', 'https://www.facebook.com/shali.allright', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704170910.png', 9, '2018-07-05 00:09:10', '2018-07-05 00:09:10', '', 'shalinda fernando, shalinda fernando song, shalinda fernando songs, shalinda fernando mp3 songs, shalinda fernando mp3 songs download, shalinda fernando all right, '),
(81, 'Thushara Subasinghe', '', 'https://www.facebook.com/Thushara-Subasinghe-Oxygen-1684691545163051/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704181551.png', 9, '2018-07-05 01:15:51', '2018-07-05 01:15:51', '', 'thushara subasinghe, thushara subasinghe new song, thushara subasinghe with oxygen, thushara subasinghe live, thushara subasinghe songs, thushara subasinghe song, thushara subasinghe new song mp3, thushara subasinghe new song 2018 mp3, thushara subasinghe amma, thushara subasinghe with oxygen 2018, thushara subasinghe new song mp3, thushara subasinghe new song live, thushara subasinghe mp3, thushara subasinghe nonstop, '),
(82, 'Udesh Indula', '', 'https://www.facebook.com/udesh.indula.3', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704185930.png', 9, '2018-07-05 01:59:31', '2018-07-05 01:59:31', '', 'udesh indula, udesh indula new song, derana dream star, udesh indula band, udesh indula film song, udesh indula live show mp3, udesh indula song mp3, udesh indula mp3 download, udesh indula songs list, udesh indula new songs, udesh indula songs, '),
(83, 'Rahathangama Nagitha Thero', '', 'https://www.facebook.com/rahathangamanagithathero/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704190538.png', 9, '2018-07-05 02:05:38', '2018-07-05 02:05:38', '', ''),
(84, 'Gihen Nishan', '', 'https://www.facebook.com/Gihen-Nishan-142494862537680/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180704194521.png', 9, '2018-07-05 02:45:21', '2018-07-05 02:45:21', '', ''),
(85, 'Nalinda Ranasinghe', '', 'https://www.facebook.com/nalindaranasingheevents/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705141639.png', 9, '2018-07-05 21:16:40', '2018-07-05 21:16:40', '', 'nalinda ranasinghe, nalinda ranasinghe songs, nalinda ranasinghe best songs, nalinda ranasinghe music videos, nalinda ranasinghe new songs, nalinda ranasinghe new song, nalinda ranasinghe video songs, nalinda ranasinghe song list, nalinda ranasinghe songs mp3, nalinda ranasinghe live, sinhala songs, sinhala songs old, sinhala video songs, sinhala music video, nalinda, '),
(86, 'Anushka Madushanka', '', 'https://www.facebook.com/desawanaofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705160421.png', 9, '2018-07-05 23:04:21', '2018-07-05 23:04:21', '', 'anushka madushanka, anushka madushanka songs, anushka madushanka mp3 songs, anushka madushanka songs download, '),
(87, 'Manoj V', '', 'https://www.facebook.com/profile.php?id=100012253838320', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705171017.png', 9, '2018-07-06 00:10:17', '2018-07-06 00:10:17', '', ''),
(88, 'Amila M Wickramasingha', '', 'https://www.facebook.com/amilamblackdiamondz', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180705171619.png', 9, '2018-07-06 00:16:20', '2018-07-06 00:16:20', '', ''),
(89, 'Dileepa Thamel', '', 'https://www.facebook.com/Dileepaofficial/', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180707130253.png', 9, '2018-07-07 20:02:53', '2018-07-07 20:03:31', '', ''),
(90, 'Mithila Randika', '', 'https://www.facebook.com/mithila.randika.1', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180708100443.png', 9, '2018-07-08 17:04:43', '2018-07-08 17:04:43', '', ''),
(91, 'Saman Pushpakumara', '', 'https://www.facebook.com/profile.php?id=100014407653497', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180710181110.png', 9, '2018-07-11 01:11:10', '2018-07-11 01:11:10', '', ''),
(92, 'Errol Jayawardena', '', 'https://www.facebook.com/anthonyerrol', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180712153218.png', 9, '2018-07-12 22:32:18', '2018-07-12 22:32:18', '', 'errol jayawardena, errol jayawardena song, errol jayawardena song download, errol jayawardena video, errol jayawardena mp3, errol jayawardena video song, errol jayawardena new song, '),
(93, 'Sasindu Gunawardhana', '', 'https://www.facebook.com/profile.php?id=100004275435134', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180712154314.png', 9, '2018-07-12 22:43:14', '2018-07-12 22:43:14', '', 'sasindu gunawardhana, sasindu gunawardhana song, sasindu gunawardhana mp3, sasindu gunawardhana songs, sasindu gunawardhana new songs, '),
(94, 'Janaka Katipearachchige', '', 'https://www.facebook.com/profile.php?id=100008394213952', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713133830.png', 9, '2018-07-13 20:38:30', '2018-07-13 20:38:30', '', 'janaka katipearachchige, janaka katipearachchige song, janaka katipearachchige song, janaka katipearachchige mp3 download, janaka katipearachchige video, janaka katipearachchige mp3 free download, '),
(95, 'Prageeth Vidana Pathirana', '', 'https://www.facebook.com/prageethvp.official', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713141632.png', 9, '2018-07-13 21:16:32', '2018-07-13 21:16:32', '', 'prageeth vidana pathirana, prageeth vidana pathirana song, prageeth vidana pathirana mp3 song, prageeth vidana pathirana songs download'),
(96, 'Sathsara Kasun Pathirana', '', 'https://www.facebook.com/sathsara.kasunpathirana.1', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713144844.png', 9, '2018-07-13 21:48:44', '2018-07-13 21:48:44', '', 'Sathsara Kasun Pathirana, Sathsara Kasun Pathirana song, Sathsara Kasun Pathirana Video,'),
(97, 'Roshen Walisundara', '', 'https://www.facebook.com/profile.php?id=100009502429594', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180713145421.png', 9, '2018-07-13 21:54:21', '2018-07-13 21:54:21', '', ''),
(98, 'Ajith Anthony', '', '', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180716153033.png', 9, '2018-07-16 22:30:36', '2018-07-16 22:30:36', '', 'ajith anthony, ajith anthony song, ajith anthony songs, ajith anthony song mp3, ajith anthony mp3 song download, '),
(99, 'Tehan Perera', '', 'https://www.facebook.com/tehanperera.fanpage/', 'https://twitter.com/realtehanperera', 'https://www.youtube.com/channel/UCbEcbZElFiOsXSY-Gj0_PDQ', 'core/storage/uploads/images/artists/artists-20180725091727.png', 9, '2018-07-25 16:17:27', '2018-07-25 16:17:27', '', 'tehan perera, tehan perera songs, tehan perera new song, tehan perera new songs, '),
(100, 'Sampath Fernandopulle', '', 'https://www.facebook.com/sampath.fernandopulle.1', 'https://twitter.com/Desawanalk', 'https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng', 'core/storage/uploads/images/artists/artists-20180725092231.png', 9, '2018-07-25 16:22:32', '2018-07-25 16:22:32', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `artist_song`
--

CREATE TABLE `artist_song` (
  `artist_id` int(10) UNSIGNED DEFAULT NULL,
  `song_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `artist_song`
--

INSERT INTO `artist_song` (`artist_id`, `song_id`) VALUES
(1, 1),
(1, 2),
(4, 2),
(1, 3),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(5, 10),
(5, 11),
(7, 12),
(17, 13),
(15, 14),
(17, 15),
(5, 17),
(5, 18),
(5, 19),
(5, 20),
(5, 21),
(5, 22),
(5, 23),
(5, 24),
(5, 25),
(5, 26),
(5, 27),
(5, 28),
(5, 29),
(5, 30),
(5, 31),
(5, 33),
(5, 34),
(14, 35),
(42, 36),
(45, 37),
(14, 38),
(14, 39),
(49, 40),
(51, 41),
(54, 42),
(56, 43),
(55, 44),
(59, 45),
(61, 46),
(58, 47),
(6, 48),
(63, 49),
(65, 50),
(67, 51),
(70, 52),
(5, 32),
(75, 53),
(77, 54),
(11, 55),
(80, 56),
(13, 57),
(81, 58),
(82, 59),
(6, 60),
(86, 61),
(87, 62),
(85, 63),
(14, 64),
(90, 65),
(91, 66),
(92, 67),
(94, 68),
(95, 69),
(98, 70),
(1, 71),
(99, 72);

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE `audio` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `src` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `audio`
--

INSERT INTO `audio` (`id`, `title`, `src`, `created_at`, `updated_at`, `user_id`) VALUES
(3, 'Man Pathanawa (Samu Aran Ya Yuthui)', '/core/storage/uploads/audio/audio-20180511195007.mp3', '2018-05-12 02:50:13', '2018-05-12 02:50:13', 9),
(4, 'Heenayakda Me', '/core/storage/uploads/audio/audio-20180511203847.mp3', '2018-05-12 03:38:51', '2018-06-13 21:19:10', 9),
(5, 'Saththai Oya (Man Witharada Adare Kale)', '/core/storage/uploads/audio/audio-20180511204212.mp3', '2018-05-12 03:42:14', '2018-05-12 03:42:14', 9),
(7, 'Kawadahari Wetahei', '/core/storage/uploads/audio/audio-20180513202907.mp3', '2018-05-14 03:29:11', '2018-06-13 21:18:55', 9),
(8, 'Me Nihanda Bhawaye', '/core/storage/uploads/audio/audio-20180513203453.mp3', '2018-05-14 03:34:53', '2018-06-13 21:18:43', 9),
(9, 'Nolabena Senehe New Version', '/core/storage/uploads/audio/audio-20180513203617.mp3', '2018-05-14 03:36:17', '2018-06-13 21:18:26', 9),
(10, 'Oya Nathuwa Dannam Mata (Me Adarayai Theme Song)', '/core/storage/uploads/audio/audio-20180513203709.mp3', '2018-05-14 03:37:15', '2018-06-13 21:18:09', 9),
(11, 'Adarei Katawath Nathi Tharam Kiya', '/core/storage/uploads/audio/audio-20180514124204.mp3', '2018-05-14 19:42:08', '2018-06-13 21:17:48', 9),
(12, 'Adarei Wasthu', '/core/storage/uploads/audio/audio-20180514124246.mp3', '2018-05-14 19:42:50', '2018-06-13 21:17:33', 9),
(13, 'Akeekaruma Man Udura', '/core/storage/uploads/audio/audio-20180514124331.mp3', '2018-05-14 19:43:34', '2018-06-13 21:17:15', 9),
(14, 'Waradak Kiyanne Na', '/core/storage/uploads/audio/audio-20180516173932.mp3', '2018-05-17 00:39:39', '2018-06-13 21:16:57', 9),
(15, 'Sudu (Mathu Dineka)', '/core/storage/uploads/audio/audio-20180521150324.mp3', '2018-05-21 22:03:24', '2018-06-13 21:16:39', 9),
(16, 'Sansara Sihine', '/core/storage/uploads/audio/audio-20180521175259.mp3', '2018-05-22 00:53:05', '2018-06-13 21:16:21', 9),
(17, 'Saaritha', '/core/storage/uploads/audio/audio-20180521183125.mp3', '2018-05-22 01:31:32', '2018-06-13 21:16:03', 9),
(18, 'Saragaye Raye', '/core/storage/uploads/audio/audio-20180521190303.mp3', '2018-05-22 02:03:09', '2018-06-13 21:15:41', 9),
(19, 'Ma Hara Giya Dine', '/core/storage/uploads/audio/audio-20180527181454.mp3', '2018-05-28 01:14:56', '2018-06-13 21:15:24', 9),
(20, 'Amathaka Karanna Ba', '/core/storage/uploads/audio/audio-20180529112307.mp3', '2018-05-29 18:23:15', '2018-06-13 21:15:08', 9),
(21, 'Arayum Karanne', '/core/storage/uploads/audio/audio-20180529112606.mp3', '2018-05-29 18:26:06', '2018-06-13 21:14:52', 9),
(22, 'Awasan Pema Mage', '/core/storage/uploads/audio/audio-20180529121805.mp3', '2018-05-29 19:18:11', '2018-06-13 21:13:44', 9),
(23, 'Durin Hinda Ma', '/core/storage/uploads/audio/audio-20180529122003.mp3', '2018-05-29 19:20:06', '2018-06-13 21:13:15', 9),
(24, 'Kiyabu Lathawe', '/core/storage/uploads/audio/audio-20180529122038.mp3', '2018-05-29 19:20:43', '2018-06-13 21:13:00', 9),
(26, 'Ma Me Tharam Handawala', '/core/storage/uploads/audio/audio-20180529122149.mp3', '2018-05-29 19:21:52', '2018-06-13 21:12:40', 9),
(27, 'Me Hitha Langa', '/core/storage/uploads/audio/audio-20180529122306.mp3', '2018-05-29 19:23:11', '2018-06-13 21:12:25', 9),
(28, 'Nidukin Inu Mana', '/core/storage/uploads/audio/audio-20180529122350.mp3', '2018-05-29 19:23:54', '2018-06-13 21:12:10', 9),
(29, 'Oba Ekka Mama', '/core/storage/uploads/audio/audio-20180529122448.mp3', '2018-05-29 19:24:54', '2018-06-13 21:11:57', 9),
(30, 'Obamada Me Hithata Mage', '/core/storage/uploads/audio/audio-20180529122528.mp3', '2018-05-29 19:25:28', '2018-06-13 21:11:43', 9),
(31, 'Para Kiyana Tharukawi', '/core/storage/uploads/audio/audio-20180529122647.mp3', '2018-05-29 19:26:52', '2018-06-13 21:11:28', 9),
(32, 'Senehasa Bidunath', '/core/storage/uploads/audio/audio-20180529122744.mp3', '2018-05-29 19:27:48', '2018-06-13 21:11:07', 9),
(33, 'Awasana Premayai Mage', '/core/storage/uploads/audio/audio-20180529123835.mp3', '2018-05-29 19:38:37', '2018-06-13 21:10:40', 9),
(34, 'Maa Nisa Pa Sina', '/core/storage/uploads/audio/audio-20180529123912.mp3', '2018-05-29 19:39:12', '2018-06-13 21:10:22', 9),
(35, 'Wenna Thiyena', '/core/storage/uploads/audio/audio-20180529123951.mp3', '2018-05-29 19:39:58', '2018-06-13 21:09:51', 9),
(36, 'Diwranna Behe Neda', '/core/storage/uploads/audio/audio-20180530141438.mp3', '2018-05-30 21:14:38', '2018-06-13 17:55:38', 9),
(37, 'Dahata Nopeni Inna', '/core/storage/uploads/audio/audio-20180601153201.mp3', '2018-06-01 22:32:03', '2018-06-13 17:55:26', 9),
(38, 'Oyata Vitharak', '/core/storage/uploads/audio/audio-20180602145553.mp3', '2018-06-02 21:55:55', '2018-06-13 17:55:11', 9),
(39, 'Hitha Gawa Heena', '/core/storage/uploads/audio/audio-20180602171740.mp3', '2018-06-03 00:17:45', '2018-06-13 17:56:07', 9),
(40, 'Miya Yanna Sudanam (Monawathma Wena Aye)', '/core/storage/uploads/audio/audio-20180602171830.mp3', '2018-06-03 00:18:30', '2018-06-13 17:56:19', 9),
(41, 'Obe Adare', '/core/storage/uploads/audio/audio-20180606101730.mp3', '2018-06-06 17:17:33', '2018-06-13 17:54:41', 9),
(42, 'Mathaka Mawee', '/core/storage/uploads/audio/audio-20180606114901.mp3', '2018-06-06 18:49:04', '2018-06-13 17:54:21', 9),
(43, 'Pem Karala', '/core/storage/uploads/audio/audio-20180606122526.mp3', '2018-06-06 19:25:28', '2018-06-13 17:54:07', 9),
(44, 'Wirasakawee', '/core/storage/uploads/audio/audio-20180607125653.mp3', '2018-06-07 19:56:56', '2018-06-13 17:53:45', 9),
(45, 'Antima Mohothedi', '/core/storage/uploads/audio/audio-20180607132230.mp3', '2018-06-07 20:22:34', '2018-06-11 18:41:27', 9),
(46, 'Pemwathiye', '/core/storage/uploads/audio/audio-20180614230344.mp3', '2018-06-15 06:03:48', '2018-06-15 06:03:48', 9),
(47, 'Aulak Nane', '/core/storage/uploads/audio/audio-20180614230438.mp3', '2018-06-15 06:04:38', '2018-06-15 06:04:38', 9),
(48, 'Sulan Podak Wee', '/core/storage/uploads/audio/audio-20180614235635.mp3', '2018-06-15 06:56:38', '2018-06-15 06:56:38', 9),
(49, 'Sammatheta Pitupe', '/core/storage/uploads/audio/audio-20180619182759.mp3', '2018-06-20 01:27:59', '2018-06-20 01:27:59', 9),
(50, 'Nindath Hora Gaththa Oya', '/core/storage/uploads/audio/audio-20180622105317.mp3', '2018-06-22 17:53:20', '2018-06-22 17:53:20', 9),
(51, 'Seya (Nabara Wu)', '/core/storage/uploads/audio/audio-20180629145836.mp3', '2018-06-29 21:58:36', '2018-06-29 21:58:36', 9),
(52, 'Hithuwak Kari', '/core/storage/uploads/audio/audio-20180629154718.mp3', '2018-06-29 22:47:21', '2018-07-05 16:54:14', 9),
(53, 'Aththama Kiyannam', '/core/storage/uploads/audio/audio-20180629161402.mp3', '2018-06-29 23:14:02', '2018-06-29 23:14:02', 9),
(54, 'Unmada Rathriye', '/core/storage/uploads/audio/audio-20180630142307.mp3', '2018-06-30 21:23:08', '2018-06-30 21:23:08', 9),
(55, 'Parana Hithuwakkari', '/core/storage/uploads/audio/audio-20180630152102.mp3', '2018-06-30 22:21:02', '2018-06-30 22:21:02', 9),
(56, 'Dukak Denenna Epa', '/core/storage/uploads/audio/audio-20180704133150.mp3', '2018-07-04 20:31:56', '2018-07-04 20:31:56', 9),
(57, 'Mage Asurin', '/core/storage/uploads/audio/audio-20180704165913.mp3', '2018-07-04 23:59:17', '2018-07-04 23:59:17', 9),
(58, 'Therum Giya', '/core/storage/uploads/audio/audio-20180704174150.mp3', '2018-07-05 00:41:56', '2018-07-05 00:41:56', 9),
(59, 'Heen Sare', '/core/storage/uploads/audio/audio-20180704181708.mp3', '2018-07-05 01:17:08', '2018-07-05 01:17:08', 9),
(60, 'Man Vindina', '/core/storage/uploads/audio/audio-20180704191313.mp3', '2018-07-05 02:13:14', '2018-07-05 02:13:14', 9),
(61, 'Nodakapu Dewani Budun', '/core/storage/uploads/audio/audio-20180704195539.mp3', '2018-07-05 02:55:43', '2018-07-05 02:55:43', 9),
(62, 'Sudu Manika', '/core/storage/uploads/audio/audio-20180705150320.mp3', '2018-07-05 22:03:23', '2018-07-05 22:03:23', 9),
(63, 'Mata Wetahuna', '/core/storage/uploads/audio/audio-20180705161131.mp3', '2018-07-05 23:11:32', '2018-07-05 23:11:32', 9),
(64, 'Salli Na', '/core/storage/uploads/audio/audio-20180705171130.mp3', '2018-07-06 00:11:36', '2018-07-06 00:11:36', 9),
(65, 'Rawatuna Nowe', '/core/storage/uploads/audio/audio-20180707125941.mp3', '2018-07-07 19:59:44', '2018-07-07 19:59:44', 9),
(66, 'Aswaha Wadune', '/core/storage/uploads/audio/audio-20180708102159.mp3', '2018-07-08 17:22:05', '2018-07-08 17:22:05', 9),
(67, ' Oba Warade Pataleddi', '/core/storage/uploads/audio/audio-20180710181520.mp3', '2018-07-11 01:15:25', '2018-07-11 01:15:25', 9),
(68, ' Purudu Thanikama', '/core/storage/uploads/audio/audio-20180712160415.mp3', '2018-07-12 23:04:18', '2018-07-12 23:04:18', 9),
(69, 'Nodaka Oya', '/core/storage/uploads/audio/audio-20180713135737.mp3', '2018-07-13 20:57:40', '2018-07-13 20:57:40', 9),
(70, 'Adare Hodi Pothe', '/core/storage/uploads/audio/audio-20180713141734.mp3', '2018-07-13 21:17:37', '2018-07-13 21:17:37', 9),
(71, 'Labimai Me Ape', '/core/storage/uploads/audio/audio-20180716153156.mp3', '2018-07-16 22:31:58', '2018-07-18 05:46:01', 9),
(72, 'Wen Wenna', '/core/storage/uploads/audio/audio-20180719171850.mp3', '2018-07-20 00:18:54', '2018-07-20 00:18:54', 9),
(73, 'Manamaliye', '/core/storage/uploads/audio/audio-20180725095557.mp3', '2018-07-25 16:55:58', '2018-07-25 16:55:58', 9);

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_pages`
--

CREATE TABLE `dynamic_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dynamic_pages`
--

INSERT INTO `dynamic_pages` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Privacy Policy Page', '<p>Introduction<br />This privacy policy (&ldquo;Policy&rdquo;) and this site&rsquo;s Terms of Service (together the &ldquo;Terms&rdquo;) govern all use of https://desawana.com and that site&rsquo;s services (together the &ldquo;Site&rdquo; or &ldquo;Services&rdquo;). The owners and contributors to the Site will be referred to as &ldquo;we,&rdquo; &ldquo;us,&rdquo; or &ldquo;our&rdquo; in this Policy. By using the Site or its Services, and/or by clicking anywhere on this Site to agree to the Terms and this Policy, you are deemed to be a &ldquo;user&rdquo; for purposes of this Policy. You and every other user (&ldquo;you&rdquo; or &ldquo;User&rdquo; as applicable) are subject to this Policy. You and each user also agree to the Terms by using the Services. In these Terms, the word &ldquo;Site&rdquo; includes the site referenced above, its owner(s), contributors, suppliers, licensors, and other related parties.</p>\r\n<p>We provide this privacy statement explaining our online information practices, so that you can decide whether and how to interact with the Site and the Services.</p>\r\n<p>We may release your information when we deem it appropriate to comply with the law, enforce our site policies, or protect ours or others&rsquo; rights, property, or safety.</p>\r\n<p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>\r\n<p>Please also review our Terms of Use section that governs the use and the users of the Site.</p>\r\n<p>By using our site, you consent to our privacy policy.</p>\r\n<p>If we decide to change our privacy policy, we will post those changes on this page. If we have your email address, we may also send an email notifying you of any changes.</p>\r\n<p>Contact Data and Other Identifiable Information<br />This site collects certain user information, which may include a username and password, contact information, or any other data that you type in to the site. It may also identify your IP address to help identify you on future visits to the site. At our discretion, the Site may use this data to:</p>\r\n<p>Personalize the user experience and/or customer service<br />Improve the site<br />To process transactions<br />Administer a contest, promotion, survey or other site feature or function<br />Send email to users<br />Mobile Device Privacy<br />The following applies to our site, when viewed on a mobile device:<br />When accessed with a mobile deivce, our site may collect information automatically, such as the type of mobile device you have, device identifiers, and information about your use of the site. Regardless of the device you use to access the site, it will also collect information you provide, as well as information about your interaction with the site and its content.<br />If location services are activated on your mobile device, our site may collect information about the location of your device. Your mobile network service providers may collect device-specific information, such as a device identifier, when you use our website or one of our mobile applications. This information collected by your mobile network service will not be associated with your user account with us, or with your personally identifiable information.</p>\r\n<p>Protection of user information<br />We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you share with the site.</p>\r\n<p>Advertising Network<br />We use one or more third party vendors to serve ads on the Site. To serve ads and determine how our users use the Site, these services use cookies, or small pieces of code to serve ads to Site users based on users&rsquo; visits to the Site and others. Users may adjust their browser settings to disallow the use of cookies. With cookies turned off, certain features of the Site may work less efficiently or not at all.</p>\r\n<p>We use Google as an advertising services provider for the Site. Users may opt out of Google&rsquo;s use of the DART use-tracking cookie by visiting the Google advertising Policies &amp; Principles page. If you opt out of ad tailoring, you will still see ads, but they will not be based on your browsing history, and they may appear in other languages.</p>\r\n<p>Cookies<br />This site uses cookies. Cookies are small pieces of code that the Site or a service provider will put on your computer if your Web browser allows it. The Site uses cookies to recognize and keep certain information. On the Site, that information may be used to recognize your computer and browser from current or past visits to the Site or related sites. We may use this cookie-captured information to improve our service to you, to aggregate information about visitors to the Site and their behavior, to remember and process items in your shopping cart, to understand and save user preferences, or to keep track of advertising. We may contract with third-party service providers to assist us in better understanding our site visitors.<br />In most Internet browsers, you can change your settings so that you will be warned each time a cookie is being sent, or so that cookies will be turned off. With cookies blocked, some functions of the Site may not operate properly.</p>\r\n<p>Usernames, Passwords, and Profiles<br />If prompted, Users must provide a valid email address to the Site, at which the User can receive messages. User must also update the Site if that email address changes. The Site reserves the right to terminate any User account if a valid email is requested but is not provided by the User.<br />If the Site prompts or allows a User to create a username or profile, Users agree not to pick a username or provide any profile information that would impersonate someone or that is likely to cause confusion with any other person or entity. The Site reserves the right to cancel a User account or change a username or profile data at any time. Similarly, if the Site prompts or allows a User to create an avatar or upload a picture, User agrees not to use any image that impersonates some other person or entity, or that is otherwise likely to cause confusion.<br />You are responsible for protecting your username and password for the Site, and you agree not to disclose it to any third party. We recommend that you use a passwork that is more than eight characters long. You are responsible for all activity on your account, whether or not you authorized it. You agree to inform us of unauthorized use of your account, by email to Admin@Desawana.Com. You acknowledge that if you wish to protect your interactions the Site, it is your responsibility to use a secure encrypted connection, virtual private network, or other appropriate measures.</p>\r\n<p>Disputes<br />We are based in Moratuwa and you are contracting to use our Site. This Policy and all matters arising from your use of the Site are governed by and will be construed according to the laws of Moratuwa, without regard to any choice of laws rules of any jurisdiction. The federal courts and state courts that have geographical jurisdiction over disputes arising at our office location in Moratuwa will be the only permissible venues for any and all disputes arising out of or in connection with this Policy or the Site and Service.</p>\r\n<p>ARBITRATION<br />Notwithstanding anything that may be contrary within the &ldquo;Disputes&rdquo; provisions above, all matters, and all claims within a multi-claim matter, that are arbitrable, including all claims for monetary damages, shall be decided by a single arbitrator to be selected by us, who shall hold hearings in or near Moratuwa, under the rules of the American Arbitration Association.</p>\r\n<p>Terms Contact<br />If you have any questions about these Terms, please address them to Admin@Desawana.Com.</p>\r\n<p>Last Updated<br />These terms were last updated on May 30, 2018</p>', '0000-00-00 00:00:00', '2018-05-30 23:52:58'),
(2, 'Terms Of Service Page', '<p>Introduction<br />Site Terms of Service, an Enforceable Legal Agreement.<br />As of May 30, 2018</p>\r\n<p>These Terms of Service and our privacy policy (together the &ldquo;Terms&rdquo;) govern all use of https://desawana.com and that site&rsquo;s services (together the &ldquo;Site&rdquo; or &ldquo;Services&rdquo;). The Site is owned by Kasun Madushanka, a individual.</p>\r\n<p>The owners and contributors to the Site will be referred to as &ldquo;we,&rdquo; &ldquo;us,&rdquo; or &ldquo;our&rdquo; in these Terms. By using the Site or its Services, and/or by clicking anywhere on this Site to agree to these Terms, you are deemed to be a &ldquo;user&rdquo; for purposes of the Terms. You and every other user (&ldquo;you&rdquo; or &ldquo;User&rdquo; as applicable) are bound by these Terms. You and each user also agree to the Terms by using the Services. If any User does not agree to the Terms or the Privacy Policy, such User may not access the Site or use the Services. In these Terms, the word &ldquo;Site&rdquo; includes the site referenced above, its owner(s), contributors, suppliers, licensors, and other related parties.</p>\r\n<p>User Prohibited From Illegal Uses<br />User shall not use, and shall not allow any person to use, the Site or Services in any way that violates a federal, state, or local law, regulation, or ordinance, or for any disruptive, tortious, or illegal purpose, including but not limited to harassment, slander, defamation, data theft or inappropriate dissemination, or improper surveillance of any person.</p>\r\n<p>User represents and warrants that:<br />User will use the Services only as provided in these Terms;<br />User is at least 18 years old and has all right, authority, and capacity to agree to these Terms;<br />User will provide accurate, complete, and current information to the Site and its owner(s);<br />User will notify the Site and its owner(s) regarding any material change to information User provides, either by updating and correcting the information, or by alerting the Site and its owner(s) via the functions of the Site or the email address provided below.<br />Disclaimer of Warranties<br />TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SITE PROVIDES THE SERVICES &ldquo;AS IS,&rdquo; WITH ALL FAULTS. THE SITE DOES NOT WARRANT UNINTERRUPTED USE OR OPERATION OF THE SERVICES, OR THAT ANY DATA WILL BE TRANSMITTED IN A MANNER THAT IS TIMELY, UNCORRUPTED, FREE OF INTERFERENCE, OR SECURE. THE SITE DISCLAIMS REPRESENTATIONS, WARRANTIES, AND CONDITIONS OF ANY KIND, WHETHER EXPRESS, IMPLIED, WRITTEN, ORAL, CONTRACTUAL, COMMON LAW, OR STATUTORY, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES, DUTIES, OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, OR THAT MAY ARISE FROM A COURSE OF DEALING OR USAGE OF TRADE.</p>\r\n<p>Liability Is Limited<br />THE SITE SHALL NOT BE LIABLE FOR INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY, OR PUNITIVE DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOST PROFITS (REGARDLESS OF WHETHER WE HAVE BEEN NOTIFIED THAT SUCH LOSS MAY OCCUR) OR EXPOSURE TO ANY THIRD PARTY CLAIMS BY REASON OF ANY ACT OR OMISSION. THE SITE SHALL NOT BE LIABLE FOR ANY ACT OR OMISSION OF ANY THIRD PARTY INVOLVED WITH THE SERVICES, SITE OFFERS, OR ANY ACT BY SITE USERS. THE SITE SHALL NOT BE LIABLE FOR ANY DAMAGES THAT RESULT FROM ANY SERVICE PROVIDED BY, OR PRODUCT OR DEVICE MANUFACTURED BY, THIRD PARTIES.</p>\r\n<p>NOTWITHSTANDING ANY DAMAGES THAT USER MAY SUFFER FOR ANY REASON, THE ENTIRE LIABILITY OF THE SITE IN CONNECTION WITH THE SITE OR SERVICES, AND ANY PARTY&rsquo;S EXCLUSIVE REMEDY, SHALL BE LIMITED TO THE AMOUNT, IF ANY, ACTUALLY PAID BY USER TO THE SITE OWNER DURING THE 12 MONTHS PRIOR TO THE EVENT THAT USER CLAIMS CAUSED THE DAMAGES.</p>\r\n<p>The Site shall not be liable for any damages incurred as a result of any loss, disclosure, or third party use of information, regardless of whether such disclosure or use is with or without User&rsquo;s knowledge or consent. The Site shall have no liability for any damages related to: User&rsquo;s actions or failures to act, the acts or omissions of any third party, including but not limited to any telecommunications service provider, or events or causes beyond the Site&rsquo;s reasonable control. The Site has no obligations whatever, and shall have no liability to, any third party who is not a User bound by these Terms. Limitations, exclusions, and disclaimers in these Terms shall apply to the maximum extent permitted by applicable law, even if any remedy fails its essential purpose.</p>\r\n<p>Third party products, links, and actions<br />The Site may include or offer third party products or services. The Site may also have other users or members who interact with each other, through the Site, elsewhere online, or in person. These third party products and any linked sites have separate and independent terms of service and privacy policies. We have no control or responsibility for the content and activities of these linked sites, sellers, and third parties in general, regardless of whether you first were introduced or interacted with such businesses, services, products, and people through the Site, and therefore you agree that we are not liable for any of them. We do, however, welcome any feedback about these sites, sellers, other users or members, and third parties.</p>\r\n<p>Changes to the Site and the Services<br />The owners and contributors to the Site will work to improve the Site for our users, and to further our business interests in the Site. We reserve the right to add, change, and remove features, content, and data, including the right to add or change any pricing terms. You agree that we will not be liable for any such changes. Neither your use of the Site nor these terms give you any right, title, or protectable legal interest in the Site or its content.</p>\r\n<p>Indemnity<br />If your activity or any activity on your behalf creates potential or actual liability for us, or for any of our users, partners, or contributors, you agree to indemnify and hold us and any such user, partner, contributor, or any agent harmless from and against all claims, costs of defense and judgment, liabilities, legal fees, damages, losses, and other expenses in relation to any claims or actions arising out of or relating to your use of the Site, or any breach by you of these Terms of Use.</p>\r\n<p>Intellectual Property<br />This site and some delivery modes of our product are built on the WordPress platform. For information about intellectual property rights, including General Public License (&ldquo;GPL&rdquo;) terms under which the WordPress software is licensed, see here http://wordpress.org/about/gpl/</p>\r\n<p>The Site grants User a revocable, non-transferable, and non-exclusive license to use the Site solely in connection with the Site and the Services, under these Terms.</p>\r\n<p>Copyright in all content and works of authorship included in the Site are the property of the Site or its licensors. Apart from links which lead to the Site, accurately attributed social media references, and de minimus text excerpts with links returning to the Site, no text, images, video or audio recording, or any other content from the Site shall be copied without explicit and detailed, written permission from the Site&rsquo;s owner. User shall not sublicense or otherwise transfer any rights or access to the Site or related Services to any other person.</p>\r\n<p>The names and logos used by the Site, and all other trademarks, service marks, and trade names used in connection with the Services are owned by the Site or its licensors and may not be used by User without written consent of the rights owners. Use of the Site does not in itself give any user any license, consent, or permission, unless and then only to the extent granted explicitly in these Terms.</p>\r\n<p>All rights not expressly granted in these Terms are reserved by the Site.</p>\r\n<p>Privacy<br />Any information that you provide to the Site is subject to the Site&rsquo;s Privacy Policy, which governs our collection and use of User information. User understands that through his or her use of the Site and its Services, User consents to the collection and use (as set forth in the Privacy Policy) of the information, including the transfer of this information to the United States and/or other countries for storage, processing and use by the Site. The Site may make certain communications to some or all Users, such as service announcements and administrative messages. These communications are considered part of the Services and a User&rsquo;s account with the Site, and Users are not able to opt out of all of them.</p>\r\n<p>Usernames, Passwords, and Profiles<br />If prompted, Users must provide a valid email address to the Site, at which email address the User can receive messages. User must also update the Site if that email address changes. The Site reserves the right to terminate any User account and/or User access to the Site if a valid email is requested but is not provided by the User.</p>\r\n<p>If the Site prompts or allows a User to create a username or profile, Users agree not to pick a username or provide any profile information that would impersonate someone else or that is likely to cause confusion with any other person or entity. The Site reserves the right to cancel a User account or to change a username or profile data at any time. Similarly, if the Site allows comments or user input, or prompts or allows a User to create an avatar or upload a picture, User agrees not to use any image that impersonates some other person or entity, or that is otherwise likely to cause confusion.</p>\r\n<p>You are responsible for protecting your username and password for the Site, and you agree not to disclose it to any third party. We recommend that you use a password that is more than eight characters long. You are responsible for all activity on your account, whether or not you authorized it. You agree to inform us of unauthorized use of your account, by email to Admin@Desawana.Com. You acknowledge that if you wish to protect your interactions with the Site, it is your responsibility to use a secure encrypted connection, virtual private network, or other appropriate measures. The Site&rsquo;s own security measures are reasonable in terms of their level of protection, but are not helpful if the interactions of you or any other User with Site are not secure or private.</p>\r\n<p>Disputes<br />We are based in Moratuwa and you are contracting to use our Site. These Terms and all matters arising from your use of the Site are governed by and will be construed according to the laws of Moratuwa, without regard to any choice of laws rules of any jurisdiction. The federal courts and state courts that have geographical jurisdiction over disputes arising at our office location in the Moratuwa will be the only permissible venues for any and all disputes arising out of or in connection with these Terms or the Site and Service.</p>\r\n<p>ARBITRATION<br />Notwithstanding anything that may be contrary within the &ldquo;Disputes&rdquo; provisions above, all matters, and all arbitrable claims within a multi-claim matter, including all claims for monetary damages, shall be decided by a single arbitrator to be selected by us, which arbitrator shall hold hearings in or near Moratuwa, under the rules of the American Arbitration Association.</p>\r\n<p>Advertising<br />The Site may include advertisements, which may be targeted for relevance to the Site, queries made, or other information to improve relevance to the Site&rsquo;s users. The types and extent of advertising on the Site will change over time. In consideration for User access to and use of the Site, User agrees that the Site and third party providers and partners may place advertising anywhere on the Site. For the remaining terms that will apply to our advertising practices, including use of your information, see our Privacy Policy.</p>\r\n<p>General<br />These Terms, including the incorporated Privacy Policy, supersede all oral or written communications and understandings between User and the Site.</p>\r\n<p>Any cause of action User may have relating to the Site or the Services must be commenced within one (1) year after the claim or cause of action arises.</p>\r\n<p>Both parties waive the right to a jury trial in any dispute relating to the Terms, the Site, or the Services.</p>\r\n<p>If for any reason a court of competent jurisdiction finds any aspect of the Terms to be unenforceable, the Terms shall be enforced to the maximum extent permissible, to give effect to the intent of the Terms, and the remainder of the Terms shall continue in full force and effect.</p>\r\n<p>User may not assign his or her rights or delegate his or her responsibilities under these Terms or otherwise relating to the Site or its Services.</p>\r\n<p>There shall be no third party beneficiaries under these Terms, except for the Site&rsquo;s affiliates, suppliers, and licensors, or as required by law.</p>\r\n<p>Use of the Site and its Services is unauthorized in any jurisdiction that does not give effect to all provisions of these Terms, including without limitation this paragraph.</p>\r\n<p>The failure of the Site to exercise or enforce any right or provision of these Terms shall not constitute a waiver of that right or provision.</p>\r\n<p>Terms Contact<br />If you have any questions about these Terms, please address them to Admin@Desawana.Com.</p>\r\n<p>Last Updated<br />These terms were last updated on May 30, 2018</p>', '0000-00-00 00:00:00', '2018-05-30 23:53:38');

-- --------------------------------------------------------

--
-- Table structure for table `fonts-list`
--

CREATE TABLE `fonts-list` (
  `id` int(15) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `unicode` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fonts-list`
--

INSERT INTO `fonts-list` (`id`, `type`, `icon`, `unicode`) VALUES
(1, 'fa', 'fa-adjust', '&#xf042;'),
(2, 'fa', 'fa-adn', '&#xf170;'),
(3, 'fa', 'fa-align-center', '&#xf037;'),
(4, 'fa', 'fa-align-justify', '&#xf039;'),
(5, 'fa', 'fa-align-left', '&#xf036;'),
(6, 'fa', 'fa-align-right', '&#xf038;'),
(7, 'fa', 'fa-ambulance', '&#xf0f9;'),
(8, 'fa', 'fa-anchor', '&#xf13d;'),
(9, 'fa', 'fa-android', '&#xf17b;'),
(10, 'fa', 'fa-angellist', '&#xf209;'),
(11, 'fa', 'fa-angle-double-down', '&#xf103;'),
(12, 'fa', 'fa-angle-double-left', '&#xf100;'),
(13, 'fa', 'fa-angle-double-right', '&#xf101;'),
(14, 'fa', 'fa-angle-double-up', '&#xf102;'),
(15, 'fa', 'fa-angle-down', '&#xf107;'),
(16, 'fa', 'fa-angle-left', '&#xf104;'),
(17, 'fa', 'fa-angle-right', '&#xf105;'),
(18, 'fa', 'fa-angle-up', '&#xf106;'),
(19, 'fa', 'fa-apple', '&#xf179;'),
(20, 'fa', 'fa-archive', '&#xf187;'),
(21, 'fa', 'fa-area-chart', '&#xf1fe;'),
(22, 'fa', 'fa-arrow-circle-down', '&#xf0ab;'),
(23, 'fa', 'fa-arrow-circle-left', '&#xf0a8;'),
(24, 'fa', 'fa-arrow-circle-o-down', '&#xf01a;'),
(25, 'fa', 'fa-arrow-circle-o-left', '&#xf190;'),
(26, 'fa', 'fa-arrow-circle-o-right', '&#xf18e;'),
(27, 'fa', 'fa-arrow-circle-o-up', '&#xf01b;'),
(28, 'fa', 'fa-arrow-circle-right', '&#xf0a9;'),
(29, 'fa', 'fa-arrow-circle-up', '&#xf0aa;'),
(30, 'fa', 'fa-arrow-down', '&#xf063;'),
(31, 'fa', 'fa-arrow-left', '&#xf060;'),
(32, 'fa', 'fa-arrow-right', '&#xf061;'),
(33, 'fa', 'fa-arrow-up', '&#xf062;'),
(34, 'fa', 'fa-arrows', '&#xf047;'),
(35, 'fa', 'fa-arrows-alt', '&#xf0b2;'),
(36, 'fa', 'fa-arrows-h', '&#xf07e;'),
(37, 'fa', 'fa-arrows-v', '&#xf07d;'),
(38, 'fa', 'fa-asterisk', '&#xf069;'),
(39, 'fa', 'fa-at', '&#xf1fa;'),
(40, 'fa', 'fa-automobile(alias)', '&#xf1b9;'),
(41, 'fa', 'fa-backward', '&#xf04a;'),
(42, 'fa', 'fa-ban', '&#xf05e;'),
(43, 'fa', 'fa-bank(alias)', '&#xf19c;'),
(44, 'fa', 'fa-bar-chart', '&#xf080;'),
(45, 'fa', 'fa-bar-chart-o(alias)', '&#xf080;'),
(46, 'fa', 'fa-barcode', '&#xf02a;'),
(47, 'fa', 'fa-bars', '&#xf0c9;'),
(48, 'fa', 'fa-bed', '&#xf236;'),
(49, 'fa', 'fa-beer', '&#xf0fc;'),
(50, 'fa', 'fa-behance', '&#xf1b4;'),
(51, 'fa', 'fa-behance-square', '&#xf1b5;'),
(52, 'fa', 'fa-bell', '&#xf0f3;'),
(53, 'fa', 'fa-bell-o', '&#xf0a2;'),
(54, 'fa', 'fa-bell-slash', '&#xf1f6;'),
(55, 'fa', 'fa-bell-slash-o', '&#xf1f7;'),
(56, 'fa', 'fa-bicycle', '&#xf206;'),
(57, 'fa', 'fa-binoculars', '&#xf1e5;'),
(58, 'fa', 'fa-birthday-cake', '&#xf1fd;'),
(59, 'fa', 'fa-bitbucket', '&#xf171;'),
(60, 'fa', 'fa-bitbucket-square', '&#xf172;'),
(61, 'fa', 'fa-bitcoin(alias)', '&#xf15a;'),
(62, 'fa', 'fa-bold', '&#xf032;'),
(63, 'fa', 'fa-bolt', '&#xf0e7;'),
(64, 'fa', 'fa-bomb', '&#xf1e2;'),
(65, 'fa', 'fa-book', '&#xf02d;'),
(66, 'fa', 'fa-bookmark', '&#xf02e;'),
(67, 'fa', 'fa-bookmark-o', '&#xf097;'),
(68, 'fa', 'fa-briefcase', '&#xf0b1;'),
(69, 'fa', 'fa-btc', '&#xf15a;'),
(70, 'fa', 'fa-bug', '&#xf188;'),
(71, 'fa', 'fa-building', '&#xf1ad;'),
(72, 'fa', 'fa-building-o', '&#xf0f7;'),
(73, 'fa', 'fa-bullhorn', '&#xf0a1;'),
(74, 'fa', 'fa-bullseye', '&#xf140;'),
(75, 'fa', 'fa-bus', '&#xf207;'),
(76, 'fa', 'fa-buysellads', NULL),
(77, 'fa', 'fa-cab(alias)', NULL),
(78, 'fa', 'fa-calculator', NULL),
(79, 'fa', 'fa-calendar', NULL),
(80, 'fa', 'fa-calendar-o', NULL),
(81, 'fa', 'fa-camera', NULL),
(82, 'fa', 'fa-camera-retro', NULL),
(83, 'fa', 'fa-car', NULL),
(84, 'fa', 'fa-caret-down', NULL),
(85, 'fa', 'fa-caret-left', NULL),
(86, 'fa', 'fa-caret-right', NULL),
(87, 'fa', 'fa-caret-square-o-down', NULL),
(88, 'fa', 'fa-caret-square-o-left', NULL),
(89, 'fa', 'fa-caret-square-o-right', NULL),
(90, 'fa', 'fa-caret-square-o-up', NULL),
(91, 'fa', 'fa-caret-up', NULL),
(92, 'fa', 'fa-cart-arrow-down', NULL),
(93, 'fa', 'fa-cart-plus', NULL),
(94, 'fa', 'fa-cc', NULL),
(95, 'fa', 'fa-cc-amex', NULL),
(96, 'fa', 'fa-cc-discover', NULL),
(97, 'fa', 'fa-cc-mastercard', NULL),
(98, 'fa', 'fa-cc-paypal', NULL),
(99, 'fa', 'fa-cc-stripe', NULL),
(100, 'fa', 'fa-cc-visa', NULL),
(101, 'fa', 'fa-certificate', NULL),
(102, 'fa', 'fa-chain(alias)', NULL),
(103, 'fa', 'fa-chain-broken', NULL),
(104, 'fa', 'fa-check', NULL),
(105, 'fa', 'fa-check-circle', NULL),
(106, 'fa', 'fa-check-circle-o', NULL),
(107, 'fa', 'fa-check-square', NULL),
(108, 'fa', 'fa-check-square-o', NULL),
(109, 'fa', 'fa-chevron-circle-down', NULL),
(110, 'fa', 'fa-chevron-circle-left', NULL),
(111, 'fa', 'fa-chevron-circle-right', NULL),
(112, 'fa', 'fa-chevron-circle-up', NULL),
(113, 'fa', 'fa-chevron-down', NULL),
(114, 'fa', 'fa-chevron-left', NULL),
(115, 'fa', 'fa-chevron-right', NULL),
(116, 'fa', 'fa-chevron-up', NULL),
(117, 'fa', 'fa-child', NULL),
(118, 'fa', 'fa-circle', NULL),
(119, 'fa', 'fa-circle-o', NULL),
(120, 'fa', 'fa-circle-o-notch', NULL),
(121, 'fa', 'fa-circle-thin', NULL),
(122, 'fa', 'fa-clipboard', NULL),
(123, 'fa', 'fa-clock-o', NULL),
(124, 'fa', 'fa-close(alias)', NULL),
(125, 'fa', 'fa-cloud', NULL),
(126, 'fa', 'fa-cloud-download', NULL),
(127, 'fa', 'fa-cloud-upload', NULL),
(128, 'fa', 'fa-cny(alias)', NULL),
(129, 'fa', 'fa-code', NULL),
(130, 'fa', 'fa-code-fork', NULL),
(131, 'fa', 'fa-codepen', NULL),
(132, 'fa', 'fa-coffee', NULL),
(133, 'fa', 'fa-cog', NULL),
(134, 'fa', 'fa-cogs', NULL),
(135, 'fa', 'fa-columns', NULL),
(136, 'fa', 'fa-comment', NULL),
(137, 'fa', 'fa-comment-o', NULL),
(138, 'fa', 'fa-comments', NULL),
(139, 'fa', 'fa-comments-o', NULL),
(140, 'fa', 'fa-compass', NULL),
(141, 'fa', 'fa-compress', NULL),
(142, 'fa', 'fa-connectdevelop', NULL),
(143, 'fa', 'fa-copy(alias)', NULL),
(144, 'fa', 'fa-copyright', NULL),
(145, 'fa', 'fa-credit-card', NULL),
(146, 'fa', 'fa-crop', NULL),
(147, 'fa', 'fa-crosshairs', NULL),
(148, 'fa', 'fa-css3', NULL),
(149, 'fa', 'fa-cube', NULL),
(150, 'fa', 'fa-cubes', NULL),
(151, 'fa', 'fa-cut(alias)', NULL),
(152, 'fa', 'fa-cutlery', NULL),
(153, 'fa', 'fa-dashboard(alias)', NULL),
(154, 'fa', 'fa-dashcube', NULL),
(155, 'fa', 'fa-database', NULL),
(156, 'fa', 'fa-dedent(alias)', NULL),
(157, 'fa', 'fa-delicious', NULL),
(158, 'fa', 'fa-desktop', NULL),
(159, 'fa', 'fa-deviantart', NULL),
(160, 'fa', 'fa-diamond', NULL),
(161, 'fa', 'fa-digg', NULL),
(162, 'fa', 'fa-dollar(alias)', NULL),
(163, 'fa', 'fa-dot-circle-o', NULL),
(164, 'fa', 'fa-download', NULL),
(165, 'fa', 'fa-dribbble', NULL),
(166, 'fa', 'fa-dropbox', NULL),
(167, 'fa', 'fa-drupal', NULL),
(168, 'fa', 'fa-edit(alias)', NULL),
(169, 'fa', 'fa-eject', NULL),
(170, 'fa', 'fa-ellipsis-h', NULL),
(171, 'fa', 'fa-ellipsis-v', NULL),
(172, 'fa', 'fa-empire', NULL),
(173, 'fa', 'fa-envelope', NULL),
(174, 'fa', 'fa-envelope-o', NULL),
(175, 'fa', 'fa-envelope-square', NULL),
(176, 'fa', 'fa-eraser', NULL),
(177, 'fa', 'fa-eur', NULL),
(178, 'fa', 'fa-euro(alias)', NULL),
(179, 'fa', 'fa-exchange', NULL),
(180, 'fa', 'fa-exclamation', NULL),
(181, 'fa', 'fa-exclamation-circle', NULL),
(182, 'fa', 'fa-exclamation-triangle', NULL),
(183, 'fa', 'fa-expand', NULL),
(184, 'fa', 'fa-external-link', NULL),
(185, 'fa', 'fa-external-link-square', NULL),
(186, 'fa', 'fa-eye', NULL),
(187, 'fa', 'fa-eye-slash', NULL),
(188, 'fa', 'fa-eyedropper', NULL),
(189, 'fa', 'fa-facebook', NULL),
(190, 'fa', 'fa-facebook-f(alias)', NULL),
(191, 'fa', 'fa-facebook-official', NULL),
(192, 'fa', 'fa-facebook-square', NULL),
(193, 'fa', 'fa-fast-backward', NULL),
(194, 'fa', 'fa-fast-forward', NULL),
(195, 'fa', 'fa-fax', NULL),
(196, 'fa', 'fa-female', NULL),
(197, 'fa', 'fa-fighter-jet', NULL),
(198, 'fa', 'fa-file', NULL),
(199, 'fa', 'fa-file-archive-o', NULL),
(200, 'fa', 'fa-file-audio-o', NULL),
(201, 'fa', 'fa-file-code-o', NULL),
(202, 'fa', 'fa-file-excel-o', NULL),
(203, 'fa', 'fa-file-image-o', NULL),
(204, 'fa', 'fa-file-movie-o(alias)', NULL),
(205, 'fa', 'fa-file-o', NULL),
(206, 'fa', 'fa-file-pdf-o', NULL),
(207, 'fa', 'fa-file-photo-o(alias)', NULL),
(208, 'fa', 'fa-file-picture-o(alias)', NULL),
(209, 'fa', 'fa-file-powerpoint-o', NULL),
(210, 'fa', 'fa-file-sound-o(alias)', NULL),
(211, 'fa', 'fa-file-text', NULL),
(212, 'fa', 'fa-file-text-o', NULL),
(213, 'fa', 'fa-file-video-o', NULL),
(214, 'fa', 'fa-file-word-o', NULL),
(215, 'fa', 'fa-file-zip-o(alias)', NULL),
(216, 'fa', 'fa-files-o', NULL),
(217, 'fa', 'fa-film', NULL),
(218, 'fa', 'fa-filter', NULL),
(219, 'fa', 'fa-fire', NULL),
(220, 'fa', 'fa-fire-extinguisher', NULL),
(221, 'fa', 'fa-flag', NULL),
(222, 'fa', 'fa-flag-checkered', NULL),
(223, 'fa', 'fa-flag-o', NULL),
(224, 'fa', 'fa-flash(alias)', NULL),
(225, 'fa', 'fa-flask', NULL),
(226, 'fa', 'fa-flickr', NULL),
(227, 'fa', 'fa-floppy-o', NULL),
(228, 'fa', 'fa-folder', NULL),
(229, 'fa', 'fa-folder-o', NULL),
(230, 'fa', 'fa-folder-open', NULL),
(231, 'fa', 'fa-folder-open-o', NULL),
(232, 'fa', 'fa-font', NULL),
(233, 'fa', 'fa-forumbee', NULL),
(234, 'fa', 'fa-forward', NULL),
(235, 'fa', 'fa-foursquare', NULL),
(236, 'fa', 'fa-frown-o', NULL),
(237, 'fa', 'fa-futbol-o', NULL),
(238, 'fa', 'fa-gamepad', NULL),
(239, 'fa', 'fa-gavel', NULL),
(240, 'fa', 'fa-gbp', NULL),
(241, 'fa', 'fa-ge(alias)', NULL),
(242, 'fa', 'fa-gear(alias)', NULL),
(243, 'fa', 'fa-gears(alias)', NULL),
(244, 'fa', 'fa-genderless(alias)', NULL),
(245, 'fa', 'fa-gift', NULL),
(246, 'fa', 'fa-git', NULL),
(247, 'fa', 'fa-git-square', NULL),
(248, 'fa', 'fa-github', NULL),
(249, 'fa', 'fa-github-alt', NULL),
(250, 'fa', 'fa-github-square', NULL),
(251, 'fa', 'fa-gittip(alias)', NULL),
(252, 'fa', 'fa-glass', NULL),
(253, 'fa', 'fa-globe', NULL),
(254, 'fa', 'fa-google', NULL),
(255, 'fa', 'fa-google-plus', NULL),
(256, 'fa', 'fa-google-plus-square', NULL),
(257, 'fa', 'fa-google-wallet', NULL),
(258, 'fa', 'fa-graduation-cap', NULL),
(259, 'fa', 'fa-gratipay', NULL),
(260, 'fa', 'fa-group(alias)', NULL),
(261, 'fa', 'fa-h-square', NULL),
(262, 'fa', 'fa-hacker-news', NULL),
(263, 'fa', 'fa-hand-o-down', NULL),
(264, 'fa', 'fa-hand-o-left', NULL),
(265, 'fa', 'fa-hand-o-right', NULL),
(266, 'fa', 'fa-hand-o-up', NULL),
(267, 'fa', 'fa-hdd-o', NULL),
(268, 'fa', 'fa-header', NULL),
(269, 'fa', 'fa-headphones', NULL),
(270, 'fa', 'fa-heart', NULL),
(271, 'fa', 'fa-heart-o', NULL),
(272, 'fa', 'fa-heartbeat', NULL),
(273, 'fa', 'fa-history', NULL),
(274, 'fa', 'fa-home', NULL),
(275, 'fa', 'fa-hospital-o', NULL),
(276, 'fa', 'fa-hotel(alias)', NULL),
(277, 'fa', 'fa-html5', NULL),
(278, 'fa', 'fa-ils', NULL),
(279, 'fa', 'fa-image(alias)', NULL),
(280, 'fa', 'fa-inbox', NULL),
(281, 'fa', 'fa-indent', NULL),
(282, 'fa', 'fa-info', NULL),
(283, 'fa', 'fa-info-circle', NULL),
(284, 'fa', 'fa-inr', NULL),
(285, 'fa', 'fa-instagram', NULL),
(286, 'fa', 'fa-institution(alias)', NULL),
(287, 'fa', 'fa-ioxhost', NULL),
(288, 'fa', 'fa-italic', NULL),
(289, 'fa', 'fa-joomla', NULL),
(290, 'fa', 'fa-jpy', NULL),
(291, 'fa', 'fa-jsfiddle', NULL),
(292, 'fa', 'fa-key', NULL),
(293, 'fa', 'fa-keyboard-o', NULL),
(294, 'fa', 'fa-krw', NULL),
(295, 'fa', 'fa-language', NULL),
(296, 'fa', 'fa-laptop', NULL),
(297, 'fa', 'fa-lastfm', NULL),
(298, 'fa', 'fa-lastfm-square', NULL),
(299, 'fa', 'fa-leaf', NULL),
(300, 'fa', 'fa-leanpub', NULL),
(301, 'fa', 'fa-legal(alias)', NULL),
(302, 'fa', 'fa-lemon-o', NULL),
(303, 'fa', 'fa-level-down', NULL),
(304, 'fa', 'fa-level-up', NULL),
(305, 'fa', 'fa-life-bouy(alias)', NULL),
(306, 'fa', 'fa-life-buoy(alias)', NULL),
(307, 'fa', 'fa-life-ring', NULL),
(308, 'fa', 'fa-life-saver(alias)', NULL),
(309, 'fa', 'fa-lightbulb-o', NULL),
(310, 'fa', 'fa-line-chart', NULL),
(311, 'fa', 'fa-link', NULL),
(312, 'fa', 'fa-linkedin', NULL),
(313, 'fa', 'fa-linkedin-square', NULL),
(314, 'fa', 'fa-linux', NULL),
(315, 'fa', 'fa-list', NULL),
(316, 'fa', 'fa-list-alt', NULL),
(317, 'fa', 'fa-list-ol', NULL),
(318, 'fa', 'fa-list-ul', NULL),
(319, 'fa', 'fa-location-arrow', NULL),
(320, 'fa', 'fa-lock', NULL),
(321, 'fa', 'fa-long-arrow-down', NULL),
(322, 'fa', 'fa-long-arrow-left', NULL),
(323, 'fa', 'fa-long-arrow-right', NULL),
(324, 'fa', 'fa-long-arrow-up', NULL),
(325, 'fa', 'fa-magic', NULL),
(326, 'fa', 'fa-magnet', NULL),
(327, 'fa', 'fa-mail-forward(alias)', NULL),
(328, 'fa', 'fa-mail-reply(alias)', NULL),
(329, 'fa', 'fa-mail-reply-all(alias)', NULL),
(330, 'fa', 'fa-male', NULL),
(331, 'fa', 'fa-map-marker', NULL),
(332, 'fa', 'fa-mars', NULL),
(333, 'fa', 'fa-mars-double', NULL),
(334, 'fa', 'fa-mars-stroke', NULL),
(335, 'fa', 'fa-mars-stroke-h', NULL),
(336, 'fa', 'fa-mars-stroke-v', NULL),
(337, 'fa', 'fa-maxcdn', NULL),
(338, 'fa', 'fa-meanpath', NULL),
(339, 'fa', 'fa-medium', NULL),
(340, 'fa', 'fa-medkit', NULL),
(341, 'fa', 'fa-meh-o', NULL),
(342, 'fa', 'fa-mercury', NULL),
(343, 'fa', 'fa-microphone', NULL),
(344, 'fa', 'fa-microphone-slash', NULL),
(345, 'fa', 'fa-minus', NULL),
(346, 'fa', 'fa-minus-circle', NULL),
(347, 'fa', 'fa-minus-square', NULL),
(348, 'fa', 'fa-minus-square-o', NULL),
(349, 'fa', 'fa-mobile', NULL),
(350, 'fa', 'fa-mobile-phone(alias)', NULL),
(351, 'fa', 'fa-money', NULL),
(352, 'fa', 'fa-moon-o', NULL),
(353, 'fa', 'fa-mortar-board(alias)', NULL),
(354, 'fa', 'fa-motorcycle', NULL),
(355, 'fa', 'fa-music', NULL),
(356, 'fa', 'fa-navicon(alias)', NULL),
(357, 'fa', 'fa-neuter', NULL),
(358, 'fa', 'fa-newspaper-o', NULL),
(359, 'fa', 'fa-openid', NULL),
(360, 'fa', 'fa-outdent', NULL),
(361, 'fa', 'fa-pagelines', NULL),
(362, 'fa', 'fa-paint-brush', NULL),
(363, 'fa', 'fa-paper-plane', NULL),
(364, 'fa', 'fa-paper-plane-o', NULL),
(365, 'fa', 'fa-paperclip', NULL),
(366, 'fa', 'fa-paragraph', NULL),
(367, 'fa', 'fa-paste(alias)', NULL),
(368, 'fa', 'fa-pause', NULL),
(369, 'fa', 'fa-paw', NULL),
(370, 'fa', 'fa-paypal', NULL),
(371, 'fa', 'fa-pencil', NULL),
(372, 'fa', 'fa-pencil-square', NULL),
(373, 'fa', 'fa-pencil-square-o', NULL),
(374, 'fa', 'fa-phone', NULL),
(375, 'fa', 'fa-phone-square', NULL),
(376, 'fa', 'fa-photo(alias)', NULL),
(377, 'fa', 'fa-picture-o', NULL),
(378, 'fa', 'fa-pie-chart', NULL),
(379, 'fa', 'fa-pied-piper', NULL),
(380, 'fa', 'fa-pied-piper-alt', NULL),
(381, 'fa', 'fa-pinterest', NULL),
(382, 'fa', 'fa-pinterest-p', NULL),
(383, 'fa', 'fa-pinterest-square', NULL),
(384, 'fa', 'fa-plane', NULL),
(385, 'fa', 'fa-play', NULL),
(386, 'fa', 'fa-play-circle', NULL),
(387, 'fa', 'fa-play-circle-o', NULL),
(388, 'fa', 'fa-plug', NULL),
(389, 'fa', 'fa-plus', NULL),
(390, 'fa', 'fa-plus-circle', NULL),
(391, 'fa', 'fa-plus-square', NULL),
(392, 'fa', 'fa-plus-square-o', NULL),
(393, 'fa', 'fa-power-off', NULL),
(394, 'fa', 'fa-print', NULL),
(395, 'fa', 'fa-puzzle-piece', NULL),
(396, 'fa', 'fa-qq', NULL),
(397, 'fa', 'fa-qrcode', NULL),
(398, 'fa', 'fa-question', NULL),
(399, 'fa', 'fa-question-circle', NULL),
(400, 'fa', 'fa-quote-left', NULL),
(401, 'fa', 'fa-quote-right', NULL),
(402, 'fa', 'fa-ra(alias)', NULL),
(403, 'fa', 'fa-random', NULL),
(404, 'fa', 'fa-rebel', NULL),
(405, 'fa', 'fa-recycle', NULL),
(406, 'fa', 'fa-reddit', NULL),
(407, 'fa', 'fa-reddit-square', NULL),
(408, 'fa', 'fa-refresh', NULL),
(409, 'fa', 'fa-remove(alias)', NULL),
(410, 'fa', 'fa-renren', NULL),
(411, 'fa', 'fa-reorder(alias)', NULL),
(412, 'fa', 'fa-repeat', NULL),
(413, 'fa', 'fa-reply', NULL),
(414, 'fa', 'fa-reply-all', NULL),
(415, 'fa', 'fa-retweet', NULL),
(416, 'fa', 'fa-rmb(alias)', NULL),
(417, 'fa', 'fa-road', NULL),
(418, 'fa', 'fa-rocket', NULL),
(419, 'fa', 'fa-rotate-left(alias)', NULL),
(420, 'fa', 'fa-rotate-right(alias)', NULL),
(421, 'fa', 'fa-rouble(alias)', NULL),
(422, 'fa', 'fa-rss', NULL),
(423, 'fa', 'fa-rss-square', NULL),
(424, 'fa', 'fa-rub', NULL),
(425, 'fa', 'fa-ruble(alias)', NULL),
(426, 'fa', 'fa-rupee(alias)', NULL),
(427, 'fa', 'fa-save(alias)', NULL),
(428, 'fa', 'fa-scissors', NULL),
(429, 'fa', 'fa-search', NULL),
(430, 'fa', 'fa-search-minus', NULL),
(431, 'fa', 'fa-search-plus', NULL),
(432, 'fa', 'fa-sellsy', NULL),
(433, 'fa', 'fa-send(alias)', NULL),
(434, 'fa', 'fa-send-o(alias)', NULL),
(435, 'fa', 'fa-server', NULL),
(436, 'fa', 'fa-share', NULL),
(437, 'fa', 'fa-share-alt', NULL),
(438, 'fa', 'fa-share-alt-square', NULL),
(439, 'fa', 'fa-share-square', NULL),
(440, 'fa', 'fa-share-square-o', NULL),
(441, 'fa', 'fa-shekel(alias)', NULL),
(442, 'fa', 'fa-sheqel(alias)', NULL),
(443, 'fa', 'fa-shield', NULL),
(444, 'fa', 'fa-ship', NULL),
(445, 'fa', 'fa-shirtsinbulk', NULL),
(446, 'fa', 'fa-shopping-cart', NULL),
(447, 'fa', 'fa-sign-in', NULL),
(448, 'fa', 'fa-sign-out', NULL),
(449, 'fa', 'fa-signal', NULL),
(450, 'fa', 'fa-simplybuilt', NULL),
(451, 'fa', 'fa-sitemap', NULL),
(452, 'fa', 'fa-skyatlas', NULL),
(453, 'fa', 'fa-skype', NULL),
(454, 'fa', 'fa-slack', NULL),
(455, 'fa', 'fa-sliders', NULL),
(456, 'fa', 'fa-slideshare', NULL),
(457, 'fa', 'fa-smile-o', NULL),
(458, 'fa', 'fa-soccer-ball-o(alias)', NULL),
(459, 'fa', 'fa-sort', NULL),
(460, 'fa', 'fa-sort-alpha-asc', NULL),
(461, 'fa', 'fa-sort-alpha-desc', NULL),
(462, 'fa', 'fa-sort-amount-asc', NULL),
(463, 'fa', 'fa-sort-amount-desc', NULL),
(464, 'fa', 'fa-sort-asc', NULL),
(465, 'fa', 'fa-sort-desc', NULL),
(466, 'fa', 'fa-sort-down(alias)', NULL),
(467, 'fa', 'fa-sort-numeric-asc', NULL),
(468, 'fa', 'fa-sort-numeric-desc', NULL),
(469, 'fa', 'fa-sort-up(alias)', NULL),
(470, 'fa', 'fa-soundcloud', NULL),
(471, 'fa', 'fa-space-shuttle', NULL),
(472, 'fa', 'fa-spinner', NULL),
(473, 'fa', 'fa-spoon', NULL),
(474, 'fa', 'fa-spotify', NULL),
(475, 'fa', 'fa-square', NULL),
(476, 'fa', 'fa-square-o', NULL),
(477, 'fa', 'fa-stack-exchange', NULL),
(478, 'fa', 'fa-stack-overflow', NULL),
(479, 'fa', 'fa-star', NULL),
(480, 'fa', 'fa-star-half', NULL),
(481, 'fa', 'fa-star-half-empty(alias)', NULL),
(482, 'fa', 'fa-star-half-full(alias)', NULL),
(483, 'fa', 'fa-star-half-o', NULL),
(484, 'fa', 'fa-star-o', NULL),
(485, 'fa', 'fa-steam', NULL),
(486, 'fa', 'fa-steam-square', NULL),
(487, 'fa', 'fa-step-backward', NULL),
(488, 'fa', 'fa-step-forward', NULL),
(489, 'fa', 'fa-stethoscope', NULL),
(490, 'fa', 'fa-stop', NULL),
(491, 'fa', 'fa-street-view', NULL),
(492, 'fa', 'fa-strikethrough', NULL),
(493, 'fa', 'fa-stumbleupon', NULL),
(494, 'fa', 'fa-stumbleupon-circle', NULL),
(495, 'fa', 'fa-subscript', NULL),
(496, 'fa', 'fa-subway', NULL),
(497, 'fa', 'fa-suitcase', NULL),
(498, 'fa', 'fa-sun-o', NULL),
(499, 'fa', 'fa-superscript', NULL),
(500, 'fa', 'fa-support(alias)', NULL),
(501, 'fa', 'fa-table', NULL),
(502, 'fa', 'fa-tablet', NULL),
(503, 'fa', 'fa-tachometer', NULL),
(504, 'fa', 'fa-tag', NULL),
(505, 'fa', 'fa-tags', NULL),
(506, 'fa', 'fa-tasks', NULL),
(507, 'fa', 'fa-taxi', NULL),
(508, 'fa', 'fa-tencent-weibo', NULL),
(509, 'fa', 'fa-terminal', NULL),
(510, 'fa', 'fa-text-height', NULL),
(511, 'fa', 'fa-text-width', NULL),
(512, 'fa', 'fa-th', NULL),
(513, 'fa', 'fa-th-large', NULL),
(514, 'fa', 'fa-th-list', NULL),
(515, 'fa', 'fa-thumb-tack', NULL),
(516, 'fa', 'fa-thumbs-down', NULL),
(517, 'fa', 'fa-thumbs-o-down', NULL),
(518, 'fa', 'fa-thumbs-o-up', NULL),
(519, 'fa', 'fa-thumbs-up', NULL),
(520, 'fa', 'fa-ticket', NULL),
(521, 'fa', 'fa-times', NULL),
(522, 'fa', 'fa-times-circle', NULL),
(523, 'fa', 'fa-times-circle-o', NULL),
(524, 'fa', 'fa-tint', NULL),
(525, 'fa', 'fa-toggle-down(alias)', NULL),
(526, 'fa', 'fa-toggle-left(alias)', NULL),
(527, 'fa', 'fa-toggle-off', NULL),
(528, 'fa', 'fa-toggle-on', NULL),
(529, 'fa', 'fa-toggle-right(alias)', NULL),
(530, 'fa', 'fa-toggle-up(alias)', NULL),
(531, 'fa', 'fa-train', NULL),
(532, 'fa', 'fa-transgender', NULL),
(533, 'fa', 'fa-transgender-alt', NULL),
(534, 'fa', 'fa-trash', NULL),
(535, 'fa', 'fa-trash-o', NULL),
(536, 'fa', 'fa-tree', NULL),
(537, 'fa', 'fa-trello', NULL),
(538, 'fa', 'fa-trophy', NULL),
(539, 'fa', 'fa-truck', NULL),
(540, 'fa', 'fa-try', NULL),
(541, 'fa', 'fa-tty', NULL),
(542, 'fa', 'fa-tumblr', NULL),
(543, 'fa', 'fa-tumblr-square', NULL),
(544, 'fa', 'fa-turkish-lira(alias)', NULL),
(545, 'fa', 'fa-twitch', NULL),
(546, 'fa', 'fa-twitter', NULL),
(547, 'fa', 'fa-twitter-square', NULL),
(548, 'fa', 'fa-umbrella', NULL),
(549, 'fa', 'fa-underline', NULL),
(550, 'fa', 'fa-undo', NULL),
(551, 'fa', 'fa-university', NULL),
(552, 'fa', 'fa-unlink(alias)', NULL),
(553, 'fa', 'fa-unlock', NULL),
(554, 'fa', 'fa-unlock-alt', NULL),
(555, 'fa', 'fa-unsorted(alias)', NULL),
(556, 'fa', 'fa-upload', NULL),
(557, 'fa', 'fa-usd', NULL),
(558, 'fa', 'fa-user', NULL),
(559, 'fa', 'fa-user-md', NULL),
(560, 'fa', 'fa-user-plus', NULL),
(561, 'fa', 'fa-user-secret', NULL),
(562, 'fa', 'fa-user-times', NULL),
(563, 'fa', 'fa-users', NULL),
(564, 'fa', 'fa-venus', NULL),
(565, 'fa', 'fa-venus-double', NULL),
(566, 'fa', 'fa-venus-mars', NULL),
(567, 'fa', 'fa-viacoin', NULL),
(568, 'fa', 'fa-video-camera', NULL),
(569, 'fa', 'fa-vimeo-square', NULL),
(570, 'fa', 'fa-vine', NULL),
(571, 'fa', 'fa-vk', NULL),
(572, 'fa', 'fa-volume-down', NULL),
(573, 'fa', 'fa-volume-off', NULL),
(574, 'fa', 'fa-volume-up', NULL),
(575, 'fa', 'fa-warning(alias)', NULL),
(576, 'fa', 'fa-wechat(alias)', NULL),
(577, 'fa', 'fa-weibo', NULL),
(578, 'fa', 'fa-weixin', NULL),
(579, 'fa', 'fa-whatsapp', NULL),
(580, 'fa', 'fa-wheelchair', NULL),
(581, 'fa', 'fa-wifi', NULL),
(582, 'fa', 'fa-windows', NULL),
(583, 'fa', 'fa-won(alias)', NULL),
(584, 'fa', 'fa-wordpress', NULL),
(585, 'fa', 'fa-wrench', NULL),
(586, 'fa', 'fa-xing', NULL),
(587, 'fa', 'fa-xing-square', NULL),
(588, 'fa', 'fa-yahoo', NULL),
(589, 'fa', 'fa-yelp', NULL),
(590, 'fa', 'fa-yen(alias)', NULL),
(591, 'fa', 'fa-youtube', NULL),
(592, 'fa', 'fa-youtube-play', NULL),
(593, 'fa', 'fa-youtube-square', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lyrics`
--

CREATE TABLE `lyrics` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lyrics`
--

INSERT INTO `lyrics` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Man Pathanawa (Samu Aran Ya Yuthui)', '<p>man pathanawa test</p>', '/core/storage/uploads/images/lyrics/lyrics-20180513105417.jpg', '2018-05-13 17:54:17', '2018-05-13 17:54:17'),
(2, 'Dahata Nopeni Inna Lyrics', '<p>Song | Dahata Nopeni Inna<br />Artist | Prasanna Thakshila<br />Music | Rusiru Rupasinghe<br />Lyrics | Yasith Dulshan<br />Directed By | Ravindu Abeyrathna<br />Video | Golden Film House</p>', '/core/storage/uploads/images/lyrics/lyrics-20180601165135.jpg', '2018-06-01 23:51:35', '2018-06-30 00:12:47'),
(3, 'Oyata Vitharak Lyrics', '<p><strong>Oyata Vitharak - Ishan Priyasanka</strong></p>', '/core/storage/uploads/images/lyrics/lyrics-20180602145824.jpg', '2018-06-02 21:58:25', '2018-06-02 21:58:25'),
(4, 'Mathaka Mawee Lyrics', '<p><strong>Song Title: Mathaka Mewee</strong><br /><strong>Artist: Shenu Kalpa</strong><br /><strong>Music: Hashan Thilina</strong><br /><strong>Lyrics: Prasanna Kumara Dammalage</strong></p>', '/core/storage/uploads/images/lyrics/lyrics-20180606120048.jpg', '2018-06-06 19:00:48', '2018-06-30 00:12:30'),
(5, 'Pem Karala Lyrics', '<p>Pem Karala - Udesh Nilanga<br />Music &amp; Melody | DilShan L Silva<br />Lyrics | Lasitha Jayaneththi Arachchige</p>', '/core/storage/uploads/images/lyrics/lyrics-20180606214438.jpg', '2018-06-07 04:44:39', '2018-06-30 00:12:16'),
(6, 'Obe Adare Lyrics', '<p><em>Song Title: Obe Adare</em><br /><em>Artist: Sameera Chathuranga</em><br /><em>Music: Amila Muthugala</em><br /><em>Lyrics: Supun Ravishan</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180606214627.jpg', '2018-06-07 04:46:28', '2018-06-30 00:11:57'),
(7, 'Wirasakawee Lyrics', '<p>Song Title | Wirasakawee<br />Artist | Buddika Krishan<br />Music | Hashan Thilina @ H AudioLab<br />Lyrics | Prasanna Kumara Dammalage<br />Mixed &amp; Masterd | Hashan Thilina @ H Audiolab</p>', '/core/storage/uploads/images/lyrics/lyrics-20180607210504.jpg', '2018-06-08 04:05:04', '2018-06-30 00:11:38'),
(8, 'Sammatheta Pitu Pe Lyrics', '<p><em>Songs - Sammatheta Pitupe</em><br /><em>Artist - Jude Rogans</em><br /><em>Music - Chandana Walpola</em><br /><em>Lyrics - Reru</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180619194138.jpg', '2018-06-20 02:41:38', '2018-06-30 00:11:14'),
(9, 'Seya (Nabara Wu) Lyrics', '<p><em>Artist : Roony (Indika Ruwan)</em><br /><em>Lyrics : Roshan Samarawickrama</em><br /><em>Music : Nimesh Kulasinghe</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180629170959.jpg', '2018-06-30 00:09:59', '2018-06-30 00:09:59'),
(10, 'Unmada Rathriye Lyrics', '<p><em>Artist : Nimash Fernando</em><br /><em>Lyrics : Nisal Fernando</em><br /><em>Music : Ashan Fernando</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180630153159.jpg', '2018-06-30 22:31:59', '2018-06-30 22:36:37'),
(11, 'Parana Hithuwakkari Lyrics', '<p><em>Artist : Manoj Dewarajage</em><br /><em>Music : Prageeth Perera</em><br /><em>Lyrics : Manoj Dewarajage</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180630164224.jpg', '2018-06-30 23:42:24', '2018-06-30 23:42:24'),
(12, 'Sudu Manika Lyrics', '<p><em>Song : Sudu Manika</em><br /><em>Artist By : Nalinda Ranasinghe</em><br /><em>Lyrics By : Lasitha Jayaneththi Arachchige</em><br /><em>Music By : DilShan L Sillva</em></p>', '/core/storage/uploads/images/lyrics/lyrics-20180706162855.jpg', '2018-07-06 23:28:56', '2018-07-06 23:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `lft` int(11) NOT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `label`, `link`, `icon`, `parent`, `permissions`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Root Menu', '#', NULL, NULL, NULL, 1, 40, 0, 1, '2016-09-20 18:30:00', '2018-07-15 17:36:45'),
(2, 'MENU MANAGEMENT', 'menu/list', NULL, 1, '[\"menu.add\",\"admin\"]', 2, 3, 1, 1, '2016-09-20 18:30:00', '2017-12-24 11:09:01'),
(4, 'USER MANAGEMENT', '#', NULL, 1, '[\"user.add\",\"user.edit\",\"user.delete\",\"user.list\",\"admin\"]', 4, 13, 1, 1, '2016-09-21 05:26:25', '2018-04-21 03:49:10'),
(5, 'PERMISSION', 'permission/list', NULL, 4, '[\"admin\"]', 5, 6, 2, 1, '2016-09-21 05:26:51', '2018-04-21 03:49:10'),
(6, 'ROLE', 'user/role/list', NULL, 4, '[\"admin\"]', 7, 8, 2, 1, '2016-09-21 05:27:15', '2018-04-21 03:49:10'),
(7, 'USER', 'user/list', NULL, 4, '[\"user.list\",\"user.add\",\"user.edit\",\"user.delete\",\"admin\"]', 9, 10, 2, 1, '2016-09-21 05:27:51', '2018-04-21 03:49:10'),
(43, 'publication', 'publication/list', NULL, 1, '[\"publication.list\",\"publication.add\",\"publication.edt\",\"publication.delete\",\"admin\"]', 40, 41, 1, 1, NULL, '2018-07-15 17:36:45'),
(52, 'Coupon Management', 'coupon/list', NULL, 1, '[\"admin\"]', 40, 41, 1, 1, '2017-10-17 16:59:22', '2018-07-15 17:36:45'),
(57, 'Artists', 'admin/artist/list', NULL, 1, '[\"admin\"]', 20, 21, 1, 1, '2018-04-21 03:44:31', '2018-04-21 04:07:52'),
(58, 'Audios', 'admin/audio/list', NULL, 1, '[\"admin\"]', 16, 17, 1, 1, '2018-04-21 03:45:31', '2018-04-21 04:07:19'),
(59, 'Contact Us', 'admin/contact-us/list', NULL, 1, '[\"admin\"]', 36, 37, 1, 1, '2018-04-21 03:46:28', '2018-07-15 17:36:45'),
(60, 'Dynamic Pages', 'admin/dynamic-pages/list', NULL, 1, '[\"admin\"]', 32, 33, 1, 1, '2018-04-21 03:48:39', '2018-07-15 17:36:45'),
(61, 'Lyrics', 'admin/lyrics/list', NULL, 1, '[\"admin\"]', 22, 23, 1, 1, '2018-04-21 04:06:31', '2018-04-21 04:07:52'),
(62, 'Songs', 'admin/song/list', NULL, 1, '[\"admin\"]', 14, 15, 1, 1, '2018-04-21 04:07:18', '2018-04-21 04:07:19'),
(63, 'Videos', 'admin/video/list', NULL, 1, '[\"admin\"]', 18, 19, 1, 1, '2018-04-21 04:07:52', '2018-04-21 04:07:52'),
(64, 'Image slider', 'admin/slider/list', NULL, 1, '[\"admin\"]', 24, 25, 1, 1, '2018-06-15 20:18:29', '2018-06-15 20:18:29'),
(65, 'Banner Ads', '#', NULL, 1, '[\"admin\"]', 26, 31, 1, 1, '2018-07-15 17:33:45', '2018-07-15 17:36:45'),
(66, 'Ads details', 'admin/ads/ads', NULL, 65, '[\"admin\"]', 27, 28, 2, 1, '2018-07-15 17:34:26', '2018-07-15 17:35:08'),
(67, 'Ads Locations', 'admin/ads/locations', NULL, 65, '[\"admin\"]', 29, 30, 2, 1, '2018-07-15 17:36:45', '2018-07-15 17:36:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2018_02_25_122523_change_galley_image_table', 1),
('2018_02_25_142445_change_blog_table', 2),
('2018_02_28_213250_create_destination_images_table', 2),
('2018_03_01_114809_create_destination_table', 2),
('2018_03_01_141028_create_hotel_table', 3),
('2018_03_01_141256_create_hotel_images_table', 3),
('2018_03_03_095133_create_package_table', 4),
('2018_03_03_095849_create_hotel_package_table', 5),
('2018_03_03_100347_create_destination_package_table', 6),
('2018_03_03_132245_add_offer_column_to_package_table', 7),
('2018_03_03_145154_add_duration_column_to_package_table', 8),
('2018_03_04_220824_create_table_reviews', 9),
('2018_03_13_215110_add_contact_no_to_reviews_table', 10),
('2018_03_13_221501_create_feedbacks_table', 11),
('2018_03_13_221841_add_contcat_no_to_feedback_table', 12),
('2018_04_03_201900_create_lyrics_table', 13),
('2018_04_04_212315_create_table_artists', 14),
('2018_04_08_215209_create_audio_table', 15),
('2018_04_08_225237_create_video_table', 16),
('2018_04_08_233441_create_song_table', 17),
('2018_04_08_235040_create_table_artist_song', 18),
('2018_04_08_124701_create_contact_us', 19),
('2018_04_08_130439_create_dynamic_pages_table', 19),
('2018_04_25_202527_add_artist_imge', 19),
('2018_05_01_211410_create_song_lyrics_artist_table', 20),
('2018_05_02_045001_create_advertisement_locations_table', 21),
('2018_05_02_045224_create_advertisements_table', 22),
('2018_05_02_211107_create_song_music_artist_table', 23),
('2018_05_05_133028_add_old_column_to_song_table', 23),
('2018_05_05_135013_add_bio_to_artists_table', 24),
('2018_05_05_142237_add_image_to_videos_table', 25),
('2018_05_07_125024_add_user_id_column_to_table', 26),
('2018_05_07_143116_add_meta_to_song_table', 27);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'user', 'Normal Registered User', 1, 1, '2015-07-25 07:00:00', '2015-12-02 11:54:39'),
(2, 'menu.add', NULL, 1, 1, '2015-07-25 07:00:00', '2015-12-03 15:02:41'),
(3, 'menu.list', NULL, 1, 1, '2015-07-25 07:00:00', '2015-12-02 11:54:54'),
(4, 'menu.edit', NULL, 1, 1, '2015-07-25 07:00:00', '2015-12-02 11:54:57'),
(5, 'menu.status', NULL, 1, 1, '2015-07-25 07:00:00', '2015-12-02 11:55:01'),
(6, 'admin', 'Super Admin Permission', 1, 1, '2015-07-25 07:00:00', '2015-07-25 07:00:00'),
(7, 'index', 'Home Page Permission', 1, 1, '2015-07-25 07:00:00', '2015-12-02 11:55:03'),
(8, 'menu.delete', NULL, 1, 1, '2015-09-06 21:30:06', '2015-09-06 21:30:09'),
(9, 'user.add', NULL, 1, 1, '2015-10-16 07:00:00', '2015-10-16 07:00:00'),
(10, 'user.edit', NULL, 1, 1, '2015-10-16 07:00:00', '2015-10-16 07:00:00'),
(11, 'user.delete', NULL, 1, 1, '2015-10-16 07:00:00', '2015-10-16 07:00:00'),
(12, 'user.list', NULL, 1, 1, '2015-10-20 07:00:00', '2015-10-21 03:01:57'),
(13, 'user.role.add', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(14, 'user.role.edit', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(15, 'user.role.list', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(16, 'user.role.delete', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(17, 'permission.add', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(18, 'permission.edit', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(19, 'permission.delete', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(20, 'permission.list', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(21, 'permission.group.add', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(22, 'permission.group.edit', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(23, 'permission.group.list', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(24, 'permission.group.delete', NULL, 1, 1, '2015-10-22 07:00:00', '2015-10-22 07:00:00'),
(25, 'user.status', NULL, 1, 1, '2015-12-19 07:00:47', '2015-12-19 07:00:47'),
(26, 'device.list', 'DEVICE LIST', 1, 1, '2017-01-24 01:44:34', '2017-03-13 05:53:58'),
(27, 'dashboard', 'DASHBOARD', 1, 1, '2017-01-24 01:48:39', '2017-01-24 01:48:39'),
(28, 'device.add', 'DEVICE ADD', 1, 1, '2017-01-24 01:49:46', '2017-01-24 01:49:46'),
(29, 'device.edit', 'DEVICE EDIT', 1, 1, '2017-01-24 01:50:02', '2017-01-24 01:50:02'),
(30, 'device.delete', 'DEVICE DELETE', 1, 1, '2017-01-24 01:50:24', '2017-01-24 01:50:24'),
(31, 'gallery.list', 'GALLERY LIST', 1, 1, '2017-01-24 01:51:05', '2017-01-24 01:51:05'),
(32, 'gallery.add', 'Gallery ADD', 1, 1, '2017-01-24 01:51:23', '2017-01-24 01:51:23'),
(33, 'gallery.edit', 'GALLERY EDIT', 1, 1, '2017-01-24 01:51:36', '2017-01-24 01:51:36'),
(34, 'gallery.delete', 'GALLERY DELETE', 1, 1, '2017-01-24 01:51:53', '2017-01-24 01:51:53'),
(35, 'reservation.list', 'RESERVATION LIST', 1, 1, '2017-01-28 18:04:27', '2017-01-28 18:04:27'),
(36, 'product.list', 'PRODUCT LIST', 1, 1, '2017-02-20 21:37:26', '2017-02-20 21:37:26'),
(37, 'product.add', 'PRODUCT ADD', 1, 1, '2017-02-20 21:37:49', '2017-02-20 21:37:49'),
(38, 'product.edit', 'PRODUCT EDIT', 1, 1, '2017-02-20 21:38:08', '2017-02-20 21:38:08'),
(39, 'product.delete', 'PRODUCT DELETE', 1, 1, '2017-03-15 03:54:22', '2017-03-15 03:54:22'),
(40, 'series.list', 'SERIES LIST', 1, 1, '2017-03-15 04:01:49', '2017-03-15 04:01:49'),
(41, 'series.add', 'SERIES ADD', 1, 1, '2017-03-15 04:02:03', '2017-03-15 04:02:03'),
(42, 'series.edit', 'SERIES EDIT', 1, 1, '2017-03-15 04:02:17', '2017-03-15 04:02:17'),
(43, 'series.delete', 'SERIES DELETE', 1, 1, '2017-03-15 04:02:33', '2017-03-15 04:02:33'),
(44, 'feature.add', 'ADD FEATURE', 1, 1, '2017-03-26 16:42:51', '2017-03-26 16:42:51'),
(45, 'feature.edit', 'EDIT FEATURE', 1, 1, '2017-03-26 16:43:08', '2017-03-26 16:43:08'),
(46, 'feature.list', 'LIST FEATURE', 1, 1, '2017-03-26 16:43:23', '2017-03-26 16:43:23'),
(47, 'feature.delete', 'DELETE FEATURE', 1, 1, '2017-03-26 16:43:41', '2017-03-26 16:43:41'),
(48, 'blog', 'BLOG', 1, 1, '2017-04-04 06:02:46', '2017-04-04 06:02:46'),
(49, 'blog.add', 'ADD BLOG', 1, 1, '2017-05-09 04:47:46', '2017-05-09 04:47:46'),
(50, 'blog.edit', 'EDIT BLOG', 1, 1, '2017-05-09 04:48:01', '2017-05-09 04:48:01'),
(51, 'blog.list', 'LIST BLOG', 1, 1, '2017-05-09 04:48:13', '2017-05-09 04:48:13'),
(52, 'blog.delete', 'DELETE BLOG', 1, 1, '2017-05-09 04:48:28', '2017-05-09 04:48:28'),
(53, 'destination.list', '', 1, 1, '2018-02-25 23:16:36', '2018-02-25 23:16:36'),
(54, 'destination.add', '', 1, 1, '2018-02-25 23:16:43', '2018-02-25 23:16:43'),
(55, 'destination.edit', '', 1, 1, '2018-02-25 23:17:04', '2018-02-25 23:17:04'),
(56, 'destination.delete', '', 1, 1, '2018-02-25 23:17:12', '2018-02-25 23:17:12'),
(57, 'hotel.add', '', 1, 1, '2018-03-01 21:45:53', '2018-03-01 21:45:53'),
(58, 'hotel.edit', '', 1, 1, '2018-03-01 21:46:01', '2018-03-01 21:46:01'),
(59, 'hotel.list', '', 1, 1, '2018-03-01 21:46:50', '2018-03-01 21:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, '0q0ygX6vhRbhWLfDVMQjCpuVVi4Uv21m', '2015-07-11 18:39:55', '2015-07-11 18:39:55'),
(2, 1, 'efBjwoN42yjE5Pbbbn3NOvAQMh6Hc47p', '2015-07-13 17:12:45', '2015-07-13 17:12:45'),
(4, 1, 'dkYUwD816i7YeZLaLmENn7b7qXyRV6jE', '2015-07-13 22:13:07', '2015-07-13 22:13:07'),
(5, 1, 'fsVkzYpy5e5SIno5317Viix318Ipevum', '2015-07-14 21:42:12', '2015-07-14 21:42:12'),
(7, 1, 'BYdFxgUBhE9H2BqP6PEg5tQXjQvapGkk', '2015-07-15 01:53:52', '2015-07-15 01:53:52'),
(8, 1, 'UuheCz7Zmb1WM2zsJVnJv3yMtHrCZXZP', '2015-07-19 02:35:42', '2015-07-19 02:35:42'),
(9, 1, 'ugwMinnLII8pAvdX48uibN4tCfFncKxL', '2015-07-25 19:44:39', '2015-07-25 19:44:39'),
(10, 1, '6sv5Vmes7x5zzF5kp9BIiF0e3J7uatEA', '2015-07-26 04:32:42', '2015-07-26 04:32:42'),
(12, 1, 'AAPH0gv7ueGmg1GHT3CDDb8CuqliETTr', '2015-07-27 16:00:05', '2015-07-27 16:00:05'),
(17, 1, 'EvOMH9hGBkW3nS9TJjtnZOajBh8b2nH8', '2015-08-14 03:59:00', '2015-08-14 03:59:00'),
(18, 1, 'sgY1xcvVRC3q8s2Qe5IgY6k4NVdw8Bw2', '2015-08-14 04:00:50', '2015-08-14 04:00:50'),
(19, 1, 'smRBCmpzbBL4RTXJZcaAW5Wr4quWzvnA', '2015-08-14 17:08:36', '2015-08-14 17:08:36'),
(20, 1, 'C1xwzvvNREHhtx6JqQEZSTEPMbzY7J6F', '2015-08-14 17:13:16', '2015-08-14 17:13:16'),
(23, 1, '0XUcBHFDSxUgWDZsdPt9Oigx5cIfQ5KR', '2015-08-14 22:49:04', '2015-08-14 22:49:04'),
(24, 1, 'LeqsntX9HwB19oOzfqFICR3TB2xHZt5c', '2015-08-15 04:16:49', '2015-08-15 04:16:49'),
(25, 1, 'ZRilkZu1axOqoPqkAbNzA4ZPQccDHLEf', '2015-08-15 21:19:49', '2015-08-15 21:19:49'),
(26, 1, 'fHHSkcipQkFfpeTYIcxmFqKS2IKL0ozJ', '2015-08-17 16:41:53', '2015-08-17 16:41:53'),
(27, 1, 'Gev3KH0ERZX7UFA6QASlHd4wW9Tm47cz', '2015-08-17 20:22:08', '2015-08-17 20:22:08'),
(28, 1, 'Pu0bSNJwO7XEhfmr8Ubg28c5HWp2BCfN', '2015-08-18 16:01:21', '2015-08-18 16:01:21'),
(29, 1, 'FkavnjEGjcidC4hGsJwf5JepGpglq09u', '2015-08-18 18:03:49', '2015-08-18 18:03:49'),
(30, 1, 'GfVyVFbRB7yBaekFtBu3l16pB95iDYyv', '2015-08-19 01:11:28', '2015-08-19 01:11:28'),
(32, 1, '7Qa9ckHVYsRkTgnZNSQ9szqJ7ORRwqqP', '2015-08-20 02:43:25', '2015-08-20 02:43:25'),
(34, 1, '7gzkQXDRziKCU3V6YN3os6oty3ZYmm3V', '2015-08-20 17:40:32', '2015-08-20 17:40:32'),
(35, 1, 'PPni5OZ4t4Ukf7GqJhJTowQUbG1BKTCO', '2015-08-25 00:02:19', '2015-08-25 00:02:19'),
(39, 1, '1Vq5gtTDFk3uhKn9AHMOnWwQxiNq8eNm', '2015-08-25 03:32:07', '2015-08-25 03:32:07'),
(40, 1, '9tal1AP1HQ4usfTnI3Q0kXSrPzScKtnz', '2015-08-25 19:56:55', '2015-08-25 19:56:55'),
(41, 1, 'FDkLOdsEvF0HzB1XvBQ8GKAQZ9oq80xS', '2015-08-26 01:50:00', '2015-08-26 01:50:00'),
(42, 1, 'LW0maecSnWGHR1uvR1ev67CmiePNeYQb', '2015-08-26 15:43:11', '2015-08-26 15:43:11'),
(43, 1, 'c7UCeZhd2AdNOrNIrzFeP8Z2xHSPBOat', '2015-08-26 20:21:39', '2015-08-26 20:21:39'),
(44, 1, 'DmiuJhBD8ndKrxeQzYUgKJ0qdr5VfgEp', '2015-08-26 20:23:05', '2015-08-26 20:23:05'),
(45, 1, 'EvLSeVbZ4LpQHpIwn3ESkPmKiHZYqChR', '2015-08-27 02:43:08', '2015-08-27 02:43:08'),
(46, 1, 'ZaFHszTHVJxdXVHyYyDUiVrjDsVLF2os', '2015-08-27 15:50:53', '2015-08-27 15:50:53'),
(47, 1, 'E26Ajf3IuoSAQBDRGUnJxBXuLWf45qTE', '2015-08-28 18:00:55', '2015-08-28 18:00:55'),
(48, 1, '6gvIBqAaNqIHUC1GXGnvpobgXDqyGFWV', '2015-08-28 21:22:35', '2015-08-28 21:22:35'),
(49, 1, 'C9iOcAwSX8vcFZVUkRrnb7BTEmFfb1lW', '2015-08-28 23:54:30', '2015-08-28 23:54:30'),
(50, 1, 'm8HlkBppqVOmVtvm8WXn1gflqnpxZ7ZX', '2015-08-29 04:13:47', '2015-08-29 04:13:47'),
(51, 1, 'Ga2L52MViQQHOjQmC0B3NIwKoFoeC5LU', '2015-08-29 19:26:46', '2015-08-29 19:26:46'),
(52, 1, 'pEnesSMPpp8XwoXBGc0AQIxG8a0NlJL7', '2015-08-29 22:41:51', '2015-08-29 22:41:51'),
(53, 1, 'DOyCW9LubRxZlUZI1VM9VWmwEHFqEtuy', '2015-08-30 20:45:08', '2015-08-30 20:45:08'),
(54, 1, 'hQnXdF66aT6GWv1bjMfdQlrFzXCKzPaD', '2015-09-02 01:13:49', '2015-09-02 01:13:49'),
(55, 1, 'SofbScd28GADuBXJkBe02t4wNesj1pyc', '2015-09-02 16:16:21', '2015-09-02 16:16:21'),
(58, 1, 'S9zBLvLOtfn86jn2YZ7o4cDDIXir5odO', '2015-09-06 23:33:49', '2015-09-06 23:33:49'),
(59, 1, 'jhnFLwdIIq3akmuFjwNT5hXRbskFN8Cv', '2015-09-07 16:00:41', '2015-09-07 16:00:41'),
(65, 1, 'Yzp3Ic2rJbT21gHfSHQAyNH9sySMzNx8', '2015-09-17 19:38:02', '2015-09-17 19:38:02'),
(66, 1, 'FPxtt7aeMIpDuoEEIrFeXfS289y6YyrI', '2015-09-17 19:40:28', '2015-09-17 19:40:28'),
(67, 1, 'DE0xeUB6zv2l9VC7hlbHeVKZdNVgba2k', '2015-09-17 19:42:20', '2015-09-17 19:42:20'),
(68, 1, 'VSigkljclaPzU2jbUTnEPb7uAqbq7OO4', '2015-09-17 19:46:25', '2015-09-17 19:46:25'),
(69, 1, 'u1u64fxr5E8cVetPE2xfwXXlZg8ETdMt', '2015-09-17 19:48:29', '2015-09-17 19:48:29'),
(70, 1, 'IsPKSyDwN4yyBDpHDCQTFIA9cb9tDvho', '2015-09-17 19:50:56', '2015-09-17 19:50:56'),
(71, 1, 'Mqwwg7Ji564GosYUs8ew6p56lVqTNggm', '2015-09-17 19:51:06', '2015-09-17 19:51:06'),
(72, 1, 'ReDZfYlXcI3vckn50feKIYFukwEIaJxW', '2015-09-17 19:55:47', '2015-09-17 19:55:47'),
(73, 1, '1zPiFaCd2KentN0BEVayuoIqOfxi3lMS', '2015-09-17 19:56:15', '2015-09-17 19:56:15'),
(75, 1, 'FiS9P7LCFRk2i3wnNHZJqODlZaBMPBsH', '2015-09-17 19:57:16', '2015-09-17 19:57:16'),
(77, 1, 'w6afABnpTAwYkETiZ1LpLEG31RUMR9J9', '2015-09-17 20:14:14', '2015-09-17 20:14:14'),
(80, 1, 'zEQJ08oNoKnzyenYvSBPlGNkYrQRb9iU', '2015-09-17 22:08:48', '2015-09-17 22:08:48'),
(81, 1, 'ngBYUNkxL2QkLY5Lj7EHCsAY8VRUPIss', '2015-09-17 23:07:24', '2015-09-17 23:07:24'),
(82, 1, 'qPnddeucFgMvXEyCDsm99LGWgtiKQJI0', '2015-09-17 23:37:58', '2015-09-17 23:37:58'),
(83, 1, 'y8KppplOMITXfiYtuU10r96MmBw2fn4Z', '2015-09-17 23:54:17', '2015-09-17 23:54:17'),
(87, 1, '1O0lJH2OdlZ5JYPb7OzU95hN9DD9D08f', '2015-09-18 18:17:44', '2015-09-18 18:17:44'),
(91, 1, 'F2EASmRV02xRfOorilcer2HPJR0ff10w', '2015-09-21 15:53:43', '2015-09-21 15:53:43'),
(93, 1, 'XtZHW8aWJtnVDHLcFpJjfzuFQCI4BarH', '2015-09-21 16:01:03', '2015-09-21 16:01:03'),
(94, 1, '4MvhjQuFQMa3d3kTpaMJIhrHIqrpd0Ud', '2015-09-21 16:04:16', '2015-09-21 16:04:16'),
(95, 1, 'wIeSvojnWxVdqc8Ko2g4wSlgadyT46HX', '2015-09-22 01:46:41', '2015-09-22 01:46:41'),
(96, 1, 'IfNOgaDeoIHRgr1wMMQvzGO2zyedyHzQ', '2015-10-16 16:55:25', '2015-10-16 16:55:25'),
(97, 1, 'xb49Au965edKpfxust8QyV2A1ZtN1Jir', '2015-10-16 20:29:38', '2015-10-16 20:29:38'),
(98, 1, '8KKqFXos72faML2g6w1gZK8GecSR2A7B', '2015-10-16 20:55:28', '2015-10-16 20:55:28'),
(99, 1, 'IrxeYFpjWjhZdazrZz7tp253etqZlkuh', '2015-10-16 23:09:25', '2015-10-16 23:09:25'),
(100, 1, 'iuuX2Q6nvfyKfiNLDjOHJ0Jnmd3ZHLEM', '2015-10-17 18:58:06', '2015-10-17 18:58:06'),
(102, 1, 'vQW13aw7kJoovALydds8W9175IqZq7sh', '2015-10-20 21:15:41', '2015-10-20 21:15:41'),
(103, 1, 'kznRgyUivNAxj1LolOUloaV52kxc1TOg', '2015-10-21 02:49:12', '2015-10-21 02:49:12'),
(104, 1, 'UMqlxsaazj72vwRmCVfv5qB5oujn5eO1', '2015-10-21 02:50:38', '2015-10-21 02:50:38'),
(105, 1, 'xpjJT8wJEAA4xe0WNP9UbGPlEbwgZi7L', '2015-10-21 15:58:35', '2015-10-21 15:58:35'),
(106, 1, 'PMf4A3yNtiDOJzYEA2Pi58STBrJpYzi9', '2015-10-21 16:06:54', '2015-10-21 16:06:54'),
(107, 1, 'ix4iaApoo9ohGbwkBNcYyHv3nSkYMuOJ', '2015-10-22 03:21:23', '2015-10-22 03:21:23'),
(108, 1, 'klm8QxxO7mIG6Ljj5liFWR1qikUv3y1H', '2015-10-22 15:17:41', '2015-10-22 15:17:41'),
(109, 1, 'TMTs9AXiVGg3Dn8AmaXt5EZS4Z4WeVgp', '2015-10-26 16:18:24', '2015-10-26 16:18:24'),
(110, 1, 'gihggqvhu0ngRD3ZMkeBhMLMNW6uKXHJ', '2015-10-28 17:09:40', '2015-10-28 17:09:40'),
(111, 1, 'KDNzGA42AWdXV63AG90IhJF6Q36pmtpE', '2015-10-29 02:08:36', '2015-10-29 02:08:36'),
(112, 1, '35yTDDYO1IYhs9ujWXGWfAS9cxXIs4Hj', '2015-11-04 15:43:06', '2015-11-04 15:43:06'),
(113, 1, 'FE2s4HBsv9RoBO7oUsUT7KxFTaXqdpUs', '2015-11-06 21:36:44', '2015-11-06 21:36:44'),
(115, 1, 'a35iqeHc7BvGEbgIx9t357tFXklT9bqO', '2015-11-06 23:08:06', '2015-11-06 23:08:06'),
(116, 1, 'eE6SkzCzg1FtCi2E24UNmFm7OBVLWORs', '2015-11-07 23:32:42', '2015-11-07 23:32:42'),
(117, 1, 't71JCTd8yKBblCePz7bWznuSpkU2XqDv', '2015-11-07 23:57:44', '2015-11-07 23:57:44'),
(119, 1, 'ECMvGKmUgYm2qtMN0OMrPqpaDn5jx08d', '2015-11-19 06:03:17', '2015-11-19 06:03:17'),
(120, 1, 'PtyWimBS6rsnKlRxj96BDq9L7SDKG2rk', '2015-11-19 06:43:11', '2015-11-19 06:43:11'),
(121, 1, 'l9FIuW4AIkJij7BhzeiSEfzU4VyTWHMv', '2015-11-19 07:26:41', '2015-11-19 07:26:41'),
(122, 1, 'bFb85D3SNEMRAoWExKagngxrR9mpWG9a', '2015-11-19 09:56:32', '2015-11-19 09:56:32'),
(123, 1, 'SXL5MRTMSjhUxM2hHtJlZTUCbqgFF5dp', '2015-11-19 10:06:15', '2015-11-19 10:06:15'),
(124, 1, 'xe3efdBNBGu5Ftyzf6SBg1IpUjzMiODv', '2015-11-19 12:17:47', '2015-11-19 12:17:47'),
(125, 1, 'H0KI9TFvutyqksX9WFdo7ltXlIdi6LBO', '2015-11-19 19:01:04', '2015-11-19 19:01:04'),
(126, 1, 'b5U0MlNhXnSybtPBfB6OOmx2UD0jp3rK', '2015-11-20 05:56:33', '2015-11-20 05:56:33'),
(127, 1, 'aY5cfsCbK3SIOhyIfqn5swgMpi7DdOaH', '2015-11-20 06:48:16', '2015-11-20 06:48:16'),
(128, 1, 'LFMf9yFQYkV1iEc4MPSmy2cPyAlIBI7e', '2015-11-20 13:56:45', '2015-11-20 13:56:45'),
(129, 1, 'RzLW8COzh7iRkoCA0sY3VAejSmQbAFGS', '2015-11-21 05:43:07', '2015-11-21 05:43:07'),
(130, 1, 'QyjXCs2H1KLaiSveWaBenK05uG6Z0S8D', '2015-11-21 10:28:30', '2015-11-21 10:28:30'),
(131, 1, '9wrIGqqTTScbdXWxrxsu4mPGI30xTTJn', '2015-11-21 12:22:41', '2015-11-21 12:22:41'),
(132, 1, 'bBc9L5jexsFXL5iGZV8sIlgiyiEpvCo1', '2015-11-24 05:54:24', '2015-11-24 05:54:24'),
(134, 1, 'MVm2uIPKGoClnBOtPnZLeMne0cxqm79A', '2015-11-24 07:36:16', '2015-11-24 07:36:16'),
(135, 1, 'UdEhb1HUGGFeNUAHSzSEAd28LBtML6ti', '2015-11-24 13:24:58', '2015-11-24 13:24:58'),
(136, 1, 'CZGAahPMZICfT9C2pz8DKf3N9XLZpzwT', '2015-11-24 13:44:32', '2015-11-24 13:44:32'),
(138, 1, 'ZS8C30f5fheTscPSyKKcwnHFzwJi5YUg', '2015-11-25 05:17:44', '2015-11-25 05:17:44'),
(139, 1, 'hviLwIVE77jiFjpRBx6DmB29nDCTiEoa', '2015-11-25 06:21:06', '2015-11-25 06:21:06'),
(140, 1, 'k4YjDklmjCK3XOheyFj5tZRgkgQLcvPZ', '2015-11-25 07:14:04', '2015-11-25 07:14:04'),
(141, 1, 'Q0pgzUMaw1jbbjbJ76ZEoiPNHbvEaGhb', '2015-11-25 09:09:15', '2015-11-25 09:09:15'),
(142, 1, 'j1Ll3vyCfxk8WYrfGRWbLcFlmmB5gvuf', '2015-11-25 13:57:11', '2015-11-25 13:57:11'),
(143, 1, '36OdiItmKntFhhusCsOxRDRUmRLi3aIk', '2015-11-27 05:10:45', '2015-11-27 05:10:45'),
(144, 1, 'EY00OQByVxbkksjXJeB779sTw0PVBwQo', '2015-11-27 05:31:33', '2015-11-27 05:31:33'),
(145, 1, 'wFYXrcHs2VT8JxpVeu2nkYpEDIiyveCi', '2015-11-27 10:36:39', '2015-11-27 10:36:39'),
(146, 1, 'fnPGwFNPFaup09Pi8h3HRZkW7g2o89UB', '2015-11-27 11:10:49', '2015-11-27 11:10:49'),
(147, 1, 'EoByZX0seoP71TO3mh5YkAiMw41dQjfI', '2015-11-28 05:26:43', '2015-11-28 05:26:43'),
(148, 1, 'N4O3wiGd7DlOZPPsiXQnU3GaIUDVs0pk', '2015-11-28 05:35:49', '2015-11-28 05:35:49'),
(149, 1, 'wvrtqymHww86JtFR4ySTB7PRMVe6CYyZ', '2015-12-01 07:29:39', '2015-12-01 07:29:39'),
(150, 1, '0c1cjjzAmVVAFRwhzEBmL2fBR2a0cZ47', '2015-12-01 07:34:17', '2015-12-01 07:34:17'),
(151, 1, 'wXdvBHnlVA6VxrKWojj4MPaQ15WIDALo', '2015-12-01 08:44:11', '2015-12-01 08:44:11'),
(152, 1, '6KgSa5WB6Fcd1RhTQe4bpnc3NLRc4jKs', '2015-12-01 16:31:40', '2015-12-01 16:31:40'),
(153, 1, 'b9DvbCSyCVGeHps05XTN3kDLjG9i6UID', '2015-12-02 05:20:21', '2015-12-02 05:20:21'),
(154, 1, 'V1ebekY9wLdVXEXYaDXiDKizKIwV2YoZ', '2015-12-02 07:35:33', '2015-12-02 07:35:33'),
(155, 1, 'U1TDvsp3l7S50HKZJilwD8kolj3oKMZY', '2015-12-02 08:44:31', '2015-12-02 08:44:31'),
(156, 1, 'tWGlxczBI5ISqJsyb8i4t1GAGLVmCxdg', '2015-12-03 06:27:48', '2015-12-03 06:27:48'),
(157, 1, 'sX9s0vE8zk12ZJkKZOj53is0FOSzm3h5', '2015-12-03 07:06:14', '2015-12-03 07:06:14'),
(158, 1, 'YZ1T4vrGkxVOS91okt3lSiz7OFyibEWY', '2015-12-03 07:15:44', '2015-12-03 07:15:44'),
(160, 1, '0asRo8XZo9ictz6mNc941GyW81j7mqnY', '2015-12-03 13:42:18', '2015-12-03 13:42:18'),
(161, 1, 'FVal7sFDex7QqJibHPmQ3BqJOjMxY4zG', '2015-12-03 14:27:27', '2015-12-03 14:27:27'),
(162, 1, 'R82HDrmisEFi9HDBpF4Cb6Ffg8r9o3RI', '2015-12-03 15:02:32', '2015-12-03 15:02:32'),
(163, 1, 'bYRhi5W5Z5sShoOXorpyq9PTzruV4ISR', '2015-12-04 06:13:08', '2015-12-04 06:13:08'),
(164, 1, '6wFdYiPTSZW4ldZn62NgTYaTQrAyMRFW', '2015-12-04 08:10:42', '2015-12-04 08:10:42'),
(165, 1, '7Zpc8lp11v5CtWyMWGlonBNEHCsQCWhi', '2015-12-08 06:04:48', '2015-12-08 06:04:48'),
(166, 1, '9vbkG1yJuNrxJbPccH6inExHABpHtQMU', '2015-12-08 07:26:07', '2015-12-08 07:26:07'),
(167, 1, 'O61lttpy7l77lwEm2vQBpYftms5BbuWz', '2015-12-08 07:27:47', '2015-12-08 07:27:47'),
(168, 1, 'z1BKohUNT9iUIx0QDiNHknohKyQLxgwR', '2015-12-08 09:51:57', '2015-12-08 09:51:57'),
(169, 1, 'AutjPb5lGuWESM9dFfzsSpFyDH6vJifT', '2015-12-08 13:12:57', '2015-12-08 13:12:57'),
(170, 1, 'IjjDqcWuwLtABfWU4FeKbmfFnYVZeo81', '2015-12-09 08:21:17', '2015-12-09 08:21:17'),
(171, 1, 'MvJ9bEq3DNsBfz6eBPpbvweOEqZUZTZZ', '2015-12-09 10:59:56', '2015-12-09 10:59:56'),
(172, 1, 'YE0xTDpxGhMKoxuRYEqODQdhjsg0HBWN', '2015-12-09 13:44:15', '2015-12-09 13:44:15'),
(173, 1, 'qtPfV4NGQUHK8nPilb3zMSkPtOFa5fA7', '2015-12-09 13:48:06', '2015-12-09 13:48:06'),
(174, 1, 'bRiBCAqEMyoZMFGcVh0ppiCe7jJQtmL6', '2015-12-10 05:39:45', '2015-12-10 05:39:45'),
(175, 1, 'kvxZtGoLNIrPws0jng0hSky09B4w52xo', '2015-12-10 05:54:21', '2015-12-10 05:54:21'),
(177, 1, 'CYcO33TQxApc3BsrIxKhA1TG2XfjvjuE', '2015-12-10 16:31:48', '2015-12-10 16:31:48'),
(178, 1, 'ch00cMtPi9ZXy38Y8xsVKkeQT4Tdyz0F', '2015-12-11 05:32:28', '2015-12-11 05:32:28'),
(179, 1, 'ucujCYid3Ed1fWRzUc33bszW0Da5WpoR', '2015-12-11 06:22:35', '2015-12-11 06:22:35'),
(180, 1, 'NQRHc1Aw4NGYpvd8UJHdacDUcMoN6CWT', '2015-12-11 06:28:07', '2015-12-11 06:28:07'),
(181, 1, 'lNch1fxa3m3v7w7QaQKOxJSAq8MM1oWV', '2015-12-11 08:12:57', '2015-12-11 08:12:57'),
(182, 1, 'AhU6Q1goojDW2QHqZtWGrOFIJ5yvyN29', '2015-12-11 09:16:19', '2015-12-11 09:16:19'),
(183, 1, 'HlNikcVYXQjaM4RTiuBaXpUVHvz6BVRD', '2015-12-12 11:53:54', '2015-12-12 11:53:54'),
(184, 1, 'aNFY4tCtnikY3dkpYL5GCTNiU3uil6dK', '2015-12-15 08:16:12', '2015-12-15 08:16:12'),
(185, 1, 'uFP0pEF4i8hpkuXI3XbN5HyyL45IEg9d', '2015-12-15 08:23:42', '2015-12-15 08:23:42'),
(186, 1, 'vHVcNsAZ5cHqBBCBesd8a4O5bWERDwta', '2015-12-15 09:00:17', '2015-12-15 09:00:17'),
(187, 1, 'ChWOSdh7XwdgcCY811PGvll0OvNwuijO', '2015-12-15 09:00:39', '2015-12-15 09:00:39'),
(188, 1, 'HmekPBJ7ICRGCVPXebGjrXKaeqFxAsUz', '2015-12-15 16:10:11', '2015-12-15 16:10:11'),
(189, 1, '3Y7IXo5Fav5lMC18YJOBaw3LyGm3Zqti', '2015-12-16 05:48:39', '2015-12-16 05:48:39'),
(190, 1, 'VWfHAS0MK3w8urby0C1O5gBydkw5nRXt', '2015-12-16 06:37:52', '2015-12-16 06:37:52'),
(192, 1, 'x18zuJwn8SteKHNbt4URG9Uq1XR0f2Pi', '2015-12-17 05:15:37', '2015-12-17 05:15:37'),
(193, 1, 'mf0llY1RIuyVIBH7UZnwayL1EaVgaFFX', '2015-12-17 06:26:48', '2015-12-17 06:26:48'),
(194, 1, 'z9DhpWh9V8MpvWZSwFjx0hKFxoI2DGgK', '2015-12-17 07:34:29', '2015-12-17 07:34:29'),
(195, 1, 'CR87O4WkLFp06iAR7lqniAlqjmh1k8we', '2015-12-17 10:03:19', '2015-12-17 10:03:19'),
(196, 1, 'cPDJLvk735GPW0ra0gW4t56wJ5pDmMRk', '2015-12-18 05:56:23', '2015-12-18 05:56:23'),
(197, 1, 'oVn2YFQRwd2gAWhlclpXyDrhjeVppsLU', '2015-12-18 06:36:31', '2015-12-18 06:36:31'),
(198, 1, 'vuNBEvFcd18udu3JQnsW0ofs2KzferFr', '2015-12-18 11:48:06', '2015-12-18 11:48:06'),
(199, 1, 'Ywq2iUj3JoQ0Nm3RfYTVYVEHmaQdebk9', '2015-12-18 12:47:28', '2015-12-18 12:47:28'),
(201, 1, 'qQwfqZPYXB5z1AGj6X0sA564nSQFxfAK', '2015-12-19 07:59:07', '2015-12-19 07:59:07'),
(205, 1, 'vcFhrXNme0lqyD9NHvhq70lohOEjKwXm', '2015-12-19 09:47:59', '2015-12-19 09:47:59'),
(221, 1, 'v8PGTwhcVg9tbCprRcnz7O51wzOd7u6E', '2015-12-19 14:02:04', '2015-12-19 14:02:04'),
(222, 1, 'NqZN9jtcJEUBarUEcjhBHW3WEKeC7Y4U', '2015-12-22 09:06:49', '2015-12-22 09:06:49'),
(223, 1, 'JCXWsqKC99RPpa6ETVbmnuEwv6bBuQdL', '2015-12-29 04:46:56', '2015-12-29 04:46:56'),
(224, 1, 'oDVXJBM8w9YRFKWb8HbGYD8TGtlt3X4b', '2015-12-30 06:01:35', '2015-12-30 06:01:35'),
(225, 1, 'Ao8BnmQ433VKsCngYlkBA3XwmdG6KxtH', '2015-12-31 11:01:50', '2015-12-31 11:01:50'),
(226, 1, 'iRxZ1afS4VTBy5VwpG5NQxQISj8EnNVP', '2015-12-31 12:10:39', '2015-12-31 12:10:39'),
(227, 1, 'k37znZ2CQPln2OLinYELHQ2Tkw8j56ht', '2016-01-01 04:44:34', '2016-01-01 04:44:34'),
(228, 1, '3PjJ5RyRe8ZPBHfQ9ro1UI3iVhG49rGR', '2016-01-01 05:11:05', '2016-01-01 05:11:05'),
(229, 1, 'BLBuxGAaqABxtg08U0SO5L8RYf3YTlrJ', '2016-01-05 05:53:26', '2016-01-05 05:53:26'),
(230, 1, '3GezeUSlnh72asykrBlhVqlOZW9dVBgc', '2016-01-07 06:03:28', '2016-01-07 06:03:28'),
(233, 1, 'U04AcwG7T17Ky9gy72tThaE5pa7cMKRb', '2016-01-08 08:56:55', '2016-01-08 08:56:55'),
(234, 1, 'NKfuqj7oiybLSyVEn4gfvgzdlrTIYXJh', '2016-01-08 11:00:23', '2016-01-08 11:00:23'),
(236, 1, 'HIJ6IMqSV3EybIZFiNu3cxpbabgBbqWy', '2016-01-09 14:05:32', '2016-01-09 14:05:32'),
(238, 1, 'sWxeUSntPPSkt6pQp0llxmHLbkEaChYD', '2016-01-13 15:22:14', '2016-01-13 15:22:14'),
(239, 1, 'ZJD3jkIsNXOfl3Ns3h7Zdc1c8YTWZszk', '2016-01-20 10:51:02', '2016-01-20 10:51:02'),
(240, 1, 'IYTXwaz9POTqnhccszxP5xrsVS6pVfsT', '2016-01-20 12:17:09', '2016-01-20 12:17:09'),
(247, 1, 'sK03UtbWuendDySxW4QHNwFk7oUfGGQh', '2016-01-22 11:21:02', '2016-01-22 11:21:02'),
(248, 1, 'cEupKusIH32KPfSFxsgrNsbyPtOrAKmJ', '2016-01-24 18:55:37', '2016-01-24 18:55:37'),
(249, 1, 'rh3YgfuzOeuR1XTHcv6C8va0IeF6suBG', '2016-01-27 09:57:20', '2016-01-27 09:57:20'),
(251, 1, 'O43TYM85iDgTel2AL9Vx9R0XfCQBkLVi', '2016-01-29 05:40:07', '2016-01-29 05:40:07'),
(252, 1, 'PwS9qjcqbOWH0Vfmi3r2Rsyt6DD4a0Zu', '2016-01-30 05:50:25', '2016-01-30 05:50:25'),
(253, 1, 'zPd7scLweBBjdsNU4z9W1ypNHkJXUluh', '2016-02-02 08:31:41', '2016-02-02 08:31:41'),
(254, 1, 'Bs7DtcBQghw6pIARDwkUTAQlFNDmBoJH', '2016-02-03 05:40:54', '2016-02-03 05:40:54'),
(255, 1, 'pHG2p2qNOyL7yGHs64ZEIlMi3gWIKoqA', '2016-02-03 09:47:42', '2016-02-03 09:47:42'),
(256, 1, '98beL2w2JCdq6tPhm8WI8aJhOn1rImR4', '2016-02-06 05:12:46', '2016-02-06 05:12:46'),
(257, 1, 'T5TIVTIWDNAQJmGWyidQyREJ5I4FzgZR', '2016-02-06 05:15:01', '2016-02-06 05:15:01'),
(258, 1, 'EnBjEdYhWPx3Oy15aRAWG4DKqhwRv5ov', '2016-02-06 08:27:20', '2016-02-06 08:27:20'),
(259, 1, 'CsMADZrehJhqSi5ZLjW818I2w4CfHDSr', '2016-02-06 08:33:35', '2016-02-06 08:33:35'),
(260, 1, 'azAaPIxSHpYhnaoX0dJSDMLNqc8lcmJc', '2016-02-09 04:52:54', '2016-02-09 04:52:54'),
(261, 1, 'HKxd27kS2kMSM2HrTAKv43R3KbDrswhA', '2016-02-09 07:19:08', '2016-02-09 07:19:08'),
(262, 1, '48Ubx2II5G9sO0mFRXvuDZ2Y7nPfD6GL', '2016-02-09 10:00:34', '2016-02-09 10:00:34'),
(263, 1, 'vT3WOvD54FuXTHFUCCGDZJTMESQGIyLm', '2016-02-10 04:37:06', '2016-02-10 04:37:06'),
(264, 1, 'wbqdkltHpAMPJBwCArq8ofEOJM78ijA8', '2016-02-11 05:56:27', '2016-02-11 05:56:27'),
(265, 1, 'rg7fD0sVCXY1tKniF72qcMC2yMT7qiIW', '2016-02-11 05:59:18', '2016-02-11 05:59:18'),
(266, 1, 'TiysVzWSHW41jwJVXICfb2HKf8SbffrA', '2016-02-11 06:57:37', '2016-02-11 06:57:37'),
(267, 1, '8W8ebwDb7e6y09mrOLNIbK1NRau4COMD', '2016-02-11 09:58:24', '2016-02-11 09:58:24'),
(268, 1, 'ZiFrNARlSVBjkIbrjZ7onhY0tJzQz507', '2016-02-12 07:15:09', '2016-02-12 07:15:09'),
(270, 1, 'mP5f65vcNaweLd9PJCf3Pk9IVnME5RBf', '2016-02-13 07:56:40', '2016-02-13 07:56:40'),
(271, 1, 'tdu6OzlLAyNW4sluvHCEWds5ZxP0bxS4', '2016-02-14 01:20:12', '2016-02-14 01:20:12'),
(275, 1, 'ztbunZDOv1s9QIiNz7RFxluWuSYbg8Fa', '2016-02-29 19:05:19', '2016-02-29 19:05:19'),
(276, 1, 'hLmFelaXvujhsSGwNlU7lfcyJpmYLnGk', '2016-03-02 16:17:03', '2016-03-02 16:17:03'),
(277, 1, 'XB9vR14I8dis89PH6B87A7TLhZKXaXJm', '2016-03-02 18:54:12', '2016-03-02 18:54:12'),
(278, 1, 'kQeWQK85H1BrXV1SCJfc27Fmw6Wp3oyN', '2016-03-02 19:30:33', '2016-03-02 19:30:33'),
(279, 1, 'WIQSxD6QHb4PFG2aU4Ha6jEj3uBub0cs', '2016-03-03 23:11:41', '2016-03-03 23:11:41'),
(280, 1, 'AEVdOFQIJrN97odGu8wIzsxDq2ZXvdp0', '2016-03-07 16:41:17', '2016-03-07 16:41:17'),
(281, 1, '94wd5Y0M2Kps5m7ie7cPccjEjOKpMQNy', '2016-03-07 16:46:38', '2016-03-07 16:46:38'),
(282, 1, 'DPVodfpPkU8Kz5mWCt3JAlqo6fwGbuxo', '2016-03-07 17:42:12', '2016-03-07 17:42:12'),
(283, 1, 'VGvZq0gH0YS1imjFqXLlvrG0WX7SOAcU', '2016-03-08 16:40:20', '2016-03-08 16:40:20'),
(284, 1, 'ANi2cc4LPzquSUcuvQpVzYcRuVOAutaZ', '2016-03-08 19:51:06', '2016-03-08 19:51:06'),
(285, 1, 'ilHKy865x9mgPlXMJP4l5fMJrkOqZLZc', '2016-03-08 21:39:59', '2016-03-08 21:39:59'),
(286, 1, 'QsNTMqkvHRecWfEJ6H1NWcaxSs1oNvfw', '2016-03-09 03:18:58', '2016-03-09 03:18:58'),
(287, 1, '37SnOgQXkjLfYjP4izY4X1UR1iXQ3Ssp', '2016-03-09 16:42:10', '2016-03-09 16:42:10'),
(288, 1, 'mpppkJaenoQAVtCIfkY36F35y1HlT86S', '2016-03-09 19:28:57', '2016-03-09 19:28:57'),
(289, 1, 'Ftyf3YMVMk5d5O0KjxdIyH0vTheyr7DR', '2016-03-10 16:50:23', '2016-03-10 16:50:23'),
(290, 1, '0LUVNJqQ667EUhMzzza8qehmqdvwRD4V', '2016-03-10 20:57:26', '2016-03-10 20:57:26'),
(291, 1, 'W2l6Ih8MTSrv0teq6J7rx42EH4oFCXy6', '2016-03-11 16:29:30', '2016-03-11 16:29:30'),
(292, 1, '2WDNZ8YPZQBPvCjkqaMLcrG2bHNpiSx2', '2016-03-14 17:23:02', '2016-03-14 17:23:02'),
(293, 1, 'sIzDhM5WZMUPdch0erMFTXt6uNaxcn7G', '2016-03-15 16:11:22', '2016-03-15 16:11:22'),
(294, 1, 'vS1UwLmFUAIsNtD3bxL7mmzkFnwEybSF', '2016-03-15 17:53:33', '2016-03-15 17:53:33'),
(295, 1, 'im30jVklOWR7TySKNe21naGEanVatJYL', '2016-03-16 15:56:31', '2016-03-16 15:56:31'),
(296, 1, 'PT6rUzFfOsrLbETkHyRUxfTxgVLIHgIE', '2016-03-16 16:20:49', '2016-03-16 16:20:49'),
(297, 1, 'Hy47UymE9xA7JsfjGCjefolnkVOOMTwY', '2016-03-16 23:23:41', '2016-03-16 23:23:41'),
(298, 1, 'paA1vDiDZxGtFx61LWWMV7tZqeNrCUW1', '2016-03-16 23:29:10', '2016-03-16 23:29:10'),
(299, 1, 'x5gISkPux4CamFmVoaeoYsPImFfBGpef', '2016-03-17 01:07:05', '2016-03-17 01:07:05'),
(300, 1, 'yjn0mOUVW3y7UVevY9DKSECOggs96j6r', '2016-03-18 18:19:44', '2016-03-18 18:19:44'),
(301, 1, 'GoIQf7DmVGs40YbFL7T8BVtbiQ8R8uBh', '2016-03-23 16:44:19', '2016-03-23 16:44:19'),
(302, 1, 'DvjAiVcASbqobvNMqzIhVBdm6jofQdt1', '2016-03-23 17:17:21', '2016-03-23 17:17:21'),
(303, 1, 'QBOZsaTD6i22spqeC2vgG2yIgCh8dwnG', '2016-03-23 17:30:56', '2016-03-23 17:30:56'),
(304, 1, 'u1lErExOOF3HRLDmIx6hruoN94qS9Dsn', '2016-03-23 17:59:47', '2016-03-23 17:59:47'),
(305, 1, 'cbIVuXZn5wyzsTGZHKxVDjLB7SLOPSYD', '2016-03-28 22:03:00', '2016-03-28 22:03:00'),
(307, 1, 'CpIqZbEGoqDFJKxHET5h3C0pzDZdvnKn', '2016-03-29 18:21:21', '2016-03-29 18:21:21'),
(308, 1, 'byerYGBIbsMg4CfY7aidC1ZNYsnmj1E6', '2016-03-30 21:43:20', '2016-03-30 21:43:20'),
(310, 1, 'ICBM7yj3By39IICV1jkoIKJxh1FFpOzg', '2016-03-31 00:07:58', '2016-03-31 00:07:58'),
(311, 1, 'hYZ8uFrbW213bFWW2IIBXFQOq5B04Kbj', '2016-03-31 16:10:55', '2016-03-31 16:10:55'),
(312, 1, 'nFv4bXOYipFv5Ix6zhKSpR1wyNhobj99', '2016-04-01 22:07:40', '2016-04-01 22:07:40'),
(313, 1, 'KHu9yIGB16YvrDV2FEWOq8BQ6PvMbKl8', '2016-04-25 16:52:04', '2016-04-25 16:52:04'),
(315, 1, '7FoQgzabeFRIWL1M7ZPsm3z9PG8F1Ugo', '2016-04-26 17:31:21', '2016-04-26 17:31:21'),
(316, 1, 'OdnZDFviqrfXC8WrQ2Cb6ZDvYz8nPgRx', '2016-04-27 22:07:40', '2016-04-27 22:07:40'),
(317, 1, 'kOebcJr1JJG5wUk5fhOfGhHxHnLOwSvc', '2016-05-02 06:48:45', '2016-05-02 06:48:45'),
(320, 1, 'ggE3UJheRV8uDVSOh95l2o0aefxiuPKt', '2016-05-02 16:34:11', '2016-05-02 16:34:11'),
(321, 1, '53mvW0wlHIMVAW0H00HVg9zkesJ4OhXD', '2016-05-02 16:57:17', '2016-05-02 16:57:17'),
(322, 1, 'hRCPvsEyyjmKVeHCXisqCoaXBYCxaScN', '2016-05-03 01:22:32', '2016-05-03 01:22:32'),
(323, 1, 'cTlcwjYOxTY6ZJCrkMNEnO5b5TzMmEGB', '2016-05-03 17:04:46', '2016-05-03 17:04:46'),
(324, 1, 'vmqEkzTG3IbAneq5OOjHzSqucV35AmnD', '2016-11-01 22:47:58', '2016-11-01 22:47:58'),
(325, 1, 'BW01QCJOOLoZIO8VUJnF52vIbu2rc11g', '2016-11-02 15:39:37', '2016-11-02 15:39:37'),
(326, 1, 'FzWFz0LRPiRYtqrp0ZYERRRuAbG1yYiV', '2016-11-09 01:48:50', '2016-11-09 01:48:50'),
(327, 1, '0xAj653k46fiqUDh19Ckozg3ZyytnCH0', '2016-11-09 18:53:05', '2016-11-09 18:53:05'),
(328, 1, '6OwwOXig9X0lFtXXVrYgJ8SrkoLgzEvg', '2016-11-10 17:21:43', '2016-11-10 17:21:43'),
(329, 1, 'FnxCrE4ava77Pf646SgsJhZZcMIfODMy', '2016-11-10 21:37:29', '2016-11-10 21:37:29'),
(330, 1, 'hXKEok3YM5YjXZjMQuL7T14hAmz43BJH', '2016-11-16 23:50:10', '2016-11-16 23:50:10'),
(331, 1, 'WlPtomsC2L3hnU7xIpW81oFQENGiNNaB', '2016-11-17 15:33:54', '2016-11-17 15:33:54'),
(332, 1, 'liZqeZ1qON6xfJK7g2oCtLjbxcjkUBCS', '2016-11-18 15:49:34', '2016-11-18 15:49:34'),
(333, 1, '0eTUyc0uXgk1fRS0dDaGQUdXVVapU8ts', '2016-11-18 21:04:38', '2016-11-18 21:04:38'),
(334, 1, 'nUEqpH5Mf6sLxlaTi9sULwKMMgVLYp2a', '2016-11-21 15:59:03', '2016-11-21 15:59:03'),
(335, 1, '88kA0cgf81T7zClaZff6EnvhJNcUtaSf', '2016-11-21 21:07:54', '2016-11-21 21:07:54'),
(336, 1, 'VepF1pmmoT2kFPHwKDIFkF6kb34GMdNs', '2016-11-22 15:24:59', '2016-11-22 15:24:59'),
(337, 1, 'nPKUHcWR3obf890hffnSkhzBH7IGFrlp', '2016-11-22 19:00:15', '2016-11-22 19:00:15'),
(338, 1, 'l4KFahczm29zpo7TakPGJM9qHJWAxOAQ', '2016-11-23 19:34:22', '2016-11-23 19:34:22'),
(339, 1, 'luOKV6kdrS9iGE56uw85la2ddiCvN67F', '2016-11-24 15:41:20', '2016-11-24 15:41:20'),
(341, 1, 'RIo4FfgIOAjDS1jjYUvtUNGBH9wjhuKY', '2016-11-28 15:35:34', '2016-11-28 15:35:34'),
(342, 1, 'OnbCVCBgNJdKh2zqX3kDgNYi4SCfSzur', '2016-11-29 15:37:22', '2016-11-29 15:37:22'),
(343, 1, 'MEYNvMqzpb5SvUki1HoGiebSnkOV6uOw', '2016-11-30 15:45:37', '2016-11-30 15:45:37'),
(344, 1, 'HmqRCpKWJ37CB8sDmMaSzn2JCE22ogQP', '2016-12-01 15:40:55', '2016-12-01 15:40:55'),
(345, 1, 'Dpbs7U9zUKm1747oicIGoLGugCMFRJXu', '2016-12-05 16:12:14', '2016-12-05 16:12:14'),
(346, 1, 'IxBT0XnquRWKn6LdI08QGNWZlGpILa1m', '2016-12-06 16:00:17', '2016-12-06 16:00:17'),
(347, 1, 'J0GWjCrIGjcxAeezuVTwr23G0tX8z9FA', '2016-12-06 19:40:21', '2016-12-06 19:40:21'),
(348, 1, '40aMIDL7SIahLW5qCDogsFDB5jrjSrAl', '2016-12-07 15:46:01', '2016-12-07 15:46:01'),
(349, 1, 'UWX5assFv0BqZHtAYvNLjj3vZBeS4F8S', '2016-12-08 16:08:25', '2016-12-08 16:08:25'),
(350, 1, 'YxETIww2Np3mmklNscNLHuro63gm80n6', '2016-12-08 21:10:45', '2016-12-08 21:10:45'),
(351, 1, 'arQP0rVbLNbuSAhBMdvb8d708jPVs7TS', '2016-12-09 16:19:26', '2016-12-09 16:19:26'),
(352, 1, 'xFAbEiz775LC7qKnr3Hnhrk6CnFicY4N', '2016-12-09 21:53:24', '2016-12-09 21:53:24'),
(353, 1, '2zXNR8a9dPx6Yh6jhqkdlXMH2uAdvWq2', '2016-12-14 16:31:01', '2016-12-14 16:31:01'),
(354, 1, 'U6yuAZYyUpFDYdIUh3ttI034vFbyOOtF', '2016-12-14 22:23:47', '2016-12-14 22:23:47'),
(355, 1, 'rn1PCB8xyinSDZzM6FveYtdRkwEYv7or', '2016-12-15 16:32:12', '2016-12-15 16:32:12'),
(356, 1, '89O8WwMadX6nZsPQVwVI5amH0KfcBG69', '2016-12-15 20:39:59', '2016-12-15 20:39:59'),
(357, 1, 'ODUR6IbIZjp6m0WlUkRBJCxWBmYy2uCP', '2016-12-28 16:09:32', '2016-12-28 16:09:32'),
(358, 1, 'PRhfdm4BXCA6VVTmJmJp2A8cck1fmQOs', '2016-12-29 21:18:03', '2016-12-29 21:18:03'),
(359, 1, 'MX2BtaL7IbVo0M9cOjHgGKtv0W3qsfvN', '2016-12-30 16:49:48', '2016-12-30 16:49:48'),
(360, 1, 'URDmiIwst9p7AH752meBRy5jnmmn12iQ', '2016-12-30 22:02:32', '2016-12-30 22:02:32'),
(368, 1, 'Xgq5p1wpwa6nMCh71oawtjYyaDBqFF0Z', '2017-01-15 11:48:10', '2017-01-15 11:48:10'),
(380, 1, 'YTOyGClrQ6j10dPZK2sEsgVdSq2z1KXp', '2017-01-16 11:37:15', '2017-01-16 11:37:15'),
(385, 1, '30rPT91aUiZggV1KuNyKH3qmhZKYUzBO', '2017-01-19 07:57:49', '2017-01-19 07:57:49'),
(386, 1, 'NMo6ESSw8QVNQcewukGFL6Wnt2tJv7FB', '2017-01-19 13:42:11', '2017-01-19 13:42:11'),
(389, 1, 'EYdjNai3eG7Yd6zYQBvUiUkI3yfj6YCk', '2017-01-20 08:31:39', '2017-01-20 08:31:39'),
(393, 1, 'XG5ZK9wWhKtftpzeVlFOyotpfn72STI4', '2017-01-22 20:53:01', '2017-01-22 20:53:01'),
(400, 1, 'vQp8uS1ljqj4UhabgjDjcP7rW0vSoXeJ', '2017-01-24 01:38:00', '2017-01-24 01:38:00'),
(401, 9, 'SWyNFqRVA8qiuWgNylGD5deH9FPDjdrk', '2017-01-24 01:45:43', '2017-01-24 01:45:43'),
(402, 1, 'dfnpIy8kp33r1SzHMChUkh2JZHe5dGRO', '2017-01-24 18:30:38', '2017-01-24 18:30:38'),
(403, 1, '33EGJXIDAduqWCSifVsPmVuMTJyLnquR', '2017-01-24 19:22:19', '2017-01-24 19:22:19'),
(406, 1, 'ZG0iCyctnYgcnkTIGV9H07fXE8s33bkd', '2017-01-25 21:35:52', '2017-01-25 21:35:52'),
(407, 9, 'bvZmNj56MW8nev4UoSExXEAjHnNtIYd7', '2017-01-25 23:08:45', '2017-01-25 23:08:45'),
(408, 1, 'ikDhS95qhS3n3tJo6foqD3TNlJ0hTMky', '2017-01-26 21:47:07', '2017-01-26 21:47:07'),
(410, 1, 'P4KSBwXBVdxIdzqUiEgofSv1K8YnWyUt', '2017-01-28 15:35:11', '2017-01-28 15:35:11'),
(415, 9, 'fHy1CJ8ZNAENNJRR2AbRxO1q3NgwDSya', '2017-01-28 18:06:57', '2017-01-28 18:06:57'),
(416, 1, 'vmnV6viMKGospBjxZN5KeJlr2JfieB80', '2017-02-08 05:00:34', '2017-02-08 05:00:34'),
(417, 1, '6FNWIuuJK9UlpO1Fic0Q0yTcYX7oRXfx', '2017-02-20 18:55:15', '2017-02-20 18:55:15'),
(418, 1, 'q9AXtBPt6ECX67wEusA6AAPWzSgSafLj', '2017-02-20 18:56:40', '2017-02-20 18:56:40'),
(420, 9, 'hS7zYXzFL2iFSdSWDW7ykwKc4khfQe2L', '2017-02-20 20:06:22', '2017-02-20 20:06:22'),
(421, 1, '9hjq43wKRKSBmxY2t3ZOejwktrvjSMEH', '2017-02-20 20:14:42', '2017-02-20 20:14:42'),
(422, 1, 'uLoVafnA4TgQrblXUJBhfA89DUWnD9Vq', '2017-02-22 20:07:53', '2017-02-22 20:07:53'),
(423, 1, '7OV8S0oBOY4rGatHhiaAzypR1DGDaZNa', '2017-02-23 20:11:41', '2017-02-23 20:11:41'),
(424, 1, 'UYUmFphBxnfpBAQyy5Uy2O60clf2v5uy', '2017-03-13 05:46:13', '2017-03-13 05:46:13'),
(425, 1, 'nVM4Mqyd6CqQyiK66KDV2RcpFlqC7iHK', '2017-03-13 06:23:52', '2017-03-13 06:23:52'),
(426, 1, 'h3pToQYIaz2jvgU4AMwUNjEc1EyTDj1Q', '2017-03-15 03:51:17', '2017-03-15 03:51:17'),
(427, 1, 'rmia1KYiKPpTgCceikY640kRsRQL24Uc', '2017-03-25 18:15:45', '2017-03-25 18:15:45'),
(428, 1, 'RkCnVMzn79IghbqS6Tol1iXpMovbLeym', '2017-03-26 16:39:41', '2017-03-26 16:39:41'),
(429, 1, '2RDU3tFlFxsv7L2cTLSgcpRGVbwjCW8G', '2017-03-27 21:19:51', '2017-03-27 21:19:51'),
(430, 1, 'A3NKM0u6WIRHFIPKWVrpRChKOWenNmNz', '2017-03-29 16:28:16', '2017-03-29 16:28:16'),
(431, 1, 'JjBwd9vjdA5lkf7Egmo2XDWkCm4wM3S6', '2017-03-29 21:05:37', '2017-03-29 21:05:37'),
(432, 1, 'TvgVvDLvHaMuIbcV5lvteo65rkYWTuzy', '2017-03-30 03:50:46', '2017-03-30 03:50:46'),
(433, 1, 'xXXlnxXtruorIKPg2zFRLQdNAbOPgulY', '2017-03-31 02:27:47', '2017-03-31 02:27:47'),
(435, 1, 'GfPMoIyrNnVSA43DaXYUoo5oJeEnaIKl', '2017-04-02 05:05:38', '2017-04-02 05:05:38'),
(436, 1, 'cln4DtiKEDuQxodFRDw4Lcra8cGQeIMz', '2017-04-02 15:47:12', '2017-04-02 15:47:12'),
(437, 1, 'gDbjgjlpQr6KOjlMh9pB4bHKi5cp1Wrc', '2017-04-04 04:10:08', '2017-04-04 04:10:08'),
(438, 1, 'FrI0pkXa4NwLpsn3Pi1PH3A0otWeGruU', '2017-04-04 05:49:27', '2017-04-04 05:49:27'),
(439, 1, 'beJtoHPg0Ht1NsKFVakpduMdidW8GpFm', '2017-04-05 04:57:48', '2017-04-05 04:57:48'),
(440, 1, '9ltPoQR91755nbeUztRi9xx8BY3cqz2J', '2017-04-06 05:09:20', '2017-04-06 05:09:20'),
(441, 1, 'UPyLjrn0wB0zzQ6zVKlxWCRPo9n73Vw1', '2017-04-06 05:59:11', '2017-04-06 05:59:11'),
(442, 1, 'YlEMN7x1ZBG3yPGYX2RVjd0iiwoPNWk4', '2017-04-09 20:45:13', '2017-04-09 20:45:13'),
(443, 9, '5pZReIfyiIz7EIjW3qfbRrmRWsMCV2xD', '2017-04-10 18:39:34', '2017-04-10 18:39:34'),
(447, 1, 'VrEBinxz1hTXXmiPVeefL138I4iSWQ52', '2017-04-11 17:13:34', '2017-04-11 17:13:34'),
(448, 1, 'kDXffNhbdLtlC2lNLnlf4HST4FI6kdal', '2017-04-12 19:07:44', '2017-04-12 19:07:44'),
(449, 1, 'lK9nPuYw3iWpNs3x5MbByORZxlWm8fim', '2017-04-12 19:21:37', '2017-04-12 19:21:37'),
(450, 9, '0aY6n6847RBlgl4dZT6e3IdzDL1sq0Xq', '2017-04-12 19:22:51', '2017-04-12 19:22:51'),
(452, 9, 'TABN9b6WT0CYjifUjMsRam5aERlRbjzZ', '2017-04-12 21:25:33', '2017-04-12 21:25:33'),
(453, 9, 'buJbqy4uYLHDQ3RdZGgliHXVjLAlAj8h', '2017-04-13 19:22:36', '2017-04-13 19:22:36'),
(455, 9, 'WZcJMFiKO3ZRhVgqVjLweteggAmdW7Tt', '2017-04-14 01:27:48', '2017-04-14 01:27:48'),
(456, 9, 'eBjaIKb8G8ugqfKIY9n5ArRJtHmu3Gsm', '2017-04-17 15:55:58', '2017-04-17 15:55:58'),
(457, 9, 'WS5SwpOuTeZAnYhQilFL0Xb0Y4b77N2h', '2017-04-17 16:03:45', '2017-04-17 16:03:45'),
(458, 9, '411nM5xC2dt3oMeSCiEl5I0lhz2Aqj8D', '2017-04-17 23:04:03', '2017-04-17 23:04:03'),
(460, 9, 'Djxgc5Jgai6TbvOk2rjzsNAPZWuTaKeH', '2017-04-18 19:25:58', '2017-04-18 19:25:58'),
(461, 9, 'qqD2PlrD3qJLRsYty7FHpDyrJQxIhiLO', '2017-04-19 05:39:04', '2017-04-19 05:39:04'),
(464, 9, 'xSxreVLlbTYg46jkRqTskBIPW89IDwpS', '2017-04-24 19:22:41', '2017-04-24 19:22:41'),
(465, 9, '14gp3LskVafODeQwTDw6h6hxgunp8mHf', '2017-04-24 20:15:52', '2017-04-24 20:15:52'),
(468, 9, 'kQ30zVbjLum6rqBwhy90Up4mcm6NcFSi', '2017-04-26 19:58:26', '2017-04-26 19:58:26'),
(469, 9, '3bqBJ0wnW7HOeeEfWLF2JoqeQ2PBzCHz', '2017-04-26 23:59:15', '2017-04-26 23:59:15'),
(472, 9, 'Z4INZw1dg8oynP4OD3tZmAY5hzOjPTEh', '2017-05-02 16:39:03', '2017-05-02 16:39:03'),
(478, 9, 'EdRqaQPyavSjqIcaq82t1vmc9f7mYcuu', '2017-05-09 04:50:40', '2017-05-09 04:50:40'),
(484, 9, 'Ri9Ea0mfx2LagbGD8BSyLt6fkgxJbfcr', '2017-05-09 22:50:23', '2017-05-09 22:50:23'),
(485, 9, '8Vj5ePjyRAICMixCletCXCDtP4HR2JBX', '2017-05-12 21:34:38', '2017-05-12 21:34:38'),
(488, 9, 'jmk6mdzwom7URjlYx9GHsb1JlZifHPSv', '2017-05-15 19:41:51', '2017-05-15 19:41:51'),
(489, 9, 'NTNI9SUXhMFjsw65hdWMpBoP07BV3y1q', '2017-05-23 19:12:10', '2017-05-23 19:12:10'),
(491, 1, 'd6f8zbo1u9lGbzYPuTFcQ4jwHm1d82KV', '2017-05-23 19:58:55', '2017-05-23 19:58:55'),
(492, 9, '90SqsYXsn0Vo2t3j0X3bqiLvjbvfJHPd', '2017-05-23 20:14:23', '2017-05-23 20:14:23'),
(493, 9, 'HbYU4JGhFiXXGwA7nharo80hoOg8xuC4', '2017-05-23 20:24:44', '2017-05-23 20:24:44'),
(494, 9, 'eB0gnJuRxuRFiBhjLfDFByoinXons3xZ', '2017-05-23 20:27:50', '2017-05-23 20:27:50'),
(495, 9, 'CuU8HSHiMhmMvb6qVDmcNsL0chZp6SWy', '2017-05-24 00:46:22', '2017-05-24 00:46:22'),
(496, 9, 'PWTkpZeVGSdtwt9IC0g0icmk1gFSlfGv', '2017-06-15 06:05:43', '2017-06-15 06:05:43'),
(497, 9, 's49fFnklRpF5OOS2jYXRoutH5VqEFewt', '2017-09-02 21:14:41', '2017-09-02 21:14:41'),
(499, 9, 'ZACd7QKPIDrMopxrvsAFCULxDo7PX2br', '2017-09-03 04:41:27', '2017-09-03 04:41:27'),
(500, 1, 'xVHdxpCRdZF2nDHP9lLeIoHpX2PXj5Yc', '2017-09-08 05:45:27', '2017-09-08 05:45:27'),
(501, 1, 'bW88sq8eaaw6u1QvEngEq5YVVQ2qhFzX', '2017-09-08 06:07:13', '2017-09-08 06:07:13'),
(502, 1, 'PlIhwjZkcOwqngtSzdRZIbtR0UGwXv6o', '2017-09-09 03:25:32', '2017-09-09 03:25:32'),
(503, 1, 'OyWtzOT01xuQEizw8ooxrqggOskOXisu', '2017-09-09 19:50:15', '2017-09-09 19:50:15'),
(504, 1, 'Z3KJTkiaygVFz1RpG5YGGHdpLCwPSzZI', '2017-09-10 04:09:51', '2017-09-10 04:09:51'),
(508, 9, 'BT1p2Bt9UqNx2JF1uBjJUQXTKiL3lejO', '2017-09-11 20:09:19', '2017-09-11 20:09:19'),
(514, 9, 'BKOgFtE1cNnrnCCR299Xh2ZRpa0L5iao', '2017-09-18 02:01:18', '2017-09-18 02:01:18'),
(515, 9, 'gdtOIPY5rI05ZHILzGZr0m4tkGAfCvcq', '2017-09-18 14:52:26', '2017-09-18 14:52:26'),
(516, 9, 'LlMhuZP8B47nJ4ORhq7uACPtaXdOv2yo', '2017-09-18 17:27:54', '2017-09-18 17:27:54'),
(517, 9, 'YMVZKtIuDyVFhS7v29nO48C5WsL8JjZF', '2017-09-18 21:38:13', '2017-09-18 21:38:13'),
(518, 9, 'BvJ12OugoJhgJs9isQL9br24C7YyTD8j', '2017-09-19 00:37:29', '2017-09-19 00:37:29'),
(519, 9, '8qcp6wNVPRBaCMpXkNQvolSpnNLDcUB7', '2017-09-19 17:16:13', '2017-09-19 17:16:13'),
(520, 9, 'TqibeIg2QSEsQDyC7LcK59VAKKMipkpn', '2017-09-25 22:42:02', '2017-09-25 22:42:02'),
(521, 9, 'a6R8PaA9vwIpGxN7MlLJuVUveYS1LpPH', '2017-09-26 02:16:18', '2017-09-26 02:16:18'),
(522, 9, 'WmzWXCLtADZ2X8xk6dEBRciyq1Ua7yiU', '2017-09-26 17:20:13', '2017-09-26 17:20:13'),
(523, 9, 'S2ojiPFjhkcqLoQ7Oi5R7lY4FGrmTJVN', '2017-09-26 17:25:28', '2017-09-26 17:25:28'),
(524, 9, 'kAl7L3eJgJYVHpbAl6WhEi8xMNnYPpnr', '2017-09-26 18:16:23', '2017-09-26 18:16:23'),
(525, 1, 'xLk8EQyXw0Sust6JfEFjOVGUuTQySRte', '2017-09-29 07:21:45', '2017-09-29 07:21:45'),
(526, 9, 'vdahqrLd0RHiCr5b8h6iKGDJFPdFAOe8', '2017-09-29 16:27:13', '2017-09-29 16:27:13'),
(527, 9, 'WS1ZNJrVSOndMKv6IynLlgtKzsSf8hKA', '2017-09-29 16:30:17', '2017-09-29 16:30:17'),
(528, 9, 'IxRsRJOY2jDlbgw5sWeiAjCvOa3kR1Ac', '2017-09-29 17:07:18', '2017-09-29 17:07:18'),
(529, 9, 'gwwpv2D4cG911t35wH4YvCcyoVQvRElK', '2017-09-29 17:09:02', '2017-09-29 17:09:02'),
(530, 9, 'G76hJghdimBSj1PHvhLiQPvcBZ22VJyo', '2017-09-29 17:27:20', '2017-09-29 17:27:20'),
(531, 9, 'pIl45I3RQuHsbg9DrTifyp7FmOFvXAbI', '2017-09-30 03:05:59', '2017-09-30 03:05:59'),
(532, 9, 'Pke5IPK4IxTUkF6XqVjnZFDPsw6wbQnB', '2017-09-30 03:08:08', '2017-09-30 03:08:08'),
(533, 9, 'pfGDyVgBbQCoyWNf2Ejn4xZwTozIkv3B', '2017-09-30 03:08:41', '2017-09-30 03:08:41'),
(534, 9, 'seMjDAG5MVSZnFzYleDHHSC9NaLUolGe', '2017-09-30 03:41:47', '2017-09-30 03:41:47'),
(535, 9, 'T33Id6bhSDDM44aYkwA9qnrqtNlJQXP4', '2017-09-30 06:54:47', '2017-09-30 06:54:47'),
(536, 9, 'LSn1x97nqG5yVzXBEWISLl1ja6m5Q08X', '2017-09-30 18:34:39', '2017-09-30 18:34:39'),
(537, 9, 'vdRsiiXnYqZbCxQWcaLCzthap7hU23dp', '2017-09-30 18:40:34', '2017-09-30 18:40:34'),
(538, 9, 'fhBlMrOEYQDO18asgB6gkwykQ8bZeRtX', '2017-10-01 20:28:05', '2017-10-01 20:28:05'),
(539, 9, 'zvuEqTNzSvCLChO6DIs4EfS52N7OWFff', '2017-10-01 20:29:07', '2017-10-01 20:29:07'),
(540, 9, 'FkpaUFI9FUh4gYyAjtfoh7bH5APYRovj', '2017-10-01 20:42:16', '2017-10-01 20:42:16'),
(541, 9, 'RzQufcstJuydJLBpgzhlqJXDsjDwFc6d', '2017-10-02 03:03:09', '2017-10-02 03:03:09'),
(542, 9, 'RfkaWBmwUmem7zCfjuAsmZLAbXkp5Ig2', '2017-10-02 06:39:40', '2017-10-02 06:39:40'),
(543, 9, 'Lwu6hoQfWQw6j8oLZqxP0ZjiaKquushe', '2017-10-03 06:52:31', '2017-10-03 06:52:31'),
(544, 9, 'QjjlkcEEKa1OdtyoQm4Pv9NzvrjWf6ex', '2017-10-05 01:28:43', '2017-10-05 01:28:43'),
(545, 9, 'uoSHV0CE9x4cyKbZ2SHst8VusHh9yI2W', '2017-10-05 07:14:15', '2017-10-05 07:14:15'),
(546, 9, 'yWRBL2TfCaOD3lTV0NZpbtIRzZmz3E2X', '2017-10-05 17:18:06', '2017-10-05 17:18:06'),
(547, 9, '1JtgEc5RNqJTEGA9PRmjYVA14l6i0HkO', '2017-10-05 17:20:34', '2017-10-05 17:20:34'),
(548, 9, 'T7VDc2KEcM6TzknmM9uUz34rjLDypGTU', '2017-10-06 00:00:44', '2017-10-06 00:00:44'),
(549, 9, 'mBN5iNDSOWFAby7SuxyrnjyhlBmREhzI', '2017-10-06 00:02:50', '2017-10-06 00:02:50'),
(550, 9, 'dk1YRLRE1JUo291Q3tirAMHuXLoYef2B', '2017-10-06 18:27:30', '2017-10-06 18:27:30'),
(551, 9, 'ry2Bxvz3m6tnd1obdzz14I87nk0v9S3q', '2017-10-06 23:26:29', '2017-10-06 23:26:29'),
(552, 9, '7oI0DCpaw0fKfqw4Yi1LCcNHn9md25Y7', '2017-10-07 04:06:25', '2017-10-07 04:06:25'),
(553, 9, 'UWLFqE78A4roavDtn4MaMU2UkEB6ZONU', '2017-10-13 16:58:45', '2017-10-13 16:58:45'),
(554, 9, 'AvgDO5mzCwq8ljBGPwU3OViHsRmk5LJJ', '2017-10-14 06:32:52', '2017-10-14 06:32:52'),
(555, 9, 'YPKsclCuQwF09CaFt44LeootzbaOt1SI', '2017-10-14 16:59:43', '2017-10-14 16:59:43'),
(556, 9, 'IZitIuzqvCI0OawtFT6lKAp06Gs7qp6C', '2017-10-16 05:22:06', '2017-10-16 05:22:06'),
(557, 9, 'XLCgtKFdE1gMAb9l9eDGeZgPh6abkk3V', '2017-10-17 00:57:45', '2017-10-17 00:57:45'),
(558, 9, 'Yi7Zx70bcJUU7vpps5VJvQBtZT4foCpr', '2017-10-17 07:55:09', '2017-10-17 07:55:09'),
(559, 9, 'HYRmxSGdoDTRPMqCP8n2yfBNUG5mXns2', '2017-10-17 08:01:39', '2017-10-17 08:01:39'),
(560, 9, 'ahqLDDxwrfdCKCFYObRnRVevKcJJNzL9', '2017-10-17 17:12:25', '2017-10-17 17:12:25'),
(561, 9, 'ck2Mn5nPgvkoYz3YARZ5Pn6rA051QDIo', '2017-10-18 05:22:50', '2017-10-18 05:22:50'),
(562, 9, 'yHN07TYMOwVTw2uatqPnJkJCw22AqyTo', '2017-10-18 19:25:04', '2017-10-18 19:25:04'),
(563, 9, 'I01jFrfGjSCS4oWyv8zKu4V97CRWFkgA', '2017-10-20 00:09:07', '2017-10-20 00:09:07'),
(564, 9, 'UvPDFfNlJNCkH2BocvFt7mBjIlECD0yM', '2017-10-23 18:27:55', '2017-10-23 18:27:55'),
(565, 9, 'VlEihMHC1c4hBsHkXQtjplZ7iAfe2F5U', '2017-10-23 21:27:47', '2017-10-23 21:27:47'),
(566, 9, 'o9yhTxKN6OWSh615hlWUbDR5qNlVzNs7', '2017-10-24 08:26:56', '2017-10-24 08:26:56'),
(567, 9, 'nOMjhVqxiujlsCCuCeS6w4jUBOi84mmX', '2017-10-24 08:50:12', '2017-10-24 08:50:12'),
(568, 9, '2fPEfJZsHVc6rqP4Y8MCLNtzRWBcQhxT', '2017-10-25 02:07:09', '2017-10-25 02:07:09'),
(569, 9, 'zvR46vim4xH94YWmNpFV6G6zgT2yuGrg', '2017-10-25 17:40:21', '2017-10-25 17:40:21'),
(570, 9, '2zTswKmr9G9dnDMGjWqS08aJLvyqWX1x', '2017-10-27 18:03:09', '2017-10-27 18:03:09'),
(571, 9, 'gbjNHN3Rbfa7BpmESzLF2YldvN72HBeM', '2017-10-28 04:08:57', '2017-10-28 04:08:57'),
(572, 9, '2UuMu7Jk3XOr0Lp1qJ8i1tcw5QGQahgO', '2017-10-31 04:57:16', '2017-10-31 04:57:16'),
(574, 9, 'K9Qr6Vgbtn8twa9ebWjJvNNkkhPWatYS', '2017-12-25 21:49:59', '2017-12-25 21:49:59'),
(576, 9, 'Bps39WAJHf6x9nEP2IBkuplsBjM6gLit', '2017-12-27 07:05:51', '2017-12-27 07:05:51'),
(577, 9, 'DUkEdTKANrqTCybnyP27nEfZsDcRCsU4', '2017-12-28 01:28:11', '2017-12-28 01:28:11'),
(579, 1, 'gwEPEK5AUypx8EW4cTqxYN6BHGvfJ9yc', '2017-12-28 02:33:21', '2017-12-28 02:33:21'),
(584, 9, 'cesY9MkupQ6U145waWYQ4HVS8DVwkvDd', '2017-12-28 03:16:57', '2017-12-28 03:16:57'),
(587, 10, 'FQAjzYxo6Q1jwUtnC4U9pkaaskqxOf3g', '2017-12-28 06:18:03', '2017-12-28 06:18:03'),
(589, 10, 'htDxNPzOHgsoYxfAg1vioBhbmlzlKBTz', '2017-12-28 06:20:30', '2017-12-28 06:20:30'),
(590, 9, '8kQrsqpSyTu4lrv3x1eC5gLvmDnMBfhK', '2017-12-29 06:27:16', '2017-12-29 06:27:16'),
(591, 9, 'f5FDNKxKnlMeQYGEb5odqUpIp1tRUFop', '2017-12-29 06:30:05', '2017-12-29 06:30:05'),
(592, 9, 'QdJRzz9eqOMX9teYtyiuKLWXOE4n4mOA', '2017-12-30 06:09:07', '2017-12-30 06:09:07'),
(594, 9, '0hJ5edWa6kmHkzFzaYJtxoC80BUGFKGy', '2017-12-30 21:57:13', '2017-12-30 21:57:13'),
(595, 9, 'wLZQm4WFgdA8Xtgx0Rt7GzyaJYif8RrC', '2017-12-31 01:20:45', '2017-12-31 01:20:45'),
(596, 9, 'idpTmk6y0wE6drnjltGHnC0DZYYsCCMC', '2017-12-31 05:42:20', '2017-12-31 05:42:20'),
(597, 9, 'mcg6PSzSCT4k02zp7pcNA2KhxI4CEM5B', '2017-12-31 14:57:47', '2017-12-31 14:57:47'),
(598, 9, 'FytzqGsNCSAxXCV4nmRTGFP4WmMv46sS', '2018-01-01 16:56:16', '2018-01-01 16:56:16'),
(599, 9, 'lWina6s3EU3UH2HLShe9Fcdg2xj63nzN', '2018-01-02 01:22:09', '2018-01-02 01:22:09'),
(600, 9, 'QUhe666JPKzaVV4Ur4PthXTALLU4BKN0', '2018-01-03 03:12:49', '2018-01-03 03:12:49'),
(601, 9, 'CdmkVmYAtYQTpZ1OkncQ4DTBAfMzKUtA', '2018-01-03 05:29:52', '2018-01-03 05:29:52'),
(602, 9, 'tH17GHidNSKwVEPkxS60FuZB9VYjBZN5', '2018-01-04 06:44:28', '2018-01-04 06:44:28'),
(603, 9, 'R41vAyFzCHyiqOXvuj111H0pYwHQ72Ze', '2018-01-04 22:06:01', '2018-01-04 22:06:01'),
(604, 9, 'rqC20hVOikfpNtFrBvtcEAWJtJQupz7M', '2018-01-04 22:08:38', '2018-01-04 22:08:38'),
(605, 9, '5JqvznBkQk3ELAM2bTMHd0FBeAHORt4V', '2018-01-05 04:48:59', '2018-01-05 04:48:59'),
(606, 9, 'xxWqOS7WFuSR7agyGsOIXOkLQdQlqUNV', '2018-01-06 02:11:00', '2018-01-06 02:11:00'),
(608, 9, 'mDVGqa9Xj9whlnASFINfJ8dMiQeS4d2D', '2018-01-07 03:15:37', '2018-01-07 03:15:37'),
(609, 1, '6bTxEgNcrge2qeymYxGeDl4i53ActgRh', '2018-01-14 01:24:30', '2018-01-14 01:24:30'),
(610, 1, 'fodEY90PIYVilzSRmUzgMavLnx8ot1Bf', '2018-01-16 02:29:51', '2018-01-16 02:29:51'),
(611, 1, 'TGyyV1URyXhCd37SaQJMHZbz0FSQqfwn', '2018-01-19 04:24:09', '2018-01-19 04:24:09'),
(612, 1, 'if2agsgCopVBHmkABT9whV5J84L9FzUw', '2018-01-26 02:29:13', '2018-01-26 02:29:13'),
(613, 1, 'MBt0gHt0sXx9MTH2yRMi3A7eO5QIxdW2', '2018-01-26 04:00:18', '2018-01-26 04:00:18'),
(614, 1, 'os3P8bBS3GDxFE5kP3ggpnjmYLStjvng', '2017-01-15 08:28:55', '2017-01-15 08:28:55'),
(615, 1, 'ppiMMLK3g1Ak0JfPJ4aUxmWhyErzttaS', '2018-01-27 17:48:09', '2018-01-27 17:48:09'),
(616, 1, 'Tr8rnTdLegAVeZnsSsRrEAYrZYlQdfdf', '2018-02-21 03:14:30', '2018-02-21 03:14:30'),
(617, 1, 'jo6vBtQvtvRwnk1iTv6wNXyjAJfOTs5S', '2018-02-25 18:51:15', '2018-02-25 18:51:15'),
(618, 1, 'WnDOCo50idwCpQXE1lRvebtzSO2XJAaI', '2018-03-01 04:28:41', '2018-03-01 04:28:41'),
(619, 1, 'K6J9Z4loo5KKXZRSv5c09yMeH11Qpyjo', '2018-03-01 18:40:40', '2018-03-01 18:40:40'),
(620, 1, 'V6fEIn9T7a038N9Bx8brRwO0sGD0dMmg', '2018-03-01 21:46:27', '2018-03-01 21:46:27'),
(621, 1, 'knEEyZDYYYJdO5wgu9DUHFhuULBmw8r3', '2018-03-03 16:48:01', '2018-03-03 16:48:01'),
(622, 1, 'WFE6YoykfG26I8n2tnGILMhqRsTpCARA', '2018-03-03 19:33:37', '2018-03-03 19:33:37'),
(623, 1, 'adbFy3Pf5ShFg8H4e7V4ahdAgC4F6e69', '2018-03-14 02:39:00', '2018-03-14 02:39:00'),
(624, 1, 'tl4XO2h7ZpqJnGF8wf7HqwsuMHONlU2A', '2018-03-14 03:47:28', '2018-03-14 03:47:28'),
(625, 1, 'jn71Dku2NJkXZM0IcAF88ky3xnWf5tHZ', '2018-03-16 02:52:14', '2018-03-16 02:52:14'),
(626, 1, 'CQJdcAA0Dt3B3Lqgd4DWqukaOgNeLn5P', '2018-03-16 03:13:03', '2018-03-16 03:13:03'),
(627, 1, 'c4XlSEfmCmA2gbYM9zurdLj2Vg67laoq', '2018-03-16 03:26:22', '2018-03-16 03:26:22'),
(628, 1, 't67f4XNitYSSlORNzyOZoAMvhHa1ofu4', '2018-03-16 03:34:50', '2018-03-16 03:34:50'),
(629, 1, '8vQXrOOkV4dF4GgjTGO3zQxSlOpo16XO', '2018-03-31 19:09:25', '2018-03-31 19:09:25'),
(630, 1, 'ReBQA7zXwfey5TM1rXoYPA6djL9Yr1yi', '2018-03-31 22:49:01', '2018-03-31 22:49:01'),
(631, 1, 'v5oUaXclOgmi4Agm1HIkVwhTBlMopS9y', '2018-04-01 05:23:47', '2018-04-01 05:23:47'),
(632, 1, 'xdz9C7ZWvUCHafNVa3eoetu91dyujEk3', '2018-04-01 15:11:24', '2018-04-01 15:11:24'),
(633, 1, 'u3upwHiHyMWxzYW1pt5vZmtPBy20trCc', '2018-04-04 02:43:46', '2018-04-04 02:43:46'),
(634, 1, 'ux616Cg3ZQwUOQPR19AlKEOPzcAoaR5d', '2018-04-04 02:47:05', '2018-04-04 02:47:05'),
(635, 1, 'X8uRZdGfuty8dJhJmmqJpoDVEe8qTSpm', '2018-04-05 03:14:55', '2018-04-05 03:14:55'),
(636, 1, 'VdsyWXkn0A9INY4uKC8m37VTQA24dkrQ', '2018-04-09 03:27:21', '2018-04-09 03:27:21'),
(637, 1, 'KwstnH2z1JiLqNA02CLkRJk7wGb3Kjya', '2018-04-10 03:27:59', '2018-04-10 03:27:59'),
(638, 1, 'L6LAtQAg7CUDNFVerYCaYj01P6pcBucf', '2018-04-18 04:16:09', '2018-04-18 04:16:09'),
(639, 1, 'tYtIfTpz1g9CazngFGqbnqN5JKoXBiQy', '2018-04-26 03:19:44', '2018-04-26 03:19:44'),
(640, 1, 'xQzzoBqnNJEsEfePYqLpEnkySBFDmO1k', '2018-04-26 03:24:22', '2018-04-26 03:24:22'),
(641, 1, 'UWPJsvsp78QapukNXbiOa7cDUr9m5Kcm', '2018-04-26 04:19:22', '2018-04-26 04:19:22'),
(642, 1, 'r3KWjYZnUBc6wYYnYpVz9CytqIj3ErI6', '2018-05-02 04:10:17', '2018-05-02 04:10:17'),
(643, 1, '4ZBatf2E8Rr6F9mIVuDj7tNDSDi8w0ye', '2018-05-03 04:18:30', '2018-05-03 04:18:30'),
(644, 1, 'iYRuq4HVbM7AiSAJXT0vRoEDN1tqemAe', '2018-05-05 07:49:30', '2018-05-05 07:49:30'),
(645, 1, 'K1BJ0oKdq1YkKgxFgCRBRUYyvG4LaiSd', '2018-05-05 20:35:19', '2018-05-05 20:35:19'),
(647, 1, '0fmvIweHpH4ahxu5ppZT7HFGWEPWGwyU', '2018-05-07 19:26:38', '2018-05-07 19:26:38'),
(648, 1, 'YQ9FVFgV747fInT39QkIdEzZhVXFF1Ci', '2018-05-07 20:27:43', '2018-05-07 20:27:43'),
(649, 1, 'nZ1nEuHVI3zsAaPwVHlhDHbBw0BEQQmr', '2018-05-11 02:44:35', '2018-05-11 02:44:35'),
(650, 9, 'X4WKWpLuJQbNd8yF9DVGH0NnVpQFoLCc', '2018-05-12 02:41:24', '2018-05-12 02:41:24'),
(651, 9, 'WmGbJ3EHxPsXQrXm6boaeQMH8nIuQVXO', '2018-05-12 03:30:08', '2018-05-12 03:30:08'),
(652, 9, 'qZmC462cld5WmJGTiiq6oxSFJs4uCW9s', '2018-05-13 03:07:56', '2018-05-13 03:07:56'),
(653, 9, 'odvFTAYb1dUTpofL0vSaL5ODG3EtsAUZ', '2018-05-13 03:22:58', '2018-05-13 03:22:58'),
(654, 9, 'fyQYhO0wM7pf77kUpAEotCTxmhvbBi94', '2018-05-13 17:47:13', '2018-05-13 17:47:13'),
(657, 9, '19PDnbxEC2xuwrF08lpZg4tj1iN2qRmT', '2018-05-14 03:57:53', '2018-05-14 03:57:53'),
(658, 9, 'W2PoE5yjnaVTiKhrHuBsMMP6ZExp1lO4', '2018-05-14 03:58:32', '2018-05-14 03:58:32'),
(659, 9, 'SvYvqwUHcQsulbk2akFR3spjXSLH3ioI', '2018-05-14 06:28:47', '2018-05-14 06:28:47'),
(660, 9, 'YIH90n5pDhU0aWA9UZdYRXFuqQlvqjDM', '2018-05-14 19:36:23', '2018-05-14 19:36:23'),
(661, 9, 'lBSOxAZ3DcnHy2KdZ9BlpLmTlAcBx2PK', '2018-05-14 20:43:22', '2018-05-14 20:43:22'),
(662, 9, 'aarLsARyIwokEiYhECnfvu1d43OViGwD', '2018-05-14 20:57:21', '2018-05-14 20:57:21'),
(663, 9, 'Ymrtm0waiwiao1kcLlBH0LolfaeXubWc', '2018-05-16 17:47:51', '2018-05-16 17:47:51'),
(664, 9, 'hItXTOWB3UvHzk7dqpFzJ8pZ4vcQOrj9', '2018-05-17 00:38:09', '2018-05-17 00:38:09'),
(665, 9, 'FQVoM2aV0br9g5O1ZSaM7JLSGRtifxfi', '2018-05-17 16:21:15', '2018-05-17 16:21:15'),
(666, 9, '6vC9Pzte395u8w5tIICbGxM3GRLfzZlp', '2018-05-18 18:14:03', '2018-05-18 18:14:03'),
(667, 9, 'ixPPGOTTzF3OMs8pm0M8OpHXiEf4LNsE', '2018-05-19 00:21:33', '2018-05-19 00:21:33'),
(668, 9, 'tIvJ4wpZPCl8FibxoJbN2gijxFUWmpYu', '2018-05-21 00:41:39', '2018-05-21 00:41:39'),
(669, 9, '1xo9OTVpamBdjaQtF5uLyPscSSaBkSZS', '2018-05-21 00:55:30', '2018-05-21 00:55:30'),
(670, 9, 'X5jubthq2cf0cbSmexaaESmAPcKbFq5B', '2018-05-21 22:01:16', '2018-05-21 22:01:16'),
(672, 9, 'zGhmobmZ5QzaMxrTigeteLFffOY7oQre', '2018-05-21 22:53:29', '2018-05-21 22:53:29'),
(674, 9, '8Sd6Fqd4fy9BMZnEF7BiUFpGVn95ltKB', '2018-05-22 00:43:08', '2018-05-22 00:43:08'),
(675, 9, 'nSd4SZfp5wGvOENikWxr8cm0f4WJqqhN', '2018-05-22 00:46:08', '2018-05-22 00:46:08'),
(676, 9, 'mo73auPuBe1WLm5HJQff1KxxvYPquJYX', '2018-05-22 00:49:42', '2018-05-22 00:49:42'),
(677, 9, 'VYVWyhduIxOMHCOov9YQxXNhamwsPC21', '2018-05-22 02:13:32', '2018-05-22 02:13:32'),
(678, 9, 'lUYSSggsqMyldyFqMzyeT8bKl3Lv2iRW', '2018-05-22 17:48:00', '2018-05-22 17:48:00'),
(679, 9, 'iaFHDDr4kel4fcE66KdAcOBeWxru0dxf', '2018-05-23 02:25:31', '2018-05-23 02:25:31'),
(680, 9, '428rtZ1u02VdfSJuJPeHa5FrzlcDHik5', '2018-05-23 16:09:26', '2018-05-23 16:09:26'),
(681, 9, 'xgldfcNSWhbIbMY9rSllJBVuSsfKrFEN', '2018-05-24 18:02:28', '2018-05-24 18:02:28'),
(682, 9, 'Xth92hCzYOAqIvoEHKEVIXaD1YWuS9D1', '2018-05-25 19:24:38', '2018-05-25 19:24:38'),
(683, 9, 'MVLkWtRsbzdONodFSo5CxUMrZ2idw5vn', '2018-05-26 02:54:29', '2018-05-26 02:54:29'),
(684, 9, 'Uz9BNtKLCNWILdsDEBUWRejSDtWV5ACV', '2018-05-26 17:57:01', '2018-05-26 17:57:01'),
(685, 9, 'UHUTUFzsC8dPJj3bx9clJUGEwMuHV4Un', '2018-05-26 19:01:52', '2018-05-26 19:01:52'),
(686, 9, 'e1mVn7Huepy6zVo9VAVVvqNS5xi6Masf', '2018-05-26 21:23:45', '2018-05-26 21:23:45'),
(687, 9, 'hqOmEdrJwHErhxQqQ3gZIOYtnkgvMd5F', '2018-05-27 00:49:04', '2018-05-27 00:49:04'),
(688, 9, '9SZS2EzXqzFUtMikd4tPDuyo2QLlrx6R', '2018-05-27 17:10:36', '2018-05-27 17:10:36'),
(689, 9, '05dECTp7TQawfbBKiOS4ToUOJVYmXmZ8', '2018-05-27 17:13:51', '2018-05-27 17:13:51'),
(690, 1, 'bObk9AyDsFjTWkxRfzbwCZwtconp2eli', '2018-05-27 17:26:28', '2018-05-27 17:26:28'),
(691, 1, 'iD79sTjzwi5HpI3BLncPl4LWVRSn5yfZ', '2018-05-27 19:09:50', '2018-05-27 19:09:50'),
(692, 9, 'weDYYpbXkrnZkYliPigqYgwmliUn0e3c', '2018-05-28 01:13:57', '2018-05-28 01:13:57'),
(693, 9, 'Wdxh7FvR5i9NsEqIt7JLrcXUVizLJYNW', '2018-05-28 05:55:51', '2018-05-28 05:55:51'),
(694, 9, 'fDF6oyGmWSBk7jaQlrEdsrOexE1p5B2W', '2018-05-29 18:16:57', '2018-05-29 18:16:57'),
(695, 9, '7TP0m2GRRhNlDIqmopo8MYhjC4Cl9lqR', '2018-05-30 06:30:08', '2018-05-30 06:30:08'),
(696, 9, 'aQtOiC0dkGgsOW8AzFgg02A61VZgVdDW', '2018-05-30 21:08:48', '2018-05-30 21:08:48'),
(697, 9, 'Hepu1wO9SBhNLTC0DeTrWHm3iBP8m4CH', '2018-05-30 23:44:48', '2018-05-30 23:44:48'),
(698, 9, 'JBztphQfzY7Ob1p3CEroikpP2KLCn7Gc', '2018-05-31 02:24:10', '2018-05-31 02:24:10'),
(699, 9, 'NMyDteJoHFLbjleMJ6iF4vqDWs0EXYUT', '2018-06-01 22:03:28', '2018-06-01 22:03:28'),
(700, 9, 'tsXSCxUfKjITxwX3p5Uc9nytIkNCklhu', '2018-06-01 23:53:27', '2018-06-01 23:53:27'),
(701, 9, 'VpLMNzKawgRdyilHngtKGVkeUDAogznm', '2018-06-02 20:27:27', '2018-06-02 20:27:27'),
(702, 9, 'RiBh7S1YpifxjYij9PZ39Y8eS14xIqkd', '2018-06-05 01:38:57', '2018-06-05 01:38:57'),
(703, 9, 'OWEpKarC3t21pLVc3hxuEf3HzgWqOQgS', '2018-06-06 16:43:05', '2018-06-06 16:43:05'),
(704, 9, 'q4iYVAh8sB5IAXuzH5vnYVYWrb0kZHNV', '2018-06-07 04:42:39', '2018-06-07 04:42:39'),
(705, 9, 'GuP3XEOedz158VfTG3UQ3gyrMUkX9sIO', '2018-06-07 19:22:51', '2018-06-07 19:22:51'),
(706, 9, 'w2EsppaK0MjgbLcEgii14O2XEsxtvRWt', '2018-06-08 04:03:38', '2018-06-08 04:03:38'),
(707, 9, 'UrYCzEYNC5JprWAXiIAtN1GOYrmY4XoT', '2018-06-08 21:05:14', '2018-06-08 21:05:14'),
(708, 9, '8GCPdcoCKfLnMPE2I64EllPgmvlEEY0w', '2018-06-08 21:46:13', '2018-06-08 21:46:13'),
(711, 1, 'HHT5Pta6BNDdUTPW9k6ynxfX0rbTSPyo', '2018-06-10 14:33:25', '2018-06-10 14:33:25'),
(712, 9, 'N6zv71eF2asEzX4uCLRHuCjN9BWR64LT', '2018-06-10 15:00:54', '2018-06-10 15:00:54'),
(713, 9, 'cteKGZ4iq7epmjqcrlUqp2aH3SeRGDJu', '2018-06-11 18:36:27', '2018-06-11 18:36:27'),
(714, 9, '783IXgjDrkdI7xLseccnDcVRluHPgbNL', '2018-06-11 18:36:45', '2018-06-11 18:36:45'),
(715, 9, 'Ve2QksBjrFnwx6HCIXG7gMbeDd0UdWiP', '2018-06-12 16:55:45', '2018-06-12 16:55:45'),
(716, 9, 'Uh4GsL2ZTcf3qQ3dWy7nwFERx4QzkTVq', '2018-06-13 17:53:20', '2018-06-13 17:53:20'),
(717, 9, '5gFKbiZ4uRCcrhcsHQgYS19wfqkq5VxD', '2018-06-13 21:09:09', '2018-06-13 21:09:09'),
(718, 9, 'MG88uqBc2Sme4mehom1cdWlIA1TPrAqm', '2018-06-14 00:12:55', '2018-06-14 00:12:55'),
(719, 9, 'thyCOCAqUykA6GQbXHtEEdjsanIrdTHO', '2018-06-14 19:24:45', '2018-06-14 19:24:45'),
(720, 9, '7Dd7Qun2cH7LlgtJMBHJuwCsYCzLZHfS', '2018-06-14 23:27:50', '2018-06-14 23:27:50'),
(721, 9, 'YP1vXguusMJCB7OOAvzSSeE9Lzyo6LcZ', '2018-06-15 05:49:26', '2018-06-15 05:49:26'),
(723, 9, 'svLMfQYP1DNUuL0zXikMSNGyUSKFL8WT', '2018-06-16 09:18:28', '2018-06-16 09:18:28'),
(724, 9, '4ExNzyejkrEkmnlpOpKN4Du23xuKCEwG', '2018-06-16 13:51:28', '2018-06-16 13:51:28'),
(725, 9, 'p7lMF8KwjQvtsLqMQCgEIWe5OkidiJ9s', '2018-06-16 17:16:18', '2018-06-16 17:16:18'),
(726, 9, 'cSHcRNXFcwmu0nVQaISBmPnZ4Qbt5ucB', '2018-06-16 22:04:50', '2018-06-16 22:04:50'),
(727, 9, 'BLkjmnSoZ9uErNCDThNbBmBQZMrow08a', '2018-06-20 01:21:14', '2018-06-20 01:21:14'),
(728, 9, 'OHg1H1MU9puJnywl5l25hg1xTaMxNHVk', '2018-06-20 02:27:24', '2018-06-20 02:27:24'),
(729, 9, 'xS5paetAA7lhryAJnOmqcs2AfGXNfl6N', '2018-06-22 07:25:17', '2018-06-22 07:25:17'),
(730, 9, 'gmiURBI5Zyuk2iXP3tqVcyrtTVEQCnxw', '2018-06-22 17:37:42', '2018-06-22 17:37:42'),
(732, 9, 'HUZmdlQzYnw4JsB0GNxSUY0gehgyC86m', '2018-06-23 15:18:34', '2018-06-23 15:18:34');
INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(733, 9, 'MpgfBxJqYCmGsFXteuygas8VE0Qw6lDx', '2018-06-26 02:54:12', '2018-06-26 02:54:12'),
(734, 9, 'V9EMPAFjrYt7uaKE3WB2LfaA87ISdoPk', '2018-06-26 14:51:08', '2018-06-26 14:51:08'),
(735, 9, 'spjdn5MAWSctQHdEqlFm0wjC2e5WKYVh', '2018-06-27 22:12:56', '2018-06-27 22:12:56'),
(736, 9, 'UMpyojtjErdM7na15jViPJWzY2tNiRim', '2018-06-29 20:45:56', '2018-06-29 20:45:56'),
(737, 9, 'E8oOrxYC7qrhqDOWnTWeGx0BP2tk06fg', '2018-06-29 21:43:59', '2018-06-29 21:43:59'),
(738, 9, 'NkrljapelkeHlR8A1DZvwD1L3jDcejtl', '2018-06-30 03:55:02', '2018-06-30 03:55:02'),
(739, 9, 'XhGj2beootX7tMkt7chryC6noey9XF0z', '2018-06-30 04:33:19', '2018-06-30 04:33:19'),
(740, 9, '2QyyqdZJOwSzBXraywOPszYvIirinBTX', '2018-06-30 20:31:27', '2018-06-30 20:31:27'),
(741, 9, 'OMcBHDy2jA5myDkLmeN7TjgPWGJdE0Sr', '2018-07-04 20:21:58', '2018-07-04 20:21:58'),
(742, 9, 'xHdh0mtKZf7AHK1fNRATSHo7QARsWLWY', '2018-07-04 23:58:21', '2018-07-04 23:58:21'),
(744, 9, 'SLr1hAJKv04hHU0uJIvPOGhL0nBXgRHC', '2018-07-05 15:31:29', '2018-07-05 15:31:29'),
(745, 9, 'BnwFTb5NvfivEUWfTS8zbe86M2FrCKut', '2018-07-05 17:41:53', '2018-07-05 17:41:53'),
(746, 9, 'Lts5n545MrA3Gu6PnYBcsDJummp4Y9ly', '2018-07-05 20:58:28', '2018-07-05 20:58:28'),
(748, 9, 'Ab30o5DzoAEkHFqZEebtzQoBCzL1U366', '2018-07-06 06:51:24', '2018-07-06 06:51:24'),
(749, 9, 'ibUg3tU5wz8UKrUBkxna0goHSp9SQegi', '2018-07-06 23:24:10', '2018-07-06 23:24:10'),
(751, 9, 'Uji4LEjHlMom7ILfqJU8Bq06HXWaVTo3', '2018-07-07 18:22:59', '2018-07-07 18:22:59'),
(752, 9, 'BsGOnv1kUxJXqBYTfqeREuBJRNpr24Wh', '2018-07-08 17:00:03', '2018-07-08 17:00:03'),
(753, 9, 'ajJSTukt3kLHx9dDxwL9kv9uwFr8cAlg', '2018-07-08 17:11:48', '2018-07-08 17:11:48'),
(754, 9, 't106WBCLGyTSsUv69l1EqMKguDox6uGI', '2018-07-11 00:45:12', '2018-07-11 00:45:12'),
(755, 9, 'razjjQLsVXpefUAPBtHmsZxLlCZg4iRn', '2018-07-12 16:27:29', '2018-07-12 16:27:29'),
(756, 9, 'RVleoA78hXko8ZyzXHacu7YnEAlqnlWL', '2018-07-12 22:16:59', '2018-07-12 22:16:59'),
(757, 9, 'aRwOyVS3pETVTNXnEF8m8xKTzk21LNLE', '2018-07-13 17:01:57', '2018-07-13 17:01:57'),
(758, 9, 'hitHa96dMFUajABLkoubrB1wZLRRWZVI', '2018-07-13 20:13:05', '2018-07-13 20:13:05'),
(759, 9, 'ljFggJZ2pPmnFFaa9WKc8yDEAONNFgeD', '2018-07-16 22:24:48', '2018-07-16 22:24:48'),
(760, 9, 'eQOCKaUftgrzcq8rD97EvkTZl975OxXs', '2018-07-18 05:44:27', '2018-07-18 05:44:27'),
(761, 9, 'ShSiqWME5yKt9xlMjLk60N7MQRKd72Nb', '2018-07-18 20:32:28', '2018-07-18 20:32:28'),
(762, 9, 'S8yzNyWFB9fhTugTgFHR7MhujCXTORf2', '2018-07-19 15:14:41', '2018-07-19 15:14:41'),
(763, 9, '69msAqNyEoONo5RfNvEWpjvYxkmtykHS', '2018-07-19 16:36:14', '2018-07-19 16:36:14'),
(764, 9, 'gZU95o6HdO2ZgHZg5bmQMUVVp3xGiBHq', '2018-07-19 18:51:46', '2018-07-19 18:51:46'),
(765, 9, 'FNt2rCkE9ccoUcsF5bRC0tNstcSskUgO', '2018-07-19 22:59:40', '2018-07-19 22:59:40'),
(766, 9, 'RQqxkaiCsBefNRYG9dhagN3owiBYEufE', '2018-07-20 22:59:04', '2018-07-20 22:59:04'),
(767, 9, 'QXzoqTnOtEX9Eb4lwfv2mOmdZUD8RFc1', '2018-07-21 15:49:25', '2018-07-21 15:49:25'),
(768, 9, 'RrN6UAl6soolzVQvfljNBVvDHYRe4jvV', '2018-07-24 05:54:16', '2018-07-24 05:54:16'),
(769, 9, 'jGzZDUnvYlyHa3V5HzZJwW3aPhJrug5n', '2018-07-24 18:40:01', '2018-07-24 18:40:01'),
(770, 9, 'ZKP2N9NpwHGwcsUjjocjNZhrFjMh33RY', '2018-07-25 03:35:52', '2018-07-25 03:35:52'),
(771, 9, 'lINtYOads2tj03oO5ldJ0Q9W3ifzLNth', '2018-07-25 16:06:38', '2018-07-25 16:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'registered', '    Registered    ', '{\"front\":true,\"my-account\":true}', 1, '2015-10-26 16:46:31', '2015-12-19 10:24:02'),
(2, 'administrator', '      Administrator      ', '{\"admin\":true,\"my-account\":true}', 1, '2015-12-19 07:00:47', '2015-12-19 10:27:50'),
(3, 'sdc-admin', 'SDC ADMIN', '{\"admin\":true}', 1, '2017-01-23 19:34:57', '2017-09-11 20:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(9, 2, '2018-05-14 02:58:58', '2018-05-14 02:58:58'),
(9, 3, '2018-05-14 02:58:58', '2018-05-14 02:58:58'),
(10, 1, '2017-12-28 02:40:24', '2017-12-28 02:40:24'),
(11, 1, '2018-04-04 02:52:12', '2018-04-04 02:52:12'),
(11, 3, '2018-04-04 02:52:13', '2018-04-04 02:52:13');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `expire_date` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `image`, `url`, `description`, `expire_date`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'Dahata Nopeni Inna - Prasanna Thakshila', '/core/storage/uploads/images/slider/slider-20180616015109.jpg', 'http://www.desawana.com/video/30', '<p>-</p>', '2018-09-08', 9, '2018-06-15 20:21:09', '2018-06-15 20:21:09'),
(5, 'Aulak Nane - Nalin Wijayasinghe Official Music Video', '/core/storage/uploads/images/slider/slider-20180616150810.jpg', 'http://www.desawana.com/video/34', '', '2018-06-30', 9, '2018-06-16 22:08:10', '2018-07-05 07:04:49'),
(6, 'Cybertech Int Advertisement', '/core/storage/uploads/images/slider/slider-20180622003226.png', 'https://www.facebook.com/cybertechInt.lk/', '', '2018-12-31', 9, '2018-06-22 07:32:26', '2018-06-22 07:32:26'),
(7, 'Obe Adare - Sameera Chathuranga Official Music Video', '/core/storage/uploads/images/slider/slider-20180625200340.jpg', 'http://www.desawana.com/video/36', '', '2018-07-07', 9, '2018-06-26 03:03:40', '2018-06-26 03:06:37');

-- --------------------------------------------------------

--
-- Table structure for table `slider_song`
--

CREATE TABLE `slider_song` (
  `slider_id` int(10) UNSIGNED NOT NULL,
  `song_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider_song`
--

INSERT INTO `slider_song` (`slider_id`, `song_id`) VALUES
(2, 36),
(5, 46),
(6, 11),
(7, 40);

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE `songs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `album_art` text COLLATE utf8_unicode_ci NOT NULL,
  `video_id` int(10) UNSIGNED DEFAULT NULL,
  `lyrics_id` int(10) UNSIGNED DEFAULT NULL,
  `audio_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `audio_download` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `title`, `album_art`, `video_id`, `lyrics_id`, `audio_id`, `created_at`, `updated_at`, `old`, `user_id`, `meta_description`, `meta_keywords`, `audio_download`) VALUES
(1, 'Man Pathanawa (Samu Aran Ya Yuthui)', 'core/storage/uploads/images/album_art/album_art-20180511195140.png', 1, 0, 3, '2018-05-12 02:51:40', '2018-07-26 21:19:13', 1, 9, '', '', 61),
(2, 'Heenayakda Me', 'core/storage/uploads/images/album_art/album_art-20180511204012.png', 0, 0, 4, '2018-05-12 03:40:12', '2018-07-26 14:23:06', 0, 9, '', '', 78),
(3, 'Saththai Oya (Man Witharada Adare Kale)', 'core/storage/uploads/images/album_art/album_art-20180511212003.png', 2, 0, 5, '2018-05-12 04:20:03', '2018-07-26 21:19:43', 0, 9, '', '', 82),
(6, 'Me Nihanda Bhawaye', 'core/storage/uploads/images/album_art/album_art-20180513204121.png', 0, 0, 8, '2018-05-14 03:41:21', '2018-07-26 16:13:19', 0, 9, '', '', 88),
(7, 'Nolabena Senehe New Version', 'core/storage/uploads/images/album_art/album_art-20180513204750.png', 0, 0, 9, '2018-05-14 03:47:50', '2018-07-26 21:21:15', 0, 9, '', '', 226),
(8, 'Kawadahari Wetahei', 'core/storage/uploads/images/album_art/album_art-20180513210702.png', 0, 0, 7, '2018-05-14 04:07:02', '2018-07-26 21:23:09', 0, 9, '', '', 90),
(9, 'Oya Nathuwa Dannam Mata', 'core/storage/uploads/images/album_art/album_art-20180513211328.png', 0, 0, 10, '2018-05-14 04:13:28', '2018-07-26 21:20:56', 0, 9, '', '', 190),
(10, 'Adarei Katawath Nathi Tharam Kiya', 'core/storage/uploads/images/album_art/album_art-20180514125657.png', 5, 0, 11, '2018-05-14 19:56:57', '2018-07-25 20:19:40', 0, 9, '', '', 56),
(11, 'Adarei Wasthu', 'core/storage/uploads/images/album_art/album_art-20180514131210.png', 4, 0, 12, '2018-05-14 20:12:10', '2018-07-23 10:48:27', 0, 9, '', '', 67),
(12, 'Sudu (Mathu Dineka)', 'core/storage/uploads/images/album_art/album_art-20180521170338.png', 6, 0, 15, '2018-05-22 00:03:38', '2018-07-26 17:05:41', 0, 9, '', '', 435),
(13, 'Sansara Sihine', 'core/storage/uploads/images/album_art/album_art-20180521181303.png', 0, 0, 16, '2018-05-22 01:13:03', '2018-07-23 09:57:27', 0, 9, '', '', 55),
(14, 'Saaritha', 'core/storage/uploads/images/album_art/album_art-20180521184134.png', 38, 0, 17, '2018-05-22 01:41:34', '2018-07-25 19:58:53', 0, 9, 'Saaritha Viraj Perera Official Music Video 2018', 'viraj perera, saritha, saaritha, viraj perera new song, mage adarema aparade, saritha viraj perera, sinhala new songs, new sinhala song, official music video, viraj new song, new music videos 2018, new sinhala songs 2018, saaritha viraj, saritha viraj perera video song download, saritha viraj perera, saritha viraj perera mp4, saritha viraj perera lyrics, saritha viraj perera live, saritha viraj perera song lyrics, saritha viraj perera dj mp3,', 252),
(15, 'Saragaye (Niya Rata Mawanawa)', 'core/storage/uploads/images/album_art/album_art-20180521191255.png', 7, 0, 18, '2018-05-22 02:12:55', '2018-07-25 20:16:45', 0, 9, '', 'sanuka wickramasinghe mp3 download, sansara sihine download mp3, Saragaye mp3,', 114),
(17, 'Akeekaruma Man Udura', 'core/storage/uploads/images/album_art/album_art-20180525124022.png', 0, 0, 13, '2018-05-25 19:40:22', '2018-07-22 14:04:08', 1, 9, '', '', 64),
(18, 'Waradak Kiyanne Na', 'core/storage/uploads/images/album_art/album_art-20180526144125.png', 9, 0, 14, '2018-05-26 21:41:25', '2018-07-23 10:06:06', 0, 9, '', '', 63),
(19, 'Ma Hara Giya Dine', 'core/storage/uploads/images/album_art/album_art-20180529115202.png', 0, 0, 19, '2018-05-29 18:52:02', '2018-07-16 23:02:11', 1, 9, '', 'dimanka wellalage, sajith v chathuranga, dimanka, radeesh vandabona, dimanka wellalage songs, music video, dimanka wellalage songs youtube, dimanka wellalage songs lyrics, dimanka wellalage songs chords, dimanka wellalage songs ananmanan.lk, dimanka wellalage video songs, dimanka wellalage new video songs, dimanka wellalage dj songs, dimanka wellalage new video 2017, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show,', 10),
(20, 'Amathaka Karanna Ba', 'core/storage/uploads/images/album_art/album_art-20180529115805.png', 10, 0, 20, '2018-05-29 18:58:06', '2018-07-18 17:41:21', 1, 9, '', '', 20),
(21, 'Arayum Karanne', 'core/storage/uploads/images/album_art/album_art-20180529120634.png', 28, 0, 21, '2018-05-29 19:06:34', '2018-06-30 04:46:35', 1, 9, '', '', 3),
(22, 'Awasan Pema Mage', 'core/storage/uploads/images/album_art/album_art-20180529125613.png', 11, 0, 22, '2018-05-29 19:56:13', '2018-07-23 10:09:41', 1, 9, '', '', 12),
(23, 'Durin Hinda Ma', 'core/storage/uploads/images/album_art/album_art-20180529130225.png', 12, 0, 23, '2018-05-29 20:02:25', '2018-07-23 10:09:14', 1, 9, '', '', 13),
(24, 'Kiyabu Lathawe', 'core/storage/uploads/images/album_art/album_art-20180529131455.png', 13, 0, 24, '2018-05-29 20:14:55', '2018-07-25 16:52:21', 0, 9, '', '', 65),
(25, 'Ma Me Tharam Handawala', 'core/storage/uploads/images/album_art/album_art-20180529132514.png', 15, 0, 26, '2018-05-29 20:25:15', '2018-07-16 23:03:46', 1, 9, '', 'Ma Me Tharam Handawala, Ma Me Tharam Handawala mp3, Ma Me Tharam Handawala mp3 download, Ma Me Tharam Handawala song, ', 7),
(26, 'Me Hitha Langa', 'core/storage/uploads/images/album_art/album_art-20180529132958.png', 16, 0, 27, '2018-05-29 20:29:58', '2018-06-30 05:36:13', 1, 9, '', 'Me Hitha Langa, Me Hitha Langa song, Me Hitha Langa song download, Me Hitha Langa mp3, Me Hitha Langa mp3 download,  ', 16),
(27, 'Nidukin Inu Mana', 'core/storage/uploads/images/album_art/album_art-20180529133205.png', 17, 0, 28, '2018-05-29 20:32:05', '2018-07-07 22:40:04', 1, 9, '', '', 4),
(28, 'Oba Ekka Mama', 'core/storage/uploads/images/album_art/album_art-20180529134744.png', 18, 0, 29, '2018-05-29 20:47:44', '2018-06-30 05:30:37', 1, 9, '', 'Oba Ekka Mama, Oba Ekka Mama song, Oba Ekka Mama song download, Oba Ekka Mama song mp3, Oba Ekka Mama song download, ', 5),
(29, 'Obamada Me Hithata Mage', 'core/storage/uploads/images/album_art/album_art-20180529134946.png', 19, 0, 30, '2018-05-29 20:49:46', '2018-06-30 05:28:38', 1, 9, '', 'Obamada Me Hithata Mage, Obamada Me Hithata Mage song, Obamada Me Hithata Mage mp3, Obamada Me Hithata Mage song mp3, Obamada Me Hithata Mage song mp3 download, ', 3),
(30, 'Para Kiyana Tharukawi', 'core/storage/uploads/images/album_art/album_art-20180529135638.png', 20, 0, 31, '2018-05-29 20:56:38', '2018-06-30 05:16:09', 1, 9, '', 'Para Kiyana Tharukawi, Para Kiyana Tharukawi song, Para Kiyana Tharukawi mp3, Para Kiyana Tharukawi song download, Para Kiyana Tharukawi mp3 song download, ', 7),
(31, 'Senehasa Bidunath', 'core/storage/uploads/images/album_art/album_art-20180529135741.png', 21, 0, 32, '2018-05-29 20:57:41', '2018-06-30 05:08:54', 1, 9, '', 'Senehasa Bidunath, Senehasa Bidunath song, Senehasa Bidunath mp3,', 5),
(32, 'Awasana Premayai Mage', 'core/storage/uploads/images/album_art/album_art-20180529135849.png', 22, 0, 33, '2018-05-29 20:58:49', '2018-07-23 10:07:04', 0, 9, '', '', 33),
(33, 'Maa Nisa Pa Sina', 'core/storage/uploads/images/album_art/album_art-20180529140419.png', 0, 0, 34, '2018-05-29 21:04:19', '2018-07-05 17:27:32', 1, 9, '', 'Maa Nisa Pa Sina, Maa Nisa Pa Sina song, Maa Nisa Pa Sina mp3, Maa Nisa Pa Sina mp3 download, Maa Nisa Pa Sina song video, ', 15),
(34, 'Wenna Thiyena', 'core/storage/uploads/images/album_art/album_art-20180529140526.png', 24, 0, 35, '2018-05-29 21:05:26', '2018-06-30 05:02:56', 1, 9, '', '', 36),
(35, 'Diwranna Behe Neda', 'core/storage/uploads/images/album_art/album_art-20180530141724.png', 29, 0, 36, '2018-05-30 21:17:24', '2018-07-26 14:53:04', 0, 9, '', 'Diwranna Behe Neda Thushara Joshap, Diwranna Behe Neda Thushara Sandakelum, Diwranna Behe Neda (Hadawathe Niruwatha), Thushara Sandakelum New Song, Thushara New Songs, Sahara Flash, Diwranna Behe Neda Mp3, Diwranna Behe Neda Mp3 Download', 3730),
(36, 'Dahata Nopeni Inna', 'core/storage/uploads/images/album_art/album_art-20180601155328.png', 30, 2, 37, '2018-06-01 22:53:28', '2018-07-26 13:06:49', 0, 9, 'Kind of a song that will wet your tears sung by Prasanna Thakshila to a heart melting melody of Rusiru Dulshan and lyrics by Yasith Dulshan...', 'Dahata Nopeni Inna, Dahata Nopeni Inna song, Dahata Nopeni Inna song download, Dahata Nopeni Inna video song, Dahata Nopeni Inna song lyrics, Dahata Nopeni Inna song music, sinhala new songs, sinhala new songs 2018', 579),
(37, 'Oyata Vitharak', 'core/storage/uploads/images/album_art/album_art-20180602145937.png', 32, 3, 38, '2018-06-02 21:59:38', '2018-07-25 21:33:06', 0, 9, 'Oyata Vitharak - Ishan Priyasanka Official Music Video 2018', 'Oyata Vitharak, Oyata Vitharak song, Oyata Vitharak mp3, Oyata Vitharak song mp3 download, Oyata Vitharak song video, ', 780),
(38, 'Miya Yanna Sudanam', 'core/storage/uploads/images/album_art/album_art-20180602173433.png', 0, 0, 40, '2018-06-03 00:34:33', '2018-07-26 06:20:12', 0, 9, '', '', 375),
(39, 'Hitha Gawa Heena', 'core/storage/uploads/images/album_art/album_art-20180602174528.png', 31, 0, 39, '2018-06-03 00:45:28', '2018-07-25 19:48:39', 1, 9, '', '', 484),
(40, 'Obe Adare', 'core/storage/uploads/images/album_art/album_art-20180606102518.png', 36, 6, 41, '2018-06-06 17:25:18', '2018-07-23 14:43:37', 0, 9, '', 'Obe Adare Sameera Chathuranga mp3, Obe Adare Sameera Chathuranga mp3 download, Sameera Chathuranga mp3, Obe Adare mp3, Obe Adare mp3 download, ahimi u obe adare song, ahimi u obe adare song download, ahimi u obe adare song mp3 download, ', 301),
(41, 'Mathaka Mawee', 'core/storage/uploads/images/album_art/album_art-20180606120259.png', 33, 4, 42, '2018-06-06 19:03:00', '2018-07-26 12:24:38', 0, 9, '', '', 901),
(42, 'Pem Karala', 'core/storage/uploads/images/album_art/album_art-20180606160604.png', 0, 0, 43, '2018-06-06 23:06:04', '2018-07-23 23:19:45', 0, 9, '', 'Pem Karala Udesh Nilanga song download, Pem Karala Udesh Nilanga mp3 song download, Pem Karala song download, Pem Karala Udesh mp3 download', 264),
(43, 'Anthima Mohothedi', 'core/storage/uploads/images/album_art/album_art-20180607132655.png', 40, 0, 45, '2018-06-07 20:26:55', '2018-07-26 18:16:19', 0, 9, 'Anthima Mohothedi - Nilan Hettiarachchi Official Music Video & Audio 2018', 'antima mohothedi nilan hettiarachchi mp3, antima mohothedi nilan hettiarachchi mp3 download, antima mohothedi song, antima mohothedi song download, antima mohothedi mp3 song download, antima mohothedi video song,', 187),
(44, 'Wirasakawee', 'core/storage/uploads/images/album_art/album_art-20180608141544.png', 0, 7, 44, '2018-06-08 21:15:45', '2018-07-23 14:46:31', 0, 9, '', 'Wirasakawee song, Wirasakawee song mp3, Wirasakawee song download, ', 262),
(45, 'Pemwathiye', 'core/storage/uploads/images/album_art/album_art-20180614230911.png', 0, 0, 46, '2018-06-15 06:09:11', '2018-07-25 13:42:43', 0, 9, '', 'Pemwathiye song, Pemwathiye song mp3, Pemwathiye song download, Pemwathiye mp3 song download, Pemwathiye song video, Asanjaya Imashath songs, Asanjaya Imashath Pemwathiye, Pemwathiye Asanjaya mp3,', 234),
(46, 'Aulak Nane', 'core/storage/uploads/images/album_art/album_art-20180614232833.png', 34, 0, 47, '2018-06-15 06:28:33', '2018-07-25 20:20:13', 0, 9, '', 'Aulak Nane song, Aulak Nane song mp3, Aulak Nane song download, Aulak Nane mp3 download, awlak nane song, awlak nane song download, ', 154),
(47, 'Sulan Podak Wee', 'core/storage/uploads/images/album_art/album_art-20180614235834.png', 35, 0, 48, '2018-06-15 06:58:34', '2018-07-23 08:50:34', 0, 9, '', '', 64),
(48, 'Sammatheta Pitupe', 'core/storage/uploads/images/album_art/album_art-20180619183209.png', 0, 8, 49, '2018-06-20 01:32:09', '2018-07-26 21:26:07', 0, 9, '', 'Sammatheta Pitupe, Sammatheta Pitupe mp3, Sammatheta Pitupe Mp3 Download, Sammatheta Pitupe Song Mp3, Sammatheta Pitupe song download, Sammatheta Pitupe Jude Rogans, google, youtube, original, official, new, srilankan, sinhala, song, chords, lyrics, mp3 free download, live show, Sammatheta Pitupe Jude Rogans mp3 download, Sammatheta Pitu pa, Sammatheta Pitu pa Jude Rogans, Sammatheta Pitu pa Jude Rogans song, Sammatheta Pitu pa Jude Rogans song download, Sammatheta Pitu pa Jude Rogans mp3, Sammatheta Pitu pa Jude Rogans mp3 download, Sammatheta Pitu pa mp3 download', 178),
(49, 'Nindath Hora Gaththa Oya', 'core/storage/uploads/images/album_art/album_art-20180622105705.png', 0, 0, 50, '2018-06-22 17:57:05', '2018-07-26 15:11:38', 0, 9, '', 'Nindath Hora Gaththa Oya song, Nindath Hora Gaththa Oya song download, Nindath Hora Gaththa Oya mp3 download, Nindath Hora Gaththa Oya song mp3 download, Ajith Sanjeewa song, Ajith Sanjeewa song download,', 97),
(50, 'Seya (Nabara Wu)', 'core/storage/uploads/images/album_art/album_art-20180629150142.png', 0, 9, 51, '2018-06-29 22:01:43', '2018-07-25 20:18:03', 0, 9, '', '', 141),
(51, 'Hithuwak Kari', 'core/storage/uploads/images/album_art/album_art-20180629155045.png', 0, 0, 52, '2018-06-29 22:50:45', '2018-07-26 16:45:33', 0, 9, '', '', 160),
(52, 'Aththama Kiyannam', 'core/storage/uploads/images/album_art/album_art-20180629162159.png', 0, 0, 53, '2018-06-29 23:21:59', '2018-07-26 00:01:34', 0, 9, '', '', 186),
(53, 'Unmada Rathriye', 'core/storage/uploads/images/album_art/album_art-20180630153418.png', 0, 10, 54, '2018-06-30 22:34:18', '2018-07-26 03:55:08', 0, 9, 'Unmada Rathriye Nimash Frenando Official Audio', 'unmada rathriye, unmada rathriye song, unmada rathriye song download, unmada rathriye mp3 song download, unmada rathriye mp3 download,', 99),
(54, 'Parana Hithuwakkari', 'core/storage/uploads/images/album_art/album_art-20180630161208.png', 0, 11, 55, '2018-06-30 23:12:09', '2018-07-26 21:24:41', 0, 9, 'Parana Hithuwakkari Official Audio', 'parana hithuwakkari, parana hithuwakkari song, parana hithuwakkari song video, parana hithuwakkari manoj, parana hithuwakkari mp3 song, parana hithuwakkari new song, parana hithuwakkari official video,', 100),
(55, 'Dukak Denenna Epa', 'core/storage/uploads/images/album_art/album_art-20180704133524.png', 0, 0, 56, '2018-07-04 20:35:24', '2018-07-23 09:37:54', 1, 9, 'Dukak Denenna Epa Official Audio Sadun Perera', 'sandun perera, dukak denenna epa, sandun perera new song, new sinhala mp3, sandun perera new, chandana walpola, dukak denenna epa sandun perera, dukak denenna epa mp3, dukak denenna epa video, dukak denenna epa lyrics, dukak denenna epa dj, dukak denenna epa mp3 song, dukak denenna epa download, sajith v chathuranga, dukak danenna epa obata nam kisida, sandun perera new song download mp3, ', 10),
(56, 'Mage Asurin', 'core/storage/uploads/images/album_art/album_art-20180704171253.png', 0, 0, 57, '2018-07-05 00:12:53', '2018-07-07 21:38:36', 1, 9, 'Mage Asurin Official Audio Shalinda Fernando', 'shalinda fernando, mage asurin, mage asurin song, mage asurin live, mage asurin video download, mage asurin lyrics, mage asurin mideela, mage asurin midila, mage asurin live show mp3, mage asurin shalinda fernando, mage asurin video download, mage asurin live show mp3, mage asurin dj, mage asurin shalinda fernando (all right), shalinda fernando songs, ', 4),
(57, 'Therum Giya', 'core/storage/uploads/images/album_art/album_art-20180704175055.png', 0, 0, 58, '2018-07-05 00:50:55', '2018-07-23 09:35:34', 1, 9, 'Therum Giya (Giya De Giyaden) Shahil Himansa Official Audio', 'therum giya, shahil himansa, chandana walpola, therum giya video song download, therum giya dj song, therum giya lyrics, therum giya nuba wenas wee kiya, therum giya chords, therum giya song lyrics, therum giya video, therum giya mp3, ', 9),
(58, 'Heen Sare', 'core/storage/uploads/images/album_art/album_art-20180704182147.png', 0, 0, 59, '2018-07-05 01:21:47', '2018-07-22 14:03:39', 1, 9, 'Heen Sare Thushara Subasinghe Official Audio', 'heen sare, heen sare video, heen sare video download, thushara subasinghe new song, thushara subasinha new song, thushara subasingha new song, oxygen leader, oxygen sri lanka, heen sare hithata mage, heen sare song, heen sare thushara, heen sare song thushara, ', 32),
(59, 'Man Vindina', 'core/storage/uploads/images/album_art/album_art-20180704191626.png', 0, 0, 60, '2018-07-05 02:16:26', '2018-07-08 01:58:05', 1, 9, 'Man Vindina Udesh Indula Official Audio', 'udesh indula, new song, man vindina mey duk ada, best sinhala songs, sinhala new song, udesh indula new song, udesh indula mp3 song, vindina duk ada official audio, vindina duk ada indula,', 2),
(60, 'Nodakapu Dewani Budun', 'core/storage/uploads/images/album_art/album_art-20180704200102.png', 37, 0, 61, '2018-07-05 03:01:02', '2018-07-26 21:18:02', 1, 9, 'Nodakapu Dewani Budun Jude Rogans Official Music Video', 'jude rogans, nodakapu dewani budun, nodakapu deweni budun jude rogans, jude rogans new song, sinhala songs, nodakapu deweni budun video, jude rogans new song video, nodakapu dewani budun song video, nodakapu dewani budun song lyrics, nodakapu dewani budun song free download, nodakapu dewani budun dj song, nodakapu jude, ', 14),
(61, 'Mata Wetahuna', 'core/storage/uploads/images/album_art/album_art-20180705161854.png', 0, 0, 63, '2018-07-05 23:18:54', '2018-07-26 01:20:03', 0, 9, '', 'mata wetahuna, mata wetahuna song, mata wetahuna mp3 song, mata wetahuna song download, mata wetahuna mp3 song download, ', 123),
(62, 'Salli Na', 'core/storage/uploads/images/album_art/album_art-20180705172859.png', 0, 0, 64, '2018-07-06 00:29:00', '2018-07-26 17:42:46', 0, 9, 'Salli Na Manoj V Official Audio', 'salli na, salli na song, salli na song download, salli na mp3 song download, salli na manoj, ', 75),
(63, 'Sudu Manika', 'core/storage/uploads/images/album_art/album_art-20180705174513.png', 0, 12, 62, '2018-07-06 00:45:13', '2018-07-26 17:14:07', 0, 9, 'Sudu Manika Nalinda Ranasinghe Official Audio', 'sudu manika, sudu manika song, sudu manika song nalinda, sudu manika song download, sudu manika mp3, sudu manika mp3 song download, ', 1163),
(64, 'Rawatuna Nowe', 'core/storage/uploads/images/album_art/album_art-20180707130659.png', 0, 0, 65, '2018-07-07 20:06:59', '2018-07-26 21:24:20', 0, 9, 'Rawatuna Nowe Thushara Joshap Official Audio 2018', 'rawatuna nowe, rawatuna nowe song, rawatuna nowe song mp3, rawatuna nowe thushara, rawatuna nowe mp3, rawatuna nowe mp3 download, rawatuna nowe song download, ', 3962),
(65, 'Aswaha Wadune', 'core/storage/uploads/images/album_art/album_art-20180708102657.png', 0, 0, 66, '2018-07-08 17:26:57', '2018-07-19 18:25:14', 0, 9, 'Aswaha Wadune Mithila Randika Official Audio 2018', 'aswaha wadune, aswaha wadune song, aswaha wadune song download, aswaha wadune mithila randika, aswaha wedune, aswaha wedune song, aswaha wedune song download, aswaha wedune song mp3, aswaha wedune song video, ', 140),
(66, 'Oba Warade Pataleddi', 'core/storage/uploads/images/album_art/album_art-20180710181907.png', 0, 0, 67, '2018-07-11 01:19:07', '2018-07-26 04:16:49', 0, 9, 'Oba Warade Pataleddi Saman Pushpakumara Official Audio 2018', '', 386),
(67, 'Purudu Thanikama', 'core/storage/uploads/images/album_art/album_art-20180712161157.png', 0, 0, 68, '2018-07-12 23:11:57', '2018-07-25 20:13:18', 0, 9, 'Purudu Thanikama Errol Jayawardena Official Audio 2018', 'purudu thanikama, purudu thanikama song, purudu thanikama mp3, purudu thanikama song mp3, purudu thanikama song video, purudu thanikama song download, purudu thanikama song free download, purudu thanikama free download, ', 143),
(68, 'Nodaka Oya', 'core/storage/uploads/images/album_art/album_art-20180713141026.png', 0, 0, 69, '2018-07-13 21:10:26', '2018-07-26 12:26:35', 0, 9, 'Nodaka Oya Janaka Katipearachchige Official Audio 2018', 'nodaka oya, nodaka oya mp3, nodaka oya song, nodaka oya song download, nodaka oya mp3 download, nodaka oya song free download, nodaka oya song janaka, ', 344),
(69, 'Adare Hodi Pothe', 'core/storage/uploads/images/album_art/album_art-20180713145750.png', 0, 0, 70, '2018-07-13 21:57:50', '2018-07-26 00:12:21', 0, 9, 'Adare Hodi Pothe Prageeth Vidana Pathirana Official Audio 2018', 'adare hodi pothe, adare hodi pothe song, adare hodi pothe mp3, adare hodi pothe song mp3, adare hodi pothe song download, adare hodi pothe mp3 free download, adare hodi pothe prageeth, prageeth new songs, ', 656),
(70, 'Labimai Me Ape ', 'core/storage/uploads/images/album_art/album_art-20180716153353.png', 0, 0, 71, '2018-07-16 22:33:53', '2018-07-26 21:25:23', 0, 9, 'Labimai Me Ape Ajith Anthony Official Audio 2018', 'ajith anthony, ajith anthony song, ajith anthony songs, ajith anthony song mp3, ajith anthony mp3 song download,', 386),
(71, 'Wen Wenna', 'core/storage/uploads/images/album_art/album_art-20180719172057.png', 0, 0, 72, '2018-07-20 00:20:57', '2018-07-26 21:18:46', 0, 9, 'Wen Wenna Ashan Fernando Official Audio 2018', 'wen wenna, wen wenna song, wen wenna song ashan, wen wenna new song ashan, wen wenna song ashan mp3, wen wenna song ashan mp3 download, wen wenna mp3, wen wenna song ashan free download, ', 2382),
(72, 'Manamaliye', 'core/storage/uploads/images/album_art/album_art-20180725095936.png', 39, 0, 73, '2018-07-25 16:59:36', '2018-07-26 21:25:00', 0, 9, 'Manamaliye Tehan Perera ft. Hot Chocolate 2018', 'manamaliye, manamaliye song, manamaliye tehan, manamaliye new song tehan, manamaliye download, manamaliye mp3 download,', 29);

-- --------------------------------------------------------

--
-- Table structure for table `song_lyrics_artist`
--

CREATE TABLE `song_lyrics_artist` (
  `artist_id` int(10) UNSIGNED DEFAULT NULL,
  `song_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `song_lyrics_artist`
--

INSERT INTO `song_lyrics_artist` (`artist_id`, `song_id`) VALUES
(2, 2),
(2, 3),
(2, 6),
(2, 7),
(2, 8),
(18, 9),
(19, 10),
(21, 11),
(23, 12),
(27, 13),
(21, 14),
(28, 15),
(33, 23),
(23, 27),
(47, 38),
(64, 49),
(66, 50),
(68, 51),
(69, 52),
(71, 40),
(53, 44),
(72, 45),
(53, 41),
(23, 43),
(21, 42),
(44, 36),
(62, 46),
(73, 35),
(2, 1),
(2, 32),
(23, 18),
(31, 19),
(23, 20),
(23, 22),
(34, 24),
(47, 39),
(73, 48),
(2, 37),
(23, 33),
(74, 30),
(23, 29),
(38, 28),
(36, 26),
(76, 53),
(77, 54),
(23, 55),
(79, 56),
(73, 57),
(21, 58),
(83, 59),
(76, 61),
(21, 63),
(36, 64),
(90, 65),
(91, 66),
(92, 67),
(62, 68),
(96, 69),
(64, 70),
(2, 71),
(100, 72);

-- --------------------------------------------------------

--
-- Table structure for table `song_music_artist`
--

CREATE TABLE `song_music_artist` (
  `artist_id` int(10) UNSIGNED DEFAULT NULL,
  `song_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `song_music_artist`
--

INSERT INTO `song_music_artist` (`artist_id`, `song_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(20, 10),
(22, 11),
(24, 12),
(17, 13),
(22, 14),
(17, 15),
(46, 38),
(63, 49),
(30, 48),
(50, 40),
(37, 50),
(63, 51),
(69, 52),
(52, 44),
(22, 45),
(52, 41),
(24, 43),
(22, 42),
(43, 36),
(22, 46),
(29, 35),
(30, 18),
(32, 19),
(5, 20),
(29, 22),
(33, 23),
(35, 24),
(48, 39),
(1, 37),
(41, 33),
(5, 32),
(40, 30),
(29, 29),
(37, 28),
(30, 26),
(1, 53),
(24, 54),
(30, 55),
(78, 56),
(30, 57),
(22, 58),
(82, 59),
(84, 60),
(1, 61),
(88, 62),
(22, 63),
(89, 64),
(24, 65),
(24, 66),
(93, 67),
(22, 68),
(97, 69),
(63, 70),
(1, 71),
(1, 72);

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2015-07-11 18:35:27', '2015-07-11 18:35:27'),
(2, NULL, 'ip', '127.0.0.1', '2015-07-11 18:35:28', '2015-07-11 18:35:28'),
(3, NULL, 'global', NULL, '2015-07-11 18:36:28', '2015-07-11 18:36:28'),
(4, NULL, 'ip', '127.0.0.1', '2015-07-11 18:36:28', '2015-07-11 18:36:28'),
(5, NULL, 'global', NULL, '2015-07-13 17:14:23', '2015-07-13 17:14:23'),
(6, NULL, 'ip', '127.0.0.1', '2015-07-13 17:14:23', '2015-07-13 17:14:23'),
(7, 1, 'user', NULL, '2015-07-13 17:14:23', '2015-07-13 17:14:23'),
(8, NULL, 'global', NULL, '2015-07-26 22:54:02', '2015-07-26 22:54:02'),
(9, NULL, 'ip', '127.0.0.1', '2015-07-26 22:54:02', '2015-07-26 22:54:02'),
(10, 1, 'user', NULL, '2015-07-26 22:54:02', '2015-07-26 22:54:02'),
(11, NULL, 'global', NULL, '2015-08-13 22:18:09', '2015-08-13 22:18:09'),
(12, NULL, 'ip', '127.0.0.1', '2015-08-13 22:18:09', '2015-08-13 22:18:09'),
(13, 1, 'user', NULL, '2015-08-13 22:18:09', '2015-08-13 22:18:09'),
(14, NULL, 'global', NULL, '2015-08-13 22:20:45', '2015-08-13 22:20:45'),
(15, NULL, 'ip', '127.0.0.1', '2015-08-13 22:20:45', '2015-08-13 22:20:45'),
(16, 1, 'user', NULL, '2015-08-13 22:20:45', '2015-08-13 22:20:45'),
(17, NULL, 'global', NULL, '2015-08-18 16:01:17', '2015-08-18 16:01:17'),
(18, NULL, 'ip', '127.0.0.1', '2015-08-18 16:01:17', '2015-08-18 16:01:17'),
(19, 1, 'user', NULL, '2015-08-18 16:01:17', '2015-08-18 16:01:17'),
(20, NULL, 'global', NULL, '2015-08-20 16:27:44', '2015-08-20 16:27:44'),
(21, NULL, 'ip', '127.0.0.1', '2015-08-20 16:27:44', '2015-08-20 16:27:44'),
(22, 1, 'user', NULL, '2015-08-20 16:27:45', '2015-08-20 16:27:45'),
(23, NULL, 'global', NULL, '2015-08-20 17:22:53', '2015-08-20 17:22:53'),
(24, NULL, 'ip', '127.0.0.1', '2015-08-20 17:22:53', '2015-08-20 17:22:53'),
(25, 1, 'user', NULL, '2015-08-20 17:22:54', '2015-08-20 17:22:54'),
(26, NULL, 'global', NULL, '2015-08-20 17:23:06', '2015-08-20 17:23:06'),
(27, NULL, 'ip', '127.0.0.1', '2015-08-20 17:23:06', '2015-08-20 17:23:06'),
(28, 1, 'user', NULL, '2015-08-20 17:23:06', '2015-08-20 17:23:06'),
(29, NULL, 'global', NULL, '2015-08-20 17:23:09', '2015-08-20 17:23:09'),
(30, NULL, 'ip', '127.0.0.1', '2015-08-20 17:23:09', '2015-08-20 17:23:09'),
(31, 1, 'user', NULL, '2015-08-20 17:23:09', '2015-08-20 17:23:09'),
(32, NULL, 'global', NULL, '2015-08-20 17:25:59', '2015-08-20 17:25:59'),
(33, NULL, 'ip', '127.0.0.1', '2015-08-20 17:25:59', '2015-08-20 17:25:59'),
(34, 1, 'user', NULL, '2015-08-20 17:25:59', '2015-08-20 17:25:59'),
(35, NULL, 'global', NULL, '2015-08-20 17:26:18', '2015-08-20 17:26:18'),
(36, NULL, 'ip', '127.0.0.1', '2015-08-20 17:26:19', '2015-08-20 17:26:19'),
(37, 1, 'user', NULL, '2015-08-20 17:26:19', '2015-08-20 17:26:19'),
(38, NULL, 'global', NULL, '2015-08-20 17:27:25', '2015-08-20 17:27:25'),
(39, NULL, 'ip', '127.0.0.1', '2015-08-20 17:27:25', '2015-08-20 17:27:25'),
(40, 1, 'user', NULL, '2015-08-20 17:27:25', '2015-08-20 17:27:25'),
(41, NULL, 'global', NULL, '2015-08-25 03:36:12', '2015-08-25 03:36:12'),
(42, NULL, 'ip', '127.0.0.1', '2015-08-25 03:36:12', '2015-08-25 03:36:12'),
(43, NULL, 'global', NULL, '2015-08-25 03:38:25', '2015-08-25 03:38:25'),
(44, NULL, 'ip', '127.0.0.1', '2015-08-25 03:38:25', '2015-08-25 03:38:25'),
(45, NULL, 'global', NULL, '2015-08-25 03:39:09', '2015-08-25 03:39:09'),
(46, NULL, 'ip', '127.0.0.1', '2015-08-25 03:39:09', '2015-08-25 03:39:09'),
(47, NULL, 'global', NULL, '2015-08-25 03:39:44', '2015-08-25 03:39:44'),
(48, NULL, 'ip', '127.0.0.1', '2015-08-25 03:39:44', '2015-08-25 03:39:44'),
(49, NULL, 'global', NULL, '2015-08-25 03:39:49', '2015-08-25 03:39:49'),
(50, NULL, 'ip', '127.0.0.1', '2015-08-25 03:39:50', '2015-08-25 03:39:50'),
(51, NULL, 'global', NULL, '2015-08-25 03:41:29', '2015-08-25 03:41:29'),
(52, NULL, 'ip', '127.0.0.1', '2015-08-25 03:41:29', '2015-08-25 03:41:29'),
(53, NULL, 'global', NULL, '2015-08-25 19:56:45', '2015-08-25 19:56:45'),
(54, NULL, 'ip', '127.0.0.1', '2015-08-25 19:56:45', '2015-08-25 19:56:45'),
(55, NULL, 'global', NULL, '2015-08-26 20:18:20', '2015-08-26 20:18:20'),
(56, NULL, 'ip', '192.168.1.35', '2015-08-26 20:18:21', '2015-08-26 20:18:21'),
(57, NULL, 'global', NULL, '2015-08-26 20:18:23', '2015-08-26 20:18:23'),
(58, NULL, 'ip', '192.168.1.35', '2015-08-26 20:18:23', '2015-08-26 20:18:23'),
(59, NULL, 'global', NULL, '2015-08-26 20:18:27', '2015-08-26 20:18:27'),
(60, NULL, 'ip', '192.168.1.35', '2015-08-26 20:18:27', '2015-08-26 20:18:27'),
(61, NULL, 'global', NULL, '2015-08-26 20:18:31', '2015-08-26 20:18:31'),
(62, NULL, 'ip', '192.168.1.35', '2015-08-26 20:18:31', '2015-08-26 20:18:31'),
(63, NULL, 'global', NULL, '2015-08-26 20:18:36', '2015-08-26 20:18:36'),
(64, NULL, 'ip', '192.168.1.35', '2015-08-26 20:18:36', '2015-08-26 20:18:36'),
(65, NULL, 'global', NULL, '2015-08-26 20:18:48', '2015-08-26 20:18:48'),
(66, NULL, 'global', NULL, '2015-08-27 15:50:50', '2015-08-27 15:50:50'),
(67, NULL, 'ip', '127.0.0.1', '2015-08-27 15:50:50', '2015-08-27 15:50:50'),
(68, 1, 'user', NULL, '2015-08-27 15:50:50', '2015-08-27 15:50:50'),
(69, NULL, 'global', NULL, '2015-08-30 19:12:57', '2015-08-30 19:12:57'),
(70, NULL, 'ip', '127.0.0.1', '2015-08-30 19:12:57', '2015-08-30 19:12:57'),
(71, NULL, 'global', NULL, '2015-08-30 19:21:13', '2015-08-30 19:21:13'),
(72, NULL, 'ip', '127.0.0.1', '2015-08-30 19:21:14', '2015-08-30 19:21:14'),
(73, NULL, 'global', NULL, '2015-09-06 23:33:36', '2015-09-06 23:33:36'),
(74, NULL, 'ip', '127.0.0.1', '2015-09-06 23:33:36', '2015-09-06 23:33:36'),
(75, NULL, 'global', NULL, '2015-09-18 18:15:18', '2015-09-18 18:15:18'),
(76, NULL, 'ip', '192.168.1.15', '2015-09-18 18:15:18', '2015-09-18 18:15:18'),
(77, NULL, 'global', NULL, '2015-09-18 18:15:22', '2015-09-18 18:15:22'),
(78, NULL, 'ip', '192.168.1.15', '2015-09-18 18:15:22', '2015-09-18 18:15:22'),
(79, NULL, 'global', NULL, '2015-09-18 18:15:30', '2015-09-18 18:15:30'),
(80, NULL, 'ip', '192.168.1.15', '2015-09-18 18:15:30', '2015-09-18 18:15:30'),
(81, NULL, 'global', NULL, '2015-09-18 18:15:34', '2015-09-18 18:15:34'),
(82, NULL, 'ip', '192.168.1.15', '2015-09-18 18:15:34', '2015-09-18 18:15:34'),
(83, NULL, 'global', NULL, '2015-09-18 18:15:40', '2015-09-18 18:15:40'),
(84, NULL, 'ip', '192.168.1.15', '2015-09-18 18:15:40', '2015-09-18 18:15:40'),
(85, NULL, 'global', NULL, '2015-10-30 02:07:26', '2015-10-30 02:07:26'),
(86, NULL, 'ip', '127.0.0.1', '2015-10-30 02:07:26', '2015-10-30 02:07:26'),
(87, 1, 'user', NULL, '2015-10-30 02:07:26', '2015-10-30 02:07:26'),
(88, NULL, 'global', NULL, '2015-10-30 02:07:30', '2015-10-30 02:07:30'),
(89, NULL, 'ip', '127.0.0.1', '2015-10-30 02:07:30', '2015-10-30 02:07:30'),
(90, 1, 'user', NULL, '2015-10-30 02:07:30', '2015-10-30 02:07:30'),
(91, NULL, 'global', NULL, '2015-10-30 02:07:34', '2015-10-30 02:07:34'),
(92, NULL, 'ip', '127.0.0.1', '2015-10-30 02:07:34', '2015-10-30 02:07:34'),
(93, 1, 'user', NULL, '2015-10-30 02:07:34', '2015-10-30 02:07:34'),
(94, NULL, 'global', NULL, '2015-10-30 02:07:41', '2015-10-30 02:07:41'),
(95, NULL, 'ip', '127.0.0.1', '2015-10-30 02:07:41', '2015-10-30 02:07:41'),
(96, 1, 'user', NULL, '2015-10-30 02:07:41', '2015-10-30 02:07:41'),
(97, NULL, 'global', NULL, '2015-10-30 02:07:48', '2015-10-30 02:07:48'),
(98, NULL, 'ip', '127.0.0.1', '2015-10-30 02:07:48', '2015-10-30 02:07:48'),
(99, 1, 'user', NULL, '2015-10-30 02:07:48', '2015-10-30 02:07:48'),
(100, NULL, 'global', NULL, '2015-10-30 02:07:52', '2015-10-30 02:07:52'),
(101, NULL, 'ip', '127.0.0.1', '2015-10-30 02:07:52', '2015-10-30 02:07:52'),
(102, 1, 'user', NULL, '2015-10-30 02:07:52', '2015-10-30 02:07:52'),
(103, NULL, 'global', NULL, '2015-11-04 15:43:02', '2015-11-04 15:43:02'),
(104, NULL, 'ip', '127.0.0.1', '2015-11-04 15:43:02', '2015-11-04 15:43:02'),
(105, 1, 'user', NULL, '2015-11-04 15:43:02', '2015-11-04 15:43:02'),
(106, NULL, 'global', NULL, '2015-11-07 23:56:55', '2015-11-07 23:56:55'),
(107, NULL, 'ip', '127.0.0.1', '2015-11-07 23:56:55', '2015-11-07 23:56:55'),
(108, NULL, 'global', NULL, '2015-11-07 23:57:01', '2015-11-07 23:57:01'),
(109, NULL, 'ip', '127.0.0.1', '2015-11-07 23:57:01', '2015-11-07 23:57:01'),
(110, NULL, 'global', NULL, '2015-11-19 06:39:36', '2015-11-19 06:39:36'),
(111, NULL, 'ip', '127.0.0.1', '2015-11-19 06:39:36', '2015-11-19 06:39:36'),
(112, NULL, 'global', NULL, '2015-11-19 06:39:43', '2015-11-19 06:39:43'),
(113, NULL, 'ip', '127.0.0.1', '2015-11-19 06:39:43', '2015-11-19 06:39:43'),
(114, NULL, 'global', NULL, '2015-11-19 06:42:14', '2015-11-19 06:42:14'),
(115, NULL, 'ip', '127.0.0.1', '2015-11-19 06:42:14', '2015-11-19 06:42:14'),
(116, NULL, 'global', NULL, '2015-11-19 06:42:27', '2015-11-19 06:42:27'),
(117, NULL, 'ip', '127.0.0.1', '2015-11-19 06:42:27', '2015-11-19 06:42:27'),
(118, NULL, 'global', NULL, '2015-11-19 06:42:36', '2015-11-19 06:42:36'),
(119, NULL, 'ip', '127.0.0.1', '2015-11-19 06:42:36', '2015-11-19 06:42:36'),
(120, NULL, 'global', NULL, '2015-11-19 08:57:03', '2015-11-19 08:57:03'),
(121, NULL, 'ip', '::1', '2015-11-19 08:57:03', '2015-11-19 08:57:03'),
(122, NULL, 'global', NULL, '2015-11-19 08:57:06', '2015-11-19 08:57:06'),
(123, NULL, 'ip', '::1', '2015-11-19 08:57:06', '2015-11-19 08:57:06'),
(124, NULL, 'global', NULL, '2015-11-19 09:56:24', '2015-11-19 09:56:24'),
(125, NULL, 'ip', '::1', '2015-11-19 09:56:24', '2015-11-19 09:56:24'),
(126, 1, 'user', NULL, '2015-11-19 09:56:24', '2015-11-19 09:56:24'),
(127, NULL, 'global', NULL, '2015-11-19 09:56:27', '2015-11-19 09:56:27'),
(128, NULL, 'ip', '::1', '2015-11-19 09:56:27', '2015-11-19 09:56:27'),
(129, 1, 'user', NULL, '2015-11-19 09:56:27', '2015-11-19 09:56:27'),
(130, NULL, 'global', NULL, '2015-11-24 06:43:05', '2015-11-24 06:43:05'),
(131, NULL, 'ip', '::1', '2015-11-24 06:43:05', '2015-11-24 06:43:05'),
(132, 1, 'user', NULL, '2015-11-24 06:43:05', '2015-11-24 06:43:05'),
(133, NULL, 'global', NULL, '2015-11-24 13:44:15', '2015-11-24 13:44:15'),
(134, NULL, 'ip', '::1', '2015-11-24 13:44:15', '2015-11-24 13:44:15'),
(135, NULL, 'global', NULL, '2015-12-03 13:42:12', '2015-12-03 13:42:12'),
(136, NULL, 'ip', '::1', '2015-12-03 13:42:12', '2015-12-03 13:42:12'),
(137, 1, 'user', NULL, '2015-12-03 13:42:12', '2015-12-03 13:42:12'),
(138, NULL, 'global', NULL, '2015-12-03 15:02:03', '2015-12-03 15:02:03'),
(139, NULL, 'ip', '::1', '2015-12-03 15:02:03', '2015-12-03 15:02:03'),
(140, 1, 'user', NULL, '2015-12-03 15:02:03', '2015-12-03 15:02:03'),
(141, NULL, 'global', NULL, '2015-12-04 06:12:59', '2015-12-04 06:12:59'),
(142, NULL, 'ip', '127.0.0.1', '2015-12-04 06:12:59', '2015-12-04 06:12:59'),
(143, 1, 'user', NULL, '2015-12-04 06:12:59', '2015-12-04 06:12:59'),
(144, NULL, 'global', NULL, '2015-12-04 06:13:02', '2015-12-04 06:13:02'),
(145, NULL, 'ip', '127.0.0.1', '2015-12-04 06:13:02', '2015-12-04 06:13:02'),
(146, 1, 'user', NULL, '2015-12-04 06:13:02', '2015-12-04 06:13:02'),
(147, NULL, 'global', NULL, '2015-12-19 10:58:50', '2015-12-19 10:58:50'),
(148, NULL, 'ip', '127.0.0.1', '2015-12-19 10:58:50', '2015-12-19 10:58:50'),
(149, 1, 'user', NULL, '2015-12-19 10:58:50', '2015-12-19 10:58:50'),
(150, NULL, 'global', NULL, '2016-01-09 12:58:55', '2016-01-09 12:58:55'),
(151, NULL, 'ip', '192.168.1.121', '2016-01-09 12:58:55', '2016-01-09 12:58:55'),
(153, NULL, 'global', NULL, '2016-01-09 12:59:26', '2016-01-09 12:59:26'),
(154, NULL, 'ip', '192.168.1.121', '2016-01-09 12:59:26', '2016-01-09 12:59:26'),
(156, NULL, 'global', NULL, '2016-01-09 12:59:41', '2016-01-09 12:59:41'),
(157, NULL, 'ip', '192.168.1.121', '2016-01-09 12:59:41', '2016-01-09 12:59:41'),
(159, NULL, 'global', NULL, '2016-01-09 13:00:07', '2016-01-09 13:00:07'),
(160, NULL, 'ip', '192.168.1.121', '2016-01-09 13:00:07', '2016-01-09 13:00:07'),
(162, NULL, 'global', NULL, '2016-01-12 06:20:41', '2016-01-12 06:20:41'),
(163, NULL, 'ip', '192.168.1.142', '2016-01-12 06:20:41', '2016-01-12 06:20:41'),
(165, NULL, 'global', NULL, '2016-01-12 06:20:50', '2016-01-12 06:20:50'),
(166, NULL, 'ip', '192.168.1.142', '2016-01-12 06:20:50', '2016-01-12 06:20:50'),
(168, NULL, 'global', NULL, '2016-01-12 06:21:03', '2016-01-12 06:21:03'),
(169, NULL, 'ip', '192.168.1.142', '2016-01-12 06:21:03', '2016-01-12 06:21:03'),
(171, NULL, 'global', NULL, '2016-01-12 06:21:16', '2016-01-12 06:21:16'),
(172, NULL, 'ip', '192.168.1.142', '2016-01-12 06:21:16', '2016-01-12 06:21:16'),
(174, NULL, 'global', NULL, '2016-01-12 06:22:23', '2016-01-12 06:22:23'),
(175, NULL, 'ip', '192.168.1.142', '2016-01-12 06:22:23', '2016-01-12 06:22:23'),
(177, NULL, 'global', NULL, '2016-01-12 06:22:58', '2016-01-12 06:22:58'),
(178, NULL, 'ip', '192.168.1.142', '2016-01-12 06:22:58', '2016-01-12 06:22:58'),
(180, NULL, 'global', NULL, '2016-01-12 06:36:27', '2016-01-12 06:36:27'),
(181, NULL, 'ip', '192.168.1.142', '2016-01-12 06:36:27', '2016-01-12 06:36:27'),
(183, NULL, 'global', NULL, '2016-01-12 06:36:33', '2016-01-12 06:36:33'),
(184, NULL, 'ip', '192.168.1.142', '2016-01-12 06:36:33', '2016-01-12 06:36:33'),
(186, NULL, 'global', NULL, '2016-01-12 06:36:36', '2016-01-12 06:36:36'),
(187, NULL, 'ip', '192.168.1.142', '2016-01-12 06:36:36', '2016-01-12 06:36:36'),
(189, NULL, 'global', NULL, '2016-01-12 06:36:41', '2016-01-12 06:36:41'),
(190, NULL, 'ip', '192.168.1.142', '2016-01-12 06:36:41', '2016-01-12 06:36:41'),
(192, NULL, 'global', NULL, '2016-01-12 06:48:58', '2016-01-12 06:48:58'),
(193, NULL, 'ip', '192.168.1.142', '2016-01-12 06:48:58', '2016-01-12 06:48:58'),
(195, NULL, 'global', NULL, '2016-01-24 18:55:32', '2016-01-24 18:55:32'),
(196, NULL, 'ip', '127.0.0.1', '2016-01-24 18:55:32', '2016-01-24 18:55:32'),
(197, NULL, 'global', NULL, '2016-01-27 09:57:09', '2016-01-27 09:57:09'),
(198, NULL, 'ip', '127.0.0.1', '2016-01-27 09:57:09', '2016-01-27 09:57:09'),
(200, NULL, 'global', NULL, '2016-01-29 05:40:03', '2016-01-29 05:40:03'),
(201, NULL, 'ip', '127.0.0.1', '2016-01-29 05:40:03', '2016-01-29 05:40:03'),
(202, 1, 'user', NULL, '2016-01-29 05:40:03', '2016-01-29 05:40:03'),
(203, NULL, 'global', NULL, '2016-02-11 05:59:12', '2016-02-11 05:59:12'),
(204, NULL, 'ip', '127.0.0.1', '2016-02-11 05:59:12', '2016-02-11 05:59:12'),
(205, 1, 'user', NULL, '2016-02-11 05:59:12', '2016-02-11 05:59:12'),
(206, NULL, 'global', NULL, '2016-02-24 19:15:41', '2016-02-24 19:15:41'),
(207, NULL, 'ip', '192.168.1.59', '2016-02-24 19:15:41', '2016-02-24 19:15:41'),
(209, NULL, 'global', NULL, '2016-02-24 19:17:03', '2016-02-24 19:17:03'),
(210, NULL, 'ip', '192.168.1.59', '2016-02-24 19:17:03', '2016-02-24 19:17:03'),
(212, NULL, 'global', NULL, '2016-02-24 19:17:06', '2016-02-24 19:17:06'),
(213, NULL, 'ip', '192.168.1.59', '2016-02-24 19:17:06', '2016-02-24 19:17:06'),
(215, NULL, 'global', NULL, '2016-03-02 16:17:00', '2016-03-02 16:17:00'),
(216, NULL, 'ip', '127.0.0.1', '2016-03-02 16:17:00', '2016-03-02 16:17:00'),
(217, 1, 'user', NULL, '2016-03-02 16:17:00', '2016-03-02 16:17:00'),
(218, NULL, 'global', NULL, '2016-03-28 22:02:49', '2016-03-28 22:02:49'),
(219, NULL, 'ip', '127.0.0.1', '2016-03-28 22:02:49', '2016-03-28 22:02:49'),
(220, NULL, 'global', NULL, '2016-03-28 22:02:56', '2016-03-28 22:02:56'),
(221, NULL, 'ip', '127.0.0.1', '2016-03-28 22:02:56', '2016-03-28 22:02:56'),
(222, 1, 'user', NULL, '2016-03-28 22:02:56', '2016-03-28 22:02:56'),
(223, NULL, 'global', NULL, '2016-03-31 00:07:40', '2016-03-31 00:07:40'),
(224, NULL, 'ip', '::1', '2016-03-31 00:07:41', '2016-03-31 00:07:41'),
(225, NULL, 'global', NULL, '2016-05-02 16:31:37', '2016-05-02 16:31:37'),
(226, NULL, 'ip', '192.168.1.41', '2016-05-02 16:31:37', '2016-05-02 16:31:37'),
(227, NULL, 'global', NULL, '2016-05-02 16:31:40', '2016-05-02 16:31:40'),
(228, NULL, 'ip', '192.168.1.41', '2016-05-02 16:31:40', '2016-05-02 16:31:40'),
(229, NULL, 'global', NULL, '2016-05-02 16:31:43', '2016-05-02 16:31:43'),
(230, NULL, 'ip', '192.168.1.41', '2016-05-02 16:31:43', '2016-05-02 16:31:43'),
(231, NULL, 'global', NULL, '2016-05-02 16:31:49', '2016-05-02 16:31:49'),
(232, NULL, 'ip', '192.168.1.41', '2016-05-02 16:31:49', '2016-05-02 16:31:49'),
(233, NULL, 'global', NULL, '2016-05-02 16:56:12', '2016-05-02 16:56:12'),
(234, NULL, 'ip', '192.168.1.41', '2016-05-02 16:56:12', '2016-05-02 16:56:12'),
(235, NULL, 'global', NULL, '2016-05-02 16:56:40', '2016-05-02 16:56:40'),
(236, NULL, 'ip', '192.168.1.41', '2016-05-02 16:56:40', '2016-05-02 16:56:40'),
(237, NULL, 'global', NULL, '2016-05-02 16:56:58', '2016-05-02 16:56:58'),
(238, NULL, 'ip', '192.168.1.41', '2016-05-02 16:56:58', '2016-05-02 16:56:58'),
(239, NULL, 'global', NULL, '2016-05-02 16:57:01', '2016-05-02 16:57:01'),
(240, NULL, 'ip', '192.168.1.41', '2016-05-02 16:57:01', '2016-05-02 16:57:01'),
(241, NULL, 'global', NULL, '2016-05-02 16:57:07', '2016-05-02 16:57:07'),
(242, NULL, 'ip', '192.168.1.41', '2016-05-02 16:57:07', '2016-05-02 16:57:07'),
(243, NULL, 'global', NULL, '2017-01-16 10:43:01', '2017-01-16 10:43:01'),
(244, NULL, 'ip', '::1', '2017-01-16 10:43:01', '2017-01-16 10:43:01'),
(245, NULL, 'global', NULL, '2017-01-16 11:23:12', '2017-01-16 11:23:12'),
(246, NULL, 'ip', '::1', '2017-01-16 11:23:12', '2017-01-16 11:23:12'),
(248, NULL, 'global', NULL, '2017-01-16 11:23:40', '2017-01-16 11:23:40'),
(249, NULL, 'ip', '::1', '2017-01-16 11:23:40', '2017-01-16 11:23:40'),
(251, NULL, 'global', NULL, '2017-01-19 07:35:37', '2017-01-19 07:35:37'),
(252, NULL, 'ip', '::1', '2017-01-19 07:35:37', '2017-01-19 07:35:37'),
(253, NULL, 'global', NULL, '2017-01-25 18:59:52', '2017-01-25 18:59:52'),
(254, NULL, 'ip', '::1', '2017-01-25 18:59:52', '2017-01-25 18:59:52'),
(255, NULL, 'global', NULL, '2017-04-12 21:25:26', '2017-04-12 21:25:26'),
(256, NULL, 'ip', '112.135.7.121', '2017-04-12 21:25:26', '2017-04-12 21:25:26'),
(257, 9, 'user', NULL, '2017-04-12 21:25:26', '2017-04-12 21:25:26'),
(258, NULL, 'global', NULL, '2017-04-24 18:47:15', '2017-04-24 18:47:15'),
(259, NULL, 'ip', '::1', '2017-04-24 18:47:15', '2017-04-24 18:47:15'),
(260, NULL, 'global', NULL, '2017-04-30 19:23:53', '2017-04-30 19:23:53'),
(261, NULL, 'ip', '123.231.108.3', '2017-04-30 19:23:53', '2017-04-30 19:23:53'),
(262, 9, 'user', NULL, '2017-04-30 19:23:53', '2017-04-30 19:23:53'),
(263, NULL, 'global', NULL, '2017-04-30 19:23:58', '2017-04-30 19:23:58'),
(264, NULL, 'ip', '123.231.108.3', '2017-04-30 19:23:58', '2017-04-30 19:23:58'),
(265, 9, 'user', NULL, '2017-04-30 19:23:58', '2017-04-30 19:23:58'),
(266, NULL, 'global', NULL, '2017-04-30 19:24:05', '2017-04-30 19:24:05'),
(267, NULL, 'ip', '123.231.108.3', '2017-04-30 19:24:05', '2017-04-30 19:24:05'),
(268, 9, 'user', NULL, '2017-04-30 19:24:05', '2017-04-30 19:24:05'),
(269, NULL, 'global', NULL, '2017-04-30 19:24:14', '2017-04-30 19:24:14'),
(270, NULL, 'ip', '123.231.108.3', '2017-04-30 19:24:14', '2017-04-30 19:24:14'),
(271, 9, 'user', NULL, '2017-04-30 19:24:14', '2017-04-30 19:24:14'),
(272, NULL, 'global', NULL, '2017-04-30 19:24:19', '2017-04-30 19:24:19'),
(273, NULL, 'ip', '123.231.108.3', '2017-04-30 19:24:19', '2017-04-30 19:24:19'),
(274, 9, 'user', NULL, '2017-04-30 19:24:19', '2017-04-30 19:24:19'),
(275, NULL, 'global', NULL, '2017-04-30 19:24:24', '2017-04-30 19:24:24'),
(276, NULL, 'ip', '123.231.108.3', '2017-04-30 19:24:24', '2017-04-30 19:24:24'),
(277, 9, 'user', NULL, '2017-04-30 19:24:24', '2017-04-30 19:24:24'),
(278, NULL, 'global', NULL, '2017-08-30 18:57:01', '2017-08-30 18:57:01'),
(279, NULL, 'ip', '113.59.213.196', '2017-08-30 18:57:01', '2017-08-30 18:57:01'),
(280, NULL, 'global', NULL, '2017-09-19 00:02:04', '2017-09-19 00:02:04'),
(281, NULL, 'ip', '::1', '2017-09-19 00:02:04', '2017-09-19 00:02:04'),
(282, NULL, 'global', NULL, '2017-09-26 18:16:17', '2017-09-26 18:16:17'),
(283, NULL, 'ip', '::1', '2017-09-26 18:16:17', '2017-09-26 18:16:17'),
(284, 9, 'user', NULL, '2017-09-26 18:16:17', '2017-09-26 18:16:17'),
(285, NULL, 'global', NULL, '2017-10-18 05:22:42', '2017-10-18 05:22:42'),
(286, NULL, 'ip', '::1', '2017-10-18 05:22:42', '2017-10-18 05:22:42'),
(287, NULL, 'global', NULL, '2018-01-16 02:29:41', '2018-01-16 02:29:41'),
(288, NULL, 'ip', '::1', '2018-01-16 02:29:41', '2018-01-16 02:29:41'),
(289, 1, 'user', NULL, '2018-01-16 02:29:41', '2018-01-16 02:29:41'),
(290, NULL, 'global', NULL, '2018-01-19 04:23:38', '2018-01-19 04:23:38'),
(291, NULL, 'ip', '::1', '2018-01-19 04:23:39', '2018-01-19 04:23:39'),
(292, 1, 'user', NULL, '2018-01-19 04:23:39', '2018-01-19 04:23:39'),
(293, NULL, 'global', NULL, '2018-01-26 02:28:56', '2018-01-26 02:28:56'),
(294, NULL, 'ip', '::1', '2018-01-26 02:28:56', '2018-01-26 02:28:56'),
(295, 1, 'user', NULL, '2018-01-26 02:28:56', '2018-01-26 02:28:56'),
(296, NULL, 'global', NULL, '2018-01-27 17:48:02', '2018-01-27 17:48:02'),
(297, NULL, 'ip', '::1', '2018-01-27 17:48:02', '2018-01-27 17:48:02'),
(298, 1, 'user', NULL, '2018-01-27 17:48:02', '2018-01-27 17:48:02'),
(299, NULL, 'global', NULL, '2018-02-21 03:10:48', '2018-02-21 03:10:48'),
(300, NULL, 'ip', '::1', '2018-02-21 03:10:48', '2018-02-21 03:10:48'),
(301, NULL, 'global', NULL, '2018-02-21 03:10:55', '2018-02-21 03:10:55'),
(302, NULL, 'ip', '::1', '2018-02-21 03:10:55', '2018-02-21 03:10:55'),
(303, NULL, 'global', NULL, '2018-02-21 03:14:06', '2018-02-21 03:14:06'),
(304, NULL, 'ip', '::1', '2018-02-21 03:14:07', '2018-02-21 03:14:07'),
(305, NULL, 'global', NULL, '2018-03-01 21:46:22', '2018-03-01 21:46:22'),
(306, NULL, 'ip', '::1', '2018-03-01 21:46:22', '2018-03-01 21:46:22'),
(307, 1, 'user', NULL, '2018-03-01 21:46:23', '2018-03-01 21:46:23'),
(308, NULL, 'global', NULL, '2018-03-14 03:47:06', '2018-03-14 03:47:06'),
(309, NULL, 'ip', '::1', '2018-03-14 03:47:07', '2018-03-14 03:47:07'),
(310, NULL, 'global', NULL, '2018-03-16 03:34:26', '2018-03-16 03:34:26'),
(311, NULL, 'ip', '::1', '2018-03-16 03:34:29', '2018-03-16 03:34:29'),
(312, 1, 'user', NULL, '2018-03-16 03:34:38', '2018-03-16 03:34:38'),
(313, NULL, 'global', NULL, '2018-03-31 18:52:11', '2018-03-31 18:52:11'),
(314, NULL, 'ip', '::1', '2018-03-31 18:52:11', '2018-03-31 18:52:11'),
(315, NULL, 'global', NULL, '2018-03-31 18:52:27', '2018-03-31 18:52:27'),
(316, NULL, 'ip', '::1', '2018-03-31 18:52:28', '2018-03-31 18:52:28'),
(317, NULL, 'global', NULL, '2018-03-31 18:54:03', '2018-03-31 18:54:03'),
(318, NULL, 'ip', '::1', '2018-03-31 18:54:03', '2018-03-31 18:54:03'),
(319, NULL, 'global', NULL, '2018-03-31 18:54:20', '2018-03-31 18:54:20'),
(320, NULL, 'ip', '::1', '2018-03-31 18:54:20', '2018-03-31 18:54:20'),
(321, NULL, 'global', NULL, '2018-04-04 02:43:38', '2018-04-04 02:43:38'),
(322, NULL, 'ip', '::1', '2018-04-04 02:43:39', '2018-04-04 02:43:39'),
(323, 1, 'user', NULL, '2018-04-04 02:43:39', '2018-04-04 02:43:39'),
(324, NULL, 'global', NULL, '2018-04-05 03:14:35', '2018-04-05 03:14:35'),
(325, NULL, 'ip', '::1', '2018-04-05 03:14:35', '2018-04-05 03:14:35'),
(326, 1, 'user', NULL, '2018-04-05 03:14:36', '2018-04-05 03:14:36'),
(327, NULL, 'global', NULL, '2018-04-10 03:25:44', '2018-04-10 03:25:44'),
(328, NULL, 'ip', '::1', '2018-04-10 03:25:45', '2018-04-10 03:25:45'),
(329, 1, 'user', NULL, '2018-04-10 03:25:45', '2018-04-10 03:25:45'),
(330, NULL, 'global', NULL, '2018-04-26 03:19:24', '2018-04-26 03:19:24'),
(331, NULL, 'ip', '::1', '2018-04-26 03:19:24', '2018-04-26 03:19:24'),
(332, NULL, 'global', NULL, '2018-04-26 03:24:07', '2018-04-26 03:24:07'),
(333, NULL, 'ip', '::1', '2018-04-26 03:24:07', '2018-04-26 03:24:07'),
(334, NULL, 'global', NULL, '2018-04-26 03:24:13', '2018-04-26 03:24:13'),
(335, NULL, 'ip', '::1', '2018-04-26 03:24:13', '2018-04-26 03:24:13'),
(336, NULL, 'global', NULL, '2018-05-07 18:49:15', '2018-05-07 18:49:15'),
(337, NULL, 'ip', '::1', '2018-05-07 18:49:16', '2018-05-07 18:49:16'),
(338, 1, 'user', NULL, '2018-05-07 18:49:16', '2018-05-07 18:49:16'),
(339, NULL, 'global', NULL, '2018-05-07 19:26:29', '2018-05-07 19:26:29'),
(340, NULL, 'ip', '::1', '2018-05-07 19:26:30', '2018-05-07 19:26:30'),
(341, NULL, 'global', NULL, '2018-05-14 03:00:36', '2018-05-14 03:00:36'),
(342, NULL, 'ip', '112.135.9.29', '2018-05-14 03:00:36', '2018-05-14 03:00:36'),
(343, NULL, 'global', NULL, '2018-05-14 03:01:53', '2018-05-14 03:01:53'),
(344, NULL, 'ip', '112.135.9.29', '2018-05-14 03:01:53', '2018-05-14 03:01:53'),
(345, NULL, 'global', NULL, '2018-05-14 03:02:54', '2018-05-14 03:02:54'),
(346, NULL, 'ip', '112.135.9.29', '2018-05-14 03:02:54', '2018-05-14 03:02:54'),
(347, NULL, 'global', NULL, '2018-05-14 03:03:24', '2018-05-14 03:03:24'),
(348, NULL, 'ip', '112.135.9.29', '2018-05-14 03:03:24', '2018-05-14 03:03:24'),
(349, NULL, 'global', NULL, '2018-05-14 03:03:34', '2018-05-14 03:03:34'),
(350, NULL, 'ip', '112.135.9.29', '2018-05-14 03:03:34', '2018-05-14 03:03:34'),
(351, NULL, 'global', NULL, '2018-05-14 03:07:08', '2018-05-14 03:07:08'),
(352, NULL, 'ip', '112.135.9.29', '2018-05-14 03:07:08', '2018-05-14 03:07:08'),
(353, NULL, 'global', NULL, '2018-05-14 03:20:37', '2018-05-14 03:20:37'),
(354, NULL, 'ip', '112.135.9.29', '2018-05-14 03:20:37', '2018-05-14 03:20:37'),
(355, NULL, 'global', NULL, '2018-05-14 03:20:42', '2018-05-14 03:20:42'),
(356, NULL, 'ip', '112.135.9.29', '2018-05-14 03:20:42', '2018-05-14 03:20:42'),
(357, NULL, 'global', NULL, '2018-05-14 03:20:46', '2018-05-14 03:20:46'),
(358, NULL, 'ip', '112.135.9.29', '2018-05-14 03:20:46', '2018-05-14 03:20:46'),
(359, NULL, 'global', NULL, '2018-05-14 03:57:30', '2018-05-14 03:57:30'),
(360, NULL, 'ip', '112.135.10.73', '2018-05-14 03:57:30', '2018-05-14 03:57:30'),
(361, NULL, 'global', NULL, '2018-05-21 22:46:34', '2018-05-21 22:46:34'),
(362, NULL, 'ip', '112.135.1.191', '2018-05-21 22:46:34', '2018-05-21 22:46:34'),
(363, NULL, 'global', NULL, '2018-05-22 17:47:39', '2018-05-22 17:47:39'),
(364, NULL, 'ip', '112.135.0.158', '2018-05-22 17:47:39', '2018-05-22 17:47:39'),
(365, NULL, 'global', NULL, '2018-05-22 17:47:42', '2018-05-22 17:47:42'),
(366, NULL, 'ip', '112.135.0.158', '2018-05-22 17:47:42', '2018-05-22 17:47:42'),
(367, NULL, 'global', NULL, '2018-05-22 17:47:49', '2018-05-22 17:47:49'),
(368, NULL, 'ip', '112.135.0.158', '2018-05-22 17:47:49', '2018-05-22 17:47:49'),
(369, NULL, 'global', NULL, '2018-05-22 17:47:53', '2018-05-22 17:47:53'),
(370, NULL, 'ip', '112.135.0.158', '2018-05-22 17:47:53', '2018-05-22 17:47:53'),
(371, NULL, 'global', NULL, '2018-05-27 17:13:45', '2018-05-27 17:13:45'),
(372, NULL, 'ip', '112.135.11.10', '2018-05-27 17:13:45', '2018-05-27 17:13:45'),
(373, 9, 'user', NULL, '2018-05-27 17:13:45', '2018-05-27 17:13:45'),
(374, NULL, 'global', NULL, '2018-05-30 06:29:54', '2018-05-30 06:29:54'),
(375, NULL, 'ip', '112.135.0.37', '2018-05-30 06:29:54', '2018-05-30 06:29:54'),
(376, NULL, 'global', NULL, '2018-06-10 14:33:18', '2018-06-10 14:33:18'),
(377, NULL, 'ip', '112.134.121.103', '2018-06-10 14:33:18', '2018-06-10 14:33:18'),
(378, 1, 'user', NULL, '2018-06-10 14:33:18', '2018-06-10 14:33:18'),
(379, NULL, 'global', NULL, '2018-06-16 09:18:07', '2018-06-16 09:18:07'),
(380, NULL, 'ip', '112.135.9.96', '2018-06-16 09:18:07', '2018-06-16 09:18:07'),
(381, 9, 'user', NULL, '2018-06-16 09:18:07', '2018-06-16 09:18:07'),
(382, NULL, 'global', NULL, '2018-07-12 16:26:18', '2018-07-12 16:26:18'),
(383, NULL, 'ip', '123.231.109.4', '2018-07-12 16:26:18', '2018-07-12 16:26:18'),
(384, NULL, 'global', NULL, '2018-07-12 16:27:21', '2018-07-12 16:27:21'),
(385, NULL, 'ip', '123.231.109.4', '2018-07-12 16:27:21', '2018-07-12 16:27:21'),
(386, 9, 'user', NULL, '2018-07-12 16:27:21', '2018-07-12 16:27:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fb_id` text COLLATE utf8_unicode_ci NOT NULL,
  `g_id` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8_unicode_ci,
  `branch` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `supervisor_id` int(25) DEFAULT NULL,
  `lft` int(25) DEFAULT NULL,
  `rgt` int(25) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fb_id`, `g_id`, `first_name`, `last_name`, `username`, `email`, `mobile`, `branch`, `password`, `permissions`, `last_login`, `supervisor_id`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
(1, '', '', 'Super', 'Administrator', 'super.admin', 'admin@admin.lk', NULL, 0, '$2y$10$vJ0/.9l8qByoN/ZFJlzew.U3SUrYnsI6QoPezSfeo9qDQYLJSjC/O', '{\"admin\":true,\"index\":true}', '2018-06-10 14:33:25', NULL, 7, 16, 0, 1, '2015-07-11 18:39:31', '2018-06-10 14:33:25'),
(9, '', '', 'Desawana', 'Administrator', 'desawana.admin', 'developer@admin.com', NULL, 0, '$2y$10$ijimT7X0fQADncEnlwgRZelKPlCoLE81YVjgQlX25F64Ulf5JHiaG', NULL, '2018-07-25 18:15:36', 1, 12, 15, 1, 1, '2017-01-23 19:35:38', '2018-07-25 18:28:24'),
(10, '', '', NULL, NULL, 'lakshitham', 'rullzzm@gmail.comm', NULL, 0, '$2y$10$KYJLzG26TupSdCXfRh9XRO0VctPDiWMSfbr2ooltjOGpZqZvf5KSS', NULL, '2017-12-30 21:56:38', NULL, 17, 18, 0, NULL, '2017-12-28 02:40:24', '2018-04-04 02:49:48'),
(11, '', '', 'Test', 'test', 'test', '', NULL, 0, '$2y$10$dvRBZsUn6zr2aueDjT3hVOQOlFB3AlO67NQ7aQaO4ge89y447Qo2q', NULL, NULL, 9, 13, 14, 2, NULL, '2018-04-04 02:49:44', '2018-04-04 02:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `sbu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`, `sbu_id`, `created_at`, `updated_at`) VALUES
(2, 2, 1, '2016-03-23 21:33:10', '2016-03-23 21:33:10'),
(3, 3, 2, '2016-03-23 21:56:12', '2016-03-23 21:56:12');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube_link` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `img` text COLLATE utf8_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `youtube_link`, `created_at`, `updated_at`, `img`, `user_id`) VALUES
(1, 'Man Pathanawa (Samu Aran Ya Yuthui)', 'https://www.youtube.com/watch?v=ZCdO0Xr5oG8', '2018-05-13 18:07:10', '2018-05-13 18:07:10', 'core/storage/uploads/images/video/5af7cf06209bd.jpg', 9),
(2, 'Saththai Oya (Man Witharada Adare Kale)', 'https://www.youtube.com/watch?v=oE5MewJuEI8', '2018-05-13 18:08:26', '2018-05-21 23:25:13', NULL, 9),
(3, 'Heenayakda Me - Ashan Fernando ft Dilki Uresha', 'https://www.youtube.com/watch?v=cup44QvubSs', '2018-05-13 18:09:40', '2018-05-13 18:09:40', 'core/storage/uploads/images/video/5af7cf9c017ef.jpg', 9),
(4, 'Adarei Wasthu', 'https://www.youtube.com/watch?v=CL-Tz5yQ5SQ', '2018-05-14 20:14:32', '2018-06-13 21:20:21', NULL, 9),
(5, 'Adarei Katawath', 'https://www.youtube.com/watch?v=CKiN_dU9Yuo', '2018-05-14 20:31:00', '2018-07-05 17:29:16', NULL, 9),
(6, 'SUDU Official Music Video', 'https://www.youtube.com/watch?v=FLu5mM0h2z4', '2018-05-21 23:59:45', '2018-06-13 21:29:59', NULL, 9),
(7, 'Saragaye Official Music Video', 'https://www.youtube.com/watch?v=izKxgHWzQ80', '2018-05-22 02:04:48', '2018-06-13 21:29:37', NULL, 9),
(8, 'Amathaka Karanna Ba', 'https://www.youtube.com/watch?v=kFxfPK77AXA', '2018-05-26 18:41:08', '2018-06-13 21:29:20', NULL, 9),
(9, 'Waradak Kiyanne Na', 'https://www.youtube.com/watch?v=22T5m-wT14U', '2018-05-26 21:33:20', '2018-06-13 21:29:03', NULL, 9),
(10, 'Amathaka Karanna Ba', 'https://www.youtube.com/watch?v=0GZHKWi9KUM', '2018-05-29 18:57:09', '2018-06-13 21:28:49', NULL, 9),
(11, 'Awasan Pema Mage', 'https://www.youtube.com/watch?v=rv8UsNcI_M0', '2018-05-29 19:46:10', '2018-06-13 21:28:34', NULL, 9),
(12, 'Durin Hinda Ma', 'https://www.youtube.com/watch?v=nS5rS2lSRXI', '2018-05-29 19:47:00', '2018-06-13 21:28:13', NULL, 9),
(13, 'Kiyabu Lathawe', 'https://www.youtube.com/watch?v=02nz535ELr8', '2018-05-29 19:48:19', '2018-07-05 17:29:41', NULL, 9),
(14, 'Ma Hara Giya Dine', 'https://www.youtube.com/watch?v=tSqN9HfsiT8', '2018-05-29 19:48:57', '2018-06-13 21:27:26', NULL, 9),
(15, 'Ma Me Tharam Handawala', 'https://www.youtube.com/watch?v=EMNj7Adrj7Y', '2018-05-29 19:49:21', '2018-06-13 21:27:08', NULL, 9),
(16, 'Me Hitha Langa', 'https://www.youtube.com/watch?v=FYTMDSvfPd4', '2018-05-29 19:49:49', '2018-06-13 21:26:53', NULL, 9),
(17, 'Nidukin Inu Mana', 'https://www.youtube.com/watch?v=sN2AaSZ0ogY', '2018-05-29 19:50:18', '2018-06-13 21:26:38', NULL, 9),
(18, 'Oba Ekka Mama', 'https://www.youtube.com/watch?v=LDgphkZukOQ', '2018-05-29 19:51:04', '2018-06-13 21:26:24', NULL, 9),
(19, 'Obamada Me Hithata Mage', 'https://www.youtube.com/watch?v=206gpCGqa0o', '2018-05-29 19:51:29', '2018-06-13 21:26:10', NULL, 9),
(20, 'Para Kiyana Tharukawi', 'https://www.youtube.com/watch?v=FpaPGjiOldE', '2018-05-29 19:52:09', '2018-06-13 21:25:52', NULL, 9),
(21, 'Senehasa Bidunath', 'https://www.youtube.com/watch?v=nlxU2_yPzh0', '2018-05-29 19:52:39', '2018-06-13 21:25:35', NULL, 9),
(22, 'Awasana Premayai Mage', 'https://www.youtube.com/watch?v=CSqy1zTOzW0', '2018-05-29 19:53:25', '2018-06-13 21:25:11', NULL, 9),
(24, 'Wenna Thiyena', 'https://www.youtube.com/watch?v=QvOwg08pKU4', '2018-05-29 19:54:26', '2018-06-13 21:24:13', NULL, 9),
(25, 'Akeekaruma Man Udura', 'https://www.youtube.com/watch?v=zJQ8bXmzjz4', '2018-05-29 20:18:38', '2018-06-13 21:23:59', NULL, 9),
(26, 'Waradak Kiyanne Na', 'https://www.youtube.com/watch?v=22T5m-wT14U', '2018-05-29 20:19:22', '2018-06-13 21:23:39', NULL, 9),
(28, 'Arayum Karanne', 'https://www.youtube.com/watch?v=OYAzt5836ww', '2018-05-29 20:21:05', '2018-06-13 21:23:05', NULL, 9),
(29, 'Diwranna Behe Neda Music Video', 'https://www.youtube.com/watch?v=2cCIpSWHuDY', '2018-05-31 02:27:31', '2018-06-13 21:22:49', NULL, 9),
(30, 'Dahata Nopeni Inna Official Music Video', 'https://www.youtube.com/watch?v=W9aa4IdzFhI', '2018-06-01 23:39:05', '2018-06-13 21:22:02', NULL, 9),
(31, 'Hitha Gawa Heena Official Music Video', 'https://www.youtube.com/watch?v=470nK33PpHs', '2018-06-03 00:20:38', '2018-06-13 21:22:30', NULL, 9),
(32, 'Oyata Vitharak Music Video', 'https://www.youtube.com/watch?v=g1VIjPy4eEE', '2018-06-03 01:51:57', '2018-06-13 21:21:24', NULL, 9),
(33, 'Mathaka Mawee Official Music Video', 'https://www.youtube.com/watch?v=VAwKXPWNVvY', '2018-06-06 18:53:34', '2018-06-13 21:20:56', NULL, 9),
(34, 'Aulak Nane Official Music Video', 'https://www.youtube.com/watch?v=-fCPsxVYi4s', '2018-06-15 06:37:01', '2018-06-15 06:37:01', 'core/storage/uploads/images/video/5b22aec531eb4.jpg', 9),
(35, 'Sulan Podak Wee', 'https://www.youtube.com/watch?v=tQTRRe9Jt5U', '2018-06-15 07:03:32', '2018-06-15 07:03:32', 'core/storage/uploads/images/video/5b22b4fc32ae1.jpg', 9),
(36, 'Obe Adare Official Music Video', 'https://www.youtube.com/watch?v=doPvhVdkNA4', '2018-06-26 02:56:26', '2018-06-26 02:56:26', 'core/storage/uploads/images/video/5b30fb92e74a6.jpg', 9),
(37, 'Nodakapu Dewani Budun', 'https://www.youtube.com/watch?v=5T_sH5RRWqA', '2018-07-05 02:57:34', '2018-07-05 02:57:34', 'core/storage/uploads/images/video/5b3cd9560615d.jfif', 9),
(38, 'Sareetha - Viraj Perera Official Music Video', 'https://www.youtube.com/watch?v=AHmbpkg0PaA', '2018-07-24 06:05:25', '2018-07-24 06:05:25', 'core/storage/uploads/images/video/5b5611dd3236d.jpg', 9),
(39, 'Manamaliye - Tehan Perera ft. Hot Chocolate', 'https://www.youtube.com/watch?v=yCj-DbU-aUQ', '2018-07-25 16:28:56', '2018-07-25 16:28:56', 'core/storage/uploads/images/video/5b57f5807d318.jpg', 9),
(40, 'Anthima Mohothedi - Nilan Hettiarachchi Official Music Video', 'https://www.youtube.com/watch?v=WGY7xgGHlh0', '2018-07-25 17:03:17', '2018-07-25 17:03:17', 'core/storage/uploads/images/video/5b57fd8d213f4.jpg', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement_locations`
--
ALTER TABLE `advertisement_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_song`
--
ALTER TABLE `artist_song`
  ADD KEY `artist_song_artist_id_foreign` (`artist_id`),
  ADD KEY `artist_song_song_id_foreign` (`song_id`);

--
-- Indexes for table `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audio_user_id_foreign` (`user_id`);

--
-- Indexes for table `dynamic_pages`
--
ALTER TABLE `dynamic_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fonts-list`
--
ALTER TABLE `fonts-list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lyrics`
--
ALTER TABLE `lyrics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`) USING BTREE;

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_user_id_foreign` (`user_id`);

--
-- Indexes for table `slider_song`
--
ALTER TABLE `slider_song`
  ADD KEY `slider_song_slider_id_foreign` (`slider_id`),
  ADD KEY `slider_song_song_id_foreign` (`song_id`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `songs_video_id_foreign` (`video_id`),
  ADD KEY `songs_lyrics_id_foreign` (`lyrics_id`),
  ADD KEY `songs_audio_id_foreign` (`audio_id`),
  ADD KEY `songs_user_id_foreign` (`user_id`);

--
-- Indexes for table `song_lyrics_artist`
--
ALTER TABLE `song_lyrics_artist`
  ADD KEY `song_lyrics_artist_artist_id_foreign` (`artist_id`),
  ADD KEY `song_lyrics_artist_song_id_foreign` (`song_id`);

--
-- Indexes for table `song_music_artist`
--
ALTER TABLE `song_music_artist`
  ADD KEY `music_artist_table_artist_id_foreign` (`artist_id`),
  ADD KEY `music_artist_table_song_id_foreign` (`song_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `audio`
--
ALTER TABLE `audio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `dynamic_pages`
--
ALTER TABLE `dynamic_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fonts-list`
--
ALTER TABLE `fonts-list`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;

--
-- AUTO_INCREMENT for table `lyrics`
--
ALTER TABLE `lyrics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=773;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `artist_song`
--
ALTER TABLE `artist_song`
  ADD CONSTRAINT `artist_song_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `artist_song_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `audio`
--
ALTER TABLE `audio`
  ADD CONSTRAINT `audio_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slider_song`
--
ALTER TABLE `slider_song`
  ADD CONSTRAINT `slider_song_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `slider_song_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `songs_audio_id_foreign` FOREIGN KEY (`audio_id`) REFERENCES `audio` (`id`),
  ADD CONSTRAINT `songs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `song_lyrics_artist`
--
ALTER TABLE `song_lyrics_artist`
  ADD CONSTRAINT `song_lyrics_artist_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `song_lyrics_artist_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `song_music_artist`
--
ALTER TABLE `song_music_artist`
  ADD CONSTRAINT `music_artist_table_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `music_artist_table_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
