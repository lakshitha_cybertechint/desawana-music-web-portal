<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
  public function songIds()
  {
    return $this->hasMany(ArtistSongs::class)->has("song");
  }

  public function lyricsIds()
  {
    return $this->hasMany(SongsLyricsArtist::class);
  }

  public function musicIds()
  {
    return $this->hasMany(SongMusicArtists::class);
  }
}
