<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
  protected $appends = array('shortTitle');

  public function song()
  {
    return $this->hasOne(Song::class);
  }

  public function getShortTitleAttribute($length = 25)
  {
  	if(!$length){
		$length = 25;
	}

    if (strlen($this->title) > $length) {
      return substr($this->title, 0, $length)."...";
    }
    return $this->title;
  }
}
