<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);
});

// start front end routes
Route::get('/', [
  'as' => '/', 'uses' => 'HomeController@index'
]);
Route::get('privacy-policy', [
  'as' => 'privacy-policy', 'uses' => 'DynamicPagesController@privacyPolicyPage'
]);
Route::get('terms-of-service', [
  'as' => 'terms-of-service', 'uses' => 'DynamicPagesController@termsOfServices'
]);
Route::get('about-us', [
  'as' => 'about-us', 'uses' => 'HomeController@aboutUs'
]);
Route::get('disclaimer', [
  'as' => 'disclaimer', 'uses' => 'HomeController@disclaimer'
]);


// start contact us routes
Route::get('contact-us', [
  'as' => 'contact-us', 'uses' => 'ContactUsController@create'
]);
Route::post('contact-us', [
  'as' => 'contact-us', 'uses' => 'ContactUsController@store'
]);

// lyrics routes
Route::get('lyrics', [
  'as' => 'lyrics', 'uses' => 'LyricsController@index'
]);
Route::get('lyric/{id}', [
  'as' => 'lyric/{id}', 'uses' => 'LyricsController@show'
]);
Route::get('lyrics/group/{tag}', [
  'as' => 'lyrics/group/{tag}', 'uses' => 'LyricsController@showByTag'
]);

// videos routes
Route::get('latest-videos', [
  'as' => 'latest-videos', 'uses' => 'VideosController@index'
]);
Route::get('old-videos', [
  'as' => 'old-videos', 'uses' => 'VideosController@oldVideos'
]);
Route::get('video/{id}', [
  'as' => 'video/{id}', 'uses' => 'VideosController@show'
]);
Route::get('latest-videos/group/{tag}', [
  'as' => 'latest-videos/group/{tag}', 'uses' => 'VideosController@showByTag'
]);
Route::get('old-videos/group/{tag}', [
  'as' => 'old-videos/group/{tag}', 'uses' => 'VideosController@showByTagOld'
]);


// end contact us routes

// start front audios routes
Route::get('latest-audios', [
  'as' => 'latest-audios', 'uses' => 'FrontAudiosController@index'
]);

Route::get('old-audios', [
  'as' => 'old-audios', 'uses' => 'FrontAudiosController@oldAudios'
]);
Route::get('audio/{id}', [
  'as' => 'audio/{id}', 'uses' => 'FrontAudiosController@show'
]);
Route::get('latest-audios/group/{tag}', [
  'as' => 'latest-audios/group/{tag}', 'uses' => 'FrontAudiosController@showByTag'
]);
Route::get('old-audios/group/{tag}', [
  'as' => 'old-audios/group/{tag}', 'uses' => 'FrontAudiosController@showByTagOld'
]);

Route::get("audio-download/{id}", 'FrontAudiosController@downloadAudio');
// end front audios routes



// start frnt artist routes
Route::get('artists/group/{tag}', 'ArtistController@showByTag');
Route::post('artists/{artistsId}/getAudios', 'ArtistController@getAudiosPaginate');
Route::post('artists/{artistsId}/getVideos', 'ArtistController@getVideosPaginate');
Route::post('artists/{artistsId}/getMusic', 'ArtistController@getMusicPaginate');
Route::post('artists/{artistsId}/getLyrics', 'ArtistController@getLyricPaginate');
Route::resource('artists', 'ArtistController');
// end frnt artist routes



//start front advertiesment routes
Route::get('advertise', [
  'as' => 'advertise', 'uses' => 'FrontAdController@index'
]);
Route::post('advertise', [
  'as' => 'advertise', 'uses' => 'FrontAdController@store'
]);
//end front advertiesment routes

//start front search routes
Route::get('search', [
  'as' => 'search', 'uses' => 'HomeController@searchResults'
]);

//end front search routes

// end front end routes





/**
 * USER AUTHENTICATION MIDDLEWARE
 */






/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

// Route::get('user/register', [
//   'as' => 'user.register', 'uses' => 'AuthController@userRegisterView'
// ]);
// Route::post('user/register', [
//   'as' => 'user.register', 'uses' => 'AuthController@userRegister'
// ]);

// Route::get('user/profile', [
//   'as' => 'user.profile', 'uses' => 'WebController@profileView'
// ]);

/**
 * USER LOGIN VIA FACEBOOK/GOOGLE/TWITTER/LINKDIN ETC
 */


// Route::get('auth/facebook', 'AuthController@redirectToFacebook');
// Route::get('auth/facebook/callback', 'AuthController@handleFacebookCallback');

// Route::get('auth/google', 'AuthController@redirectToGoogle');
// Route::get('auth/google/callback', 'AuthController@handleGoogleCallback');
