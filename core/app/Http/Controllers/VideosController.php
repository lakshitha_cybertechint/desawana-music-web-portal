<?php

namespace App\Http\Controllers;

use App\Video;
use App\Lyric;
use App\Audio;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $videos = Video::with("song")->whereHas("song",function($q){
        $q->where("old", 0);
      })->has("song.artistSongs")->orderBy("created_at", "desc")->paginate(20);
        return view("front.videos.videos", ["videos" => $videos]);
    }

    public function oldVideos()
    {
      $videos = Video::with("song")->whereHas("song",function($q){
        $q->where("old", 1);
      })->has("song.artistSongs")->orderBy("created_at", "desc")->paginate(20);
        return view("front.videos.old-videos", ["videos" => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $video = Video::find($id);
      if ($video) {
        $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(5)->get();
        $artists = $video->song->artistSongs->pluck("artist_id")->toArray();
        $relatedVideos = Video::where("id", "!=", $video->id)->whereHas("song", function ($q1) use ($artists){
          return $q1->whereHas('artistSongs', function ($q2) use ($artists)
          {
            return $q2->whereIn("artist_id", $artists);
          });
        })->limit(3)->get();
        $curentSongId = $video->song->id;
        $relatedLyrics = Lyric::whereHas("song", function($q1) use ($artists, $curentSongId){
          return $q1->whereHas("artistSongs", function($q2) use ($artists, $curentSongId){
            return $q2->whereIn("artist_id", $artists);
          });
        })->limit(4)->get();

        $relatedAudios = Audio::whereHas("song", function ($q1) use ($artists, $curentSongId){

          return $q1->whereHas('artistSongs', function ($q2) use ($artists, $curentSongId){

            return $q2->whereIn("artist_id", $artists);

          });

        })->limit(4)->get();

        return view("front.videos.video", [
          "video" => $video,
          "latestVideos" => $latestVideos,
          "relatedVideos" => $relatedVideos,
          "relatedLyrics" => $relatedLyrics,
          "relatedAudios" => $relatedAudios,
        ]);
      }
      return view("front.videos.videos");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showByTag($tag)
    {
      $videos = array();
      if ($tag == "0-9") {
        $videos = Video::with("song")->whereHas("song",function($q){
          $q->where("old", 0);
        })->where("title", "regexp", '^[0-9]+')->paginate(20);
      }else{
        $videos = Video::with("song")->whereHas("song",function($q){
          $q->where("old", 0);
        })->where("title", "LIKE", $tag."%")->paginate(20);
      }
      $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(12)->get();

      return view("front.videos.videos", ["videos" => $videos, "tag" => $tag, "latestVideos" => $latestVideos]);

    }
    public function showByTagOld($tag)
    {
      $videos = array();
      if ($tag == "0-9") {
        $videos = Video::with("song")->whereHas("song",function($q){
          $q->where("old", 1);
        })->where("title", "regexp", '^[0-9]+')->paginate(20);
      }else{
        $videos = Video::with("song")->whereHas("song",function($q){
          $q->where("old", 1);
        })->where("title", "LIKE", $tag."%")->paginate(20);
      }
      $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(12)->get();

      return view("front.videos.old-videos", ["videos" => $videos, "tag" => $tag, "latestVideos" => $latestVideos]);

    }
}
