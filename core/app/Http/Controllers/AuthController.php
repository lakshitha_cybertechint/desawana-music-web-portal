<?php namespace App\Http\Controllers;

use Input;
use Sentinel;
use Session;
use Socialite;
use DB;

use UserManage\Models\User;
use UserRoles\Models\UserRole;


class AuthController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Auth Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "LOGIN page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('auth');
	}

	 /**
     * Online user Registration View
     *
     * @return Redirect to online user add view
     */
    public function userRegisterView() {    
        return view('layouts.front.register');
    }
    /**
	 * Online user Registration data to database
	 *
	 * @return Redirect to online user add
	 */
	public function userRegister() {

		try {
			DB::transaction(function (){

			$user = Sentinel::registerAndActivate([								
				'email' => Input::get('email'),
				'username' => Input::get('user_name'),
				'password' => Input::get('password'),
			]);

			if (!$user) {
				throw new TransactionException('', 100);
			}

			$user->makeRoot();

			$role = Sentinel::findRoleById(1);
			$role->users()->attach($user);

			User::rebuild();

		});

		} catch (TransactionException $e) {
			if ($e->getCode() == 100) {
				Log::info("Could not register user");

				return redirect('user/register')->with(['error' => true,
					'error.message' => "Could not register user",
					'error.title' => 'Ops!']);
			}
		} catch (Exception $e) {

		}

		return redirect('/')->with(['success' => true,
				'success.message' => 'Registered successfully!',
				'success.title' => 'Registered!']);

	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function loginView() {
		try {
			if (!Sentinel::check()) {				
				return view('layouts.back.login');
			} else {
				$redirect = Session::get('loginRedirect', '');
				Session::forget('loginRedirect');
				return redirect($redirect);
			}
		} catch (\Exception $e) {
			return view('layouts.back.login')->withErrors(['login' => $e->getMessage()]);
		}
	}

	public function login() {

		
		$credentials = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
		);

		if (Input::get('remember')) {
			$remember = true;
		} else {
			$remember = false;
		}


		try {

			$user = Sentinel::authenticate($credentials, $remember);

			if ($user) {

				if($user->hasAnyAccess(['front'])){
					$redirect = Session::get('loginRedirect', '/');
				}else{
					$redirect = Session::get('loginRedirect', 'admin');
				}
				Session::forget('loginRedirect');

				return redirect($redirect);

			} else {
				$msg = 'Invalid username/password. Try again!';
			}
		} catch (\Exception $e) {
			$msg = $e->getMessage();
		}
		return redirect()->route('user.login')->withErrors(array(
			'login' => $msg))->withInput(Input::except('password'));
	}

	/*
		*	@method logout()
		*	@description Logging out the logged in user
		*	@return URL redirection
	*/
	public function logout() {
		Sentinel::logout();
		return redirect()->to('/admin');

	}

	 /**
     * Redirect the user to the FACEBOOK authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
 	/**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from FACEBOOK.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();

        // $user->token;
    }/**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->user();

        // $user->token;
    }
}
