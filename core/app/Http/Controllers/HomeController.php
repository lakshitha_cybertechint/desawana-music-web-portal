<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;
use App\Audio;
use App\Lyric;
use App\Artist;
use App\Slider;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $latestVideos = Video::with("song")->has("song")->whereHas("song", function($q){
        return $q->where("old", 0);
      })->orderBy("created_at", "desc")->limit(6)->get();
      $latestAudios = Audio::with("song")->has("song")->whereHas("song", function($q){
        return $q->where("old", 0);
      })->orderBy("created_at", "desc")->limit(6)->get();

      $mostDownloadedAudios = Audio::with("song")->has("song")->get();

      $mostDownloadedAudios = $mostDownloadedAudios->sortByDesc(function($audio, $key){
        return intval($audio->song->audio_download);
      })->take(6);

        return view("front.index", [
          "latestVideos" => $latestVideos,
          "latestAudios" => $latestAudios,
          "mostDownloadedAudios" => $mostDownloadedAudios,
          "sliders" => Slider::getActiveSliders()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function aboutUs()
    {
      return view("front.aboutUs");
    }

    public function disclaimer()
    {
      return view("front.disclaimer");
    }

    public function searchResults(Request $request)
    {

        $queries = explode(' ', $request->q);
        $queries = array_filter(array_map('trim', $queries)); // removing spaces and space array values
        $relatedVideos = [];
        $relatedAudios = [];
        $relatedLyrics = [];
        $relatedArtists = [];

        if(count($queries) > 0){
             // start videos
            $relatedVideos = Video::has('song');

            foreach ($queries as $key =>$query) {
                $relatedVideos = $relatedVideos->where('title', 'LIKE', '%' . $query . '%')
                ->orWhereHas("song.artistSongs.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                })
                ->orWhereHas("song.musicArtists.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                })
                ->orWhereHas("song.lyricArtists.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                });
            }
            $relatedVideos = $relatedVideos->get();
        // end videos

        // start audios
            $relatedAudios = Audio::has('song');

            foreach ($queries as $query) {
                $relatedAudios = $relatedAudios->Where('title', 'LIKE', '%' . $query . '%')
                ->orWhereHas("song.artistSongs.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                })
                ->orWhereHas("song.musicArtists.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                })
                ->orWhereHas("song.lyricArtists.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                });
            }
            $relatedAudios = $relatedAudios->get();
        // end audios

        // start lyrics
            $relatedLyrics = Lyric::has('song');

            foreach ($queries as $query) {
                $relatedLyrics = $relatedLyrics->Where('title', 'LIKE', '%' . $query . '%')
                ->orWhereHas("song.artistSongs.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                })
                ->orWhereHas("song.musicArtists.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                })
                ->orWhereHas("song.lyricArtists.artist", function($q) use ($query){
                  return $q->where('name',  'LIKE', '%' . $query . '%');
                });
            }
            $relatedLyrics = $relatedLyrics->get();
        // end lyrics

        // start Artists
            $relatedArtists = Artist::select('id', 'img', 'name');

            foreach ($queries as $query) {
                $relatedArtists = $relatedArtists->Where('name', 'LIKE', '%' . $query . '%');
            }
            $relatedArtists = $relatedArtists->get();
        // end Artists
        }



        return view("front.searchResults",[
            'query' =>  $request->q,
            'relatedVideos' => $relatedVideos,
            'relatedAudios' => $relatedAudios,
            'relatedLyrics' => $relatedLyrics,
            'relatedArtists' => $relatedArtists,
        ]);
    }


}
