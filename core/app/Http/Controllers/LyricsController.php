<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lyric;
use App\Video;
use App\Audio;

class LyricsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latestLyrics = Lyric::orderBy("created_at", "DESC")->has("song")->paginate(20);
        $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(5)->get();

        return view("front.lyrics.lyrics", ["latestLyrics" => $latestLyrics, "latestVideos" => $latestVideos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
          $artists = $lyric->song->artistSongs->pluck("artist_id")->toArray();

          $relatedAudios = Audio::whereHas("song", function ($q1) use ($artists){

            return $q1->whereHas('artistSongs', function ($q2) use ($artists)

            {

              return $q2->whereIn("artist_id", $artists);

            });

          })->limit(5)->get();

          $relatedVideos = Video::whereHas("song", function ($q1) use ($artists){
            return $q1->whereHas('artistSongs', function ($q2) use ($artists)
            {
              return $q2->whereIn("artist_id", $artists);
            });
          })->limit(4)->get();


          return view("front.lyrics.lyric", [
            "lyric" => $lyric,
            "relatedAudios" => $relatedAudios,
            "relatedVideos" => $relatedVideos,
          ]);
        }

        return redirect()->route('lyrics');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showByTag($tag)
    {
      $lyrics = array();
      if ($tag == "0-9") {
        $lyrics = Lyric::with("song")->has("song")->where("title", "regexp", '^[0-9]+')->paginate(20);
      }else{
        $lyrics = Lyric::with("song")->has("song")->where("title", "LIKE", $tag."%")->paginate(20);
      }

      return view("front.lyrics.lyrics", ["latestLyrics" => $lyrics, "tag" => $tag]);

    }
}
