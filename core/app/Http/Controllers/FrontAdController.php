<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Advertisement;
use App\AdvertisementLocation;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use Image;

class FrontAdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ads = AdvertisementLocation::all();
        return view("front.advertise", ["ads" => $ads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = $this->validateInputs($request->all());

      if ($validator->fails()) {
        return Response::json(['errors' => $validator->getMessageBag()->toArray()], 200);
      }else{
        // $path = 'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'ads';
        // $destinationPath = storage_path($path);
        // $project_fileName = 'ad_' . date('YmdHis'). '.'.$request->image->getClientOriginalExtension();
        // Image::make($request->image)->save(public_path('core'.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$project_fileName));

      $path = 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'ads';
      $project_fileName = 'ad_' . date('YmdHis') . '.' . $request->image->getClientOriginalExtension();
      $path1 = public_path('core' . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR);
      $request->file('image')->move($path1, $project_fileName);

      $ad =   Advertisement::create([
          "name" => $request->name,
          "email" => $request->email,
          "contact_number" => $request->contact_number,
          "position_id" => $request->position,
          "image_url" => "storage".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."ads".DIRECTORY_SEPARATOR.$project_fileName,
          "message" => $request->message,
          "status" => 1
        ]);
        if($ad) {
          return Response::json(['success' => "19199212", "message" => "Thank you for reaching us. We will get back to you shortly. Your reference is #$ad->id"], 200);
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validateInputs($inputs)
    {
      $rules = [
        "name" => "required",
        "email" => "required|email",
        "contact_number" => "required|regex:/94[0-9]{9}/",
        "message" => "required",
        "position" => "required:exists:advertisement_locations,id",
        "image" => "required|mimes:jpeg,jpg,png,gif",
        "g-recaptcha-response" => "required",
      ];
      $msgs = [
        "g-recaptcha-response.required" => "Please complete the recaptcha."
      ];
      return Validator::make($inputs, $rules, $msgs);
    }
}
