<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view("front.contact-us");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validateInputs($request->all());

        if ($validator->fails()) {
          return Response::json(['errors' => $validator->getMessageBag()->toArray()], 200);
        }else{
        $contacUs =   ContactUs::create([
            "name" => $request->name,
            "email" => $request->email,
            "contact_number" => $request->contact_number,
            "subject" => $request->subject,
            "message" => $request->message
          ]);
          if($contacUs) {
            return Response::json(['success' => "19199212", "message" => "Thank you for reaching us. We will get back to you shortly."], 200);
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validateInputs($inputs)
    {
      $rules = [
        "name" => "required",
        "email" => "required|email",
        "contact_number" => "required|regex:/94[0-9]{9}/",
        "subject" => "required",
        "message" => "required",
        "g-recaptcha-response" => "required",
      ];
      $msgs = [
        "g-recaptcha-response.required" => "Please complete the recaptcha."
      ];
      return Validator::make($inputs, $rules, $msgs);
    }
}
