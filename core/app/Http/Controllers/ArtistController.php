<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Artist;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $artists = Artist::orderBy("name", "desc")->paginate(20);

      return view("front.artist.artists", [
        "artists" => $artists
      ]);
    }

    public function showByTag($tag)
    {
      $artists = array();
      if ($tag == "0-9") {
        $artists = Artist::where("name", "regexp", '^[0-9]+')->paginate(20);
      }else{
        $artists = Artist::where("name", "LIKE", $tag."%")->paginate(20);
      }


      return view("front.artist.artists", [
        "artists" => $artists
      ]);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $artist = Artist::find($id);
      if ($artist) {
        return view("front.artist.artistDetails",[
          "artist" => $artist,
        ]);
      }
      return redirect("artists");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAudiosPaginate($artistId)
    {
      $artist = Artist::has("songIds.song.audio")->find($artistId);
      if ($artist) {
        $displayedAudios = [];
        $audiosPerPage = 6 ;
        $audiosPage = 1;
        $audioData = "";
        if (request()->has('page') && request()->get('page') > 0) {
          $audiosPage = request()->get('page');
        }

        foreach ($artist->songIds->forPage($audiosPage,  $audiosPerPage) as $songId){
          if (isset($songId->song->audio) && !in_array($songId->song_id, $displayedAudios)){

            $displayedAudios[] = $songId->song_id;

          $audioData .= '<li >
          <a class="m-thumbnail" href="'.url("audio/".$songId->song->audio->id).'"><img width="60" height="60" src="'.url($songId->song->album_art).'" alt="#"></a>
            <h3><a href="'.url("audio/".$songId->song->audio->id).'">'.$songId->song->audio->shortTitle.'</a></h3>
            <span>';
            if ($songId->song->artistSongs){

            if($songId->song->artistSongs->count()== 1 ){
              $audioData .= $songId->song->artistSongs->first()->artist->name ;
            }else{

              $audioData .= "Various Artists" ;
            }

          }
          $audioData .= ' </span>'
          .$songId->song->audio_download.' Downloads
          </li>';

        }
        }

        return response()->json([
          "audioData" => $audioData
        ], 200);
      }
      return response()->json([
        "audioData" => ""
      ], 200);
    }
    public function getMusicPaginate($artistId)
    {
      $artist = Artist::has("musicIds.song.audio")->find($artistId);
      if ($artist) {
        $displayedAudios = [];
        $audiosPerPage = 6 ;
        $audiosPage = 1;
        $audioData = "";
        if (request()->has('page') && request()->get('page') > 0) {
          $audiosPage = request()->get('page');
        }

        foreach ($artist->songIds->forPage($audiosPage,  $audiosPerPage) as $songId){
          if (isset($songId->song->audio) && !in_array($songId->song_id, $displayedAudios)){

            $displayedAudios[] = $songId->song_id;

          $audioData .= '<li >
          <a class="m-thumbnail" href="'.url("audio/".$songId->song->audio->id).'"><img width="60" height="60" src="'.url($songId->song->album_art).'" alt="#"></a>
            <h3><a href="'.url("audio/".$songId->song->audio->id).'">'.$songId->song->audio->shortTitle.'</a></h3>
            <span>';
            if ($songId->song->artistSongs){

            if($songId->song->artistSongs->count()== 1 ){
              $audioData .= $songId->song->artistSongs->first()->artist->name ;
            }else{

              $audioData .= "Various Artists" ;
            }

          }
          $audioData .= ' </span>'
          .$songId->song->audio_download.' Downloads
          </li>';

        }
        }

        return response()->json([
          "audioData" => $audioData
        ], 200);
      }
      return response()->json([
        "audioData" => ""
      ], 200);
    }
    public function getLyricPaginate($artistId)
    {
      $artist = Artist::has("lyricsIds.song.audio")->find($artistId);
      if ($artist) {
        $displayedAudios = [];
        $audiosPerPage = 6 ;
        $audiosPage = 1;
        $audioData = "";
        if (request()->has('page') && request()->get('page') > 0) {
          $audiosPage = request()->get('page');
        }

        foreach ($artist->songIds->forPage($audiosPage,  $audiosPerPage) as $songId){
          if (isset($songId->song->audio) && !in_array($songId->song_id, $displayedAudios)){

            $displayedAudios[] = $songId->song_id;

          $audioData .= '<li >
          <a class="m-thumbnail" href="'.url("audio/".$songId->song->audio->id).'"><img width="60" height="60" src="'.url($songId->song->album_art).'" alt="#"></a>
            <h3><a href="'.url("audio/".$songId->song->audio->id).'">'.$songId->song->audio->shortTitle.'</a></h3>
            <span>';
            if ($songId->song->artistSongs){

            if($songId->song->artistSongs->count()== 1 ){
              $audioData .= $songId->song->artistSongs->first()->artist->name ;
            }else{

              $audioData .= "Various Artists" ;
            }

          }
          $audioData .= ' </span>'
          .$songId->song->audio_download.' Downloads
          </li>';

        }
        }

        return response()->json([
          "audioData" => $audioData
        ], 200);
      }
      return response()->json([
        "audioData" => ""
      ], 200);
    }
    public function getVideosPaginate($artistId)
    {
      $artist = Artist::has("songIds.song.video")->find($artistId);
      if ($artist) {
        $displayedvideos = [];
        $videosPerPage = 6 ;
        $videosPage = 1;
        $videoData = "";

        if (request()->has('page') && request()->get('page') > 0) {
          $videosPage = request()->get('page');
        }

      foreach ($artist->songIds->forPage($videosPage,  $videosPerPage) as $songId){
          if (isset($songId->song->video) && !in_array($songId->song_id, $displayedvideos)){

            $displayedvideos[] = $songId->song_id;


          $videoData .= '<a href="'.url("video/".$songId->song->video->id).'" class="grid_4 setHeight_2">
            <img src="https://img.youtube.com/vi/'.explode("watch?v=", $songId->song->video->youtube_link)[1].'/sddefault.jpg"  alt="#">
            <span class="text-justify"><p class="setHeight_2" data-mh="video-group"><strong >'.substr($songId->song->video->shortTitle,0,26).'</strong></p>';
              if ($songId->song->video->song->artistSongs){

              if($songId->song->video->song->artistSongs->count()== 1){

                $videoData .=  $songId->song->video->song->artistSongs->first()->artist->name ;
              }else{

                $videoData .=  "Various Artists" ;
              }
            }
            $videoData .= "</span>
          </a>";

        }
      }

        return response()->json([
          "videoData" => $videoData
        ], 200);
      }
      return response()->json([
        "videoData" => ""
      ], 200);
    }
}
