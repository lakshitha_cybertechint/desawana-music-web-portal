<?php



namespace App\Http\Controllers;



use App\Lyric;
use App\Video;

use App\Audio;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;

use Response;

use File;

class FrontAudiosController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

      $audios = Audio::with("song")->whereHas("song", function($q){
        $q->where("old", 0);
      })->orderBy("created_at", "desc")->paginate(20);

      $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(5)->get();

      return view("front.audios.audios", ["audios" => $audios, "latestVideos" => $latestVideos]);

    }

public function oldAudios()
{
  $audios = Audio::with("song")->whereHas("song", function($q){
    $q->where("old", 1);
  })->orderBy("created_at", "desc")->paginate(20);

  $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(5)->get();

  return view("front.audios.old-audios", ["audios" => $audios, "latestVideos" => $latestVideos]);
}


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

      $audio = Audio::find($id);

      if ($audio) {

        $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(5)->get();

        $artists = $audio->song->artistSongs->pluck("artist_id")->toArray();
        $curentSongId = $audio->song->id;

        $relatedVideos = Video::whereHas("song", function ($q1) use ($artists, $curentSongId){
          return $q1->whereHas('artistSongs', function ($q2) use ($artists, $curentSongId)
          {
            return $q2->whereIn("artist_id", $artists);
          });
        })->limit(12)->get();

        $relatedAudios = Audio::where("id", "!=", $audio->id)->whereHas("song", function ($q1) use ($artists){

          return $q1->whereHas('artistSongs', function ($q2) use ($artists)

          {
            return $q2->whereIn("artist_id", $artists);

          });

        })->limit(10)->get();



        $relatedLyrics = Lyric::whereHas("song", function($q1) use ($artists, $curentSongId){
          return $q1->whereHas("artistSongs", function($q2) use ($artists, $curentSongId){
            return $q2->whereIn("artist_id", $artists);
          });
        })->limit(4)->get();



          return view("front.audios.audio", [

            "audio" => $audio,
            "latestVideos" => $latestVideos,
            "relatedAudios" => $relatedAudios,
            "relatedVideos" => $relatedVideos,
            "relatedLyrics" => $relatedLyrics,

        ]);

      }

      return redirect()->route('all-audios');

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }



    public function showByTag($tag)

    {

      $audios = array();

      if ($tag == "0-9") {

        $audios = Audio::where("title", "regexp", '^[0-9]+')->whereHas("song", function($q){
          $q->where("old", 0);
        })->paginate(20);

      }else{

        $audios = Audio::where("title", "LIKE", $tag."%")->whereHas("song", function($q){
          $q->where("old", 0);
        })->paginate(20);

      }

      $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(12)->get();



      return view("front.audios.audios", ["audios" => $audios, "tag" => $tag, "latestVideos" => $latestVideos]);



    }
    public function showByTagOld($tag)

    {

      $audios = array();

      if ($tag == "0-9") {

        $audios = Audio::where("title", "regexp", '^[0-9]+')->whereHas("song", function($q){
          $q->where("old", 1);
        })->paginate(20);

      }else{

        $audios = Audio::where("title", "LIKE", $tag."%")->whereHas("song", function($q){
          $q->where("old", 1);
        })->paginate(20);

      }

      $latestVideos = Video::with("song")->has("song")->orderBy("created_at", "desc")->limit(12)->get();



      return view("front.audios.old-audios", ["audios" => $audios, "tag" => $tag, "latestVideos" => $latestVideos]);



    }



    public function downloadAudio($idEncoded, Request $request)

    {

      $id = base64_decode($idEncoded);



      $audio = Audio::find($id);

      if ($audio) {

        $audioUrl = str_replace( "/", "\\", public_path().$audio->src); // file path for windows servers
        $audioUrl1 = $_SERVER['DOCUMENT_ROOT'].$audio->src; // file path for linux servers



          $header = [
            'desawana-download-token' => base64_encode($audio->song->audio_download)
          ];
       if (file_exists($audioUrl)) {
         if($request->header('desawana-download-token') == null){
          $audio->song->increment("audio_download");
          }

            if ($audio->song->save()) {


              $array = explode('.', $audio->src);
              $extension = end($array);
              return Response::download($audioUrl, $audio->title." [desawana.com].".$extension, $header);

            }

        }else if (file_exists($audioUrl1)) {
          if($request->header('desawana-download-token') == null){
          $audio->song->increment("audio_download");
          }

            if ($audio->song->save()) {

              $array = explode('.', $audio->src);
              $extension = end($array);

              return Response::download($audioUrl1, $audio->title." [desawana.com].".$extension, $header);
            }

        }





        return redirect('audio/'.$audio->id);

      }



      return redirect()->route('all-audios');



    }

}
