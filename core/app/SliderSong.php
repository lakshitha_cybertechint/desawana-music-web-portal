<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderSong extends Model
{
  protected $table = "slider_song";
  public function song()
  {
    return $this->belongsTo(Song::class);
  }
}
