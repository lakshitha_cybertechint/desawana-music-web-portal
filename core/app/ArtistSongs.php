<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistSongs extends Model
{
  protected $table = "artist_song";
    public function artist()
    {
      return $this->belongsTo(Artist::class);
    }

    public function song()
    {
      return $this->belongsTo(Song::class);
    }
}
