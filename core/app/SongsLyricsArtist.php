<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongsLyricsArtist extends Model
{
    protected $table = "song_lyrics_artist";

    public function artist()
    {
      return $this->belongsTo(Artist::class);
    }

    public function song()
    {
      return $this->belongsTo(Song::class);
    }
}
