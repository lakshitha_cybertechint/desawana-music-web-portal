<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
  public static function getActiveSliders()
  {
    $today = Carbon::now()->toDateString();
    return Self::whereDate('expire_date', '>=', $today)->get();
  }

  public function sliderSongs()
  {
    return $this->hasMany(SliderSong::class);
  }
}
