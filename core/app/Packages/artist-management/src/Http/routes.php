<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/artist', 'namespace' => 'ArtistManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'artist.add', 'uses' => 'ArtistController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'artist.edit', 'uses' => 'ArtistController@editView'
      ]);

      Route::get('list', [
        'as' => 'artist.list', 'uses' => 'ArtistController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'artist.list', 'uses' => 'ArtistController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'artist.image.delete', 'uses' => 'ArtistController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'artist.add', 'uses' => 'ArtistController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'artist.edit', 'uses' => 'ArtistController@edit'
      ]);


      Route::post('delete', [
        'as' => 'artist.delete', 'uses' => 'ArtistController@delete'
      ]);


    });
});
