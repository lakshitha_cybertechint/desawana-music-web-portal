<?php

namespace ArtistManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use ArtistManage\Models\Artist;
use Image;

class ArtistController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('ArtistManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'name' => 'required|max:255',
        'image' => 'required',
        'fb' => 'url',
        'twitter' => 'url',
        'utube' => 'url',
      ]);

      $path = 'uploads/images/artists';
      $destinationPath = storage_path($path);
      $project_fileName = 'artists-' . date('YmdHis'). '.png';
      Image::make($request->image)->save('core/storage/'.$path.'/'.$project_fileName);

      $request->merge(['img' => 'core/storage/'.$path.'/'.$project_fileName, 'user_id' => Sentinel::getUser()->id]);

      Artist::create($request->all());


      return redirect('admin/artist/add')->with(['success' => true,
        'success.message' => 'Artist Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('ArtistManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Artist::get();

            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $artist) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $artist->name);
                $permissions = Permission::whereIn('name', ['artist.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/artist/edit/' . $artist->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['artist.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $artist->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }

            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:artists,id']);
      $id = $request->input('id');
      $artist = Artist::with('songs','lyricsSong','musicSong')->find($id);

      if (count($artist->songs) || count($artist->lyricsSong) ||  count($artist->musicSong)) {
        return response()->json(['error' => 'cant delete artist that is added to a song'],401);
      }

      $artist->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $artist = Artist::findOrFail($id);
      return view('ArtistManage::edit',compact('artist'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'name' => 'required|max:255',
        'fb' => 'url',
        'twitter' => 'url',
        'utube' => 'url',
      ]);

      $artist = Artist::findOrFail($id);

      if ($request->has('image')) {
        $path = 'uploads/images/artists';
        $destinationPath = storage_path($path);
        $project_fileName = 'artists-' . date('YmdHis'). '.png';
        Image::make($request->image)->save('core/storage/'.$path.'/'.$project_fileName);

        $request->merge(['img' => 'core/storage/'.$path.'/'.$project_fileName]);
      }

      $request->merge(['user_id' => Sentinel::getUser()->id]);

      $artist->update($request->all());


      return redirect('admin/artist/edit/' . $id)->with(['success' => true,
        'success.message' => 'Artist updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
