<?php
namespace ArtistManage\Models;

use Illuminate\Database\Eloquent\Model;
use SongManage\Models\Song;

class Artist extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','img', 'fb', 'utube', 'twitter', 'bio', 'user_id','meta_description','meta_keywords'];

	public function user()
	{
		return $this->belongsTo(\UserManage\Models\User::class);
	}

	public function songs()
	{
		return $this->belongsToMany(Song::class);
	}

	public function lyricsSong()
	{
	  return $this->belongsToMany(Artist::class, 'song_lyrics_artist', 'artist_id' , 'song_id');
	}

	public function musicSong()
	{
	  return $this->belongsToMany(Artist::class, 'song_music_artist', 'artist_id', 'song_id');
	}
}
