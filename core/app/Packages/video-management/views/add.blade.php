@extends('layouts.back.master') @section('current_title','New Video')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style>
.rating-container .rating-stars:before {
    text-shadow: none;
}
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
<ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('admin/video/list')}}">Video Management</a></li>

    <li class="active">
        <span>New Video</span>
    </li>
</ol>
</div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
    <div class="hpanel">
        <div class="panel-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
          @endif
          <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
              {!!Form::token()!!}

              <div class="form-group"><label class="col-sm-2 control-label">TITLE</label>
                  <div class="col-sm-10"><input type="text" class="form-control" name="title" value="{{ old('title') }}"></div>
              </div>

              <div class="form-group"><label class="col-sm-2 control-label">YOUTUBE LINK</label>
                  <div class="col-sm-10"><input type="text" placeholder="https://www.youtube.com/watch?v=C0DPdy98e4c" class="form-control" name="youtube_link" value="{{ old('youtube_link') }}"></div>
              </div>
              <div class="form-group"><label class="col-sm-2 control-label">IMAGE</label>
                  <div class="col-sm-10"><input type="file" name="image" accept="image/x-png,image/jpeg"></div>
              </div>

              <div class="hr-line-dashed"></div>
              <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                      <button class="btn btn-primary" type="submit">Done</button>
                  </div>
              </div>

          </form>
        </div>
    </div>
</div>
@stop
@section('js')

@stop
