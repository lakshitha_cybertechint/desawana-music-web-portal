<?php

namespace VideoManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use VideoManage\Models\Video;

class VideoController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('VideoManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'image' => 'mimes:jpeg,png',
        'youtube_link' => ['required', 'regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/'],
      ]);

      $path = 'core/storage/uploads/images/video';
      $fullpath = '';
      if ($request->hasFile('image')) {

        $name = uniqid().'.'.$request->image->getClientOriginalExtension();

        $request->file('image')->move($path , $name);

        $fullpath = $path.'/'.$name;
      }

      $request->merge(['img' => $fullpath, 'user_id' => Sentinel::getUser()->id]);

      Video::create($request->all());


      return redirect('admin/video/add')->with(['success' => true,
        'success.message' => 'Video Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('VideoManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Video::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $video) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $video->title);
                $permissions = Permission::whereIn('name', ['video.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/video/edit/' . $video->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['video.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $video->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:videos,id']);
      $id = $request->input('id');
      $video = Video::with('song')->find($id);

      if (count($video->song)) {
        return response()->json(['error' => 'cant delete video that is added to a song'],401);
      }
      $video->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $video = Video::findOrFail($id);
      return view('VideoManage::edit',compact('video'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'youtube_link' => ['required', 'regex:/^(?:https?:\/\/)?(?:www[.])?(?:youtube[.]com\/watch[?]v=|youtu[.]be\/)([^&]{11})$/'],
      ]);

      $video = Video::findOrFail($id);

      $path = 'core/storage/uploads/images/video';
      $fullpath = $video->image;
      if ($request->hasFile('image')) {

        $name = uniqid().'.'.$request->image->getClientOriginalExtension();

        $request->file('image')->move($path , $name);

        $fullpath = $path.'/'.$name;
      }

      $request->merge(['img' => $fullpath, 'user_id' => Sentinel::getUser()->id]);

      $video->update($request->all());


      return redirect('admin/video/edit/' . $id)->with(['success' => true,
        'success.message' => 'Video updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
