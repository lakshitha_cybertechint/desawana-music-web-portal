<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/video', 'namespace' => 'VideoManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'video.add', 'uses' => 'VideoController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'video.edit', 'uses' => 'VideoController@editView'
      ]);

      Route::get('list', [
        'as' => 'video.list', 'uses' => 'VideoController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'video.list', 'uses' => 'VideoController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'video.image.delete', 'uses' => 'VideoController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'video.add', 'uses' => 'VideoController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'video.edit', 'uses' => 'VideoController@edit'
      ]);


      Route::post('delete', [
        'as' => 'video.delete', 'uses' => 'VideoController@delete'
      ]);


    });
});
