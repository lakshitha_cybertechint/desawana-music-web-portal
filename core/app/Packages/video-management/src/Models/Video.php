<?php
namespace VideoManage\Models;

use Illuminate\Database\Eloquent\Model;
use SongManage\Models\Song;

class Video extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'youtube_link','img', 'user_id'];

	public function song()
	{
	  return $this->hasMany(Song::class);
	}

	public function user()
	{
		return $this->belongsTo(\UserManage\Models\User::class);
	}
}
