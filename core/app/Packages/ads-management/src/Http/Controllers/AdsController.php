<?php

namespace AdsManage\Http\Controllers;

use AdsManage\Models\Advertisement;
use AdsManage\Models\AdvertisementLocation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Carbon\Carbon;
use Sentinel;
use Validator;
use Response;
use Image;

class AdsController extends Controller
{
    public function adsView()
    {
        return view('AdsManage::ads');
    }
    public function adsAll(Request $request)
    {
        if ($request->ajax()) {
            $data = Advertisement::with("location")->where("status", "!=", -1)->get();
            $jsonList = array();
            $i = 1;
            $todayActiveads = Advertisement::getTodayActiveAds();
            foreach ($data as $key => $ad) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, '<ul>
              <li><b>Name : </b> '.$ad->name.'</li>
              <li><b>Email : </b> '.$ad->email.'</li>
              <li><b>Con. Number : </b> '.$ad->contact_number.'</li>
              </ul>');
                array_push($dd, '<img style="max-width:150px; max-height:150px;" src="'.url('core'.DIRECTORY_SEPARATOR.$ad->image_url).'" > <br> <a href="'.url('core'.DIRECTORY_SEPARATOR.$ad->image_url).'" target="_blank"> View </a> ');
                array_push($dd, $ad->location->position);
                array_push($dd, substr(strip_tags($ad->message), 0, 75));

                $details = '<ul>';
                if ($ad->status == 0) {
                    $details .= '<li class="text-warning">INACTIVE</li>';
                } elseif ($ad->status == 1) {
                    $details .= '<li class="text-danger">PENDING</li>';
                } elseif ($ad->status == 2) {
                    if ($todayActiveads->find($ad->id)) {
                        $details .= '<li class="text-success">Active Displaying</li>';
                    } else {
                        $details .= '<li class="text-primary">Active Not Displaying</li>';
                    }
                    $details .= '<li><b>Start : </b>'.$ad->start_at.'</li>';
                    $details .= '<li><b>End : </b>'.$ad->expire_at.'</li>';
                }

                $details .= '</ul>';
                array_push($dd, $details);
                array_push($dd, $ad->created_at->toDateTimeString());

                $permissions = Permission::whereIn('name', ['hotel.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a  class="blue" href="'.url('admin/ads/manage/' . $ad->id).'" data-toggle="tooltip" data-placement="top" title="Ad Management"><i class="fa fa-edit"></i></a></center>');
                }

                $permissions = Permission::whereIn('name', ['hotel.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $ad->id . '" data-toggle="tooltip" data-placement="top" title="Delete Ad"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    public function toggleStatus($id, $status)
    {
        $ad = Advertisement::find($id);
        if ($status == 2 && $ad->status == 1) {
            $expire = Carbon::now();
            $expire = $expire->addMonth();
            $ad->expire_at = $expire->toDateString();
        }
        $ad->status = $status;
        $ad->save();
        return redirect("admin/ads/ads");
    }

    public function locationsView()
    {
        return view('AdsManage::locations');
    }

    public function locationsAll(Request $request)
    {
        if ($request->ajax()) {
            $data = AdvertisementLocation::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $ad) {
                $dd = array();
                array_push($dd, $i);
                array_push($dd, $ad->position);
                array_push($dd, $ad->width);
                array_push($dd, $ad->height);
                array_push($dd, Advertisement::getTodayActiveAds($ad->id)->count());

                $googleAdChecked= '';
                if ($ad->google_ad_status == 1) {
                  $googleAdChecked = 'checked';
                }
                array_push($dd,'<div class="text-center"><input disabled class="toggle" '.$googleAdChecked.'  type="checkbox"> <a class="btn btn-info" href="'.url('admin/ads/locations/google-ad-status/'.$ad->id).'"><i class="glyphicon glyphicon-eye-open"></i></a></div>');

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }

    public function adManage($id)
    {
        $ad = Advertisement::find($id);
        $adLocations = AdvertisementLocation::where("status", "!=", -1)->get();
        if ($ad) {
            return view('AdsManage::adManage')
      ->with("ad", $ad)
      ->with("adLocations", $adLocations);
        }

        return response()->back();
    }

    public function adManageSubmit($id, Request $request)
    {
        $validator = $this->validateAdManageInputs($request->all());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->getMessageBag()->toArray()], 200);
        } else {
            $ad = Advertisement::find($id);
            if ($ad) {
                $ad->start_at = $request->start_at;
                $ad->expire_at = $request->expire_at;
                $ad->position_id = $request->position_id;
                $ad->status = $request->status;
                $ad->approved_by = Sentinel::getUser()->id;

                if ($request->hasFile('ad_image')) {
                    $path = 'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'ads';
                    // $destinationPath = storage_path($path);
                    $project_fileName = 'ad_' . date('YmdHis'). '.'.$request->ad_image->getClientOriginalExtension();
                    $path1 = public_path('core' . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR);
                    $request->file('ad_image')->move($path1 , $project_fileName );
                    // Image::make($request->ad_image)->save(public_path('core'.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$project_fileName));
                    $ad->image_url = "storage".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."ads".DIRECTORY_SEPARATOR.$project_fileName;
                }


                if ($ad->save()) {
                    return Response::json(['success' => "19199212", "message" => "Advertiesment updated successfully."], 200);
                }
            }
        }
    }

    public function validateAdManageInputs($inputs)
    {
        $rules = [
      "position_id" => 'required:exists:advertisement_locations,id',
      "ad_image" => "mimes:jpeg,jpg,png,gif",
      "ad_link" => 'url'
    ];
    if ($inputs['status'] == 2) {
      $rules['start_at'] = 'required|date_format:Y-m-d';
      $rules['expire_at'] = 'required|date_format:Y-m-d';
    }
        $msgs = [];
        $validator = Validator::make($inputs, $rules, $msgs);
        $validator->after(function($validator) use($inputs){
        if ($inputs['status'] == 2 ) {
            $activeAdsCount = $this->getActiveAdsInDateRange($inputs['start_at'], $inputs['expire_at'], $inputs['position_id']);
            if ($activeAdsCount) {
                $activeAdsCount = $activeAdsCount->count();
            } else {
                $activeAdsCount = 0;
            }
            $adLocation = AdvertisementLocation::find($inputs['position_id']) ;
            if ($activeAdsCount >= $adLocation->ad_count) {
                $validator->errors()->add('expire_at', "Can't activate ad in given date range, because it's exceed the max ad count of the location($adLocation->ad_count)");
            }
        }
        });
        return $validator;
    }

    public function getActiveAdsInDateRange($startDate, $endDate, $locationId = -1)
    {
        $ads = Advertisement::where("status", 2)
    ->where(function ($q) use ($locationId) {
        if ($locationId != -1) {
            return $q->where('position_id', $locationId);
        }
    })
    ->where(function($q) use($startDate, $endDate){
        return $q->where(function($q1) use($startDate, $endDate){
            return $q1->whereDate('start_at', '<=', $startDate)->whereDate('expire_at', '>=', $startDate);
        })
        ->orWhere(function($q2) use($startDate, $endDate){
            return $q2->whereDate('start_at', '<=', $endDate)->whereDate('expire_at', '>=', $endDate);
        });
    })
    ->get();
        return $ads;
    }
public function googleAdStatus($location)
{
  $location = AdvertisementLocation::find($location);
  return view('AdsManage::googleAdManage')->with('location', $location);

}
    public function toggleGoogleAdStatus($id)
    {
    if (request()->status == 1 && request()->script == "") {
      return Response::json(['errors' => [
        "script" => "Script field is required"
        ]], 200);
    }else{
      $location = AdvertisementLocation::find($id);
      if ($location) {
        $location->script = request()->script;

          $location->google_ad_status = request()->status ;

            $location->save();
            return Response::json(['success' => "19199212", "message" => "Location Google Ad  updated successfully."], 200);

      }
    }
    }

    function delete(Request $request){
        $ad = Advertisement::findOrFail($request->id);
        if ($ad) {
            $ad->status = -1 ;
            $ad->save();
        }
    }
}
