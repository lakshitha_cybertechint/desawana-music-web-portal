<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/ads', 'namespace' => 'AdsManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */


      Route::get('ads', [
        'as' => 'ads', 'uses' => 'AdsController@adsView'
      ]);
      Route::get('/manage/{id}', [
        'as' => 'ads', 'uses' => 'AdsController@adManage'
      ]);
      Route::get('locations', [
        'as' => 'locations', 'uses' => 'AdsController@locationsView'
      ]);

      Route::get('locations/google-ad-status/{id}', [
        'as' => 'locations', 'uses' => 'AdsController@googleAdStatus'
      ]);

      Route::post('locations/google-ad-status/{id}', [
        'as' => 'locations', 'uses' => 'AdsController@toggleGoogleAdStatus'
      ]);



      Route::get('json/list', [
        'as' => 'json.list', 'uses' => 'AdsController@adsAll'
      ]);
      Route::get('locations/json/list', [
        'as' => 'locations.json.list', 'uses' => 'AdsController@locationsAll'
      ]);

      Route::get('status/{id}/{status}', [
        'as' => 'ads.status', 'uses' => 'AdsController@toggleStatus'
      ]);


      Route::post('/manage/{id}', [
        'as' => 'ads', 'uses' => 'AdsController@adManageSubmit'
      ]);
      Route::post('/delete', [
        'as' => 'ads', 'uses' => 'AdsController@delete'
      ]);


      /**
       * POST Routes
       */



      // Route::post('delete', [
      //   'as' => 'hotel.delete', 'uses' => 'HotelController@delete'
      // ]);


    });
});
