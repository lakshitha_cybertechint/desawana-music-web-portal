<?php
namespace AdsManage\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;


class Advertisement extends Model{

/**
* @param int $status [-1 = deleted, 0 = inactivated, 1 = pending, 2 = approved]
*/

public function location()
{
  return $this->belongsTo(AdvertisementLocation::class, "position_id");
}

public static function getTodayActiveAds($locationId = -1)
{
  $ads = Advertisement::where("status", 2)
  ->where(function($q) use ($locationId){
    if ($locationId != -1) {
      return $q->where('position_id', $locationId);
    }
  })
  ->whereDate('start_at', '<=', Carbon::now()->toDateString())
  ->whereDate('expire_at', '>=', Carbon::now()->toDateString())
  ->where('status', 2)
  ->get();
  return $ads;
}


}
