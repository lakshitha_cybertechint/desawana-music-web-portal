<?php
//meki waradda! manger eka uba compser eka dapu ekata dipn!
namespace AdsManage;

use Illuminate\Support\ServiceProvider;

class AdsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'AdsManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('adsManage', function($app){
            return new AdsManage;
        });
    }
}
