@extends('layouts.back.master') @section('current_title','Advertisement Manage')
@section('css')
  <link rel="stylesheet" href="{{url('assets\back\vendor\bootstrap-datepicker-master\dist\css\bootstrap-datepicker3.min.css')}}">
<style type="text/css">
    #floating-button{
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background: #db4437;
      position: fixed;
      bottom: 50px;
      right: 30px;
      cursor: pointer;
      box-shadow: 0px 2px 5px #666;
      z-index:2
    }

    .plus{
      color: white;
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      padding: 0;
      margin: 0;
      line-height: 55px;
      font-size: 38px;
      font-family: 'Roboto';
      font-weight: 300;
      animation: plus-out 0.3s;
      transition: all 0.3s;
    }
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/ads/ads')}}">Advertisements Management</a></li>

        <li class="active">
            <span>Advertisement Manage</span>
        </li>
    </ol>
</div>
@stop
@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              <form id="adManageForm">
                {{ csrf_field() }}
                <h4>Client Details</h4>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="">Name</label>
                      <input type="text" disabled class="form-control" placeholder="" value="{{$ad->name}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" disabled class="form-control" placeholder="" value="{{$ad->email}}">
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Contact Number</label>
                      <input type="text" disabled class="form-control" placeholder="" value="{{$ad->contact_number}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="">Message</label>
                    <textarea disabled style="min-width: 100%; max-width:100%;" rows="8" >{{$ad->message}}</textarea>
                  </div>
                </div>
                <h4>Advertisement Details</h4>
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Advertisement Location</label>
                      <select class="form-control" name="position_id">

                      @foreach ($adLocations as $adLocation)
                        <option {{$ad->position_id == $adLocation->id ? "selected" : ""}} value="{{$adLocation->id}}">{{$adLocation->position}}</option>
                      @endforeach
                    </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Status</label>
                        <select class="form-control" name="status">
                          <option {{$ad->status == 0 ? "selected" : ""}} value="0">Inactive</option>
                          <option {{$ad->status == 1 ? "selected" : ""}} value="1">Pending</option>
                          <option {{$ad->status == 2 ? "selected" : ""}} value="2">Active</option>
                          <option  value="-1">DELETE</option>
                        </select>
                      </div>
                    </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Start At</label>
                        <input type="text" name="start_at"  class="form-control datepicker" placeholder="YYYY-MM-DD" value="{{$ad->start_at}}">
                      </div>
                    </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Expire At</label>
                        <input type="text" name="expire_at"  class="form-control datepicker" placeholder="YYYY-MM-DD" value="{{$ad->expire_at}}">
                      </div>
                    </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Ad Link</label>
                        <input type="text" name="ad_link"  class="form-control"  value="{{$ad->ad_link}}" placeholder="http://desawana.com/">
                      </div>
                    </div>
                <div class="col-md-6">
                    <div class="form-group ">
                      <label class="col-xs-12" style="padding-left : 0" for="">Advertisement Image</label>
                      <img style="max-width:100%; " src="{{url('core'.DIRECTORY_SEPARATOR.$ad->image_url)}}" > <br> <a class="btn btn-info" href="{{url('core'.DIRECTORY_SEPARATOR.$ad->image_url)}}" target="_blank"> View </a>

                    </div>
                    <div class="form-group ">
                      <label for="">Change Advertisement Image</label>
                      <input type="file" name="ad_image" class="form-control" accept="image/jpeg, image/x-png, image/gif">
                    </div>
                  </div>
                  <div class="clearfix" id="adManageFormErrors">

                  </div>

                  <div class="hr-line-dashed"></div>
                  <div class="form-group text-right">
                      <div class="col-xs-12 ">
                          <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                          <button class="btn btn-primary" onclick="ajax_request()" form="noForm">Save Changes</button>
                      </div>
                  </div>
              </form>
        	</div>
    	</div>
	</div>
</div>
@stop
@section('js')
<script type="text/javascript" src="{{url('assets\back\vendor\bootstrap-datepicker-master\dist\js\bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
  $('.datepicker').datepicker({
    format : 'yyyy-mm-dd'
  });
</script>
<script type="text/javascript">


formId = "adManageForm";
baseUrl =  "{{url("admin/ads/manage/".$ad->id)}}";
  url =  baseUrl;
    function ajax_request(){
        $("#"+formId+"Errors").html("");
      data = new FormData(document.getElementById(formId));
        var xhttp;
        if (window.XMLHttpRequest) {
          xhttp = new XMLHttpRequest();
        } else {
          // code for IE6, IE5
          xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.addEventListener("load", function() {

          SubmitResultHandler(JSON.parse(this.responseText), formId);

        });
        xhttp.addEventListener("error", function() {
          setTimeout(function() {
            oops();
          }, 100);
        });

          xhttp.open("POST", baseUrl, false);

        xhttp.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));

        xhttp.send(data);
    }

    function SubmitResultHandler(data, formId) {
      if (data.hasOwnProperty('errors') && Object.keys(data.errors).length !== 0) {

          submitErrors(data.errors);


      } else if (data.hasOwnProperty('success') && data.success == "19199212") {

           submitSuccess(data.message);

      }else{
        oops();
      }
    }
    function submitErrors(errors){
      $("#"+formId+"Errors").html(createErrorsList(errors));
      $("#"+formId+"Errors").show("slow");
    }
    function submitSuccess(data){
      var datahtml = '<p class="text-success">'+data+'</p>';
      $("#"+formId+"Errors").html(datahtml);

    }

    function createErrorsList(errors){
      var retData = '<ul style="color:red; text-align:left;">';

      Object.keys(errors).forEach(function(key) {

        retData += '<li>' + errors[key] + '</li>';
      });
      retData += '</ul>';
      return retData;
    }
</script>

@stop
