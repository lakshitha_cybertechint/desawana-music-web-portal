@extends('layouts.back.master') @section('current_title','Google Ad Manage')
@section('css')

<style type="text/css">
    #floating-button{
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background: #db4437;
      position: fixed;
      bottom: 50px;
      right: 30px;
      cursor: pointer;
      box-shadow: 0px 2px 5px #666;
      z-index:2
    }

    .plus{
      color: white;
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      padding: 0;
      margin: 0;
      line-height: 55px;
      font-size: 38px;
      font-family: 'Roboto';
      font-weight: 300;
      animation: plus-out 0.3s;
      transition: all 0.3s;
    }
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/ads/locations')}}">Advertisement Locations</a></li>

        <li class="active">
            <span>Google Ad Manage</span>
        </li>
    </ol>
</div>
@stop
@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              <form id="adManageForm">
                {{ csrf_field() }}

                <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Ad Location</label>
                      <select class="form-control" name="position_id">

                      @foreach (App\AdvertisementLocation::whereStatus(1)->get() as $adLocation)
                        <option {{$location->id == $adLocation->id ? "selected" : ""}} value="{{$adLocation->id}}">{{$adLocation->position}}</option>
                      @endforeach
                    </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Status</label>
                        <select class="form-control" name="status">
                          <option {{$location->google_ad_status == 0 ? "selected" : ""}} value="0">Inactive</option>
                          <option {{$location->google_ad_status == 1 ? "selected" : ""}} value="1">Active</option>
                        </select>
                      </div>
                    </div>
                  <div class="col-xs-12">
                      <div class="form-group">
                        <label for="">Google Ad Script</label>
                        <textarea type="text" name="script"  class="form-control ">{{$location->script}}</textarea>
                      </div>
                    </div>


                  <div class="clearfix" id="adManageFormErrors">

                  </div>

                  <div class="hr-line-dashed"></div>
                  <div class="form-group text-right">
                      <div class="col-xs-12 ">
                          <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                          <button class="btn btn-primary" onclick="ajax_request()" form="noForm">Save Changes</button>
                      </div>
                  </div>
              </form>
        	</div>
    	</div>
	</div>
</div>
@stop
@section('js')

<script type="text/javascript">


formId = "adManageForm";
baseUrl =  "{{url("admin/ads/locations/google-ad-status/".$location->id)}}";
  url =  baseUrl;
    function ajax_request(){
        $("#"+formId+"Errors").html("");
      data = new FormData(document.getElementById(formId));
        var xhttp;
        if (window.XMLHttpRequest) {
          xhttp = new XMLHttpRequest();
        } else {
          // code for IE6, IE5
          xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.addEventListener("load", function() {

          SubmitResultHandler(JSON.parse(this.responseText), formId);

        });
        xhttp.addEventListener("error", function() {
          setTimeout(function() {
            oops();
          }, 100);
        });

          xhttp.open("POST", baseUrl, false);

        xhttp.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));

        xhttp.send(data);
    }

    function SubmitResultHandler(data, formId) {
      if (data.hasOwnProperty('errors') && Object.keys(data.errors).length !== 0) {

          submitErrors(data.errors);


      } else if (data.hasOwnProperty('success') && data.success == "19199212") {

           submitSuccess(data.message);

      }else{
        oops();
      }
    }
    function submitErrors(errors){
      $("#"+formId+"Errors").html(createErrorsList(errors));
      $("#"+formId+"Errors").show("slow");
    }
    function submitSuccess(data){
      var datahtml = '<p class="text-success">'+data+'</p>';
      $("#"+formId+"Errors").html(datahtml);

    }

    function createErrorsList(errors){
      var retData = '<ul style="color:red; text-align:left;">';

      Object.keys(errors).forEach(function(key) {

        retData += '<li>' + errors[key] + '</li>';
      });
      retData += '</ul>';
      return retData;
    }
</script>

@stop
