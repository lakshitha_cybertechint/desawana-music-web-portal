<?php

namespace SongManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use SongManage\Models\Song;
use VideoManage\Models\Video;
use AudioManage\Models\Audio;
use ArtistManage\Models\Artist;
use LyricsManage\Models\Lyrics;
use Image;

class SongController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        $artists = Artist::get();
        $audio = Audio::get();
        $videos = Video::get();
        $lyrics = Lyrics::get();
        return view('SongManage::add',compact('artists','audio', 'videos','lyrics'));
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required',
        'art' => 'required',
        'audio_id' => 'required|exists:audio,id',
        'artists' => 'array|required',
      ]);

      $path = 'uploads/images/album_art';
      $destinationPath = storage_path($path);
      $project_fileName = 'album_art-' . date('YmdHis'). '.png';
      Image::make($request->art)->save('core/storage/'.$path.'/'.$project_fileName);

      $request->merge(['album_art' => 'core/storage/'.$path.'/'.$project_fileName,'user_id' => Sentinel::getUser()->id]);

      $song = Song::create($request->all());

      $song->artists()->sync($request->artists);

      if ($request->has('lyricsArtists')) {
        $song->lyricsArtist()->sync($request->lyricsArtists);
      }

      if ($request->has('musicArtists')) {
        $song->musicArtist()->sync($request->musicArtists);
      }

      return redirect('admin/song/add')->with(['success' => true,
        'success.message' => 'Song Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('SongManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Song::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $song) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $song->title);
                $permissions = Permission::whereIn('name', ['song.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/song/edit/' . $song->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['song.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $song->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:songs,id']);
      $id = $request->input('id');
      $song = Song::find($id);
      $song->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $artists = Artist::get();
      $audio = Audio::get();
      $videos = Video::get();
      $lyrics = Lyrics::get();
      $song = Song::findOrFail($id);
      return view('SongManage::edit',compact('song','artists','audio', 'videos','lyrics'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'title' => 'required',
        'audio_id' => 'required|exists:audio,id',
        'artists' => 'array|required',
      ]);

      $song = Song::findOrFail($id);

      if ($request->has('art')) {
        $path = 'uploads/images/album_art';
        $destinationPath = storage_path($path);
        $project_fileName = 'album_art-' . date('YmdHis'). '.png';
        Image::make($request->art)->save('core/storage/'.$path.'/'.$project_fileName);
      
        $request->merge(['album_art' => 'core/storage/'.$path.'/'.$project_fileName]);
      }

      $request->merge(['user_id' => Sentinel::getUser()->id]);

      $song->update($request->all());

      $song->artists()->sync($request->artists, true);

      if ($request->has('lyricsArtists')) {
        $song->lyricsArtist()->sync($request->lyricsArtists,true);
      }

      if ($request->has('musicArtists')) {
        $song->musicArtist()->sync($request->musicArtists,true);
      }

      return redirect('admin/song/edit/' . $id)->with(['success' => true,
        'success.message' => 'Song updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
