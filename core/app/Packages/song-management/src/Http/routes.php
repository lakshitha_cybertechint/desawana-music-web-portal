<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/song', 'namespace' => 'SongManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'song.add', 'uses' => 'SongController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'song.edit', 'uses' => 'SongController@editView'
      ]);

      Route::get('list', [
        'as' => 'song.list', 'uses' => 'SongController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'song.list', 'uses' => 'SongController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'song.image.delete', 'uses' => 'SongController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'song.add', 'uses' => 'SongController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'song.edit', 'uses' => 'SongController@edit'
      ]);


      Route::post('delete', [
        'as' => 'song.delete', 'uses' => 'SongController@delete'
      ]);


    });
});
