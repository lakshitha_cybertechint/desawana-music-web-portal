<?php
namespace SongManage\Models;

use Illuminate\Database\Eloquent\Model;
use VideoManage\Models\Video;
use AudioManage\Models\Audio;
use ArtistManage\Models\Artist;

class Song extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','album_art','old','video_id','meta_description','meta_keywords','audio_id','lyrics_id', 'user_id'];

	public function video()
	{
	  return $this->belongsTo(Video::class);
	}

	public function audio()
	{
	  return $this->belongsTo(Audio::class);
	}

	public function user()
	{
	  return $this->belongsTo(\UserManage\Models\User::class);
	}

	public function artists()
	{
		return $this->belongsToMany(Artist::class);
	}

	public function lyricsArtist()
	{
	  return $this->belongsToMany(Artist::class, 'song_lyrics_artist', 'song_id', 'artist_id');
	}

	public function musicArtist()
	{
	  return $this->belongsToMany(Artist::class, 'song_music_artist', 'song_id', 'artist_id');
	}
}
