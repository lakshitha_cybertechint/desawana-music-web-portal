@extends('layouts.back.master') @section('current_title','Update Song')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style media="screen">
#image-cropper{
  width: 250px;
  height: 250px;
}
.cropit-preview {
  /* You can specify preview size in CSS */
  width: 250px;
  height: 250px;
  background-color: #ddd;
}
input.cropit-image-input {
  visibility: hidden;
}
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/song/list')}}">Song Management</a></li>

        <li class="active">
            <span>Update Song</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}

                  <div class="form-group"><label class="col-sm-2 control-label">TITLE</label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="title" value="{{ $song->title }}"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">META DESCRIPTION</label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="meta_description" value="{{ $song->meta_description }}"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">META KEYWORDS</label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="meta_keywords" value="{{ $song->meta_keywords }}"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">VIDEO</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" name="video_id">
                          <option value="0">--Select Video--</option>
                          @foreach ($videos as $key => $value)
                            <option value="{{$value->id  }}" {{ $value->id == $song->video_id ? "selected" : ""}}>{{ $value->title }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">AUDIO</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" name="audio_id">
                          @foreach ($audio as $key => $value)
                            <option value="{{$value->id  }}" {{ $value->id == $song->audio_id ? "selected" : ""}}>{{ $value->title }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">LYRICS</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" name="lyrics_id">
                          <option value="0">--Select Lyrics--</option>
                          @foreach ($lyrics as $key => $value)
                            <option value="{{$value->id  }}" {{ $value->id == $song->lyrics_id ? "selected" : ""}}>{{ $value->title }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">ARSTIS</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" name="artists[]" multiple>
                          @foreach ($artists as $key => $value)
                            <option value="{{$value->id  }}" {{ in_array($value->id ,$song->artists->pluck('id')->toArray()) ? "selected" : ""}}>{{ $value->name }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">LYRICS ARSTIS</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" name="lyricsArtists[]" multiple>
                          @foreach ($artists as $key => $value)
                            <option value="{{$value->id  }}" {{ in_array($value->id ,$song->lyricsArtist->pluck('id')->toArray()) ? "selected" : ""}}>{{ $value->name }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">MUSIC ARSTIS</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" name="musicArtists[]" multiple>
                          @foreach ($artists as $key => $value)
                            <option value="{{$value->id  }}" {{ in_array($value->id ,$song->musicArtist->pluck('id')->toArray()) ? "selected" : ""}}>{{ $value->name }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">OLD SONG</label>
                      <div class="col-sm-10">
                        <input type="hidden" name="old" value="0">
                        <input type="checkbox" name="old" value="1" {{ $song->old == 1 ? "checked" : "" }}>
                      </div>
                  </div>


                  <div class="form-group"><label class="col-sm-2 control-label">AlBUM ART</label>
                    <div class="col-md-10">
                      <div id="image-cropper">
                      <div class="cropit-preview"></div>

                      <input type="range" class="cropit-image-zoom-input" />

                      <!-- The actual file input will be hidden -->
                      <input type="file" class="cropit-image-input"/>
                      <input type="hidden" name="art">
                      <!-- And clicking on this button will open up select file dialog -->
                      <div class="select-image-btn btn btn-success">Browse</div>
                      <div class="btn btn-warning">Set</div>
                      </div>
                    </div>
                  </div>

    <br>
    <br>
    <br>

                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>
@stop
@section('js')

  <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}" charset="utf-8"></script>
  <script src="{{asset('assets/back/vendor/cropit/jquery.cropit.js')}}" charset="utf-8"></script>
  <script type="text/javascript">
    $('.select2').select2();

    $('#image-cropper').cropit();
    $('.cropit-preview-image').attr('src','{{ asset($song->album_art) }}')
    $('.select-image-btn').click(function() {
      $('.cropit-image-input').click();
    });

    $('.btn-warning').click(function(event) {
       var imageData = $('#image-cropper').cropit('export');
       $('input[name="art"]').val(imageData);
    });
  </script>
@stop
