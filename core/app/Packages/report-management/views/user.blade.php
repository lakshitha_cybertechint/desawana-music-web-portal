@extends('layouts.back.master')
@section('current_title','User Reprot')
@section('css')
<style type="text/css">
    #floating-button{
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background: #db4437;
      position: fixed;
      bottom: 50px;
      right: 30px;
      cursor: pointer;
      box-shadow: 0px 2px 5px #666;
      z-index:2
    }

    .plus{
      color: white;
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      padding: 0;
      margin: 0;
      line-height: 55px;
      font-size: 38px;
      font-family: 'Roboto';
      font-weight: 300;
      animation: plus-out 0.3s;
      transition: all 0.3s;
    }
</style>

@stop
@section('current_path')
{{-- <div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="active">User Report</a></li>
    </ol>
</div> --}}
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Filter</h3>
                </div>
                <div class="panel-body">
                  <div class="col-md-3">
                    <select class="form-control fliter" id="user">
                      <option value="0" disabled selected>--Select User--</option>
                      @foreach ($users as $key => $value)
                        <option value="{{ $value->id }}">{{ $value->first_name." ".$value->last_name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                    <button type="button" class="btn btn-default" id="clearFilter">
                      CLEAR FILTER
                    </button>
                  </div>
                </div>
              </div>
             	<table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>User Added/Updated</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Added Data</th>
                    </tr>
                    </thead>
                </table>
        	</div>
    	</div>
	</div>
</div>
@stop
@section('js')
<script type="text/javascript">
  var table;
  $(document).ready(function(){
    table=$('#example1').dataTable( {
        ajax: {
          url : '{{url('admin/report/user/data')}}',
          data: function (d){
            d.user = $('#user').val()
          }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        buttons: [
            {extend: 'copy',className: 'btn-sm'},
            {extend: 'csv',title: 'Device List', className: 'btn-sm'},
            {extend: 'pdf', title: 'Device List', className: 'btn-sm'},
            {extend: 'print',className: 'btn-sm'}
        ],
         "autoWidth": false
    });
  });

  $('.fliter').change(function(event) {
    table.fnReloadAjax()
  });


  $('#clearFilter').click(function(event) {
    $('.fliter').val('0')
    table.fnReloadAjax()
  });


</script>


@stop
