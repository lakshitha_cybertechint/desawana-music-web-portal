<?php
namespace ReportManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use VideoManage\Models\Video;
use AudioManage\Models\Audio;
use SongManage\Models\Song;
use ArtistManage\Models\Artist;
use UserManage\Models\User;

class ReportController extends Controller
{
  public function user()
  {
    $users = User::get();
    return view('ReportManage::user',compact('users'));
  }

  public function userData(Request $request)
  {

      $songs = new Song();
      $audio = new Audio();
      $video = new Video();



      if ($request->has('user')) {
        $songs = $songs->where('user_id',$request->user);
        $audio = $audio->where('user_id',$request->user);
        $video = $video->where('user_id',$request->user);
      }

      $songs = $songs->get();
      $audio = $audio->get();
      $video = $video->get();

      $data = [];

      foreach ($songs as $key => $value) {
        $tem = [];
        array_push($tem, $value->user->first_name." ".$value->user->last_name);
        array_push($tem, 'Song');
        array_push($tem, "<a href='admin/song/edit/'$value->id>$value->title</a>");
        array_push($tem, $value->created_at->format('Y-m-d H:i:s'));
        array_push($data, $tem);
      }
      foreach ($audio as $key => $value) {
        $tem = [];
        array_push($tem, $value->user->first_name." ".$value->user->last_name);
        array_push($tem, 'Audio');
        array_push($tem, "<a href='admin/audio/edit/'$value->id>$value->title</a>");
        array_push($tem, $value->created_at->format('Y-m-d H:i:s'));
        array_push($data, $tem);
      }
      foreach ($video as $key => $value) {
        $tem = [];
        array_push($tem, $value->user->first_name." ".$value->user->last_name);
        array_push($tem, 'Video');
        array_push($tem, "<a href='admin/video/edit/'$value->id>$value->title</a>");
        array_push($tem, $value->created_at->format('Y-m-d H:i:s'));
        array_push($data, $tem);
      }

      return response()->json(array('data' => $data));

  }

}
