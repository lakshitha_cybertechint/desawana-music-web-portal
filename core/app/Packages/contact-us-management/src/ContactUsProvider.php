<?php
//meki waradda! manger eka uba compser eka dapu ekata dipn!
namespace ContactUsManage;

use Illuminate\Support\ServiceProvider;

class ContactUsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'ContactUsManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('contactUsManage', function($app){
            return new ContactUsManage;
        });
    }
}
