<?php
namespace ContactUsManage\Models;

use Illuminate\Database\Eloquent\Model;


class ContactUs extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	 protected  $fillable = ["name", "email", "contact_number", "subject", "message", "status"];
	 protected $table = "contact_us";
}
