<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/contact-us', 'namespace' => 'ContactUsManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */


      Route::get('reply/{id}', [
        'as' => 'contact-us.reply', 'uses' => 'ContactUsController@replyView'
      ]);

      Route::get('list', [
        'as' => 'contact-us.list', 'uses' => 'ContactUsController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'contact-us.list', 'uses' => 'ContactUsController@jsonList'
      ]);





      /**
       * POST Routes
       */


      Route::post('reply/{id}', [
        'as' => 'contact-us.reply', 'uses' => 'ContactUsController@reply'
      ]);


      // Route::post('delete', [
      //   'as' => 'hotel.delete', 'uses' => 'HotelController@delete'
      // ]);


    });
});
