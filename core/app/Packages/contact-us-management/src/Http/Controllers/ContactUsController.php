<?php

namespace ContactUsManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ContactUsManage\Models\ContactUs;
use Sentinel;
use Mail;
use Carbon\Carbon;

class ContactUsController extends Controller {
  public function listView() {
      return view('ContactUsManage::list');
  }
  public function jsonList(Request $request) {
      if ($request->ajax()) {
          $data = ContactUs::get();
          $jsonList = array();
          $i = 1;
          foreach ($data as $key => $contact) {

              $dd = array();
              array_push($dd, $i);
              array_push($dd, '<ul>
              <li><b>Name : </b> '.$contact->name.'</li>
              <li><b>Email : </b> '.$contact->email.'</li>
              <li><b>Con. Number : </b> '.$contact->contact_number.'</li>
              </ul>');
              array_push($dd, $contact->subject);
              array_push($dd,  substr(strip_tags($contact->message),0,75));
              array_push($dd, '<ul>
              <li><b>Replied At : </b> '.$contact->reply_at.'</li>
              <li><b>Reply : </b> '.substr(strip_tags($contact->reply),0,75).'</li>
              </ul>');
              array_push($dd, $contact->created_at->toDateTimeString());

              $permissions = Permission::whereIn('name', ['hotel.edit', 'admin'])->where('status', '=', 1)->lists('name');
              if (Sentinel::hasAnyAccess($permissions)) {
                  array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/contact-us/reply/' . $contact->id) . '\'" data-toggle="tooltip" data-placement="top" title="Reply To This"><i class="fa fa-reply"></i></a></center>');
              } else {
                  array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Reply Disabled"><i class="fa fa-reply"></i></a>');
              }

              $permissions = Permission::whereIn('name', ['hotel.delete', 'admin'])->where('status', '=', 1)->lists('name');
              if (Sentinel::hasAnyAccess($permissions)) {
                  array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $contact->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
              } else {
                  array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
              }

              array_push($jsonList, $dd);
              $i++;
          }
          return response()->json(array('data' => $jsonList));
      } else {
          return response()->json(array('data' => []));
      }
  }

  public function replyView($id)
  {
    $contact = ContactUs::find($id);
    return view('ContactUsManage::reply')->with(["contact" => $contact]);

  }
  public function reply($id, Request $request)
  {
    $this->validate($request,[
      'message' => 'required',
    ]);
    $contact = ContactUs::findOrFail($id);
    $contact->reply = $request->message;
    $contact->reply_at = Carbon::now()->toDateTimeString();;
    if ($contact->save()) {
      Mail::send('ContactUsManage::reply-email', ['contact' => $contact ], function ($m) use ($contact) {
            $m->from('info@desawana.com', 'Deswana Music Portal');

            $m->to($contact->email, $contact->name)->subject($contact->subject);
        });
      return redirect('admin/contact-us/reply/'.$id)->with(['success' => true,
        'success.message' => 'Message send successfully!',
        'success.title' => 'Good Job!']);
    }
  }
}
