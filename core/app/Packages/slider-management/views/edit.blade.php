@extends('layouts.back.master') @section('current_title','Update Slider')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/slider/list')}}">Slider Management</a></li>

        <li class="active">
            <span>Update Slider</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}

                    <div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{$slider->name}}"></div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">URL</label>
                        <div class="col-sm-10"><input type="url" class="form-control" name="url"  value="{{$slider->url}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">EXPIRE DATE</label>
                        <div class="col-sm-10"><input type="text" class="form-control datepicker" name="expire_date" value="{{$slider->expire_date}}"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">SONGS</label>
                        <div class="col-sm-10">
                        {!! Form::select('songs[]', $songs, array_flatten ($slider->songs->lists('id')), ['class' => 'form-control select2', 'multiple']) !!}
                      </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGE</label>
                        <div class="col-sm-10">
                            <input id="cover_images" name="cover_image" type="file" class="file-loading">
                        </div>
                    </div>


                    <div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>
                         <div class="col-sm-10"><textarea name="description" class="form-control">{{$slider->description}}</textarea></div>
                    </div>




                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>

<script src="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}" charset="utf-8"></script>

  <script type="text/javascript">


    $('.datepicker').datepicker({
      format : "yyyy-mm-dd"
    });
  </script>



<script type="text/javascript">

  $('.select2').select2();

$("#cover_images").fileinput({
  uploadUrl: "", // server upload action
  uploadAsync: true,
  maxFileCount: 1,
  showUpload:false,
  allowedFileExtensions: ["jpg", "gif", "png"],
  initialPreview: [
  "<img style='height:160px' src='{{ asset($slider->image)}}'>"],
});

tinymce.init({
  selector: 'textarea',  // change this value according to your HTML
  plugins: [
      'advlist autolink lists link image charmap preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code',
      'insertdatetime media nonbreaking table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
  ],
  toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
  '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
  menubar: false
});
</script>
@stop
