<?php

namespace SliderManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Image;
use SliderManage\Models\Slider;
use SongManage\Models\Song;

class SliderController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        $songs = Song::lists('title','id');
        return view('SliderManage::add',compact('songs'));
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'cover_image' => 'required|mimes:jpeg,bmp,png,',
        'expire_date' => 'required|date|date_format:Y-m-d',
      ]);


      $file = $request->file('cover_image');
      $path = 'uploads/images/slider';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'slider-' . date('YmdHis'). '.' . $extn;
      $file->move($destinationPath,$project_fileName);


      $request->merge(['image' => '/core/storage/'.$path.'/'.$project_fileName, 'user_id' => Sentinel::getUser()->id]);

      $slider = Slider::create($request->all());

      if (is_array($request->songs)) {
        $slider->songs()->sync($request->songs);
      }

      return redirect('admin/slider/add')->with(['success' => true,
        'success.message' => 'Slider Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('SliderManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Slider::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $slider) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $slider->name);
                array_push($dd,  substr(strip_tags($slider->description),0,50));

                $permissions = Permission::whereIn('name', ['slider.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/slider/edit/' . $slider->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['slider.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $slider->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:sliders,id']);
      $id = $request->input('id');
      $blog = Slider::find($id);
      $blog->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $songs = Song::lists('title','id');
      $slider = Slider::with('songs')->findOrFail($id);
      return view('SliderManage::edit',compact('slider','songs'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'expire_date' => 'required|date|date_format:Y-m-d',
      ]);

      $slider = Slider::findOrFail($id);

      $title = $request->title;
      $description = $request->description;
      $project_fileName = $slider->image;

      if ($request->hasFile('cover_image')) {
        $file = $request->file('cover_image');
        $path = 'uploads/images/slider';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'slider-' . date('YmdHis'). '.' . $extn;
        $file->move($destinationPath,$project_fileName);
        $request->merge(['image' => '/core/storage/'.$path.'/'.$project_fileName]);
      }

      $request->merge(['user_id' => Sentinel::getUser()->id]);

      $slider->update($request->all());

      if (is_array($request->songs)) {
        $slider->songs()->sync($request->songs);
      }


      return redirect('admin/slider/edit/' . $id)->with(['success' => true,
        'success.message' => 'Slider updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
