<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/slider', 'namespace' => 'SliderManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'slider.add', 'uses' => 'SliderController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'slider.edit', 'uses' => 'SliderController@editView'
      ]);

      Route::get('list', [
        'as' => 'slider.list', 'uses' => 'SliderController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'slider.list', 'uses' => 'SliderController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'slider.image.delete', 'uses' => 'SliderController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'slider.add', 'uses' => 'SliderController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'slider.edit', 'uses' => 'SliderController@edit'
      ]);


      Route::post('delete', [
        'as' => 'slider.delete', 'uses' => 'SliderController@delete'
      ]);


    });
});
