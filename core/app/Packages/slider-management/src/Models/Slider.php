<?php
namespace SliderManage\Models;

use Illuminate\Database\Eloquent\Model;
use SongManage\Models\Song;
use UserManage\Models\User;

class Slider extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'url','description','image','expire_date','user_id'];

	public function songs()
	{
	  return $this->belongsToMany(Song::class);
	}

	public function user()
	{
	  return $this->belongsTo(User::class);
	}
}
