<?php

namespace AudioManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use AudioManage\Models\Audio;

class AudioController extends Controller {

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('AudioManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required',
        'mp3' => 'required',
      ]);


      $file = $request->file('mp3');
      $path = 'uploads/audio';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'audio-' . date('YmdHis'). '.' . $extn;
      $file->move($destinationPath,$project_fileName);

      $request->merge(['src' =>  '/core/storage/'.$path.'/'.$project_fileName, 'user_id' => Sentinel::getUser()->id]);

      Audio::create($request->all());

      return redirect('admin/audio/add')->with(['success' => true,
        'success.message' => 'Audio Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('AudioManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Audio::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $audio) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $audio->title);
                $permissions = Permission::whereIn('name', ['audio.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/audio/edit/' . $audio->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['audio.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $audio->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:audio,id']);
      $id = $request->input('id');
      $audio = Audio::with('song')->find($id);

      if (count($audio->song)) {
        return response()->json(['error' => 'cant delete audio that is added to a song'],401);
      }

      $audio->delete();
      return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $audio = Audio::findOrFail($id);
      return view('AudioManage::edit',compact('audio'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'title' => 'required',
      ]);



      $audio = Audio::findOrFail($id);

      if ($request->hasFile('mp3')) {

        $file = $request->file('mp3');
        $path = 'uploads/audio';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'audio-' . date('YmdHis'). '.' . $extn;
        $file->move($destinationPath,$project_fileName);

        $request->merge(['src' =>  '/core/storage/'.$path.'/'.$project_fileName]);
      }

      $request->merge(['user_id' => Sentinel::getUser()->id]);

      $audio->update($request->all());


      return redirect('admin/audio/edit/' . $id)->with(['success' => true,
        'success.message' => 'Audio updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
