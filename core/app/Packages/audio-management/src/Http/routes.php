<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/audio', 'namespace' => 'AudioManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'audio.add', 'uses' => 'AudioController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'audio.edit', 'uses' => 'AudioController@editView'
      ]);

      Route::get('list', [
        'as' => 'audio.list', 'uses' => 'AudioController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'audio.list', 'uses' => 'AudioController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'audio.image.delete', 'uses' => 'AudioController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'audio.add', 'uses' => 'AudioController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'audio.edit', 'uses' => 'AudioController@edit'
      ]);


      Route::post('delete', [
        'as' => 'audio.delete', 'uses' => 'AudioController@delete'
      ]);


    });
});
