<?php
namespace AudioManage\Models;

use Illuminate\Database\Eloquent\Model;
use SongManage\Models\Song;

class Audio extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['src', 'title', 'user_id','dialog','etisalat','airtel','hutch','mobitel'];

	public function song()
	{
	  return $this->hasMany(Song::class);
	}

	public function user()
	{
		return $this->belongsTo(\UserManage\Models\User::class);
	}
}
