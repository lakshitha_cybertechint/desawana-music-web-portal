@extends('layouts.back.master') @section('current_title','Update Audio')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/audio/list')}}">Audio Management</a></li>

        <li class="active">
            <span>Update Audio</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul class="">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
                <form method="POST" class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
                	{!!Form::token()!!}

                    <div class="form-group"><label class="col-sm-2 control-label">TITLE</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="title" value="{{$audio->title}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DIALOG</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="dialog" value="{{$audio->dialog}}" ></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">MOBITEL</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="mobitel" value="{{$audio->mobitel}}" ></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">HUTCH</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="hutch" value="{{$audio->hutch}}" ></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">AIRTEL</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="airtel" value="{{$audio->airtel}}" ></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">ETISALAT</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="etisalat" value="{{$audio->etisalat}}" ></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">MP3</label>
                        <div class="col-sm-10">
                            <input id="mp3" name="mp3" type="file" class="file-loading">
                        </div>
                    </div>

                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>

                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$("#mp3").fileinput({
  uploadUrl: "", // server upload action
  uploadAsync: true,
  maxFileCount: 1,
  showUpload:false,
  allowedFileExtensions: ['mp3'],
  initialPreview: [
  `<audio controls>
  <source src="{{ asset($audio->src)}}" type="audio/mp3">
</audio>`],
});

</script>
@stop
