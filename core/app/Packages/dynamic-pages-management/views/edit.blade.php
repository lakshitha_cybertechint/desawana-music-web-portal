@extends('layouts.back.master') @section('current_title','Edit Dynamic Pages')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendor/bootstrap-star-rating/css/star-rating.css')}}" media="all" />
<style>
.rating-container .rating-stars:before {
    text-shadow: none;
}
</style>

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
<ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('admin/dynamic-pages/list')}}">Dynamic Pages Management</a></li>

    <li class="active">
        <span>Edit Dynamic Pages</span>
    </li>
</ol>
</div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
    <div class="hpanel">
        <div class="panel-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
          @endif
          <form  class="form-horizontal" id="form" method="post" enctype="multipart/form-data">
              {!!Form::token()!!}

              <div class="form-group">
                <label class="col-sm-2 control-label required">Page Name</label>
                <div class="col-sm-10">
                  <input  disabled multiple type="text" class="form-control" value="{{$page->name}}">
                </div>
              </div>
              <div class="form-group"><label class="col-sm-2 control-label">Content</label>
                  <div class="col-sm-10"><textarea id="replyMes" name="content" class="form-control"><?php echo $page->content; ?></textarea></div>

              </div>

              {{-- <div class="form-group">
                  <label class="col-sm-2 control-label required">Atachments</label>
                  <div class="col-sm-10">
                      <input id="project-file" name="atachments[]" multiple type="file" class="file-loading">
                  </div>
              </div> --}}


              <div class="hr-line-dashed"></div>
              <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                      <button class="btn btn-primary" type="submit">Send</button>
                  </div>
              </div>

          </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/back/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>


<script type="text/javascript">
$("#project-file").fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif'],
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
    'showUpload': false,
    overwriteInitial: true,
    removeIcon: '<i class="glyphicon glyphicon-trash"></i>',

});


tinymce.init({
  selector: '#replyMes',  // change this value according to your HTML
  plugins: [
      'advlist autolink lists link image charmap preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code',
      'insertdatetime media nonbreaking table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
  ],
  toolbar: 'bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify ' +
  '| bullist numlist outdent indent | link | undo redo | forecolor backcolor emoticons ',
  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
  menubar: false
});
</script>
@stop
