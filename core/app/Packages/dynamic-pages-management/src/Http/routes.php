<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/dynamic-pages', 'namespace' => 'DynamicPagesManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */




      Route::get('list', [
        'as' => 'dynamic-pages.list', 'uses' => 'DynamicPagesController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'dynamic-pages.list', 'uses' => 'DynamicPagesController@jsonList'
      ]);
      Route::get('edit/{id}', [
        'as' => 'dynamic-pages.edit', 'uses' => 'DynamicPagesController@editView'
      ]);





      /**
       * POST Routes
       */


      Route::post('edit/{id}', [
        'as' => 'dynamic-pages.edit', 'uses' => 'DynamicPagesController@edit'
      ]);
      //
      //
      // Route::post('delete', [
      //   'as' => 'hotel.delete', 'uses' => 'HotelController@delete'
      // ]);


    });
});
