<?php

namespace  DynamicPagesManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use DynamicPagesManage\Models\DynamicPage;
use Sentinel;

class DynamicPagesController extends Controller {
  public function listView() {
      return view('DynamicPagesManage::list');
  }
  public function jsonList(Request $request) {
      if ($request->ajax()) {
          $data = DynamicPage::get();
          $jsonList = array();
          $i = 1;
          foreach ($data as $key => $page) {

              $dd = array();
              array_push($dd, $i);
              array_push($dd, $page->name);
              array_push($dd,  substr(strip_tags($page->content),0,75));

              $permissions = Permission::whereIn('name', ['hotel.edit', 'admin'])->where('status', '=', 1)->lists('name');
              if (Sentinel::hasAnyAccess($permissions)) {
                  array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/dynamic-pages/edit/' . $page->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></center>');
              } else {
                  array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
              }



              array_push($jsonList, $dd);
              $i++;
          }
          return response()->json(array('data' => $jsonList));
      } else {
          return response()->json(array('data' => []));
      }
  }

public function editView($id)
{
  $page = DynamicPage::find($id);
  return view('DynamicPagesManage::edit')->with(["page"=> $page]);
}
public function edit($id, Request $request)
{
  $page = DynamicPage::find($id);
if ($page) {
  $page->content =$request->content;
  if($page->save()){
    return redirect('admin/dynamic-pages/edit/'.$id)->with(['success' => true,
      'success.message' => 'Page edited successfully!',
      'success.title' => 'Good Job!']);
  }
}
}
}
