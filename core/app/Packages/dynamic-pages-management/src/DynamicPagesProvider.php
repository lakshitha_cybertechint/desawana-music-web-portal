<?php
//meki waradda! manger eka uba compser eka dapu ekata dipn!
namespace DynamicPagesManage;

use Illuminate\Support\ServiceProvider;

class DynamicPagesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'DynamicPagesManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('dynamicPagesManage', function($app){
            return new DynamicPagesManage;
        });
    }
}
