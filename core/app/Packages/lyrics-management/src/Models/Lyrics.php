<?php
namespace LyricsManage\Models;

use Illuminate\Database\Eloquent\Model;


class Lyrics extends Model{


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','description','image'];

}
