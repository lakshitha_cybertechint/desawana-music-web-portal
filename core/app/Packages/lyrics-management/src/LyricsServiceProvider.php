<?php

namespace LyricsManage;

use Illuminate\Support\ServiceProvider;

class LyricsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'LyricsManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('lyricsManage', function($app){
            return new LyricsManage;
        });
    }
}
