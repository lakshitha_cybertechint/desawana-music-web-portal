<?php
/**
 * Blog MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Praveen Chameera <praveenchameera@gmail.com>
 * @copyright 2017 Praveen Chameera
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/lyrics', 'namespace' => 'LyricsManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'lyrics.add', 'uses' => 'LyricsController@addView'
      ]);


      Route::get('edit/{id}', [
        'as' => 'lyrics.edit', 'uses' => 'LyricsController@editView'
      ]);

      Route::get('list', [
        'as' => 'lyrics.list', 'uses' => 'LyricsController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'lyrics.list', 'uses' => 'LyricsController@jsonList'
      ]);

      Route::delete('image/delete', [
        'as' => 'lyrics.image.delete', 'uses' => 'LyricsController@deleteImg'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'lyrics.add', 'uses' => 'LyricsController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'lyrics.edit', 'uses' => 'LyricsController@edit'
      ]);


      Route::post('delete', [
        'as' => 'lyrics.delete', 'uses' => 'LyricsController@delete'
      ]);


    });
});
