<?php

namespace LyricsManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Image;
use LyricsManage\Models\Lyrics;

class LyricsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Blog Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView() {
        return view('LyricsManage::add');
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'cover_image' => 'required|mimes:jpeg,bmp,png,'
      ]);


      $file = $request->file('cover_image');
      $path = 'uploads/images/lyrics';
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $project_fileName = 'lyrics-' . date('YmdHis'). '.' . $extn;
      $file->move($destinationPath,$project_fileName);


      $request->merge(['image' => '/core/storage/'.$path.'/'.$project_fileName]);

      Lyrics::create($request->all());


      return redirect('admin/lyrics/add')->with(['success' => true,
        'success.message' => 'Lyrics Created successfully!',
        'success.title' => 'Well Done!']);
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('LyricsManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Lyrics::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $lyrics) {

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $lyrics->title);
                array_push($dd,  substr(strip_tags($lyrics->description),0,50));

                $permissions = Permission::whereIn('name', ['lyrics.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('admin/lyrics/edit/' . $lyrics->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['lyrics.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $lyrics->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      // $this->validate($request,['id' => 'required|exists:hotels,id']);
      // $id = $request->input('id');
      // $blog = Hotel::find($id);
      // $blog->delete();
      // return response()->json(['status' => 'success']);
    }

    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id) {
      $lyrics = Lyrics::findOrFail($id);
      return view('LyricsManage::edit',compact('lyrics'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id) {

      $this->validate($request,[
        'title' => 'required|max:255',
        'description' => 'required',
        'image' => 'nullable|mimes:jpeg,bmp,png,'
      ]);

      $lyrics = Lyrics::findOrFail($id);

      $title = $request->title;
      $description = $request->description;
      $project_fileName = $lyrics->image;

      if ($request->hasFile('cover_image')) {
        $file = $request->file('cover_image');
        $path = 'uploads/images/lyrics';
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $project_fileName = 'lyrics-' . date('YmdHis'). '.' . $extn;
        $file->move($destinationPath,$project_fileName);
        $request->merge(['image' => '/core/storage/'.$path.'/'.$project_fileName]);
      }



      $lyrics->update($request->all());


      return redirect('admin/lyrics/edit/' . $id)->with(['success' => true,
        'success.message' => 'Lyrics updated successfully!',
        'success.title' => 'Good Job!']);
    }


}
