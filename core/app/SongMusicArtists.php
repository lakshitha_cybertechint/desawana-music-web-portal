<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongMusicArtists extends Model
{
  protected $table = "song_music_artist";
  public function artist()
  {
    return $this->belongsTo(Artist::class);
  }

  public function song()
  {
    return $this->belongsTo(Song::class);
  }
}
