<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    public function audio()
    {
        return $this->belongsTo(Audio::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }
    public function artistSongs()
    {
        return $this->hasMany(ArtistSongs::class);
    }

    public function lyric()
    {
      return $this->belongsTo(Lyric::class);
    }

    public function lyricArtists()
    {
      return $this->hasMany(SongsLyricsArtist::class);
    }

    public function musicArtists()
    {
      return $this->hasMany(SongMusicArtists::class);
    }
}
