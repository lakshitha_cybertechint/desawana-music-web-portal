<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
  protected $fillable = ["name", 'email', 'contact_number', 'position_id', 'message', 'image_url'];
  public static function getTodayActiveAds($locationId = -1)
  {
    $ads = Advertisement::where("status", 2)
    ->where(function($q) use ($locationId){
      if ($locationId != -1) {
        return $q->where('position_id', $locationId);
      }
    })
    ->whereDate('start_at', '<=', Carbon::now()->toDateString())
    ->whereDate('expire_at', '>=', Carbon::now()->toDateString())
    ->where('status', 2)
    ->get();
    return $ads;
  }
}
