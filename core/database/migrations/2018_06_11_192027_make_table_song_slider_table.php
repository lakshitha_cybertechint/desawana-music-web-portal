<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTableSongSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_song', function (Blueprint $table) {
          $table->integer('slider_id')->unsigned();
          $table->foreign('slider_id')->references('id')->on('sliders')->onDelete('cascade');
          $table->integer('song_id')->unsigned();
          $table->foreign('song_id')->references('id')->on('songs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slider_song');
    }
}
