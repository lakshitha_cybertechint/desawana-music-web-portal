<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdColumnToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('songs', function (Blueprint $table) {
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('artists', function (Blueprint $table) {
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('audio', function (Blueprint $table) {
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('videos', function (Blueprint $table) {
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('songs', function (Blueprint $table) {
        $table->dropForeign(['user_id']);
      });
      Schema::table('artists', function (Blueprint $table) {
        $table->dropForeign(['user_id']);
      });
      Schema::table('audio', function (Blueprint $table) {
        $table->dropForeign(['user_id']);
      });
      Schema::table('videos', function (Blueprint $table) {
        $table->dropForeign(['user_id']);
      });
    }
}
