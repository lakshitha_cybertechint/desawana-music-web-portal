<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTuneColumsToAudioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audio', function (Blueprint $table) {
            $table->text('dialog')->nullable();
            $table->text('etisalat')->nullable();
            $table->text('airtel')->nullable();
            $table->text('hutch')->nullable();
            $table->text('mobitel')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audio', function (Blueprint $table) {
            $table->dropColumn('dialog');
            $table->dropColumn('etisalat');
            $table->dropColumn('airtel');
            $table->dropColumn('hutch');
            $table->dropColumn('mobitel');
        });
    }
}
