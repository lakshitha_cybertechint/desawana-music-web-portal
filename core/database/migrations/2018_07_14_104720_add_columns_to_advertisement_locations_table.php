<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAdvertisementLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('advertisement_locations', function (Blueprint $table){
        $table->integer('status')->default(1);
        $table->integer('ad_count')->default(1);
        $table->integer('google_ad_status')->default(0);
        $table->integer('script')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('advertisement_locations', function (Blueprint $table){
        $table->dropColumn('status');
        $table->dropColumn('ad_count');
        $table->dropColumn('google_ad_status');
        $table->dropColumn('script');
      });
    }
}
