<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('album_art');
            $table->integer('video_id')->unsigned()->nullable();
            $table->integer('lyrics_id')->unsigned()->nullable();
            $table->integer('audio_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->foreign('lyrics_id')->references('id')->on('lyrics');
            $table->foreign('audio_id')->references('id')->on('audio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('songs');
    }
}
