<?php

use Illuminate\Database\Seeder;

class DynamicPagesTableSeeger extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('dynamic_pages')->insert([
          "id" => 1,
          'name' => "Privacy Policy Page",
          'content' => "Privacy Policy Content",
      ]);
      DB::table('dynamic_pages')->insert([
          "id" => 2,
          'name' => "Terms Of Service Page",
          'content' => "Terms Of Service Content",
      ]);
    }
}
