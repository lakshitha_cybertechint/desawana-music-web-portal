<?php

use Illuminate\Database\Seeder;

class AdvertisementLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('advertisement_locations')->insert(['id' => 1, 'position' => 'Top image (Audios, Videos, Lyrics, Artists)',
         'width' => '928px',
        'height' => '70px']);
       DB::table('advertisement_locations')->insert(['id' => 2, 'position' => 'Side panel(Audio,Video,Lyric)',
        'width' => '300px',
        'height' => '90px']);
       DB::table('advertisement_locations')->insert(  ['id' => 3, 'position' => 'Pagination Top (Audios, Videos, Lyrics, Artists)',
        'width' => '728px',
        'height' => '90px']);
       DB::table('advertisement_locations')->insert(  ['id' => 4, 'position' => 'Pagination Bottom(Audios, Videos, Lyrics, Artists)',
        'width' => '728px',
        'height' => '90px']);
       DB::table('advertisement_locations')->insert(  ['id' => 5, 'position' => 'Side Panel (Home, Audios, Audio, Videos, Video, Lyrics, Lyric)',
        'width' => '300px',
        'height' => '250px']);
       DB::table('advertisement_locations')->insert(  ['id' => 6, 'position' => 'Related Content Top (Audio, Video, Lyric)',
        'width' => '728px',
        'height' => '90px']);

         DB::table('advertisement_locations')->insert(  ['id' => 7, 'position' => 'Related Content Middle (Audio, Video, Lyric)',
        'width' => '728px',
        'height' => '90px']);

         DB::table('advertisement_locations')->insert(  ['id' => 8, 'position' => 'Related Content Bottom (Audio, Video, Lyric)',
        'width' => '728px',
        'height' => '90px']);

         DB::table('advertisement_locations')->insert(  ['id' => 9, 'position' => 'Home Floating Ad',
        'width' => '728px',
        'height' => '90px']);

        DB::table('advertisement_locations')->insert(  ['id' => 10, 'position' => 'Ringtone Codes Top In Audio Page',
       'width' => '728px',
       'height' => '90px']);
    }
}
