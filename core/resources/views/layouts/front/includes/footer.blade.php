<footer id="footer">
			<div class="footer-last">
				<div class="row clearfix">
					<span style="font-size: 11px;" class="copyright">© 2017 - <?php echo date ("Y");?> by <a href="/">Desawana Music</a>. All Rights Reserved. Another IT solution by <a href="https://www.facebook.com/cybertechInt.lk/">Cybertech Int</a>. <br/><span>Current Version DMW1.2.4L</span></span>
					<div id="toTop"><i class="icon-angle-up"></i></div><!-- Back to top -->

					<div class="foot-menu">
						<ul>
							<li><a class="footer-added" href="{{url("privacy-policy")}}">Privacy Policy</a></li>
							<li><a class="footer-added" href="{{url("terms-of-service")}}">Terms Of Service</a></li>
							<li><a class="footer-added" href="{{url("disclaimer")}}">Disclaimer</a></li>
						</ul><!-- end links -->
					</div><!-- end foot menu -->
				</div><!-- row -->
			</div><!-- end last -->

		</footer><!-- end footer -->
