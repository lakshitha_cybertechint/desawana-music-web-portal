<style type="text/css">
	@media(max-width: 970px){
		.menucolor{
			border: 1px solid #ccc;
    		font-size: 15px;
    		font-weight: bold;
    		background: rgba(204, 204, 204, 0.25);
		}

		.menucolor:hover{
			border: 1px solid #ccc;
    		font-size: 15px;
    		font-weight: bold;
    		background: rgba(204, 204, 204, 0.25);
		}


		#cssmenu .caret
					{
		   	color: inherit;
		    left: 40px;
		    width: 0;
		    height: 0;
		    overflow: hidden;
		    vertical-align: middle;
		    margin-bottom: 2px;
		    border-top: 2px solid;
		    border-right: -1px solid rgba(255, 255, 255, 0.9);
		    border-left: -1px solid (255, 255, 255, 0.9);
		    display: inline-block;
		    position: relative;
				}
	}

	.menu-logo-orange{
		max-height:30px;
	}

	.orange-logo{
		margin-top: -10%;
	}

	@media(max-width:1000px){
		.menu-logo-orange{
			max-height:50px;
			text-align: center;
		}

		.orange-logo{
			margin-top: auto;;
		}
	}
</style>
<div class="row">
	<div class="col-md-12 text-right">
			<ul class="list-inline customer-board" style="margin-top: 0px;margin-bottom: 10px;">
				<!-- <li class=""><a href="{{url('wishlist')}}">My Wishlist</a></li><span style="font-weight:bolder;color:#FFF;">|</span>
				<li class=""><a href="#">My Account</a></li><span style="font-weight:bolder;color:#FFF;">|</span> -->
				@if(Sentinel::check())
				@if(Sentinel::getUser()->hasAnyAccess(['admin']))
				<li class="icon-user"><a href="{{url('admin')}}"><i class="fa fa-wrench"></i><span> Admin</span></a></li>
				<span style="font-weight:bolder;color:#FFF;">|</span>
				@endif
				@if(Sentinel::getUser()->hasAnyAccess(['wishlist','admin']))
				<li class="icon-user"><a href="{{url('wishlist')}}"><span>Wishlist</span></a></li>
				<span style="font-weight:bolder;color:#FFF;">|</span>
				@endif
				@if(Sentinel::getUser()->hasAnyAccess(['my-account','admin']))
				<li class="icon-user"><a href="{{url('my-account')}}"><span>My Account</span></a></li>
				<span style="font-weight:bolder;color:#FFF;">|</span>
				@endif
				<li class="icon-user"><a href="{{url('user/logout')}}"><i class="fa fa-user"></i><span> Logout</span></a></li>
				@else
				<li class="icon-user"><a href="{{url('user/register')}}"><i class="fa fa-user"></i><span> Register</span></a></li>
				<span style="font-weight:bolder;color:#FFF;">|</span>
				<li class="icon-key"><a href="{{url('user/login')}}"><i class="fa fa-key"></i><span> Login</span></a></li>
				@endif
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-2 col-xs-12 menu-logo-orange" style="">
			<a href="{{url('/')}}"><img src="{{asset('assets/front/img/orange-logo1.png')}}" class="orange-logo" style="width: 155px"></a>
	</div>
	<div class="col-md-10 col-xs-12" style="padding-left: 0;padding-right: 0">
		{{--<nav style="border: 1px solid #ccc;">--}}
			{{--<ul class="nav nav-justified">--}}
				{{--<li><a href="{{url('category')}}" >Products</a></li>--}}
				{{--<li><a href="application.html">Applications</a></li>--}}
				{{--<li><a href="technology.html">About Us</a></li>--}}
				{{--<li><a href="about-us.html">Technology</a></li>--}}
				{{--<li><a href="contact-us.html">Contact us</a></li>--}}
			{{--</ul>--}}
		{{--</nav>--}}
		<div id='cssmenu'>
			<ul>
				<li ><a href='#' class="menucolor"><span>Products</span><span class="caret"></span></a>
					<ul>
						<li><a href="{{url('sbu/electrical-1')}}"><span>Electrical</span></a></li>
						<li><a href="{{url('sbu/lighting-2')}}"><span>Lighting</span></a></li>
						<li><a href="{{url('sbu/industrial-3')}}"><span>Industrial</span></a></li>
						<li><a href="{{url('sbu/it-4')}}"><span>IT</span></a></li>
					</ul>
				</li>
				<li class=''><a href='{{url('applications')}}' class="menucolor"><span>Applications</span></a></li>
				<li class=''><a href='#' class="menucolor"><span>About Us</span><span class="caret"></span></a>
					<ul>
						<li><a href="{{url('about-us')}}"><span>Who we are</span></a></li>
						<li><a href="{{url('achievements')}}"><span>Achievements</span></a></li>
						<li><a href="{{url('careers')}}"><span>Careers</span></a></li>
						<li><a href="{{url('csr')}}"><span>CSR</span></a></li>
						<li><a href="{{url('life')}}"><span>Life at Orel</span></a></li>
					</ul>
				</li>
				<li class=''><a href='#' class="menucolor"><span>Manufacturing</span><span class="caret"></span></a>
					<ul>
						<li><a href="{{url('technology')}}"><span>Technology</span></a></li>
						<li><a href="{{url('innovation')}}"><span>Innovation</span></a></li>
					</ul>
				</li>
				<li class='last'><a href='{{url('contact-us')}}' class="menucolor"><span>Contact us</span></a></li>
			</ul>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#cssmenu>ul>li').on('click',function(){
		//$(this).addClass('over');
		$(this).parent().parent().addClass('over');
	});
});

</script>
