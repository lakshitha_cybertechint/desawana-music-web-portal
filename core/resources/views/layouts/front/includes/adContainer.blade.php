<?php
  $adLocation = App\AdvertisementLocation::whereStatus(1)->where("id", $locationId)->first();
  $today = Carbon\Carbon::now()->toDateString();
  $ads = App\Advertisement::getTodayActiveAds($locationId);
?>

@if ($locationId == 9)
  {{-- home floating ad --}}
@if (is_null($adLocation))

@elseif ($ads->count() == 0)
  <div class="homeFloatingAd" style="width:100%; max-width:100%;">
  <button id="homeFloatingAdCloseBtn" type="button" name="button" class="closeBtn tbutton pull-right" title="Close Ad"><span>X</span></button>
    <img class="" src="{{url('core/storage/uploads/images/ads/defaults/'.$adLocation->width.'_'.$adLocation->height.'.png')}}" style="width:{{$adLocation->width}}; max-width:100%; max-height : {{$adLocation->height}}">
  </div>
  @else
    <div class="homeFloatingAd" style="width:100%; max-width:100%;">
      <button id="homeFloatingAdCloseBtn" type="button" name="button" class="closeBtn tbutton pull-right" title="Close Ad"><span>X</span></button>
        <a href="{{$ads[0]->ad_link}}" >
          <img class="" src="{{url('core/'.$ads[0]->image_url)}}" style="width:{{$adLocation->width}}; max-width:100%; max-height : {{$adLocation->height}}">
        </a>
    </div>

    <!-- <div class="homeFloatingAd" style="width:{{$adLocation->width}}; max-width:100%;">
    <button id="homeFloatingAdCloseBtn" type="button" name="button" class="closeBtn tbutton" title="Close Ad"><span>X</span></button>
      <a href="{{$ads[0]->ad_link}}" >
        <img class="" src="{{url('core/'.$ads[0]->image_url)}}" style="width:{{$adLocation->width}}; max-width:100%; max-height : {{$adLocation->height}}">
      </a>
    </div> -->
@endif

@elseif ($locationId == 5)


            @if (is_null($adLocation))

            @elseif ($adLocation->google_ad_status == 1)
              <span class="liner"></span>
              <div class="widget-content tac">
                <?php echo $adLocation->script;  ?>
              </div>
            @elseif ($ads->count() == 0)
              @for ($i=0; $i < $adLocation->ad_count; $i++)
              <span class="liner"></span>
              <div class="widget-content tac">
                <img src="{{url('core/storage/uploads/images/ads/defaults/'.$adLocation->width.'_'.$adLocation->height.'.png')}}" style="width:100%; max-width:100%;">
              </div>
            @endfor

            @else
              @foreach ($ads as $ad)
                <span class="liner"></span>
                <div class="widget-content tac">
                  <a href="{{$ad->ad_link}}" >
                  <img src="{{url('core/'.$ad->image_url)}}" style="width:100%; max-width:100%;">
                  </a>
                </div>
              @endforeach
            @endif

          </div>

@else
  <div class="adContainer" style="margin-bottom: 0;">

            @if (is_null($adLocation))

            @elseif ($adLocation->google_ad_status == 1)
              
                <?php echo $adLocation->script;  ?>
              
            @elseif ($ads->count() == 0)
              <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->

                <div class="carousel-inner">
              <div class="item active">
                <img src="{{url('core/storage/uploads/images/ads/defaults/'.$adLocation->width.'_'.$adLocation->height.'.png')}}" style="width:100%; max-width:100%;">
              </div>
            </div>



          </div>
            @else
              <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->

                <div class="carousel-inner">
              @foreach ($ads as $key => $ad)
                <div class="item {{$key == 0 ? "active" : ""}} " style="margin-bottom: 20px;">
                  <a href="{{$ad->ad_link}}" >
                  <img src="{{url('core/'.$ad->image_url)}}" style="width:100%; max-width:100%;">
                  </a>
                </div>
              @endforeach
            </div>



          </div>
            @endif


    </div>
@endif
