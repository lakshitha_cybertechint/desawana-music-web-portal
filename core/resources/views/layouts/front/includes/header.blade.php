      <header id="header" class="glue">
    <div class="row clearfix">
      <div class="little-head">


        <div class="social social-head">
          <a href="http://twitter.com/behzadg1" class="bottomtip" title="Follow us on Twitter" target="_blank"><i class="icon-twitter"></i></a>
          <a href="https://www.facebook.com/desawanaofficial/" class="bottomtip" title="Like us on Facebook" target="_blank"><i class="icon-facebook"></i></a>
          <a href="https://plus.google.com/u/0/b/113790328235414397273/113790328235414397273" class="bottomtip" title="GooglePlus" target="_blank"><i class="icon-google-plus"></i></a>
          <a href="https://www.instagram.com/desawanalk/" class="bottomtip" title="instagram" target="_blank"><i class="icon-instagram"></i></a>
          <!-- <a href="http://dribbble.com/behzadg" class="bottomtip" title="Dribbble" target="_blank"><i class="icon-dribbble"></i></a> -->
          <!-- <a href="https://soundcloud.com/behzad-gh" class="bottomtip" title="Soundcloud" target="_blank"><i class="icon-cloud"></i></a> -->
          <!-- <a href="http://reverbnation.com/" class="bottomtip" target="_blank" title="Reverbnation"><i class="icon-star"></i></a> -->
          <a href="https://www.youtube.com/channel/UCVwbUjoiTUXxeVjsxW9Kwng" class="bottomtip" target="_blank" title="YouTube"><i class="icon-youtube-play"></i></a>
          <!-- <a href="http://youtube.com/" class="bottomtip" target="_blank" title="Flickr"><i class="icon-flickr"></i></a> -->
          <!-- <a href="http://www.linkedin.com/" class="bottomtip" title="Linkedin" target="_blank"><i class="icon-linkedin"></i></a> -->
        </div><!-- end social -->

        <div class="search">
        <form action="{{url('search')}}" id="search" method="get">
            <input id="inputhead" name="q" type="text" placeholder="Start Searching ...">
            <button type="submit"><i class="icon-search"></i></button>
          </form><!-- end form -->
        </div><!-- search -->
      </div><!-- little head -->
    </div><!-- row -->

    <div class="headdown">
      <div class="row clearfix headdownBack">
        <div class="logo bottomtip" title="Best and Most Popular Musics">
          <a href="{{url("/")}}"><img src="{{url("assets/front/images/logo.png")}}" alt="Best and Most Popular Musics"></a>
        </div><!-- end logo -->

        <nav>
          <ul class="sf-menu">
						<li class="{{Request::is("/") ? "current selectedLava" : ""}}"><a href="{{url("/")}}">Home<span class="sub">Homepage</span></a></li>
							<li class="{{Request::is("all-audios") || Request::is("all-audios/*") || Request::is("latest-audios") || Request::is("latest-audios/*") ||  Request::is("audio/*")? "current selectedLava" : ""}}"><a  href="{{url("latest-audios")}}">Audios<span class="sub">All Audios</span></a></li>
							<li class="{{Request::is("old-videos") || Request::is("latest-videos") || Request::is("old-videos/*") || Request::is("latest-videos/*") || Request::is("video/*") ? "current selectedLava" : ""}}"><a href="{{url("latest-videos")}}">Videos<span class="sub">All Videos</span></a></li>
							<li class="{{Request::is("lyrics") || Request::is("lyric/*") ? "current selectedLava" : ""}}"><a href="{{url("lyrics")}}">Lyrics<span class="sub">All Lyrics</span></a></li>
							<li class="{{Request::is("artists") || Request::is("artists/*")? "current selectedLava" : ""}}"><a href="{{url("artists")}}">Artists<span class="sub">All Artists</span></a></li>
							<li class="{{Request::is("about-us") ? "current selectedLava" : ""}}"><a href="{{url("about-us")}}">About Us<span class="sub">About Desawana</span></a></li>
							<li><a class="@if(Request::is("contact-us"))  current selectedLava @endif" href="{{url("contact-us")}}">Contact Us<span class="sub">Reach To Us</span></a></li>
							<li class="{{Request::is("advertise") ? "current selectedLava" : ""}}"><a href="{{url("advertise")}}">Advertise<span class="sub">Send Your Banners</span></a></li>

          </ul><!-- end menu -->
        </nav><!-- end nav -->
      </div><!-- row -->
    </div><!-- headdown -->
  </header><!-- end header -->
