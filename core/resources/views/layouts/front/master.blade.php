<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9" lang="en-US"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html  lang="en-US">
<!--<![endif]-->

<head>
	@if (\Request::is("/"))
		<title>Latest Sinhala Songs | Free Sinhala MP3 Songs | Sinhala Official Music Videos | Sinhala Lyrics | Desawana.com </title>
	@else
		<title>Latest Sinhala Songs | Free Sinhala MP3 Songs | Sinhala Official Music Videos | Sinhala Lyrics | Desawana.com </title>
	@endif
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />



{{-- aditional meta --}}
@yield('meta')
	<!-- Styles -->
		 
		 <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->

		<ink href="{{url('assets/front/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
		
		<link rel="stylesheet" href="{{url('assets/front/font-awesome.min.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/bootstrap/css/bootstrap-responsive.min.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/js/rs-plugin/css/settings.css')}}" id="dark" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/style.css')}}" id="dark" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/styles/icons/icons.css')}}" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	<!-- Favicon -->
		<link rel="shortcut icon" href="{{url('assets/front/images/favicon.ico')}}">
		<link rel="apple-touch-icon" href="{{url('assets/front/images/apple-touch-icon.png')}}">

	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=EmulateIE8; IE=EDGE" />
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link rel="stylesheet" type="text/css" href="{{url('assets/front/styles/icons/font-awesome-ie7.min.css')}}" />
	<![endif]-->

@yield('css')
<style media="screen">uration{
	display: none;
}
.rating{
	display: none;
}
.fill {
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden
}
.fill img {
    flex-shrink: 0;
    min-width: 100%;
    min-height: 100%
}
.text-justify{
	text-align: justify;
}
	.rating-level{
		display: none !important;
	}
	.pagination {
display: inline-block;
padding-left: 0;
margin: 20px 0;
border-radius: 4px;
}
.pagination > li {
	display: inline;
}
.pagination > li > a, .pagination > li > span {
border: 1px solid #202020;
color: #A3A3A3;
font-size: 11px;
padding: 4px 13px;
margin: 0 2px 4px 0;
display: inline-block;
-webkit-border-radius: 35px;
border-radius: 35px;
-webkit-transition: all .2s ease;
transition: all .2s ease;
}
.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
	z-index: 3;
	color: #fff;
	cursor: default;
	background-color: #FF0078;
	border: 1px solid #202020;
}
.adContainer{
	margin-bottom: 15px;
}
</style>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<body id="fluidGridSystem">
<!-- ====================================BODY================================================== -->
<div id="layout" class="full">
<!-- =====================================HEADER====================================================== -->
@include('layouts.front.includes.header')
<!-- ========================================================================================= -->

@yield('content')
<!-- ============================================================================================= -->

<!-- =====================================FOOTER================================================= -->
@include('layouts.front.includes.footer')
<!-- =========================================================================================== -->

</div><!-- end layout -->

</body>
<!-- ==============================SCRIPT=================================================== -->





	<script type="text/javascript" src="{{url('assets/front/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/theme20.js')}}"></script>
	
	<!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script> -->

	<script src="{{url('assets/front/js//bootstrap.min.js')}}" ></script>
	
	<script type="text/javascript" src="{{url('assets/front/js/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/jquery.prettyPhoto.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/jquery.flexslider-min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/jquery.jplayer.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/ttw-music-player-min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/jquery.nicescroll.min.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/custom.js')}}"></script>
	<script type="text/javascript" src="{{url('assets/front/js/jquery.matchHeight.js')}}"></script>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
<!-- ======================================================================================= -->
@yield('scripts')
</html>
