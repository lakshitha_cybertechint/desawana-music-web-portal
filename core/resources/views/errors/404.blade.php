@extends('layouts.front.master') @section('title','Prince Hotel | Home')
@section('css')

@stop

@section('content')


</div>

    <div class="container" 
    <div class="row" style="margin-bottom:4px;vertical-align:middle;padding-bottom:10%;">
      <div class="col-md-12 col-xs-12 text-center" style="vertical-align:middle;padding-bottom:10%;">
        <img src="{{asset('assets/front/images/oop-404.png')}}" style="width:100%;" alt=""/>
        <h3 style="margin-top: 8px;">The page you are looking is not found.</h3>
        <a href="{{url('/')}}" style="text-decoration:underline;font-size: 15px;color:#000"><i class="fa fa-angle-left"></i> Back to Home</a>
      </div>
    </div>
   <!--  <div style="position:absolute;bottom:0;width:100%;">
    @include('layouts.front.includes.footer')
    </div> -->
    </div>
@stop
