@extends('layouts.front.master') @section('title', 'Search')
@section('meta')

@stop
@section('css')

@stop
@section('content')

  		<div class="under_header">
  			<img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
  		</div><!-- under header -->

      <div class="page-content back_to_up">
			<div class="row clearfix mb">
				<div class="breadcrumbIn">
					<ul>
						<li><a href="{{url("/")}}" class="toptip" title="Homepage"> <i class="icon-home"></i> </a></li>
						<li>Search</li>
						<li>{{$query}}</li>
					</ul>
				</div><!-- breadcrumb -->
			</div><!-- row -->

			<div class="row row-fluid clearfix mbf">


				<div class="span12 posts">
            <div class="grid_8">
            <div class="def-block widget">
              <h4>Related  Audios </h4><span class="liner"></span>
              <div class="widget-content" style="display : flex;">
                  @if(count($relatedAudios) == 0)
                  <h6>Sorry No Results Found..</h6>
                  @else
                <ul class="tab-content-items grid_12">
                  @foreach ($relatedAudios as $audio)

                    <li class="grid_6">
                      <a class="m-thumbnail" href="{{url("audio/".$audio->id)}}"><img width="50" height="50" src="{{url($audio->song->album_art)}}" alt="#"></a>
                      <h3><a href="{{url("audio/".$audio->id)}}">{{$audio->shortTitle}}</a></h3>
                      <span> @if ($audio->song->artistSongs)

                        {{ $audio->song->artistSongs->count()== 1 ? $audio->song->artistSongs->first()->artist->name : "Various Artists"}}
                    @endif </span>
                    {{$audio->song->audio_download}} Downloads
                    </li>

                  @endforeach

                </ul>
                @endif
              </div><!-- widget content -->
            </div><!-- block -->

            <div class="def-block widget">
              <h4>Related Videos </h4><span class="liner"></span>
              <div class="widget-content">

                <div class="video-grid clearfix">
                    @if(count($relatedVideos) == 0)
                  <h6>Sorry No Results Found..</h6>
                  @else
                  @foreach ($relatedVideos as $video)



                    <a href="{{url("video/".$video->id)}}" class="grid_4 setHeight_2">
                      <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg"  alt="#">
                      <span class="text-justify"><strong>{!!substr($video->shortTitle,0,30)!!}</strong>
                        @if ($video->song->artistSongs)

                          {{ $video->song->artistSongs->count()== 1 ? $video->song->artistSongs->first()->artist->name : "Various Artists"}}
                      @endif</span>
                    </a>


                  @endforeach
                  @endif

                </div>
              </div><!-- widget content -->
            </div><!-- block -->
          </div><!-- grid -->


          <div class="grid_4">
            <div class="def-block widget">
              <h4>Related  Artists </h4><span class="liner"></span>
              <div class="widget-content" style="display : flex;">
                  @if(count($relatedArtists) == 0)
                  <h6>Sorry No Results Found..</h6>
                  @else

                <ul class="tab-content-items grid_12">
                  @foreach ($relatedArtists as $artist)

                    <li class="grid_12" >
                        <a class="m-thumbnail" href="{{url("artists/".$artist->id)}}"><img width="50" height="50" src="{{url($artist->img)}}" alt="#"></a>
                        <h3><a href="{{url("artists/".$artist->id)}}">{{$artist->name}}</a></h3>
                    <span>{{$artist->songIds->count()}} Songs</span>
                    {{-- <span>{{$artist->songIds->count()}} Songs</span> --}}
  										</li>

                  @endforeach

                </ul>
                @endif
              </div><!-- widget content -->
            </div><!-- block -->

            <div class="def-block widget">
              <h4>Related  Lyrics </h4><span class="liner"></span>
              <div class="widget-content" style="display : flex;">
                  @if(count($relatedLyrics) == 0)
                  <h6>Sorry No Results Found..</h6>
                  @else
                <ul class="tab-content-items grid_12">

                  @foreach ($relatedLyrics as $lyric)

                    <li class="grid_12">
                      <a class="m-thumbnail" href="{{url("lyric/".$lyric->id)}}"><img width="50" height="50" src="{{url($lyric->song->album_art)}}" alt="#"></a>
                      <h3><a href="{{url("lyric/".$lyric->id)}}">{{$lyric->shortTitle}}</a></h3>
                      <span> @if ($lyric->song->artistSongs)

                        {{ $lyric->song->artistSongs->count()== 1 ? $lyric->song->artistSongs->first()->artist->name : "Various Artists"}}
                    @endif </span>
                    <span></span>
                    </li>

                  @endforeach

                  @endif

                </ul>
              </div><!-- widget content -->
            </div><!-- block -->
          </div><!-- grid -->




				</div><!-- span6 posts -->


			</div><!-- row clearfix -->
		</div><!-- end page content -->
  @stop
  @section('scripts')
<script type="text/javascript">

var tabs = $('ul.tabs');
tabs.each(function (i) {
  // get tabs
  var tab = $(this).find('> li > a');
  tab.click(function (e) {
    // get tab's location
    var contentLocation = $(this).attr('href');
    // Let go if not a hashed one
    if (contentLocation.charAt(0) === "#") {
      e.preventDefault();
      // add class active
      tab.removeClass('active');
      $(this).addClass('active');
      // show tab content & add active class
      $(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
    }
  });
});

if ($(".videos")[0]) {
  $('.videos').flexslider({
    animation: "fade",
    slideshowSpeed: 5000,
    animationSpeed: 600,
    directionNav: true,
    controlNav: false,
    pauseOnHover: true,
    initDelay: 0,
    randomize: false,
    smoothHeight: true,
    keyboardNav: true
  });
}
$('.setHeight_2').matchHeight({
    byRow: true,
    property: 'height',
    target: null,
    remove: false
});
</script>
  @endsection
