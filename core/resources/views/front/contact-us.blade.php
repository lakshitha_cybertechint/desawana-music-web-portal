@extends('layouts.front.master') @section('title','Contact Us')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style type="text/css">
  
  .min-width-100 {
    min-width: 98%!important;
  }

  .custom-input {
      height: 28px;
      border-radius: 5px;
  }

  .select {
      height: 33px;
      border-radius: 5px;
  }

</style>
@stop
@section('content')
<div class="under_header">
    <img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
</div>
<!-- under header -->
<div class="page-content back_to_up">
    <div class="row clearfix mb">
        <div class="breadcrumbIn">
            <ul>
                <li><a href="index-2.html" class="toptip" title="Homepage"> <i class="icon-home"></i> </a></li>
                <li>Contact Us</li>
            </ul>
        </div>
        <!-- breadcrumb -->
    </div>
    <!-- row -->
    <div class="row row-fluid clearfix mbf">
        <div class="def-block clearfix">
            <h4> Reach To Us </h4>
            <span class="liner"></span>
            <div class="grid_6 mt">
                <p class="text-justify">Desawana Music is the Latest World Class Music Provider in Sri Lanka. We Provide All Kind of Music Genre in a way of Sri Lankan tradition. you can Contact us Directly via the Form next or you can use our email address. If you need to contact us for any kind of Copyright Issues please use below email address to send your inquiry.</p>
                <p>Note : Before Submiting any Copyright issue please read our <a href="{{url("disclaimer")}}">disclaimer</a></p>
                <p>Phone: <strong> <a href="tel:+94112649510">+94 112 649 510</a></strong><br/>Hotline: <strong> <a href="tel:++94779840660">+9477 9 840 660</a></strong><br/>Email: <strong><a href="mailto:desawana.lk@gmail.com" >desawana.lk@gmail.com</a></strong></p>
            </div>
            <!-- end grid6 -->
            <div class="grid_6 mt">
                <form  id="contactForm" >
                    {{ csrf_field() }}
                    <div class="clearfix">
                        <div class="grid_6 alpha fll">
                          <input type="text" name="name" id="name" placeholder="Name *" class="requiredField form-control min-width-100 custom-input" />
                        </div>
                        <div class="grid_6 omega flr">
                          <input type="text" name="email" id="email" placeholder="Email Address *" class="requiredField email form-control min-width-100 custom-input" />
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="grid_6 alpha fll">
                          <input type="text" name="contact_number" id="contact_number" placeholder="Contact Number *" class="requiredField form-control min-width-100 custom-input" />
                        </div>
                        <div class="grid_6 omega flr">
                          <input type="text" name="subject" id="subject" placeholder="Subject *" class="requiredField  form-control min-width-100 custom-input" />
                        </div>
                    </div>
                    <div style="margin-top: 10px;">
                      <textarea class="form-control min-width-100" name="message" id="message" placeholder="Message *" class="requiredField" rows="8"></textarea>
                    </div>
                    <div class="clearfix grid_6" style="margin-top: 10px;">
                        <div class="g-recaptcha" data-sitekey="6LfHP18UAAAAAD9rQIls2PziQXpKE8aGJdUmlOiI" data-theme="dark"></div>
                    </div>
                    <div class="clearfix grid_6">
                        <input type="button" form="noForm" id="sendMessage" onclick="ajax_request()" class="FormSubmit" name="sendMessage" value="Submit your Request" />
                    </div>
                    <!-- <div class="clearfix grid_12" id="contactFormErrors"></div> -->
                </form>
                <!-- end form -->
            </div>
            <!-- end grid6 -->
        </div>
        <!-- def block -->
    </div>
    <!-- row clearfix -->
</div>
<!-- end page content -->
<div id="map" class="hovermap"></div>
<!-- map -->
@stop
@section('scripts')
<script type="text/javascript"src="//maps.googleapis.com/maps/api/js?key=AIzaSyCQ0bmRrzi1IFCFocSuFPbbZuW9Hqxqhio " async="" defer="defer" ></script>
<script type="text/javascript" src="{{url('assets/front/js/gmap3.js')}}"></script>
<script type="text/javascript">
    /* <![CDATA[ */
      jQuery(function () {
          jQuery("#map").gmap3({
              marker: {
                  address: "2897 Chicago"
              },
              map: {
                  options: {
                      zoom: 6
                  }
              }
          });
      });
    /* ]]> */
    formId = "contactForm";
    baseUrl =  "{{url("contact-us")}}";
    url =  baseUrl;
     function ajax_request(){
       data = new FormData(document.getElementById(formId));
         var xhttp;
         if (window.XMLHttpRequest) {
           xhttp = new XMLHttpRequest();
         } else {
           // code for IE6, IE5
           xhttp = new ActiveXObject("Microsoft.XMLHTTP");
         }
         xhttp.addEventListener("load", function() {

           SubmitResultHandler(JSON.parse(this.responseText), formId);

         });
         xhttp.addEventListener("error", function() {
           setTimeout(function() {
             oops();
           }, 100);
         });

           xhttp.open("POST", baseUrl, false);

         xhttp.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));

         xhttp.send(data);
     }

     function SubmitResultHandler(data, formId) {
       if (data.hasOwnProperty('errors') && Object.keys(data.errors).length !== 0) {

           submitErrors(data.errors);


       } else if (data.hasOwnProperty('success') && data.success == "19199212") {

            submitSuccess(data.message);

       }else{
         oops();
       }
     }
     function submitErrors(errors){
       $("#"+formId+"Errors").html(createErrorsList(errors));
       $("#"+formId+"Errors").show("slow");
     }
     function submitSuccess(data){
       var datahtml = '<p class="text-success">'+data+'</p>';
       $("#"+formId+"Errors").html(datahtml);

     }

     function createErrorsList(errors){
       var retData = '<ul style="color:red; text-align:left;">';

       Object.keys(errors).forEach(function(key) {

         retData += '<li>' + errors[key] + '</li>';
       });
       retData += '</ul>';
       return retData;
     }

</script>
@endsection
