@extends('layouts.front.master') @section('title','Advertise')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style media="screen">
    .fll {
    margin: 0 0px 10px 0 !important;
    }
    .flr {
    margin: 0 0 10px 0 !important;
    }
    #message {
    display: block;
    width: 100%;
    border: 1px solid #EAEAEA;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    padding: 16px 10px;
    background: #F7F7F7;
    box-shadow: none;
    -webkit-box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-transition: all 0.3s;
    transition: all 0.3s;
    }
    #sendMessage {
    width: auto;
    margin-top: 10px;
    float: right;
    padding: 8px 10px;
    font-family: Oswald, Tahoma, Helvetica;
    display: inline-block;
    cursor: pointer;
    position: relative;
    word-spacing: 0.2em;
    background: #ff0078;
    background-image: none;
    border: 0 !important;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    color: #fff !important;
    white-space: nowrap;
    text-transform: uppercase;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
    border: 1px solid rgba(0,0,0,0.1);
    background-image: linear-gradient(bottom, rgba(0,0,0,0.08) 0%, rgba(128,128,128,0.08) 50%, rgba(255,255,255,0.08) 100%);
    background-image: -o-linear-gradient(bottom, rgba(0,0,0,0.08) 0%, rgba(128,128,128,0.08) 50%, rgba(255,255,255,0.08) 100%);
    background-image: -moz-linear-gradient(bottom, rgba(0,0,0,0.08) 0%, rgba(128,128,128,0.08) 50%, rgba(255,255,255,0.08) 100%);
    background-image: -webkit-linear-gradient(bottom, rgba(0,0,0,0.08) 0%, rgba(128,128,128,0.08) 50%, rgba(255,255,255,0.08) 100%);
    background-image: -ms-linear-gradient(bottom, rgba(0,0,0,0.08) 0%, rgba(128,128,128,0.08) 50%, rgba(255,255,255,0.08) 100%);
    background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0, rgba(0,0,0,0.08)),color-stop(0.5, rgba(128,128,128,0.08)),color-stop(1, rgba(255,255,255,0.08)));
    -moz-box-shadow: inset 0 0 1px rgba(0,0,0,0.1);
    -webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.1);
    box-shadow: inset 0 0 1px rgba(0,0,0,0.1);
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
    }

    .min-width-100 {
        min-width: 98%!important;
    }

    .custom-input {
        height: 28px;
        border-radius: 5px;
    }

    .select {
        height: 33px;
        border-radius: 5px;
    }
</style>
@stop
@section('content')
<div class="under_header">
    <img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
</div>
<!-- under header -->
<div class="page-content back_to_up">
    <div class="row clearfix mb">
        <div class="breadcrumbIn">
            <ul>
                <li><a href="index-2.html" class="toptip" title="Homepage"> <i class="icon-home"></i> </a></li>
                <li> Advertise</li>
            </ul>
        </div>
        <!-- breadcrumb -->
    </div>
    <!-- row -->
    <div class="row row-fluid clearfix mbf">
        <div class="def-block clearfix">
            <h4> Send Your Banners </h4>
            <span class="liner"></span>
            <div class="grid_6 mt">
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </p>
                <p>It has survived not only five centuries, but also the leap into electronic typesetting.</p>
                <p>Phone: <strong> <a href="tel:+496170961709">496170961709</a> </strong> <br> Email: <strong><a href="mailto:someone@example.com" >info@remixmusic.com</a></strong></p>
            </div>
            <!-- end grid6 -->
            <div class="grid_6 mt">
                <form  id="advertiseForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="clearfix">
                        <div class="grid_6 alpha fll"><input name="name"  placeholder="Name *" class="requiredField form-control min-width-100 custom-input" type="text"></div>
                        <div class="grid_6 omega flr"><input name="email"  placeholder="Email Address *" class="requiredField email form-control min-width-100 custom-input" type="text"></div>
                    </div>
                    <div class="clearfix">
                        <div class="grid_6 alpha fll"><input type="text" name="contact_number" id="contact_number" placeholder="Contact Number (94xxxxxxxxx) *" class="requiredField form-control min-width-100 custom-input" /></div>
                        <div  class="grid_6 omega flr">
                            <select name="position" class="form-control grid_12 select">
                                <option  value="" selected disabled>Select Position (Width X Height)</option>
                                @foreach ($ads as $ad)
                                <option value="{{$ad->id}}">{{$ad->position}} ({{$ad->width}} X {{$ad->height}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div  class="clearfix ">
                        <input type="file" class="form-control" name="image" accept="image/jpeg, image/gif, image/x-png" >
                    </div>
                    
                    <!-- <div style="margin-top: 10px;">
                      <textarea name="message" placeholder="Message *" class="requiredField form-control" rows="8"></textarea>
                    </div> -->

                    <div style="margin-top: 10px;">
                      <textarea class="form-control min-width-100" name="message" id="message" placeholder="Message *" class="requiredField" rows="8"></textarea>
                    </div>
                    
                    <div class="clearfix grid_6" style="margin-top: 10px;">
                        <div class="g-recaptcha" data-sitekey="6LfHP18UAAAAAD9rQIls2PziQXpKE8aGJdUmlOiI" data-theme="dark"></div>
                    </div>
                    <div class="clearfix grid_6">
                        <input type="button" form="noForm" id="sendMessage" onclick="ajax_request()"  class="FormSubmit" name="sendMessage" value="Submit your Ad"/>
                    </div>
                    <div class="clearfix clearfix grid_12" id="advertiseFormErrors">
                    </div>

                </form>
                <!-- end form -->
            </div>
            <!-- end grid6 -->
        </div>
        <!-- def block -->
    </div>
    <!-- row clearfix -->
</div>
<!-- end page content -->
@stop
@section('scripts')
<script type="text/javascript">
    formId = "advertiseForm";
    baseUrl =  "{{url("advertise")}}";
      url =  baseUrl;
        function ajax_request(){
          data = new FormData(document.getElementById(formId));
            var xhttp;
            if (window.XMLHttpRequest) {
              xhttp = new XMLHttpRequest();
            } else {
              // code for IE6, IE5
              xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.addEventListener("load", function() {
    
              SubmitResultHandler(JSON.parse(this.responseText), formId);
    
            });
            xhttp.addEventListener("error", function() {
              setTimeout(function() {
                oops();
              }, 100);
            });
    
              xhttp.open("POST", baseUrl, false);
    
            xhttp.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
    
            xhttp.send(data);
        }
    
        function SubmitResultHandler(data, formId) {
          if (data.hasOwnProperty('errors') && Object.keys(data.errors).length !== 0) {
    
              submitErrors(data.errors);
    
    
          } else if (data.hasOwnProperty('success') && data.success == "19199212") {
    
               submitSuccess(data.message);
    
          }else{
            oops();
          }
        }
        function submitErrors(errors){
          $("#"+formId+"Errors").html(createErrorsList(errors));
          $("#"+formId+"Errors").show("slow");
        }
        function submitSuccess(data){
          var datahtml = '<p class="text-success">'+data+'</p>';
          $("#"+formId+"Errors").html(datahtml);
    
        }
    
        function createErrorsList(errors){
          var retData = '<ul style="color:red; text-align:left;">';
    
          Object.keys(errors).forEach(function(key) {
    
            retData += '<li>' + errors[key] + '</li>';
          });
          retData += '</ul>';
          return retData;
        }
</script>
@endsection