@extends('layouts.front.master') @section('title','Artist Details')
@section('meta')
  <meta name="keywords" content="{{$artist->meta_keywords}}">
  <meta name="description" content="{{$artist->meta_description}}">
@stop
@section('css')

<style type="text/css">
	a.btn-new {
		font-size: 14px;
		padding-top: 5px;
		padding-bottom: 5px;
		border-radius: 4px;
		text-align: center;
		font-weight: 500;
	}
	.btn-new:hover {
		transition: background-color 0.6s ease-out;
		font-weight: 1000;
	}
	.btn-facebook {
		background-color: #3b5998!important;
		color: #fff;
	}
	.btn-facebook:hover {
		background-color: #fff!important;
		color: #3b5998;
	}
	.btn-youtube {
		background-color: #cc0000!important;
		color: #fff;
	}
	.btn-youtube:hover {
		background-color: #fff!important;
		color: #cc0000;
	}
	.btn-twitter {
		background-color: #00aced!important;
		color: #fff;
	}
	.btn-twitter:hover {
		background-color: #fff!important;
		color: #00aced;
	}
</style>

@stop
@section('content')

      <div class="under_header">
        <img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
      </div><!-- under header -->

      <div class="page-content back_to_up">
        <div class="row clearfix mb">
                <div class="breadcrumbIn">
                  <ul>
                    <li><a href="{{url("/")}}" class="toptip" original-title="Homepage"> <i class="icon-home"></i> </a></li>
                    <li><a href="{{url("artists")}}" class="toptip" original-title="Artists"> Artists </a></li>
                    <li> {{$artist->name}} </li>
                  </ul>
                </div><!-- breadcrumb -->
              </div>

              <div class="row row-fluid clearfix mbf">
              <div class="posts">
                <div class="grid_12">
                  <div class="def-block widget" style="display: flow-root;">
                    <img src="{{url($artist->img)}}" class="fll artist-image" style="border-radius: 200px;">

                  <div class="grid_4">
                    <h3 style="margin: 0 0 35px 0;">{{$artist->name}}</h3>
                  	<p style="text-align: justify;">{{$artist->bio}}</p>
                  </div>

                  <div class="grid_4">
                    <h3 style="margin: 0 0 45px 0; font-weight: 500; font-size: 16px;">Want to know more?</h3>
                    <a href="{{$artist->fb}}" class="btn-block btn-new btn-facebook grid_12">
                      <span class="fa fa-facebook"></span> Check artist's facebook account
                    </a>
                    <a href="{{$artist->utube}}"  class="btn-block btn-new btn-youtube grid_12" style="margin-top: 20px;">
                      <span class="fa fa-youtube"></span> Check artist's youtube channel
                    </a>
                    <a href="{{$artist->twitter}}" class="btn-block btn-new btn-twitter grid_12" style="margin-top: 20px;">
                      <span class="fa fa-twitter"></span> Check artist's twitter account
                    </a>
                  </div>

                </div>
                </div>
                <div class="grid_4">
                  <div class="def-block widget">
                    <h4> Audios </h4><span class="liner"></span>
                    <div class="widget-content">
                      <ul class="tab-content-items" id="audoDetails">

                      </ul>
                      <div class="widget-content text-center">
                        <button onclick="getAudiosData()" id="audiosLoadMoreBtn" class="tbutton small"><span>Load More</span></button>
                      </div>
                    </div><!-- widget content -->
                  </div><!-- block -->

                </div><!-- grid -->

                <div class="grid_8">
                  <div class="def-block widget">
                    <h4> Videos </h4><span class="liner"></span>
                    <div class="widget-content">

                      <div class="video-grid clearfix" id="videoDetails">

                      </div>
                      <div class="widget-content text-center">
                        <button onclick="getVideosData()" id="videosLoadMoreBtn" class="tbutton small"><span>Load More</span></button>
                      </div>
                    </div><!-- widget content -->
                  </div><!-- block -->
                  <div class="grid_6">
                  <div class="def-block widget">
                    <h4> Musics </h4><span class="liner"></span>
                    <div class="widget-content">
                      <ul class="tab-content-items" id="musicDetails">

                      </ul>
                      <div class="widget-content text-center">
                        <button onclick="getMusicData()" id="musicLoadMoreBtn" class="tbutton small"><span>Load More</span></button>
                      </div>
                    </div><!-- widget content -->
                  </div><!-- block -->
                  </div>
                  <div class="grid_6">
                  <div class="def-block widget">
                    <h4> Lyrics </h4><span class="liner"></span>
                    <div class="widget-content">
                      <ul class="tab-content-items" id="lyricDetails">

                      </ul>
                      <div class="widget-content text-center">
                        <button onclick="getLyricsData()" id="lyricsLoadMoreBtn" class="tbutton small"><span>Load More</span></button>
                      </div>
                    </div><!-- widget content -->
                  </div><!-- block -->
                  </div>
                </div><!-- grid -->

              </div><!-- posts -->

            </div><!-- row clearfix -->
      </div><!-- end page content -->
  @stop
  @section('scripts')
    <script type="text/javascript" src="{{url('assets/front/js/laod-more.js')}}"></script>
<script type="text/javascript">
var currrentAudiosPage = 1;
var currrentVideosPage = 1;
var currrentMusicPage = 1;
var currrentLyrcPage = 1;
var getAudiosUrl = '{{request()->url()."/getAudios"}}' ;
var getVideosUrl = '{{request()->url()."/getVideos"}}' ;
var getMusicsUrl = '{{request()->url()."/getMusic"}}' ;
var getLyricsUrl = '{{request()->url()."/getLyrics"}}' ;
var token = "{{csrf_token()}}";
getAudiosData();
getVideosData();
getMusicData();
getLyricsData();

var tabs = $('ul.tabs');
tabs.each(function (i) {
  // get tabs
  var tab = $(this).find('> li > a');
  tab.click(function (e) {
    // get tab's location
    var contentLocation = $(this).attr('href');
    // Let go if not a hashed one
    if (contentLocation.charAt(0) === "#") {
      e.preventDefault();
      // add class active
      tab.removeClass('active');
      $(this).addClass('active');
      // show tab content & add active class
      $(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
    }
  });
});

if ($(".videos")[0]) {
  $('.videos').flexslider({
    animation: "fade",
    slideshowSpeed: 5000,
    animationSpeed: 600,
    directionNav: true,
    controlNav: false,
    pauseOnHover: true,
    initDelay: 0,
    randomize: false,
    smoothHeight: true,
    keyboardNav: true
  });
}


</script>
  @endsection
