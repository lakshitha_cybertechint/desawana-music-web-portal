@extends('layouts.front.master') @section('title', $lyric->title.' | Lyric')
@section('meta')
  <meta name="keywords" content="@foreach ($lyric->song->artistSongs as $songArtist){{$songArtist->artist->meta_keywords}},  @endforeach">
<meta name="description" content="@foreach ($lyric->song->artistSongs as $songArtist){{$songArtist->artist->meta_description}}.  @endforeach">
@stop
@section('css')

@stop
@section('content')

  		<div class="under_header">
  			<img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
  		</div><!-- under header -->

      <div class="page-content back_to_up">
			<div class="row clearfix mb">
				<div class="breadcrumbIn">
					<ul>
						<li><a href="{{url("/")}}" class="toptip" title="Homepage"> <i class="icon-home"></i> </a></li>
						<li><a href="{{url("lyrics")}}" class="toptip" title="Lyrics">Lyrics</a></li>
						<li>{{$lyric->title}}</li>
					</ul>
				</div><!-- breadcrumb -->
			</div><!-- row -->

			<div class="row row-fluid clearfix mbf">


				<div class="span9 posts">
					<div class="def-block">
						<div class="post row-fluid clearfix">

							<h3 class="post-title"> <i class="icon-play"></i>{{$lyric->title}}</h3>
							<?php
                echo $lyric->description;
              ?>
							<div class="meta">
								{{-- @if (file_exists($lyric->image)) --}}
                  <img src="{{asset($lyric->image)}}" alt="" class="col-sm-6 col-md-4" style="max-width:100%;">
                {{-- @endif --}}

							</div><!-- meta -->
              <p>
                <span> Artists: </span>
                  {{ $lyric->song->artistSongs->count()== 1 ? $lyric->song->artistSongs->first()->artist->name : "Various Artists"}}

</p>
						</div><!-- post -->

					</div><!-- def block -->
          <br><br>
          <div class="grid_12">
              @include('layouts.front.includes.adContainer', ["locationId" => 6])
            <div class="def-block widget">
              <h4>Related Videos </h4><span class="liner"></span>
              <div class="widget-content">

                <div class="video-grid clearfix">
                  @foreach ($relatedVideos as $video)



                    <a href="{{url("video/".$video->id)}}" class="grid_6">
                      <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg"  alt="#">
                      <span class="text-justify"><strong>{!!substr($video->shortTitle,0,30)!!}</strong>
                        @if ($video->song->artistSongs)

                          {{ $video->song->artistSongs->count()== 1 ? $video->song->artistSongs->first()->artist->name : "Various Artists"}}
                      @endif</span>
                    </a>


                  @endforeach

                </div>
              </div><!-- widget content -->
            </div><!-- block -->
          </div><!-- grid -->
          @include('layouts.front.includes.adContainer', ["locationId" => 7])

          <div class="grid_12">
            <div class="def-block widget">
              <h4>Related  Audios </h4><span class="liner"></span>
              <div class="widget-content">
                <ul class="tab-content-items">
                  @foreach ($relatedAudios as $audio)

                    <li >
                      <a class="m-thumbnail" href="{{url("audio/".$audio->id)}}"><img width="60" height="60" src="{{url($audio->song->album_art)}}" alt="#"></a>
                      <h3><a href="{{url("audio/".$audio->id)}}">{{$audio->shortTitle}}</a></h3>
                      <span> @if ($audio->song->artistSongs)

                        {{ $audio->song->artistSongs->count()== 1 ? $audio->song->artistSongs->first()->artist->name : "Various Artists"}}
                    @endif </span>
                    {{$audio->song->audio_download}} Downloads
                    </li>

                  @endforeach

                </ul>
              </div><!-- widget content -->
            </div><!-- block -->
          </div><!-- grid -->
          @include('layouts.front.includes.adContainer', ["locationId" => 8])

				</div><!-- span6 posts -->

				<div class="span3 sidebar">



          @if(isset($latestVideos))


          <div class="def-block widget">
            <h4> Featured Videos </h4><span class="liner"></span>
            <div class="widget-content row-fluid">
              <div class="videos clearfix flexslider">
                <ul class="slides">
                  @foreach ($latestVideos as $video)

                    <li class="featured-video">
                      <a href="{{url("video/".$video->id)}}">
                        <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                        <i class="icon-play-sign"></i>
                        <h3>{{$video->shortTitle}}</h3>
                      </a>
                    </li><!-- slide -->
                  @endforeach
                </ul>
              </div>
            </div><!-- widget content -->
          </div><!-- def block widget videos -->
        @endif

					<div class="def-block widget">
						<h4> Ads </h4><span class="liner"></span>
						<div class="widget-content tac">
              @include('layouts.front.includes.adContainer', ["locationId" => 2])
						</div><!-- widget content -->
					</div><!-- def block widget ads -->
          <div class="def-block widget">
            <h4> Ads </h4>
            @include('layouts.front.includes.adContainer', ["locationId" => 5])
          </div><!-- def block widget ads -->

				</div><!-- span3 sidebar -->
			</div><!-- row clearfix -->
		</div><!-- end page content -->
  @stop
  @section('scripts')
<script type="text/javascript">

var tabs = $('ul.tabs');
tabs.each(function (i) {
  // get tabs
  var tab = $(this).find('> li > a');
  tab.click(function (e) {
    // get tab's location
    var contentLocation = $(this).attr('href');
    // Let go if not a hashed one
    if (contentLocation.charAt(0) === "#") {
      e.preventDefault();
      // add class active
      tab.removeClass('active');
      $(this).addClass('active');
      // show tab content & add active class
      $(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
    }
  });
});

if ($(".videos")[0]) {
  $('.videos').flexslider({
    animation: "fade",
    slideshowSpeed: 5000,
    animationSpeed: 600,
    directionNav: true,
    controlNav: false,
    pauseOnHover: true,
    initDelay: 0,
    randomize: false,
    smoothHeight: true,
    keyboardNav: true
  });
}
</script>
  @endsection
