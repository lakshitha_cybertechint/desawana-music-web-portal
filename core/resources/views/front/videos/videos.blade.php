@extends('layouts.front.master') @section('title','Videos')
@section('css')
<style media="screen">
    .pagination {
    display: block!important;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
    }
    .pagination > li {
    display: inline;
    }
    .pagination > li > a, .pagination > li > span {
    border: 1px solid #202020;
    color: #A3A3A3;
    font-size: 11px;
    padding: 4px 10px;
    margin: 0 0px 4px 0;
    display: inline-block;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -webkit-transition: all .2s ease;
    transition: all .2s ease;
    }
    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #FF0078;
    border: 1px solid #202020;
    }
</style>
@stop
@section('content')
<div class="under_header">
    <img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
</div>
<!-- under header -->
<div class="page-content back_to_up">
    <div class="row clearfix mb">
        <div class="Alphabet">
            <ul>
                <li><a href="{{url("latest-videos")}}" title="Latest Videos"> Browse All </a></li>
                @foreach (range("A", "Z") as $key => $value)
                <li><a href="{{url("latest-videos/group/".$value)}}" title="Latest Videos of {{$value}}"> {{$value}} </a></li>
                @endforeach
                <li><a href="{{url("latest-videos/group/0-9")}}" title="Latest Videos Of 0-9"> 0-9 </a></li>
            </ul>
        </div>
        <!-- breadcrumb -->
    </div>
    <!-- row -->
    <div class="row row-fluid clearfix mbf mbottom-0">
        @include('layouts.front.includes.adContainer', ["locationId" => 1])
    </div>
    <div class="row row-fluid clearfix mbf">
        <div class="span8 posts">
            <div class="def-block">
                <ul class="tabs">
                    @if (isset($tag))
                    <li><a href="{{url("latest-videos/group/".$tag)}}" class="active">Latest Videos Of {{$tag}}</a></li>
                    @else
                    <li><a href="{{url("latest-videos")}}" class="active"> Latest Videos </a></li>
                    @endif
                    <li><a href="{{url("old-videos")}}" class=""> Old Videos </a></li>
                </ul>
                <!-- tabs -->
                <ul class="tabs-content">
                    <li id="Latest" class="active">
                        <div class="post no-border no-mp clearfix">
                            <div class="video-grid">
                                @foreach ($videos as $video)
                                @if ($video->song->old == 0)
                                <a href="{{url("video/".$video->id)}}" class="grid_3 setHeight_2">
                                <img  src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                                <span><strong>{!!substr($video->shortTitle,0,20)!!}</strong>
                                @if ($video->song->artistSongs)
                                {{ $video->song->artistSongs->count()== 1 ? $video->song->artistSongs->first()->artist->name : "Various Artists"}}
                                @endif
                                </span>
                                </a>
                                @endif
                                @endforeach
                            </div>
                            <!-- video grid -->
                        </div>
                        <!-- latest -->
                    </li>
                    <!-- tab content -->
                </ul>
                <!-- end tabs -->
                <div class="span12 text-center" style="margin-left: 0; margin-top: 15px; display : contents;">
                    @include('layouts.front.includes.adContainer', ["locationId" => 3])
                    {!! $videos->render() !!}
                    @include('layouts.front.includes.adContainer', ["locationId" => 4])
                </div>
            </div>
            <!-- def block -->
        </div>
        <!-- span8 posts -->
        <div class="span4 sidebar">
            <div class="def-block widget mbottom-0">
                <h4> Ads </h4>
                @include('layouts.front.includes.adContainer', ["locationId" => 5])
            </div>
            <!-- def block widget ads -->
            @if(isset($latestVideos))
            <div class="def-block widget">
                <h4> Featured Videos </h4>
                <span class="liner"></span>
                <div class="widget-content row-fluid">
                    <div class="videos clearfix flexslider">
                        <ul class="slides">
                            @foreach ($latestVideos as $video)
                            <li class="featured-video">
                                <a href="{{url("video/".$video->id)}}">
                                <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                                <i class="icon-play-sign"></i>
                                <h3>{{$video->shortTitle}}</h3>
                                </a>
                            </li>
                            <!-- slide -->
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- widget content -->
            </div>
            <!-- def block widget videos -->
            @endif
        </div>
        <!-- span4 sidebar -->
    </div>
    <!-- row clearfix -->
</div>
<!-- end page content -->
@stop
@section('scripts')
<script type="text/javascript">
    var tabs = $('ul.tabs');
    tabs.each(function (i) {
      // get tabs
      var tab = $(this).find('> li > a');
      tab.click(function (e) {
        // get tab's location
        var contentLocation = $(this).attr('href');
        // Let go if not a hashed one
        if (contentLocation.charAt(0) === "#") {
          e.preventDefault();
          // add class active
          tab.removeClass('active');
          $(this).addClass('active');
          // show tab content & add active class
          $(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
        }
      });
    });
    
    if ($(".videos")[0]) {
      $('.videos').flexslider({
        animation: "fade",
        slideshowSpeed: 5000,
        animationSpeed: 600,
        directionNav: true,
        controlNav: false,
        pauseOnHover: true,
        initDelay: 0,
        randomize: false,
        smoothHeight: true,
        keyboardNav: true
      });
    }
    
    $('.setHeight_1').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
    $('.setHeight_2').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
</script>
@endsection