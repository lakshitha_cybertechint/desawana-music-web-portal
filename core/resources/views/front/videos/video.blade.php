@extends('layouts.front.master') @section('title','Videos')
@section('meta')
<meta property="og:title"              content="{{$video->title}}" />
<meta property="og:image"              content="{{url($video->img)}}" />
<meta property="og:type"   content="video.other" />
<meta property="og:description"        content="@foreach ($video->song->artistSongs as $songArtist){{$songArtist->artist->meta_description}}.  @endforeach" />

<meta name="keywords" content="@foreach ($video->song->artistSongs as $songArtist){{$songArtist->artist->meta_keywords}},  @endforeach">
<meta name="description" content="@foreach ($video->song->artistSongs as $songArtist){{$songArtist->artist->meta_description}}.  @endforeach">
@stop
@section('css')
  <style media="screen">
  .videoWrapper {
  position: relative;
  padding-bottom: 56.25%; /* 16:9 */
  padding-top: 25px;
  height: 0;
}
.videoWrapper iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
#toTop{
  padding: 7px 0 0 10px;
  position: fixed;
  top: 90%;
  right: 50px;
  background: #ff0078;
  color: #fff;
}
#toTop i {
  padding: 0;
}
  </style>
@stop
@section('content')

  		<div class="under_header">
  			<img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
  		</div><!-- under header -->

  		<div class="page-content back_to_up">
  			{{-- <div class="row clearfix mb">
  				<div class="Alphabet">
  					<ul>
  						<li><a href="{{url("videos")}}"> Browse All </a></li>
              @foreach (range("A", "Z") as $key => $value)

                <li><a href="{{url("videos/group/".$value)}}"> {{$value}} </a></li>
              @endforeach

  						<li><a href="{{url("videos/group/0-9")}}"> 0-9 </a></li>
  					</ul>
  				</div><!-- breadcrumb -->
  			</div><!-- row --> --}}

  			<div class="row row-fluid clearfix mbf">
          <div class="span8 posts">
  					<div class="def-block">
  						<div class="post row-fluid clearfix videoWrapper">

  							<iframe  src="http://www.youtube.com/embed/{{explode("watch?v=", $video->youtube_link)[1]}}?feature=player_embedded" frameborder="0" allowfullscreen height="360"></iframe>








  						</div><!-- post -->
              <h3 class="post-title"> {{$video->shortTitle}} </h3>


              <p>
                <span> Artists: </span>

                {{ $video->song->artistSongs->count()== 1 ? $video->song->artistSongs->first()->artist->name : "Various Artists"}}
                <br>
                <span> Lyrics By: </span>
                @if ( $video->song->lyricArtists->count() == 0)
                  Undefined artist
                @else

                {{ $video->song->lyricArtists->count()== 1 ? $video->song->lyricArtists->first()->artist->name : "Various Artists"}}
              @endif

                <br>
                <span> Music By: </span>
                @if ( $video->song->musicArtists->count() == 0)
                  Undefined artist
                @else

                {{ $video->song->musicArtists->count()== 1 ? $video->song->musicArtists->first()->artist->name : "Various Artists"}}
              @endif


                <div class="social social-head">

                          <a href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}" class="bottomtip" target="_blank" original-title="Share this  on Facebook"><i class="icon-facebook"></i></a>

                        </div>
<br>
              </p><!-- tags -->


                @include('layouts.front.includes.adContainer', ["locationId" => 6])
  						<div class="post row-fluid clearfix">
                <h4>Related Videos</h4>
                <div class="video-grid clearfix" style="padding-top: 30px;">
                  @foreach ($relatedVideos as $video)

                    <a href="{{url("video/".$video->id)}}" class="grid_3">
                      <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                      <span><strong>{!!substr($video->shortTitle,0,20)!!}</strong>
                        @if ($video->song->artistSongs)
                        @foreach ($video->song->artistSongs as $songArtist)
                          {{$songArtist->artist->name}},
                        @endforeach
                      @endif
                    </span>
                    </a>

                  @endforeach
                </div>

  						</div><!-- post -->
  @include('layouts.front.includes.adContainer', ["locationId" => 7])
              <div class="post row-fluid clearfix">
                <h4>Related Audios</h4>
                <div class="post no-border no-mp clearfix" >
                  <ul class="tab-content-items" style="padding-top: 30px;">

                    @foreach ($relatedAudios as $audio)

                    <li class="grid_6" style="margin-bottom: 20px;">
                      <a class="m-thumbnail" href="{{url("audio/".$audio->id)}}"><img width="60" height="60" src="{{url($audio->song->album_art)}}" alt="#"></a>
                      <h3><a href="{{url("audio/".$audio->id)}}">{{$audio->shortTitle}}</a></h3>
                      @if ($audio->song->artistSongs)
                      @foreach ($audio->song->artistSongs as $songArtist)
                        <a href="{{url("artists/".$songArtist->artist->id)}}">{{$songArtist->artist->name}}</a>,&nbsp;
                      @endforeach
                    @endif
                    <span> {{$audio->song->audio_download}} Downloads </span>
                    </li>

                  @endforeach

                  </ul>
                </div><!-- latest -->

              </div><!-- post -->
                @include('layouts.front.includes.adContainer', ["locationId" => 8])
              <div class="post row-fluid clearfix">
                <h4>Related Lyrics</h4>
                <div class="post no-border no-mp clearfix ">
                  <ul class="tab-content-items" style="padding-top: 30px;">
                    @foreach ($relatedLyrics as $lyric)

                    <li class="grid_6">
                      <a class="m-thumbnail" href="{{url("lyric/".$lyric->id)}}"><img width="60" height="60" src="{{url($lyric->song->album_art)}}" alt="#"></a>
                      <h3><a href="{{url("lyric/".$lyric->id)}}">{{$lyric->title}}</a></h3>
                      <span>
                        @if ($lyric->song->artistSongs)
                        @foreach ($lyric->song->artistSongs as $songArtist)
                          {{$songArtist->artist->name}},
                        @endforeach
                      @endif
                     </span>
                    </li>
                  @endforeach

                  </ul>
                </div><!-- latest -->

              </div><!-- post -->



  					</div><!-- def block -->


  				</div><!-- span8 posts -->

  				<div class="span4 sidebar">
  					<div class="def-block widget">
  						<h4> Ads </h4><span class="liner"></span>
  						<div class="widget-content tac">
                @include('layouts.front.includes.adContainer', ["locationId" => 2])
  						</div><!-- widget content -->
  					</div><!-- def block widget ads -->

  					<div class="def-block widget">
  						<h4> Latest Videos </h4><span class="liner"></span>
  						<div class="widget-content row-fluid">
  							<div class="videos clearfix flexslider">
  								<ul class="slides">
                    @foreach ($latestVideos as $video)

                      <li class="featured-video">
                        <a href="{{url("video/".$video->id)}}">
                          <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                          <i class="icon-play-sign"></i>
                          <h3>{{$video->shortTitle}}</h3>
                        </a>
                      </li><!-- slide -->
                    @endforeach


  								</ul>
  							</div>
  						</div><!-- widget content -->
  					</div><!-- def block widget videos -->
            <div class="def-block widget">
              <h4> Ads </h4>
              @include('layouts.front.includes.adContainer', ["locationId" => 5])
            </div><!-- def block widget ads -->
  				</div><!-- span4 sidebar -->
  			</div><!-- row clearfix -->
  		</div><!-- end page content -->
  @stop
  @section('scripts')
<script type="text/javascript">

var tabs = $('ul.tabs');
tabs.each(function (i) {
  // get tabs
  var tab = $(this).find('> li > a');
  tab.click(function (e) {
    // get tab's location
    var contentLocation = $(this).attr('href');
    // Let go if not a hashed one
    if (contentLocation.charAt(0) === "#") {
      e.preventDefault();
      // add class active
      tab.removeClass('active');
      $(this).addClass('active');
      // show tab content & add active class
      $(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
    }
  });
});

if ($(".videos")[0]) {
  $('.videos').flexslider({
    animation: "fade",
    slideshowSpeed: 5000,
    animationSpeed: 600,
    directionNav: true,
    controlNav: false,
    pauseOnHover: true,
    initDelay: 0,
    randomize: false,
    smoothHeight: true,
    keyboardNav: true
  });
}
</script>
  @endsection
