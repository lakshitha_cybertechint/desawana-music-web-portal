@extends('layouts.front.master') @section('title','Terms Of Service')
@section('css')
@stop
@section('content')

  		<div class="under_header">
  			<img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
  		</div><!-- under header -->

  		<div class="page-content back_to_up">
        <div class="row clearfix mb">
                <div class="breadcrumbIn">
                  <ul>
                    <li><a href="{{url("/")}}" class="toptip" original-title="Homepage"> <i class="icon-home"></i> </a></li>
                    <li> Terms Of Service </li>
                  </ul>
                </div><!-- breadcrumb -->
              </div>

  			<div class="row row-fluid clearfix mbf">
  				<div class="span12 posts">
  					<div class="def-block text-justify">

	<?php echo $content ?>

  					</div><!-- def block -->
  				</div><!-- span8 posts -->

  				
  			</div><!-- row clearfix -->
  		</div><!-- end page content -->
  @stop
  @section('scripts')
<script type="text/javascript">
var tabs = $('ul.tabs');
tabs.each(function (i) {
  // get tabs
  var tab = $(this).find('> li > a');
  tab.click(function (e) {
    // get tab's location
    var contentLocation = $(this).attr('href');
    // Let go if not a hashed one
    if (contentLocation.charAt(0) === "#") {
      e.preventDefault();
      // add class active
      tab.removeClass('active');
      $(this).addClass('active');
      // show tab content & add active class
      $(contentLocation).fadeIn(500).addClass('active').siblings().hide().removeClass('active');
    }
  });
});

if ($(".videos")[0]) {
  $('.videos').flexslider({
    animation: "fade",
    slideshowSpeed: 5000,
    animationSpeed: 600,
    directionNav: true,
    controlNav: false,
    pauseOnHover: true,
    initDelay: 0,
    randomize: false,
    smoothHeight: true,
    keyboardNav: true
  });
}
</script>
  @endsection
