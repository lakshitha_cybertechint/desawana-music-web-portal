@extends('layouts.front.master') @section('title','Audios')
@section('meta')
<meta name="keywords" content="@foreach ($audio->song->artistSongs as $songArtist){{$songArtist->artist->meta_keywords}},  @endforeach">
<meta name="description" content="@foreach ($audio->song->artistSongs as $songArtist){{$songArtist->artist->meta_description}}.  @endforeach">
@stop
@section('css')
<style media="screen">
    .ringtoneCodes{
    margin-bottom: 10px;
    height: 30px;
    }
    .ringtone-icon{
    max-width: 55px;
    }
    .ringtone-p{
    color: #fff;
    font-size: 15px;
    }
    .tbutton{
    /*padding: 0px 10px 5px 10px;*/
    padding : 3px 5px 6px 5px;
    }
    .call-button{
    padding-left : 8px;
    padding-right : 8px;
    }
    .link-copied{
    color: #ff0000;
    font-size: 15px;
    display: none;
    }
</style>
@stop
@section('content')
<div class="under_header">
    <img src="{{url("assets/front/images/breadcrumbs10.png")}}" alt="#">
</div>
<!-- under header -->
<div class="page-content back_to_up">
    <div class="row row-fluid clearfix mbf">
        <div class="span8 posts">
            <div class="def-block">
                <div class="clearfix">
                    <div class="music-single mbf clearfix"></div>
                    <!-- Player -->
                </div>
                <!-- post -->
                <div class="clearfix">
                    @include('layouts.front.includes.adContainer', ["locationId" => 10])
                </div>
                @if($audio->dialog != "" || $audio->mobitel != "" || $audio->hutch != "" || $audio->etisalat != "" || $audio->airtel != ""  )
                <div class="post row-fluid clearfix">
                    <h4>Ringingtone Codes</h4>
                    <div class="clearfix" style="padding-top: 30px;">
                        @if ($audio->dialog != "")
                        <div class="grid_12 ringtoneCodes" >
                            <img class="grid_2 ringtone-icon" src="{{'../assets/front/images/dialog.png'}}">
                            <p class="grid_4 ringtone-p">{{$audio->dialog}}</p>
                            <p class="grid_1 copied">
                                <button type="button" data-toggle="tooltip" data-placement="right" title="Copy this code!" class="tbutton" name="button" onclick='coptyClipBoard("{{$audio->dialog}}", "dialog")'><i class="fa fa-copy"></i></button>
                            </p>
                            <p class="grid_1 phone">
                                <a type="button" href="tel:{{$audio->dialog}}" title="Dial this code!" class="call-button" ><i class="fa fa-phone"></i></a>
                            </p>
                            <p id="dialog-copied" class="grid_3 link-copied mcopied">
                                Link copied!
                            </p>
                        </div>
                        @endif
                        @if ($audio->mobitel != "")
                        <div class="grid_12 ringtoneCodes" >
                            <img class="grid_2 ringtone-icon" src="{{'../assets/front/images/mobitel.png'}}">
                            <p class="grid_4 ringtone-p">{{$audio->mobitel}}</p>
                            <p class="grid_1 copied">
                                <button type="button" data-toggle="tooltip" data-placement="right" title="Copy this code!" class="tbutton" name="button" onclick='coptyClipBoard("{{$audio->mobitel}}", "mobitel")'><i class="fa fa-copy"></i></button>
                            </p>
                            <p class="grid_1 phone">
                                <a type="button" href="tel:{{$audio->mobitel}}" title="Dial this code!" class="call-button" ><i class="fa fa-phone"></i></a>
                            </p>
                            <p id="mobitel-copied" class="grid_3 link-copied mcopied">
                                Link copied!
                            </p>
                        </div>
                        @endif
                        @if ($audio->airtel != "")
                        <div class="grid_12 ringtoneCodes" >
                            <img class="grid_2 ringtone-icon" src="{{'../assets/front/images/airtel.png'}}">
                            <p class="grid_4 ringtone-p">{{$audio->airtel}}</p>
                            <p class="grid_1 copied">
                                <button type="button" data-toggle="tooltip" data-placement="right" title="Copy this code!" style="" class="tbutton" name="button" onclick='coptyClipBoard("{{$audio->airtel}}", "airtel")'><i class="fa fa-copy"></i></button>
                            </p>
                            <p class="grid_1 phone">
                                <a type="button" href="tel:{{$audio->airtel}}" title="Dial this code!" class="call-button" ><i class="fa fa-phone"></i></a>
                            </p>
                            <p id="airtel-copied" class="grid_3 link-copied mcopied">
                                Link copied!
                            </p>
                        </div>
                        @endif
                        @if ($audio->hutch != "")
                        <div class="grid_12 ringtoneCodes" >
                            <img class="grid_2 ringtone-icon" src="{{'../assets/front/images/hutch.png'}}">
                            <p class="grid_4 ringtone-p">{{$audio->hutch}}</p>
                            <p class="grid_1 copied">
                                <button type="button" data-toggle="tooltip" data-placement="right" title="Copy this code!" class="tbutton" name="button" onclick='coptyClipBoard("{{$audio->hutch}}", "hutch")'><i class="fa fa-copy"></i></button>
                            </p>
                            <p class="grid_1 phone">
                                <a type="button" href="tel:{{$audio->hutch}}" title="Dial this code!" class="call-button"><i class="fa fa-phone"></i></a>
                            </p>
                            <p id="hutch-copied" class="grid_3 link-copied mcopied">
                                Link copied!
                            </p>
                        </div>
                        @endif
                        @if ($audio->etisalat != "")
                        <div class="grid_12 ringtoneCodes" >
                            <img class="grid_2 ringtone-icon" src="{{'../assets/front/images/etisalat.png'}}">
                            <p class="grid_4 ringtone-p">{{$audio->etisalat}}</p>
                            <p class="grid_1 copied">
                                <button type="button" data-toggle="tooltip" data-placement="right" title="Copy this code!" class="tbutton" name="button" onclick='coptyClipBoard("{{$audio->etisalat}}", "etisalat")'><i class="fa fa-copy"></i></button>
                            </p>
                            <p class="grid_1 phone">
                                <a type="button" href="tel:{{$audio->etisalat}}" title="Dial this code!" class="call-button" ><i class="fa fa-phone"></i></a>
                            </p>
                            <p id="etisalat-copied" class="grid_3 link-copied mcopied">
                                Link copied!
                            </p>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
                <!-- post -->
                <div class="clearfix">
                    @include('layouts.front.includes.adContainer', ["locationId" => 6])
                </div>
                <div class="post row-fluid clearfix">
                    <h4>Related Audios</h4>
                    <div class="post no-border no-mp clearfix" >
                        <ul class="tab-content-items" style="padding-top: 30px;">
                            @foreach ($relatedAudios as $rAudio)
                            <li class="grid_6" style="margin-bottom: 20px;">
                                <a class="m-thumbnail" href="{{url("audio/".$rAudio->id)}}"><img width="60" height="60" src="{{url($rAudio->song->album_art)}}" alt="#"></a>
                                <h3><a href="{{url("audio/".$rAudio->id)}}">{{$rAudio->shortTitle}}</a></h3>
                                @if ($rAudio->song->artistSongs)
                                @foreach ($rAudio->song->artistSongs as $songArtist)
                                <a href="{{url("artists/".$songArtist->artist->id)}}">{{$songArtist->artist->name}}</a>,&nbsp;
                                @endforeach
                                @endif
                                <span> {{$rAudio->song->audio_download}} Downloads </span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- latest -->
                </div>
                <!-- post -->
                <div class="clearfix">
                    @include('layouts.front.includes.adContainer', ["locationId" => 7])
                </div>
                <div class="post row-fluid clearfix">
                    <h4>Related Videos</h4>
                    <div class="video-grid clearfix" style="padding-top: 30px;">
                        @foreach ($relatedVideos as $video)
                        <a href="{{url("video/".$video->id)}}" class="grid_3">
                        <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                        <span><strong>{!!substr($video->shortTitle,0,20)!!}</strong>
                        @if ($video->song->artistSongs)
                        @foreach ($video->song->artistSongs as $songArtist)
                        {{$songArtist->artist->name}},
                        @endforeach
                        @endif
                        </span>
                        </a>
                        @endforeach
                    </div>
                </div>
                <!-- post -->
                <div class="clearfix">
                    @include('layouts.front.includes.adContainer', ["locationId" => 8])
                </div>
                <div class="post row-fluid clearfix">
                    <h4>Related Lyrics</h4>
                    <div class="post no-border no-mp clearfix ">
                        <ul class="tab-content-items" style="padding-top: 30px;">
                            @foreach ($relatedLyrics as $lyric)
                            <li class="grid_6">
                                <a class="m-thumbnail" href="{{url("lyric/".$lyric->id)}}"><img width="60" height="60" src="{{url($lyric->song->album_art)}}" alt="#"></a>
                                <h3><a href="{{url("lyric/".$lyric->id)}}">{{$lyric->title}}</a></h3>
                                <span>
                                @if ($lyric->song->artistSongs)
                                @foreach ($lyric->song->artistSongs as $songArtist)
                                {{$songArtist->artist->name}},
                                @endforeach
                                @endif
                                </span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- latest -->
                </div>
                <!-- post -->
            </div>
            <!-- def block -->
        </div>
        <!-- span8 posts -->
        <div class="span4 sidebar">
            <div class="def-block widget">
                <h4> Ads </h4>
                <span class="liner"></span>
                <div class="widget-content tac">
                    @include('layouts.front.includes.adContainer', ["locationId" => 2])
                </div>
                <!-- widget content -->
            </div>
            <!-- def block widget ads -->
            @if(isset($latestVideos))
            <div class="def-block widget">
                <h4> Latest Videos </h4>
                <span class="liner"></span>
                <div class="widget-content row-fluid">
                    <div class="videos clearfix flexslider">
                        <ul class="slides">
                            @foreach ($latestVideos as $video)
                            <li class="featured-video">
                                <a href="{{url("video/".$video->id)}}">
                                <img src="https://img.youtube.com/vi/{{explode("watch?v=", $video->youtube_link)[1]}}/sddefault.jpg" alt="#">
                                <i class="icon-play-sign"></i>
                                <h3>{{$video->title}}</h3>
                                </a>
                            </li>
                            <!-- slide -->
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- widget content -->
            </div>
            <!-- def block widget videos -->
            @endif
            <div class="def-block widget">
                <h4> Ads </h4>
                @include('layouts.front.includes.adContainer', ["locationId" => 5])
            </div>
            <!-- def block widget ads -->
        </div>
        <!-- span4 sidebar -->
    </div>
    <!-- row clearfix -->
</div>
<!-- end page content -->
@stop
@section('scripts')
<script type="text/javascript" src="{{url("assets/front/music/single-track.js")}}"></script>
<script type="text/javascript">
    <?php
        $musicBy = "";
        $lyricsBy = "";
        if ($audio->song->musicArtists->count() == 0){
          $musicBy = "Undefined Artist ";
        }elseif ($audio->song->musicArtists->count() == 1){
          foreach ($audio->song->musicArtists as $musicArtist){
           $musicBy =  $musicArtist->artist->name;

          }
        }else{
          $musicBy = "Various Artists";
        }

        if ($audio->song->lyricArtists->count() == 0){
          $lyricsBy = "Undefined Artist ";
        }elseif ($audio->song->lyricArtists->count() == 1){
          foreach ($audio->song->lyricArtists as $lyricArtist){
           $lyricsBy =  $lyricArtist->artist->name;
          }
        }else{
          $lyricsBy = "Various Artists";
        }
        ?>
    var myPlaylist = [
      {
        mp3:'{{url($audio->src)}}',
        title:'{{$audio->shortTitle}}',
        artist:'{{ $audio->song->artistSongs->count()== 1 ? $audio->song->artistSongs->first()->artist->name : "Various Artists"}},   <br> Lyrics By : {{$lyricsBy}}  <br> Music By : {{$musicBy}}',
        // rating:5,
        buy:'{{url("audio-download/".base64_encode($audio->id))}}',
        price:'',
        // duration:'0:38',
        cover:'{{url($audio->song->album_art)}}'
      },

    ];

    if ($(".videos")[0]) {
      $('.videos').flexslider({
        animation: "fade",
        slideshowSpeed: 5000,
        animationSpeed: 600,
        directionNav: true,
        controlNav: false,
        pauseOnHover: true,
        initDelay: 0,
        randomize: false,
        smoothHeight: true,
        keyboardNav: true
      });
    }
</script>
<script>
    function coptyClipBoard(text, provider){
      var tempInput = document.createElement('INPUT');
     document.body.appendChild(tempInput);
    tempInput.setAttribute('value', text)
    tempInput.select();
    document.execCommand('copy');
     document.body.removeChild(tempInput);
     $("#"+provider+"-copied").show();

     setTimeout(function(){
       $("#"+provider+"-copied").hide();
     }, 2000);

    }

    $('.ringtoneCodes').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
</script>
@endsection
