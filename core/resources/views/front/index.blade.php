@extends('layouts.front.master') @section('title','Welcome | Desawana')
@section('meta')
<meta name="description" content="Desawana.lk is the World Class Music Provider in Sri Lanka. Sinhala Mp3,Sinhala Music Videos,Sinhala Lyrics, Free Download and Online Listening.">
<meta name="keywords" content="sinhala mp3,sinhala songs,free sinhala mp3,sinhala videos,sinhala music videos,sinhala lyrics,sinhala video songs,Original Sinhala music MP3,Free Sinhala Songs,Sri Lanka Songs,popular sinhala songs,Download Sinhala Mp3 Songs, New Sinhala Mp3 Songs, Old Sinhala Mp3 Songs, Mp3, Songs,New Sinhala Songs,Sinhala Songs,Sinhala Songs Download,Singers Sri Lanka,Sinhala Songs free download">
@stop
@section('css')
<style media="screen">
    .ttw-music-player .track-info, .ttw-music-player .player-controls{
    /* width: -moz-available; */
    /* width: inherit; */
    }
    .Newspaper-Button {
    color: rgba(255,255,255,1.00);
    font-size: 13px;
    line-height: 17px;
    font-weight: 700;
    font-style: normal;
    font-family: Roboto;
    text-decoration: none;
    background-color: rgba(255,255,255,0);
    border-color: rgba(255,255,255,0.25);
    border-style: solid;
    border-width: 1px;
    border-radius: 0px 0px 0px 0px;
    letter-spacing: 2px;
    }
    .rev-btn {
    z-index: 8;
    white-space: nowrap;
    border-color: #ff0078;
    outline: currentcolor none medium;
    box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;
    box-sizing: border-box;
    cursor: pointer;
    visibility: inherit;
    transition: none 0s ease 0s;
    text-align: left;
    line-height: 17px;
    margin: 0px;
    padding: 12px 35px;
    letter-spacing: 2px;
    font-weight: 700;
    font-size: 13px;
    min-height: 0px;
    min-width: 0px;
    max-height: none;
    max-width: none;
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
    transform-origin: 50% 50% 0px;
    color: rgb(255, 255, 255);
    border-radius: 0px;
    right: 35px;
    top: 185px;
    }
    .homeFloatingAd .closeBtn{
    float: right;
    position: fixed;
    }
    .homeFloatingAd{
    bottom: 0;
    position: fixed;
    bottom: 0;
    position: fixed;
    float: none;
    right: 0;
    left: 0;
    }
    @media (min-width: 0px) and (max-width: 768px){
    .tp-banner-container {
    height: 100%;
    }
    .homeFloatingAd{
    margin-left: auto;
    margin-right: auto;
    float: none;
    }
    .homeFloatingAd .closeBtn{
    margin: 0 0 0 0;
    }
    .tp-banner-container {
    height: auto;
    }
    .sliderr {
	max-width: 100%;
	margin: 0 0;
	}
    }
    @media (min-width: 769px) {
    .tp-banner-container {
    height: 600px;
    }
    .homeFloatingAd{
    margin-left: 20%;
    margin-right: 20%;
    float: none;
    }
    }
</style>
@stop
@section('content')
<!-- Start Revolution Slider -->
<div class="sliderr">
    <div class="tp-banner-container">
        <div class="tp-banner" style="min-width: 100%;">
            <ul>
                @if ($sliders->count() > 0)
                @foreach ($sliders as $slider)
                <li data-transition="random" data-slotamount="7" data-masterspeed="5000" data-link="{{$slider->url}}">
                    <!-- :::::::::: Data link to the related song start ::::::::::
                        {{url("audio/".$slider->sliderSongs->first()->song->audio->id)}}
                        :::::::::: Data link over :::::::::: -->
                    <!-- MAIN IMAGE -->
                    <img src="{{url($slider->image)}}" style="min-width: 100%; max-width: 100%;" alt="Home Slider" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <a href="{{$slider->url}}" class="tp-caption Newspaper-Button rev-btn rs-hover-ready fadeout" data-speed="500" data-start="1200" data-easing="Power4.easeOut">Visit Ad
                    </a>
                </li>
                @endforeach
                @else
                <!-- DEFAULT SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-masterspeed="5000" >
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('assets/front/images/slides/slider1.jpg')}}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                </li>
                @endif
            </ul>
            <!-- End Slides -->
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>
<!-- End Revolution Slider -->
<div class="page-content">
    <div class="row clearfix mbf">
        <div class="music-player-list"></div>
    </div>
    <!-- row music player -->
    <div class="row row-fluid clearfix mbf">
        <div class="span8">
            <div class="def-block">
                <h4> Latest Videos </h4>
                <span class="liner"></span>
                @foreach ($latestVideos as $latestVideo)
                <div class="news row-fluid animtt" data-gen="fadeUp" style="opacity:0;">
                    <div class="span5"><img class="four-radius" src="https://img.youtube.com/vi/{{explode("watch?v=", $latestVideo->youtube_link)[1]}}/sddefault.jpg" alt="#"></div>
                    <div class="span7">
                        <h3 class="news-title"> <a href="{{url("video/".$latestVideo->id)}}">{{$latestVideo->shortTitle}}</a> </h3>
                        <p>
                            <span> Artists: </span>
                            {{ $latestVideo->song->artistSongs->count()== 1 ? $latestVideo->song->artistSongs->first()->artist->name : "Various Artists"}}
                            <br>
                            <span> Lyrics By: </span>
                            @if ($latestVideo->song->lyricArtists->count() == 0)
                            Undefined artist
                            @else
                            @foreach ($latestVideo->song->lyricArtists as $lyricArtist)
                            {{$lyricArtist->artist->name}}
                            @endforeach
                            @endif
                            <br>
                            <span> Music By: </span>
                            @if ($latestVideo->song->musicArtists->count() == 0)
                            Undefined artist
                            @else
                            @foreach ($latestVideo->song->musicArtists as $musicArtist)
                            {{$musicArtist->artist->name}}
                            @endforeach
                            @endif
                        </p>
                        <!-- tags -->
                        <a href="{{url("video/".$latestVideo->id)}}" class="sign-btn tbutton small"><span>View Video</span></a>
                    </div>
                    <!-- span7 -->
                </div>
                <!-- news -->
                @endforeach
                
                <!-- <div class="load-news tac"><a href="#" class="tbutton small"><span>Load More</span></a></div> -->

            </div>
            <!-- def block -->
        </div>
        <!-- span8 news -->
        <div class="span4">
            <div class="def-block widget animtt" data-gen="fadeUp" style="opacity:0;">
                <h4> Latest Audios </h4>
                <span class="liner"></span>
                <div class="widget-content row-fluid">
                    <div class="scroll-mp3" style="height: 220px;">
                        <div class="content">
                            <ul class="tab-content-items">
                                @foreach ($latestAudios as $latestAudio)
                                <li class="clearfix">
                                    <a class="m-thumbnail" href="{{url("audio/".$latestAudio->id)}}"><img width="60" height="60" src="{{url($latestAudio->song->album_art)}}" alt="#"></a>
                                    <h3><a href="{{url("audio/".$latestAudio->id)}}">{{$latestAudio->shortTitle}}</a></h3>
                                    <span> @if ($latestAudio->song->artistSongs)
                                    {{ $latestAudio->song->artistSongs->count()== 1 ? $latestAudio->song->artistSongs->first()->artist->name : "Various Artists"}}
                                    @endif </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- widget content -->
            </div>
            <!-- def block widget popular items -->
            <div class="def-block widget animtt" data-gen="fadeUp" style="opacity:0;">
                <h4> Most Downloaded </h4>
                <span class="liner"></span>
                <div class="widget-content row-fluid">
                    <div class="scroll-mp3" style="height: 220px;">
                        <div class="content">
                            <ul class="tab-content-items">
                                @foreach ($mostDownloadedAudios as $audio)
                                <li class="clearfix">
                                    <a class="m-thumbnail" href="{{url("audio/".$audio->id)}}"><img width="60" height="60" src="{{url($audio->song->album_art)}}" alt="#"></a>
                                    <h3><a href="{{url("audio/".$audio->id)}}">{{$audio->shortTitle}}</a></h3>
                                    <span> @if ($audio->song->artistSongs)
                                    {{ $audio->song->artistSongs->count()== 1 ? $audio->song->artistSongs->first()->artist->name : "Various Artists"}}
                                    @endif </span>
                                    <span>{{$audio->song->audio_download}} Downloads</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- widget content -->
            </div>
            <!-- def block widget popular items -->
            <div class="def-block widget animtt" data-gen="fadeUp" style="opacity:0;">
                <h4> Ads </h4>
                <div class="widget-content row-fluid">
                    @include('layouts.front.includes.adContainer', ["locationId" => 5])
                </div>
                <!-- widget content -->
            </div>
            <!-- def block widget popular items -->
        </div>
        <!-- span4 sidebar -->
    </div>
    <!-- row clearfix -->
</div>
<!-- end page content -->
@include('layouts.front.includes.adContainer', ["locationId" => 9])
@stop
@section('scripts')
<script type="text/javascript" src="{{url('assets/front/music/myplaylist.js')}}"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    	jQuery(document).ready(function() {
    		jQuery('.tp-banner').revolution({
    			delay:7500,
    			// startwidth:1000,
    			// startheight:710,
    			hideThumbs:10,
    			navigationType:"on",
    			fullWidth:"on",
    			forceFullWidth:"off"
    		});

    	});
    /* ]]> */

     var myPlaylist = [
    @foreach ($latestAudios as $latestAudio)
    {
     mp3:'{{url($latestAudio->src)}}',
     title:'{{$latestAudio->getShortTitleAttribute(25)}} by {{ $latestAudio->song->artistSongs->count()== 1 ? $latestAudio->song->artistSongs->first()->artist->name : "Various Artists"}}',
     artist:'{{ $latestAudio->song->artistSongs->count()== 1 ? $latestAudio->song->artistSongs->first()->artist->name : "Various Artists"}}',
     // rating:5,
     	buy:'{{url("audio-download/".base64_encode($latestAudio->id))}}',
     price:'',
     // duration:'0:38',
     cover:'{{url($latestAudio->song->album_art)}}'
    },
    @endforeach

     ];
</script>
<script type="text/javascript">
    $("#homeFloatingAdCloseBtn").on('click', function(){
      $(".homeFloatingAd").hide();
    })
</script>
@endsection
